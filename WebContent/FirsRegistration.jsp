
<%@include file= "header.jsp" %>
	<div>
		<div id="container-fluid">
			<!-- User Registration Page Starts -->

			<div class="container-fluid">
				<form action="registrationControllerService">
					<div id="">
						<p class="alert alert-info"
							style="border-radius: 0px; background-color: rgba(119, 119, 119, 0.16); border-color: rgba(85, 85, 85, 0.19); color: #333;">
							Registration for <%=request.getParameter("applicationType").toUpperCase() %> Account</p>
						<div class="row">
							<div class="col-lg-12">
								<span class="text-info-custom">* marked fields are
									mandatory.</span>
							</div>
						</div>
						<div class="row custom-row-margin">
						                <%          
				            		    String message = (String)request.getAttribute("error");
									    //System.out.println("Frim " + message);
				            		    if(message!=null){
				            		    %>
				            		     <span style="color:red"><%=message%></span>
				            		    
				            		    <%} %>
						   
							<div
								class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-3">
								<label class="control-label custom_label" for="applicant-type">Applicant
									Type:<sup class="text-danger">*</sup>
								</label>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
								<input class="form-control input-sm" id=""
									name="applicationType" type="text"
									value="<%=request.getParameter("applicationType")%>" readonly>
							</div>
						</div>
						<div class="row custom-row-margin">
							<div
								class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-3">
								<label class="control-label custom_label" for="tin">Tax
									Identification Number:<sup class="text-danger">*</sup>
								</label>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3">
								<input class="form-control input-sm" id="" name="tin"
									type="text"  placeholder="Enter your TIN" required>
							</div>


							<div class="col-lg-3 col-md-3 col-sm-3">
								<button type="submit" name="command" value="RetrieveUser"
									class="btn btn-default custom_btn">Retrieve</button>
							</div>
							<div
								class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8"
								id="invalidTINMsg" style="margin-top: 10px;"></div>
						</div>
					</div>

				</form>
			</div>
		</div>

	</div>

	<div class="container-fluid">
		<footer class="footer">
		<div class="container-fluid"
			style="font-family: sans-serif; font-size: 12px;">
			<p class="text-muted">
				<div class="text-center">
					&copy;&nbsp;Copyright 2017 <a href="#" target="_blank">Federal
						Inland Revenue Service. All Right Reserved </a>.
					<!-- <span class="text-right small"><b>Need help?&nbsp</b><a href="#" class="text-info"><u>Click here for LIVE CHAT</u></a></span> -->
				</div>
			</p>
		</div>
		</footer>


	</div>


</body>
</html>