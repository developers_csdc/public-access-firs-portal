<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>  
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FIRS TCC Application Portal</title>

<!-- Loading third party fonts -->
		<link href="./Template_files/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="./Template_files/style.css" rel="stylesheet" type="text/css">

		<!-- Loading main css file -->
		<link rel="stylesheet" href="./Template_files/style(1).css">

<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script src="js/portal/highchart/highcharts.js"></script>
<script src="js/portal/highchart/drilldown.js"></script>
<script src="js/portal/highchart/modulesexporting.js"></script>
<script src="js/portal/highchart/no-data-to-display.js"></script>
<script src="js/portal/highchart/exporting.js"></script>
<script src="js/portal/highchart/export-csv.js"></script>
<script src="js/portal/tcccountyearly.js"></script>
</head>
<body>
<div id="site-content">
			<header class="site-header">
				<div class="top-header">
					<div class="container">
						<a href="#" id="branding">
							<img src="./Template_files/logo.png" alt="Company Name" class="logo">
							<!--<div class="logo-text">
								<h1 class="site-title">Company Name</h1>
								<small class="description">Taline goes here</small>
							</div>-->
						</a> <!-- #branding -->
					
						
					</div> <!-- .container -->
				</div> <!-- .top-header -->

				
				<div class="bottom-header">
					<div class="container">
						
					</div>
				</div>
				
			</header> <!-- .site-header -->

			 

			<main class="main-content">
			<br />
				<span style="margin-left: 10px;">Please Select Year</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<Select id="reportDate" onchange="generateReport();">
				<option value="">Select Year</option>
				<option>2013</option>
				<option>2014</option>
				<option>2015</option>
				<option>2016</option>
				<option>2017</option>
				</Select>
				
				<!-- <div id="container"style="width:100%; height:400px;" margin: 0 auto"></div> -->
				<div id="container1" style="height: 300px; margin: 0 auto"></div>
                <div id="container2" style="height: 400px; margin: 0 auto"></div>
				
			</main>

			<div class="site-footer">
				

				<div class="bottom-footer">
					<div class="container">
						<div class="colophon">ęCopyright 2017 Federal Inland Revenue Service.All Right Reserved . Need help? Click here for LIVE CHAT</div>
					</div>
				</div>
			</div>
		</div>

</body>
</html>