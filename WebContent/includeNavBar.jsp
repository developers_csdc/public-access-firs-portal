<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@include file="include/includeconstant.jsp"%>
<% 
	String Name 		= "";
	String JTBTIN 		= "";
	
	WsPeople people 				= null;
	
	people 			= (WsPeople) session.getAttribute("people");
	
	if(!PortalUtility.isNull(people)){
		Name 	= PortalUtility.getString(people.getOrganizationName());
		JTBTIN 	= PortalUtility.getString(people.getLicenceNumber());
	}
	
%>

<nav role="navigation" class="navbar navbar-custom">
   <div class="container-fluid">
     <div class="navbar-header col-lg-5 col-md-5 col-sm-4">
       <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a href="#" class="navbar-brand" style="font-size:13px;" onclick="return false;"><span style="font-family: cursive;font-size:15px;">Welcome&nbsp&nbsp</span><span style="font-size:16px;"><%=Name %></span></a>
     </div>
     <div class="text-center col-lg-4 col-md-4 col-sm-5">
     	<label style="margin-top: 3%;">&nbsp;</label>
     	<a href="#" style="font-size:13px;color:#bec0c3;font-weight:700">FIRS TIN:<span style="font-size:14px;"><%=JTBTIN %></span></a>
     </div>
     <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse pull-right col-lg-3 col-md-3 col-sm-3">
       <ul class="nav navbar-nav navbar-right">
         <li class="active"><a href="#"></a></li>
         <%-- <li class="active disabled"><a href="#">FIRS Tax Identification Number:<span style="font-size:16px;"><%=JTBTIN %></span></a></li> --%>
         <li class="active"><a href="javascript:"
			onclick="window.location.href = 'logoff.jsp';">Log Out&nbsp<span class="glyphicon glyphicon-log-out"></span></a></li>
       </ul>

     </div>
   </div>
</nav>