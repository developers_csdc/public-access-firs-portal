<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.axis.encoding.Base64"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderFreeform"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@include file="include/includeconstant.jsp"%>
<%@ page language="java" contentType="charset=UTF-8" pageEncoding="UTF-8"%>
<%!
	private static String getRequestBodyJSONString (HttpServletRequest request) throws Exception{
		BufferedReader bufferReader = request.getReader();
		StringBuilder stringBuilder = new StringBuilder();
	    String line;
	    
	    while ((line = bufferReader.readLine()) != null) {
	    	stringBuilder.append(line.trim());
	    }
	    
	    return stringBuilder.toString();
	}
	
	/* private Map<String, String> getRequestHeadersInfo(HttpServletRequest request) {
		boolean isExceptionOccoured = false;	
	    Map<String, String> map = new HashMap<String, String>();
		
	    try{
			Enumeration headerNames = request.getHeaderNames();
		    while (headerNames.hasMoreElements()) {
		        String key = (String) headerNames.nextElement();
		        String value = request.getHeader(key);
		        map.put(key, value);
		    }
		}catch(Exception e){
			isExceptionOccoured = true;
			e.printStackTrace();
		}
		
	    map.put("isExceptionOccoured", Boolean.toString(isExceptionOccoured));
	    return map;
	} */
	
	private Map<String,Object> authenticateRequest(HttpServletRequest request,String lid) throws Exception{
		String requestUserName = "";
		String requestPassword = "";
		String storedPassword = "";
		String activeFlag = "";
		String errorCode = "";
		String errorMessage = "";
		
		boolean isRequestAuthentic = false;
		final String authorization = request.getHeader("Authorization");
		Map<String,Object> authRes = new HashMap<String,Object>();
		
		
		if (authorization != null && authorization.startsWith("Basic")) {
	        String base64Credentials = authorization.substring("Basic".length()).trim();
	        System.out.println(base64Credentials);
	        
	        String authCredentials = new String(Base64.decode(base64Credentials));
	        System.out.println(authCredentials);
	        
	        final String[] authCredentialsValues = authCredentials.split(":",2);
	        
	        requestUserName = PortalUtility.getString(authCredentialsValues[0]);
	        requestPassword = PortalUtility.getString(authCredentialsValues[1]);
	        
	        Map<String,Object> passwordResMap = getPasswordFromDBByUserName(requestUserName,lid);
	        storedPassword = passwordResMap.get("password").toString();
	        activeFlag = passwordResMap.get("activeFlag").toString();
	        
	        if(!activeFlag.equals("") && !activeFlag.equals("1")){
	        	errorCode = "3";
	        	errorMessage = "Inactive User. Please contact your System Administrator";
	        	/* String credentials = requestUserName+":"+storedPassword;
	 			String base64_Credentials = Base64.encode(credentials.getBytes());
	 	        
	 	        if(base64_Credentials.equals(base64Credentials)){
	 	        	isRequestAuthentic = true;
	 	        }  */
	        }else if(!activeFlag.equals("") && requestPassword.equals(storedPassword)){
	        	isRequestAuthentic = true;
	        	errorCode = "0";
	        	errorMessage = "Success";
	        }else{
	        	errorCode = "2";
	        	errorMessage = "Unauthorized Request / Invalid UserName or Password";
	        }
		}
		
		authRes.put("isRequestAuthentic", isRequestAuthentic);
		authRes.put("errorCode", errorCode);
		authRes.put("errorMessage", errorMessage);
		authRes.put("requestUserName", requestUserName);
		return authRes;
	}
	
	private Map<String,Object> getPasswordFromDBByUserName(String userName,String lid){
		final int GET_TCC_API_PASSWORD_TRANSACTION_CODE = 131;
		boolean isExceptionOccured = false;
		String password = "";
		String activeFlag = "";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		try{
			WsTransactionRequest[] transactionRequests = new WsTransactionRequest[1];
			transactionRequests[0] = new WsTransactionRequest("argUserName",PortalUtility.getString(userName));
			//TRANSACTION CODE 131 in FIRS_STAGING and LIVE BO
			
			WsTransactionResponse[] transactionResponse = 
					PortalService.getWSClient().executeCustomTransaction(lid, GET_TCC_API_PASSWORD_TRANSACTION_CODE, transactionRequests);
			if (!PortalUtility.isNull(transactionResponse)) {
				for(WsTransactionResponse response : transactionResponse){
					switch(PortalUtility.getString(response.getColumnName())) {
			         case "password" :
			        	password = PortalUtility.getString(response.getColumnValues(0));
			            break;
			         case "flag" :
			        	activeFlag = PortalUtility.getString(response.getColumnValues(0));
				        break;
			         default :
			            System.out.println("No value found");
			      }
				}
			}
		}catch(Exception e){
			isExceptionOccured = true;
			e.printStackTrace();
		}
		
		resMap.put("isExceptionOccured", isExceptionOccured);
		resMap.put("password", password);
		resMap.put("activeFlag", activeFlag);
		return resMap;
	}
	
	private Map<String,Object> getTaxOfficeByFolderRSN(String folderRSN,String lid){
		final int GET_TAX_OFFICE_BY_FOLDERRSN_TRANSACTION_CODE = 129;
		boolean isExceptionOccured = false;
		String taxOfficeDesc = "";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		try{
			WsTransactionRequest[] transactionRequests = new WsTransactionRequest[1];
			transactionRequests[0] = new WsTransactionRequest("argFolderRSN",PortalUtility.getString(folderRSN));
			//TRANSACTION CODE 129 in FIRS_STAGING and LIVE BO
			
			WsTransactionResponse[] transactionResponse = 
					PortalService.getWSClient().executeCustomTransaction(lid, GET_TAX_OFFICE_BY_FOLDERRSN_TRANSACTION_CODE, transactionRequests);
			if (!PortalUtility.isNull(transactionResponse)) {
				for(WsTransactionResponse response : transactionResponse){
					switch(PortalUtility.getString(response.getColumnName())) {
			         case "InfoDesc" :
			        	 taxOfficeDesc = PortalUtility.getString(response.getColumnValues(0));
			            break;
			         default :
			            System.out.println("No value found");
			      }
				}
			}
		}catch(Exception e){
			isExceptionOccured = true;
			e.printStackTrace();
		}
		
		resMap.put("isExceptionOccured", isExceptionOccured);
		resMap.put("taxOfficeDesc", taxOfficeDesc);
		return resMap;
	}
	
	private JSONObject getTCCDetails(int tccNumber,String requestJSONTIN,String lid){
		String tccstatus = "";
		String tccIssueDate = "";
		String tccExpiry = "";
		String tccApplicantName = "";
		String tccApplicantTIN = "";
		String tccRCNumber = "";
		String taxOfficeDesc = "";
		int floatRoundOffTo = 2;
		
		final String INDIVIDUAL_FOLDER_TYPE = "ITCC";
		final String CORPORATE_FOLDER_TYPE 	= "TCC";
		final String FOREX_FOLDER_TYPE 		= "FTCC";
		
		JSONObject tccDetailsObj = new JSONObject();
		WsFolder tccfolder = new WsFolder();
		SimpleDateFormat formatter = new SimpleDateFormat("DD-MMM-yyyy"); 
		StringBuilder tccApplicantNameBuilder = new StringBuilder();
		WsPeople associatedPeople = new WsPeople();
		WsFolderFreeform[] freeformList = null;
		JSONArray freeformInfo = new JSONArray();
		
		String statusCode = "";
		String description = "";
		
		try{
			tccfolder = PortalService.getFolderByTCC(String.valueOf(tccNumber));
			
			if(!PortalUtility.isNull(tccfolder)){
				associatedPeople = PortalService.getPeopleByFolder(PortalUtility.getInt(tccfolder.getFolderRSN()));
				
				if(requestJSONTIN.equals(PortalUtility.getString(associatedPeople.getLicenceNumber()))){
					int folderrsn = PortalUtility.getInt(tccfolder.getFolderRSN());
					tccstatus = PortalUtility.getString(tccfolder.getStatusDesc());
					tccRCNumber = PortalUtility.getString(associatedPeople.getReferenceFile());
					//tccIssueDate=PortalUtility.getDate(tccfolder.getIndate());
					tccIssueDate = PortalUtility.getDate(tccfolder.getIssueDate());
					tccExpiry = PortalUtility.getDate(tccfolder.getExpiryDate());
					//tccApplicantName = PortalService.getFolderInfoByFolderRSNInMap(folderrsn).get(5000);
					tccApplicantName = PortalUtility.getString(associatedPeople.getOrganizationName());
					tccApplicantTIN = PortalUtility.getString(associatedPeople.getLicenceNumber());
					
					Map<String,Object> taxOfficeResMap = getTaxOfficeByFolderRSN(String.valueOf(folderrsn),lid);
					taxOfficeDesc = taxOfficeResMap.get("taxOfficeDesc").toString();
					
					if(!PortalUtility.isNull(tccfolder)){
						if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
							freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(folderrsn,3,new int[]{2});
						}else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
							freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(folderrsn,3,new int[]{1});
						}else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(FOREX_FOLDER_TYPE)){
							freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(folderrsn,1,new int[]{3});
						}
					}
					
					if(freeformList !=null && freeformList.length>0){
						if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
							tccDetailsObj.put("taxPayerType", "corporate");
							for(WsFolderFreeform freeform : freeformList){
								String 	s_turnover 			= PortalUtility.getString(freeform.getC14()).equals("") ? "0" : PortalUtility.getString(freeform.getC14()),
										s_assessableProfit 	= PortalUtility.getString(freeform.getC10()).equals("") ? "0" : PortalUtility.getString(freeform.getC10()),
										s_taxPaid 			= PortalUtility.getString(freeform.getC12()).equals("") ? "0" : PortalUtility.getString(freeform.getC12()),
										s_totalProfit 		= PortalUtility.getString(freeform.getC11()).equals("") ? "0" : PortalUtility.getString(freeform.getC11());
								
								/* Double 	turnover 			= s_turnover.equals("") ? 0.0 : Double.parseDouble(freeform.getC14()),
											assessableProfit 	= s_assessableProfit.equals("") ? 0.0 : Double.parseDouble(freeform.getC10()),
											taxPaid 			= s_taxPaid.equals("") ? 0.0 : Double.parseDouble(freeform.getC12()); */
								
								s_turnover 			= s_turnover.contains(".") ? s_turnover : s_turnover+".00";
								s_assessableProfit 	= s_assessableProfit.contains(".") ? s_assessableProfit : s_assessableProfit+".00";
								s_taxPaid 			= s_taxPaid.contains(".") ? s_taxPaid : s_taxPaid+".00";
								s_totalProfit 		= s_totalProfit.contains(".") ? s_totalProfit : s_totalProfit+".00";
								
								DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
								
								JSONObject freeformObj = new JSONObject();
								freeformObj.put("year",freeform.getN01());
								freeformObj.put("turnOver",s_turnover.equals("0.00") ? s_turnover : decimalformatter.format(new BigDecimal(s_turnover)));
								freeformObj.put("assessableProfit",s_assessableProfit.equals("0.00") ? s_assessableProfit : decimalformatter.format(new BigDecimal(s_assessableProfit)));
								freeformObj.put("totalProfit",s_totalProfit.equals("0.00") ? s_totalProfit : decimalformatter.format(new BigDecimal(s_totalProfit)));
								freeformObj.put("taxPaid",s_taxPaid.equals("0.00") ? s_taxPaid : decimalformatter.format(new BigDecimal(s_taxPaid)));
								freeformInfo.put(freeformObj);
							}	
						 }else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
							 tccDetailsObj.put("taxPayerType", "individual");
							for(WsFolderFreeform freeform : freeformList){
								String s_totalIncome 	= PortalUtility.getString(freeform.getC02());
								String s_taxPaid 		= PortalUtility.getString(freeform.getC03());
								
								s_totalIncome 	= s_totalIncome.contains(".") ? s_totalIncome : s_totalIncome+".00";
								s_taxPaid 		= s_taxPaid.contains(".") ? s_taxPaid : s_taxPaid+".00";
								
								DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
								
								JSONObject freeformObj = new JSONObject();
								freeformObj.put("year",freeform.getN01());
								freeformObj.put("totalIncome",s_totalIncome.equals("0.00") ? s_totalIncome : decimalformatter.format(new BigDecimal(s_totalIncome)));
								freeformObj.put("taxPaid",s_taxPaid.equals("0.00") ? s_taxPaid : decimalformatter.format(new BigDecimal(s_taxPaid)));
								freeformInfo.put(freeformObj);
							}
						}else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(FOREX_FOLDER_TYPE)){
							tccDetailsObj.put("taxPayerType", "forex");
							for(WsFolderFreeform freeform : freeformList){
								String s_F04 = PortalUtility.getString(freeform.getC08()).equals("") ? "0" : PortalUtility.getString(freeform.getC08());
								s_F04 = s_F04.contains(".") ? s_F04 : s_F04+".00";
								
								DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
								String s_formatted_F04 = s_F04.equals("0.00") ? s_F04 : decimalformatter.format(new BigDecimal(s_F04));
								
								JSONObject freeformObj = new JSONObject();
								freeformObj.put("remmitanceAmountApproved",s_formatted_F04);
								freeformInfo.put(freeformObj);
							}
						}
						
					} 
					
					tccDetailsObj.put("taxPayerName", tccApplicantName);
					tccDetailsObj.put("taxOffice", taxOfficeDesc);
					tccDetailsObj.put("tin",tccApplicantTIN);
					tccDetailsObj.put("tccNumber", tccNumber);
					tccDetailsObj.put("tccStatus", tccstatus);
					tccDetailsObj.put("tccIssueDate", tccIssueDate);
					tccDetailsObj.put("tccExpiryDate", tccExpiry);
					tccDetailsObj.put("rcNumber", tccRCNumber);
					tccDetailsObj.put("assessment",freeformInfo);
					tccDetailsObj.put("statusCode", "0");
					tccDetailsObj.put("description", "Success");
					
				}else{
					tccDetailsObj.put("statusCode", "6");
					tccDetailsObj.put("description", "Incorrect TIN");
				}
			}else{
				tccDetailsObj.put("statusCode", "7");
				tccDetailsObj.put("description", "TCC not found");
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return tccDetailsObj;
	}
	
	private static Map<String,Object> validateRequestBodyData(JSONObject requestBodyJSONObj){
		boolean isExceptionOccured = false;
		
		Map<String,Object> responseMap = new HashMap<String,Object>();
		List<String> validKeySet = new ArrayList<String>();
		validKeySet.add("tccNumber");
		validKeySet.add("tin");
		
		List<String> mismatchedKeyList = new ArrayList<String>();
		List<String> emptyValueKeyList = new ArrayList<String>();
		
		try{
			for(Iterator<String> keys = requestBodyJSONObj.keys();keys.hasNext();) {
				String jsonKey = keys.next();
				String value = requestBodyJSONObj.get(jsonKey).toString();
				if (!validKeySet.contains(jsonKey)) mismatchedKeyList.add(jsonKey);
				if (value.equals("")) emptyValueKeyList.add(jsonKey);
			}
			
			
		}catch(Exception e){
			isExceptionOccured = true;
			e.printStackTrace();
		}
		
		responseMap.put("mismatchedKeyList", mismatchedKeyList);
		responseMap.put("emptyValueKeyList", emptyValueKeyList);
		responseMap.put("isExceptionOccured", isExceptionOccured);
		
		return responseMap;
	}
%>
<%
	System.out.println(">TCCValidation API");

	response.setContentType("application/json");
	response.setHeader("Content-Disposition", "inline");
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With ,X-ConsumerKey, X-ConsumerSecretKey");
	response.setHeader("Access-Control-Request-Headers","Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, X-ConsumerKey, X-ConsumerSecretKey");
	
	boolean isRequestAuthentic = false;
	boolean isRequestBodyEmpty = true;
	boolean isRequestBodyDataValid = false;
	
	String requestBodyJSONString = "";
	String statusCode = "";
	String description = "";
	String stampUser = "";
	
	JSONObject tccDetailsJSONObj = new JSONObject();
	JSONObject responseJSONObj = new JSONObject();
	JSONObject requestBodyJSONObj = new JSONObject();
	
	Map<String, String> requestHeaderMap = new HashMap<String, String>();
	Map<String,Object> authRespMap = new HashMap<String,Object>();
	
	try{
		authRespMap = authenticateRequest(request,lid);
		isRequestAuthentic = (boolean)authRespMap.get("isRequestAuthentic");
		System.out.println("isRequestAuthentic --- "+Boolean.toString(isRequestAuthentic));
		
		requestBodyJSONString = getRequestBodyJSONString(request);
		System.out.println("requestBodyJSONString --- "+requestBodyJSONString);
		
		/* requestHeaderMap = getRequestHeadersInfo(request);
		System.out.println("requestHeaderMap --- "+requestHeaderMap.toString()) */;
		
		if(isRequestAuthentic){
			stampUser = PortalUtility.getString(authRespMap.get("requestUserName").toString());
			
			if(!requestBodyJSONString.equals("")){
				isRequestBodyEmpty = false;
				
				requestBodyJSONObj = new JSONObject(requestBodyJSONString);
				
				Map<String,Object> validationResponseMap = validateRequestBodyData(requestBodyJSONObj);
				
				List<String> mismatchedKeyList = (List<String>)validationResponseMap.get("mismatchedKeyList");
				List<String> emptyValueKeyList = (List<String>)validationResponseMap.get("emptyValueKeyList");
				
				if(mismatchedKeyList.size() > 0){
					statusCode = "4";
					description = "Missing Required Field";
				}else if(emptyValueKeyList.size() > 0){
					statusCode = "4";
					description = "Missing Required Field";
				}else if(requestBodyJSONObj.length() < 2){
					statusCode = "4";
					description = "Missing Required Field";
				}else{
					tccDetailsJSONObj = getTCCDetails(requestBodyJSONObj.getInt("tccNumber"),requestBodyJSONObj.getString("tin"),lid);
					
					if(!tccDetailsJSONObj.has("statusCode") && !tccDetailsJSONObj.has("description") ){
						statusCode = "1";
						description = "Failure";
					}else{
						statusCode = tccDetailsJSONObj.getString("statusCode");
						description = tccDetailsJSONObj.getString("description");
					}
					
					responseJSONObj = tccDetailsJSONObj;
					System.out.println(tccDetailsJSONObj.toString());
				}
				
			}else{
				statusCode = "5";
				description = "Invalid Request";
			}
			
		}else{
			statusCode = authRespMap.get("errorCode").toString();
			description = authRespMap.get("errorMessage").toString();
		}
		
	}catch(Exception e){
		String exceptionClass = e.getClass().getName();
		
		if(exceptionClass.equals("org.codehaus.jettison.json.JSONException")){
			statusCode = "5";
			description = "Invalid Request";
		}else{
			statusCode = "1";
			description = "Failure";
		}
		
		e.printStackTrace();
	}finally{
		boolean isthisAPIRequestLogged = false;
		final int VERIFY_TCC_API_LOG_TRANSACTION_CODE = 130;
		
		responseJSONObj.put("statusCode", statusCode);
		responseJSONObj.put("description", description);
		
		WsTransactionRequest[] transactionRequests = new WsTransactionRequest[4];
		transactionRequests[0] = new WsTransactionRequest("argREQUESTSTRING",PortalUtility.getString(requestBodyJSONString));
		transactionRequests[1] = new WsTransactionRequest("argRESPONSESTRING",PortalUtility.getString(responseJSONObj.toString()));
		transactionRequests[2] = new WsTransactionRequest("argRESPONSESTATUS",PortalUtility.getString(statusCode));
		transactionRequests[3] = new WsTransactionRequest("argSTAMPUSER",PortalUtility.getString(stampUser));
		
		WsTransactionResponse[] transactionResponse = 
				PortalService.getWSClient().executeCustomTransaction(lid, VERIFY_TCC_API_LOG_TRANSACTION_CODE, transactionRequests);
		
		if(!PortalUtility.isNull(transactionResponse)){
			if(!transactionResponse[0].getColumnValues()[0].equals("")){
				isthisAPIRequestLogged = transactionResponse[0].getColumnValues()[0].equals("true") ? true : false;
				System.out.println("isthisAPIRequestLogged ---- "+Boolean.toString(isthisAPIRequestLogged));
			}
		}
	}
	
	response.getWriter().write(responseJSONObj.toString());
%>
