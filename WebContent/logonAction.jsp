<%@page import="org.apache.commons.lang.RandomStringUtils"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@ page language="java" buffer="8kb" import="java.util.*,com.csdcsystems.amanda.common.*,java.sql.SQLException" autoFlush="true" isThreadSafe="true" isErrorPage="false" session="true"%>

<%@ include file="include/includeconstant.jsp"%>
<%

	request.setAttribute("pageName","logon");
	String fromEmailAddress = "fromaddress@senegalportal.com"; // But it takes the email address configured during the SMTP configuration.
	String CCemailAddress = "";
	String loginLid = "";

	boolean  adminUser = false;
	String pagename = request.getParameter("pagename");
	String SubAccount = "";
	int peopleRSN = 0;
	long passwordExpiryLimitInHour = 24;
	long passwordExpiryTime = 0;
	WsPeople people = new WsPeople();
	
	if ("".equals(pagename)) pagename = "index";

	String theUrl = null;
	
	String registrationNumber = PortalUtility.isNull(request.getParameter("registrationNumber")) ? "" :request.getParameter("registrationNumber");
	String userName = PortalUtility.isNull(request.getParameter("UserName")) ? "" : request.getParameter("UserName").toLowerCase();
	String password = PortalUtility.isNull(request.getParameter("PassWord")) ? "" :request.getParameter("PassWord");
	
	
	if (userName.equals("")) {
		response.sendRedirect("home.jsp"); 
		return;
	}
	
	
	
	//String connectionCache = CONNECTION_CACHE;
	//Assuming amandauser doesn't have emailaddress as username
	//senegal
	if(userName.indexOf("@")>-1)
		loginLid = SecurityPortalService.doPortalLogin(true, request);
	else
		loginLid = SecurityPortalService.doPortalLogin(false, request);
	

	if (loginLid.equals("")) {
		request.getRequestDispatcher("portal?cmd=logon&logon=failed").forward(request, response);
		return;
	}else if(!loginLid.equals("")){
		lid = loginLid;
	}
	
	//this is required as we are not logging in includeconstant
	//SetHeader.addHeader(headerBindingProvider, lid, LanguageCode.getLanguageCode());
	adminUser = PortalService.isAdminUser(userName);
	LidHandler.setLoggedInLid(lid,request);
	session.setAttribute("isAdmin", adminUser);
	
	
	peopleRSN = PortalService.getLoggedPeopleRSN(request);//PortalService.PeopleRSN;

	LogonName = "Admin";
	if (!adminUser) {
		people = PortalService.getWSClient().getPeople(lid, peopleRSN);
		LogonName = PortalUtility.getString(people.getNameFirst()) + " "
				+ PortalUtility.getString(people.getNameMiddle()) + " "
				+ PortalUtility.getString(people.getNameLast());

		//request.setAttribute("PeopleRSN", peopleRSN);
		//request.setAttribute("resetPass", "true");
		session.setAttribute("peopleType", PortalUtility.getInt(people.getPeopleCode()));
		session.setAttribute("PeopleRSN", peopleRSN);
		if(PortalUtility.getInt(people.getStatusCode()) != 0)
			session.setAttribute("peopleStatus", PortalUtility.getInt(people.getStatusCode()));
	}
	session.setAttribute("LogonName", LogonName);
	//request.setAttribute("lid", lid);
	//request.setAttribute("LogonName", "Welcome: " + LogonName);
	//session.setAttribute("LogonName", LogonName);

	//Checking difference between registration date and current date. If the difference crocess the password expiry limit then reset new password.
	if(people!=null && people.getBirthDate() !=null)
		passwordExpiryTime = (Calendar.getInstance().getTimeInMillis()-people.getBirthDate().getTimeInMillis())/ (60 * 60 * 1000);//Getting date difference between current date and registered date.
	if(people!=null && PortalUtility.getString(people.getEmailFlag()).equals("Y")){
		if(passwordExpiryTime<passwordExpiryLimitInHour){
			theUrl = "portal?cmd=resetPassword&peopleRSN="+peopleRSN+"&emailId="+people.getEmailAddress();
		}else{
			
			boolean flag = false;
			String tempPassword = RandomStringUtils.random(8, true, true);
			people.setInternetPassword(tempPassword);
			people.setEmailFlag("Y");
			people.setBirthDate(Calendar.getInstance());
			
			try{
				flag = PortalService.getWSClient().updatePeople(lid, people);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(flag){
				PortalService.getWSClient().sendHtmlEmail(lid, PortalUtility.getString(people.getEmailAddress()), fromEmailAddress, CCemailAddress, resourceBundle.getText("MESSAGE_MAIL_SUBJET"),
						resourceBundle.getText("MESSAGE_MAIL_BODY0")+" "+PortalUtility.getString(people.getNameFirst())+",  <br/><br/>"+resourceBundle.getText("MESSAGE_MAIL_BODY1")+" <br/>"
						+resourceBundle.getText("MESSAGE_MAIL_BODY2")+"<br/><br/>"+resourceBundle.getText("MESSAGE_MAIL_BODY3")+" <strong style='color:red;'>"+tempPassword+"<br/><br/>"
						+resourceBundle.getText("MESSAGE_MAIL_BODY4")+"</strong>");
			}
			session.invalidate();
			session = request.getSession(true);
			session.setAttribute("LanguageCode", LanguageCode);
			theUrl = "portal?cmd=logon&logon=passwordExpired";
		}
	}
	//To check wheather the mandatory datas are associated with the people record for the users-
	//-added by admin and they have set their password on user_registration page. Here we are checking for NINIA number.
	else if(!adminUser && PortalUtility.isNull(people.getInternetQuestion())){
		theUrl = "portal?cmd=myProfile&updateProfile=true";
	}else{
		theUrl = "portal";
	}
	application.setAttribute("biling", "fr");
	//session.setAttribute("loggedonLid", lid);
	//session.setAttribute("PeopleRSN", peopleRSN);

	String clickedOn = request.getParameter("clickedOn");
	String sf = request.getParameter("sf");
	String sk = request.getParameter("sk");
	if (!"".equals(clickedOn)) {
		theUrl = "portal?cmd=" + clickedOn;
		if (!"".equals(sk)) {
			theUrl += "&searchFlag=" + sf + "&searchKeyword=" + sk;
		}
	}
	response.sendRedirect(theUrl);
%>
