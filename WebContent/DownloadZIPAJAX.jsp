<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.nio.file.Paths"%>
<%@page import="java.nio.file.Path"%>
<%@page import="java.nio.file.Files"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.zip.ZipEntry"%>
<%@page import="java.util.zip.ZipOutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="jxl.Workbook"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@include file="include/includeconstant.jsp"%>
<%!
	public static void prepareZIP(String fileName) throws IOException {

	  String folderName = fileName;
	  File destFolder = new File("D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\GeneratedPdfs\\"+folderName);
	  
	  File[] files = destFolder.listFiles();

	  File zipFolder = new File("D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\GeneratedPdfs\\"+folderName+".zip");
	  
	  if (!zipFolder.exists()) {
         	System.out.println("ZIP Directory is not Exsist");
         	FileOutputStream zipFile 
	  			= new FileOutputStream(new File("D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\GeneratedPdfs\\"+folderName+".zip"));
	  		ZipOutputStream output = new ZipOutputStream(zipFile);

			for (File pdfFIle : files) {
			   ZipEntry zipEntry = new ZipEntry(pdfFIle.getName());
			   output.putNextEntry(zipEntry);
		
			   FileInputStream pdfFile = new FileInputStream(new File(pdfFIle.getPath()));
			   IOUtils.copy(pdfFile, output);
			   pdfFile.close();
			   output.closeEntry();
			}
	  		
			output.finish();
	  		output.close();
	  		
      }else{
    	  System.out.println("ZIP Directory is Exsist");
      }
	  
	 }
%>
<%
	String 	result = "",
			command = "",
			key = "",
			s_fileName = "",
			pdfDownloadPath = "D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\GeneratedPdfs\\";
			//pdfDownloadPath = "/opt/tomcat/FIRSRECEIPTDOWNLOAD/GeneratedPdfs/";
	
	JSONObject validationDetails = new JSONObject();
	String filename = PortalUtility.getString(request.getParameter("key"));
	
	try{
		/* prepareZIP(filename);	
		
		String filepath = "D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\GeneratedPdfs\\";   
		response.setContentType("application/zip");
		response.setHeader("Content-Disposition","attachment; filename=\"" + filename+".zip" + "\"");   
		
		Path path = Paths.get(filepath + filename+".zip");
		byte[] ba = Files.readAllBytes(path);
		
		response.setHeader("Content-Length", String.valueOf((ba.length)));
		
		java.io.FileInputStream fileInputStream = new java.io.FileInputStream(filepath + filename+".zip");
		BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
		
		bos.write(ba); */ 
		    
		/* byte[] outputByte = new byte[4096];
		while(fileInputStream.read(outputByte, 0, 4096) != -1){
			bos.write(outputByte, 0, 4096);
        } */
		
		/* int i;   
		while ((i=fileInputStream.read()) != -1) {  
	 		out.write(i);   
		}  */  
		String folderName = filename;
		File destFolder = new File(pdfDownloadPath + folderName);
		
		if(destFolder.exists()){
			File[] files = destFolder.listFiles();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			byte bytes[] = new byte[2048];

			for (File fileName : files) {
				FileInputStream fis = new FileInputStream(fileName);
				BufferedInputStream bis = new BufferedInputStream(fis);

				zos.putNextEntry(new ZipEntry(fileName.getName()));

				int bytesRead;
				while ((bytesRead = bis.read(bytes)) != -1) {
					zos.write(bytes, 0, bytesRead);
				}
				zos.closeEntry();
				bis.close();
				fis.close();
			}
			zos.flush();
			baos.flush();
			zos.close();
			baos.close();
			byte[] zipData = baos.toByteArray();

			response.setContentType("application/zip");
			response.setHeader("Content-Disposition", "attachment; filename=\"ReceiptData.zip\"");

			response.getOutputStream().write(zipData);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}else{
			out.println("Invalid Key !");
		}
		
	}catch(Exception e){
		e.printStackTrace();
		out.println(e.getMessage());
	}finally{
		response.getOutputStream().flush();
		response.getOutputStream().close();
		
		validationDetails.put("result", result);
		System.out.print(validationDetails.toString());
		//out.print(validationDetails.toString());
	}
%>
