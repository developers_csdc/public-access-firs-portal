<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="include/includeconstant.jsp"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>

<%!
%>

<%
	response.setHeader("Content-Type", "application/json; charset=UTF-8");
  	response.setHeader( "Pragma", "no-cache" );
   	response.setHeader( "Cache-Control", "no-cache" );
   	response.setDateHeader( "Expires", 0 );
%>

<%
	log.info("File Name:AccountActivationAjax.jsp-->Begins");
	
	JSONObject activationResponse = new JSONObject();
	String peopleRSN 	= request.getParameter("peopleRSN");
	String key 			= request.getParameter("key");
	boolean isAccountActive = false;
	boolean peopleExists = false;
	boolean validUrlFlag = false;
	boolean activationSuccessFlag = false;
	boolean exceptionFlag = false;
	String cmd = request.getParameter("cmd");
	WsPeople user = new WsPeople();
	String activationKey = "";
	
	if(cmd.equalsIgnoreCase("activateAccount")){
		try{
			user = PortalService.getWSClient().getPeople(lid, PortalUtility.toInt(peopleRSN));
			if(!PortalUtility.isNull(user)){
				validUrlFlag = true;
				peopleExists = true;
				String accountActiveFlag = "";
				accountActiveFlag = PortalUtility.getString(user.getSmsFlag());
				isAccountActive = accountActiveFlag.equalsIgnoreCase("Y")?true :false ;
				if(!isAccountActive){
					activationKey = PortalService.getSHA2Hash(PortalUtility.getString(user.getInternetAnswer()));
				}
			}
			
			if(peopleExists && !isAccountActive){
				if(activationKey.equals(key)){
					user.setSmsFlag("Y");
					activationSuccessFlag = PortalService.getWSClient().updatePeople(lid, user);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			exceptionFlag = true;
			activationResponse.put("exceptionHere", e.toString());
		}
		
	}else if(cmd.equalsIgnoreCase("cmd-2")){
		
	}
	
	activationResponse.put("isAccountActive", isAccountActive);
	activationResponse.put("activationSuccessFlag", activationSuccessFlag);
	activationResponse.put("peopleExists", peopleExists);
	activationResponse.put("validUrlFlag", validUrlFlag);
	activationResponse.put("exceptionFlag", exceptionFlag);
	
	log.info("File Name:AccountActivationAjax.jsp-->Ends");
	out.print(activationResponse.toString());
	log.info("AccountActivationAjax.jsp ..activationResponse::"+ activationResponse.toString());
%>
