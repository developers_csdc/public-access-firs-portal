<%@ page import = "java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>FIRS Release</title>
</head>
<body>
<div style="font-weight: bold;">
 <%
	 Date date = new Date();
	 String b = new String("");
	 SimpleDateFormat format = new SimpleDateFormat("YYMMddhhmm");
	 b = format.format(date);
 %>
	Release Details are given below:
	<br/>
	 <br/>
	 - Version : 7.2.0.1707041215
	 <br/>
	
	 - Release date : 04.07.2017
	 <br/>
	 <br/>
	 Regards,
	 <br/>
	 FIRS Development Team&copy;.
</div>
</body>
</html>