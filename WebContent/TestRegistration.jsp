<%@ page import="java.util.*, com.org.firs.*" language="java"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<jsp:include page="header.jsp" />
<main class="main-content">
<div class="fullwidth-block greet-section">
	<div class="container">
		<div class="col-md-6">
			<ul class="list-group">
				<li class="list-group-item">Application Type : <span>FOREX</span></li>
				<li class="list-group-item">Tax Identification Number: <span></span></li>
				<li class="list-group-item">Name of Company: <span></span></li>
				<li class="list-group-item">Tax Office ID : <span></span></li>
				<li class="list-group-item">Address: <span></span></li>
				<li class="list-group-item">Tax Office Type : <span></span></li>
				<li class="list-group-item">RC Number : <span></span></li>
				<li class="list-group-item">Email Address: <span></span></li>
				<li class="list-group-item">Phone Number: <span></span></li>
				
			</ul>
		</div>
		<form action="registrationControllerService">
			<div class="col-md-6">
				<p style="background: #6092ab; color: #fff; padding: 7px;">Registration
					for FOREX account</p>
				<p>
					<small>*Date of Incorporation is required*</small>
				</p>
				
				<div class="form-group">
					<input type="text" class="form-control" id="tin"
						value="<%=request.getParameter("applicationType")%>" readonly>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="tin"
						value="" readonly>
				</div>
				<div class="form-group">
					<input type="date" placeholder="Enter Date of Incorporation"
						class="form-control">
				</div>
				<button type="submit" class="btn btn-default btn-default">Cancel</button>
				<button type="submit" class="btn btn-default btn-primary"
					name="command" value="update">Submit</button>
			</div>
		</form>
	</div>
</div>

</div>
<!-- .container -->
</div>

</main>

</div>


</body>

</html>