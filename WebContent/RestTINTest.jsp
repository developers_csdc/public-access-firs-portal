<%@page import="org.apache.http.util.EntityUtils"%>
<%@page import="org.apache.http.client.methods.CloseableHttpResponse"%>
<%@page import="org.apache.http.client.methods.HttpGet"%>
<%@page import="org.apache.http.client.protocol.HttpClientContext"%>
<%@page import="org.apache.http.impl.auth.BasicScheme"%>
<%@page import="org.apache.http.impl.client.BasicAuthCache"%>
<%@page import="org.apache.http.client.AuthCache"%>
<%@page import="org.apache.http.impl.client.HttpClients"%>
<%@page import="org.apache.http.impl.client.CloseableHttpClient"%>
<%@page import="org.apache.http.auth.UsernamePasswordCredentials"%>
<%@page import="org.apache.http.auth.AuthScope"%>
<%@page import="org.apache.http.impl.client.BasicCredentialsProvider"%>
<%@page import="org.apache.http.client.CredentialsProvider"%>
<%@page import="org.apache.http.HttpHost"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.util.Base64"%>
<%@page import="java.net.URL"%>
<%@page import="com.softworks.tinvalidation.ws.Taxpayer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
	Taxpayer taxpayer = new Taxpayer();
	JsonObject responseJSON = new JsonObject();
	String TIN = request.getParameter("TIN");
	
	final String tinRetrivevalURL = "https://apps.firs.gov.ng/apis/tinvalidation/";
	String completeURL = tinRetrivevalURL+TIN.trim();
	final String userName = "CSDC";
	final String password = "T!Nvl#fh4";
	String tinWebServiceResponseLine = "";
	
	HttpHost target = new HttpHost("apps.firs.gov.ng", 443, "https");
	CredentialsProvider credsProvider = new BasicCredentialsProvider();
	credsProvider.setCredentials(
	        new AuthScope(target.getHostName(), target.getPort()),
	        new UsernamePasswordCredentials(userName,password));
	
	CloseableHttpClient httpclient = HttpClients.custom()
	        .setDefaultCredentialsProvider(credsProvider).build();
	
	try {
	    /*Create AuthCache instance*/
	    AuthCache authCache = new BasicAuthCache();
	    /*Generate BASIC scheme object and add it to the local*/
	    /*Auth cache*/
	    BasicScheme basicAuth = new BasicScheme();
	    authCache.put(target, basicAuth);
	
	    // Add AuthCache to the execution context
	    HttpClientContext localContext = HttpClientContext.create();
	    localContext.setAuthCache(authCache);
	
	    HttpGet httpget = new HttpGet(completeURL);
	
	    System.out.println("Executing request " + httpget.getRequestLine() + " to target " + target);
	    
	    for (int i = 0; i < 1; i++) {
	        CloseableHttpResponse response1 = httpclient.execute(target, httpget, localContext);
	        try {
	            System.out.println(">>>Response Status : " + response1.getStatusLine());
	            tinWebServiceResponseLine = EntityUtils.toString(response1.getEntity());
	            responseJSON = (new JsonParser()).parse(tinWebServiceResponseLine).getAsJsonObject();
	            out.println(tinWebServiceResponseLine);
	            System.out.println(">>>TinWebService Response Line : " +tinWebServiceResponseLine);
	        } finally {
	            response1.close();
	        }
	    }
	    
	} finally {
	    httpclient.close();
	}
	
	/*Setting up taxpayer Bean*/
	out.println(responseJSON);
%>
