<%@page import="java.net.URLEncoder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsBillInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsBillPayment"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderFee"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsAccountBill"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPaymentInfo"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidLookup"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderFreeform"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderWithPeople"%>
<%@include file="include/includeconstant.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>

<%
	log.info("Entering" + pageName);
	//System.out.println(new Date().toString()+"  Entering >>>"+pageName);
	
	boolean dataFetched 		= true;
	boolean tinCheck 			= true;
	int tccNumber 				= -1;
	String tccstatus 			= null;
	String tccIssueDate 		= null;
	String tccExpiry 			= null;
	String tccApplicantName 	= null;
	int floatRoundOffTo 		= 2;
	
	JSONObject receiptDetails 	= new JSONObject();
	
	receiptDetails.put("DataFetched", dataFetched);
	
	String receiptNumberInHexa 				= PortalUtility.getString(request.getParameter("receipt_number"));
	String receiptnumber 					= Integer.toString(PortalUtility.hex2decimal(receiptNumberInHexa));
	String TIN 								= PortalUtility.getString(request.getParameter("TIN"));
	String receiptFolderType 				= "";
	//String receiptTypeFromUI 				= PortalUtility.getString(request.getParameter("receiptType"));
	
	SimpleDateFormat formatter 				= new SimpleDateFormat("DD-MMM-yyyy");
	StringBuilder tccApplicantNameBuilder 	= new StringBuilder();
	
	boolean isExceptionOccured 	= false;
	String exceptionClassName 	= "";
	String exceptionTrace 		= "";

	WsFolder tccfolder 					= null;
	WsPeople PayerPeople 				= null;
	WsFolderFreeform[] freeformList 	= null;
	WsPaymentInfo paymentinfo 			= null;
	WsFolder folder 					= null;
	WsFolder parentFolder 				= null;
	WsAccountBill[] bills 				= null;
	WsFolderFee[] fees 					= null;
	
	Map<Integer, String> mapFolderInfo 	= new HashMap<Integer, String>();
	List<String> TINNumbers 			= new ArrayList<String>();

	final int TAX_OFFICE_INFOCODE 		= 5090;
	//final int PAYER_PEOPLECODE = 10;
	/* Beneficiary Code has been changed to 10 according to new Logic */
	final int BENEFICIARY_PEOPLECODE 	= 10;
	final int FEECODE_LOOKUP_CODE 		= 3;

	Map<String, String> TAX_OFFICE_MAP 		= null;
	Map<String, String> receiptDetailsMap 	= new HashMap<String,String>();
	
	String PaymentNumber 			= "";
	String childPaymentResponse1 	= "";
	String rate 					= "";
	String bank 					= "";
	String paymentResponse 			= "";
	String assessmentPeriodFrom 	= "";
	String assessmentPeriodTo 		= "";
	String currency 				= "";
	String contractDescription 		= "";
	String contractAmount 			= "";
	String billToRSN 				= "";
	String taxOfficeDescription     = "";

	/* 
		NEEDS TO ADD CODE WHICH WIL GET FOLDER TYPE FROM RECEIPT NUMBER BY USING A CUSTOM TANSACTION
		FR 		- FORIGN RECIPT
		FRCT 	- FORIGN RECIPT CREDIT NOTES
		RCPT 	- RECIPT
		RCTI 	- Individual Credit Notes
	*/
	
	try{
		WsTransactionRequest[] transactionRequests2 = new WsTransactionRequest[1];
		transactionRequests2[0] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
		
		WsTransactionResponse[] transactionResponse2 = PortalService.getWSClient().executeCustomTransaction(lid, 136, transactionRequests2);

		if (!PortalUtility.isNull(transactionResponse2)) {
			//HERE we will receiptFolderType
			receiptFolderType = PortalUtility.getString(transactionResponse2[0].getColumnValues(0));
		}
		
		//receiptFolderType = "RCPT";
		
	}catch(Exception ex){
		dataFetched = false;
		isExceptionOccured = true;
		exceptionClassName = ex.getClass().getName();
		exceptionTrace = ex.getMessage();
		receiptDetails.put("tinCheck", tinCheck);
		receiptDetails.put("DataFetched", dataFetched);
		receiptDetails.put("isExceptionOccured", isExceptionOccured);
		receiptDetails.put("exceptionClassName", exceptionClassName);
		receiptDetails.put("exceptionTrace", exceptionTrace);
		
		log.info("Printing... receiptDetails >>>"+receiptDetails.toString());
		log.info("Printing... Exiting >>>"+pageName);
		response.getWriter().write(receiptDetails.toString());
		ex.printStackTrace();
	}
	
	if(!isExceptionOccured){
		if(!(receiptFolderType.equals("FR") || receiptFolderType.equals("FRCT"))){
			try {
				TAX_OFFICE_MAP = PortalService.getInfoValidValues(TAX_OFFICE_INFOCODE);
				if (receiptnumber != "") {
					/* Getting receiptDetailsMap by passing Receipt Number */
					receiptDetailsMap = PortalService.getReceiptDetailsMap(receiptnumber);
					System.out.println(new Date().toString()+"  receiptDetailsMap >>>"+receiptDetailsMap.toString());
					
					isExceptionOccured = Boolean.valueOf(receiptDetailsMap.get("isExceptionOccured"));
					
					if(!Boolean.valueOf(receiptDetailsMap.get("isExceptionOccured")) && PortalUtility.getString("paymentNumber") != "" ){
						PaymentNumber 			= PortalUtility.getString(receiptDetailsMap.get("paymentNumber"));
						childPaymentResponse1 	= PortalUtility.getString(receiptDetailsMap.get("childPaymentResponse1"));
						rate 					= PortalUtility.getString(receiptDetailsMap.get("rate"));
						bank 					= PortalUtility.getString(receiptDetailsMap.get("bank"));
						paymentResponse 		= PortalUtility.getString(receiptDetailsMap.get("paymentResponse"));
						assessmentPeriodFrom 	= PortalUtility.getString(receiptDetailsMap.get("assessment_period_from"));
						assessmentPeriodTo 		= PortalUtility.getString(receiptDetailsMap.get("assessment_period_to"));
						currency 				= PortalUtility.getString(receiptDetailsMap.get("currency"));
						contractDescription 	= PortalUtility.getString(receiptDetailsMap.get("contractDescription"));
						contractAmount 			= PortalUtility.getString(receiptDetailsMap.get("contractAmount"));
						billToRSN 				= PortalUtility.getString(receiptDetailsMap.get("billToRSN"));
						taxOfficeDescription    = PortalUtility.getString(receiptDetailsMap.get("taxOfficeName"));
						
						receiptDetails.put("rate", rate);
						receiptDetails.put("bank", bank);
						receiptDetails.put("assessmentPeriodFrom", assessmentPeriodFrom);
						receiptDetails.put("assessmentPeriodTo", assessmentPeriodTo);
						receiptDetails.put("contractAmount", contractAmount);
						receiptDetails.put("contractDescription", URLEncoder.encode(contractDescription, "UTF-8"));
						receiptDetails.put("tax_office_name", taxOfficeDescription);
					}
					
					if (PaymentNumber != "") {
						paymentinfo = PortalService.getPaymentInfo(Integer.valueOf(PaymentNumber));
						System.out.println(new Date().toString()+"  paymentinfo >>>"+paymentinfo.toString());
						
						folder = PortalService.getFolderByFolderRSN(paymentinfo.getFolderRSN().toString());
						System.out.println(new Date().toString()+"  folder >>>"+folder.toString());
						
						TINNumbers.add(PortalUtility.getString(folder.getReferenceFile2()));
						
						String paymentReference = "N/A";
						/*int parentFolderRSN = PortalUtility.getInt(folder.getParentRSN());
						parentFolder = PortalService.getWSClient().getFolder(lid, parentFolderRSN);*/

						/* Getting PAYMENTREFERENCE from ACCOUNTPAYMNT.PAYMENTRESPONSE1 by compairing PAYMENTRESPONSE2 of both folder and its Parent Folder */
						/*WsTransactionRequest[] transactionRequests2 = new WsTransactionRequest[3];
						transactionRequests2[0] = new WsTransactionRequest("argFolderRSN",PortalUtility.getString(String.valueOf(folder.getFolderRSN())));
						transactionRequests2[1] = new WsTransactionRequest("argParentFolderRSN",String.valueOf(parentFolderRSN));
						transactionRequests2[2] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
						WsTransactionResponse[] transactionResponse2 = PortalService.getWSClient().executeCustomTransaction(lid, 118, transactionRequests2);

						String paymentReference = "N/A";
						if (!PortalUtility.isNull(transactionResponse2)) {
							paymentReference = PortalUtility.getString(transactionResponse2[0].getColumnValues(0));
							if(paymentReference.equals("null")){
								paymentReference = "N/A";
							}
						}*/
							
						if(folder.getFolderType().equalsIgnoreCase("RCTI")){
							
							int parentFolderRSN = PortalUtility.getInt(folder.getParentRSN());
							parentFolder = PortalService.getWSClient().getFolder(lid, parentFolderRSN);

							/* Getting PAYMENTREFERENCE from ACCOUNTPAYMNT.PAYMENTRESPONSE1 by compairing PAYMENTRESPONSE2 of both folder and its Parent Folder */
							WsTransactionRequest[] transactionRequests2 = new WsTransactionRequest[2];
							transactionRequests2[0] = new WsTransactionRequest("argFolderRSN",PortalUtility.getString(String.valueOf(folder.getFolderRSN())));
							transactionRequests2[1] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
							WsTransactionResponse[] transactionResponse2 = PortalService.getWSClient().executeCustomTransaction(lid, 118, transactionRequests2);

							//String paymentReference = "N/A";
							String paymentDate = "";
							if (!PortalUtility.isNull(transactionResponse2)) {
								paymentReference = PortalUtility.getString(transactionResponse2[0].getColumnValues(0));
								paymentDate = PortalUtility.getString(transactionResponse2[1].getColumnValues(0));
								
								if(paymentReference.equals("null")){
									paymentReference = "N/A";
								}
							}
							receiptDetails.put("paymentdate",paymentDate);
							receiptDetails.put("paymentReference", paymentReference+" | "+childPaymentResponse1);
							
						}else if(folder.getFolderType().equalsIgnoreCase("RCPT")){
					
							/* Getting PAYMENTREFERENCE from ACCOUNTPAYMNT.PAYMENTRESPONSE1 by compairing PAYMENTRESPONSE2 of both folder and its Parent Folder */
							WsTransactionRequest[] transactionRequests2 = new WsTransactionRequest[2];
							transactionRequests2[0] = new WsTransactionRequest("argFolderRSN",PortalUtility.getString(String.valueOf(folder.getFolderRSN())));
							transactionRequests2[1] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
							WsTransactionResponse[] transactionResponse2 = PortalService.getWSClient().executeCustomTransaction(lid, 128, transactionRequests2);

							//String paymentReference = "N/A";
							if (!PortalUtility.isNull(transactionResponse2)) {
								paymentReference = PortalUtility.getString(transactionResponse2[0].getColumnValues(0));
								if(paymentReference.equals("null")){
									paymentReference = "N/A";
								}
							}
							
							receiptDetails.put("paymentReference", paymentReference);
						}
			
						//receiptDetails.put("paymentReference", paymentReference+" | "+childPaymentResponse1);
						//receiptDetails.put("paymentReference", paymentReference);
						//receiptDetails.put("paymentReference", PortalUtility.getString(paymentReference));

						WsFolderWithPeople[] folderWithPeoples = PortalService.getWSClient().getFolderWithPeople(lid,folder.getFolderRSN());

						if (!PortalUtility.isNull(folderWithPeoples)) {
							for (WsFolderWithPeople folderWithPeople : folderWithPeoples) {
								String peopleTIN = PortalUtility.getString(folderWithPeople.getPeople().getLicenceNumber());
								TINNumbers.add(peopleTIN.trim());
							}
						}
						
						PayerPeople = PortalService.getPeople(Integer.parseInt(billToRSN));
						TINNumbers.add(PortalUtility.getString(PayerPeople.getLicenceNumber()));
						
						if (PayerPeople != null) {
							receiptDetails.put("payer_name",URLEncoder.encode(PortalUtility.getString(PayerPeople.getOrganizationName()), "UTF-8"));
							receiptDetails.put("payer_tin",PortalUtility.getString(PayerPeople.getLicenceNumber()));
							receiptDetails.put("payer_address",URLEncoder.encode(PortalUtility.getString(PayerPeople.getAddressLine1()), "UTF-8"));
						}

						if (TINNumbers.contains(TIN)) {
							/*Setting up TAX TYPE*/
							/* It is not required as CONTRACT Description = Tax Type according to the new Logic */
							/* if (!PortalUtility.isNull(folder)) {
								String subCodeDESC = PortalUtility.getString(folder.getSubCodeDesc());
								receiptDetails.put("tax_type_code", subCodeDESC);
								receiptDetails.put("tax_type_name", subCodeDESC);
							} else {
								receiptDetails.put("tax_type_code", "");
								receiptDetails.put("tax_type_name", "");
							} */

							WsPeople peopleBen;
							WsPeople peoplePayer;
							String payerAddress = "";
							String beneficiaryAddress = "";
							String beneficiaryTIN = "";
							String beneficiaryName = "";
							
							/* if (!PortalUtility.isNull(folderWithPeoples)) {
								for (WsFolderWithPeople folderWithPeople : folderWithPeoples) {
									if (folderWithPeople.getPeopleCode() == BENEFICIARY_PEOPLECODE) {
										peopleBen = folderWithPeople.getPeople();
										beneficiaryAddress = PortalUtility.getString(peopleBen.getAddressLine1());
										beneficiaryTIN = PortalUtility.getString(peopleBen.getLicenceNumber());
										beneficiaryName = PortalUtility.getString(peopleBen.getOrganizationName());
									}else if (folderWithPeople.getPeopleCode() == PAYER_PEOPLECODE) {
										peoplePayer = folderWithPeople.getPeople();
										receiptDetails.put("payerName",PortalUtility.getString(peoplePayer.getOrganizationName()));
										receiptDetails.put("payerTIN",PortalUtility.getString(peoplePayer.getLicenceNumber()).trim());
										receiptDetails.put("payer_address",PortalUtility.getString(peoplePayer.getAddressLine1()));
									}
								}
							} */
							
							/* Getting Beneficiary Details */
							peopleBen = PortalService.getPeopleByTIN(PortalUtility.getString(folder.getReferenceFile2()));
							
							if(!PortalUtility.isNull(peopleBen)){
								beneficiaryAddress = PortalUtility.getString(peopleBen.getAddressLine1());
								beneficiaryTIN = PortalUtility.getString(peopleBen.getLicenceNumber());
								beneficiaryName = PortalUtility.getString(peopleBen.getOrganizationName());
							}
							
							receiptDetails.put("beneficiaryName" , URLEncoder.encode(beneficiaryName, "UTF-8"));
							receiptDetails.put("ben_address" , URLEncoder.encode(beneficiaryAddress, "UTF-8"));
							receiptDetails.put("benificiary_tin", beneficiaryTIN);
							//receiptDetails.put("payer_address" , URLEncoder.encode(payerAddress, "UTF-8"));
							receiptDetails.put("folderType", PortalUtility.getString(folder.getFolderType()));
							receiptDetails.put("TaxPayerName", URLEncoder.encode(PortalUtility.getString(folder.getFolderName()), "UTF-8"));
							//receiptDetails.put("cust_address",PortalUtility.getString(folder.getFolderDescription()));
							receiptDetails.put("receipt_number", PortalUtility.getString(receiptnumber));
							
							//bills=PortalService.getAccountBill(paymentinfo.getFolderRSN());
							WsBillInfo billsinfo[]=paymentinfo.getBillInfos();
							
							//fees = PortalService.getFolderFees(paymentinfo.getFolderRSN());
							int feeCode = 0;
							if (fees != null && fees.length > 0 || true) {
								//receiptDetails.put("tax_type_code",PortalUtility.getString((fees[0].getComments())));
								//receiptDetails.put("tax_type_name",PortalUtility.getString((fees[0].getComments())));
								//receiptDetails.put("amount",PortalUtility.NumberFormatter(fees[0].getFeeAmount()));
								//receiptDetails.put("amount", String.format("%,.2f", fees[0].getFeeAmount()));
								receiptDetails.put("amount", String.format("%,.2f", billsinfo[0].getFeeAmount()));
								
								//receiptDetails.put("amount_in_word",PortalService.getConvertCurrencyToWord(fees[0].getFeeAmount()));
								receiptDetails.put("amount_in_word",PortalService.getConvertCurrencyToWord(billsinfo[0].getFeeAmount()));

								/* for (WsFolderFee fee : fees) {
									if (PortalUtility.getInt(fee.getPaymentNumber()[0]) == paymentinfo.getPaymentNumber()) {
										feeCode = PortalUtility.getInt(fee.getFeeCode());
									}
								} */

								WsValidLookup[] feeCodeDescLookUps = PortalService.getWSClient().getValidLookup(lid,FEECODE_LOOKUP_CODE);
								if (!PortalUtility.isNull(feeCodeDescLookUps)) {
									for (WsValidLookup feeCodeDescLookUp : feeCodeDescLookUps) {
										if (feeCodeDescLookUp.getLookUp1().intValue() == billsinfo[0].getFeeCode()) {
											receiptDetails.put("fee_description", feeCodeDescLookUp.getLookUpString2());
											//System.out.println(feeCodeDescLookUp.getLookUpString());
										}
									}
								}
								
								receiptDetails.put("transaction_amount",String.format("%,.2f", paymentinfo.getPaymentAmount()));
								receiptDetails.put("transaction_amount_in_word",PortalService.getConvertCurrencyToWord(paymentinfo.getPaymentAmount()));
								receiptDetails.put("transaction_desc", paymentinfo.getPaymentComment());
								receiptDetails.put("bill_no", paymentinfo.getBillInfos(0).getBillNumber());
								
								if(folder.getFolderType().equalsIgnoreCase("RCPT")){
									receiptDetails.put("paymentdate",PortalUtility.getDateByPattern(paymentinfo.getPaymentDate(), "dd-MM-yyyy"));
								}
							}
							
							mapFolderInfo = PortalService.getFolderInfoByFolderRSNInMap(paymentinfo.getFolderRSN());
							if (!mapFolderInfo.isEmpty()) {
								//receiptDetails.put("tax_office_name", PortalUtility.getString(TAX_OFFICE_MAP.get((PortalUtility.getString(mapFolderInfo.get(5090))))));
								
								final int TAX_OFFICE_INFO_CODE = 5090;
								String taxOfficeID = PortalUtility.getString(mapFolderInfo.get(5090));
								//taxOfficeDescription = PortalService.getInfoDescription(TAX_OFFICE_INFO_CODE, taxOfficeID);
								
								//receiptDetails.put("tax_office_name", taxOfficeDescription);
								receiptDetails.put("email_address", PortalUtility.getString(mapFolderInfo.get(5025)));
								//receiptDetails.put("assessment_period_from",PortalUtility.getString(mapFolderInfo.get(6085)));
								//receiptDetails.put("assessment_period_to",PortalUtility.getString(mapFolderInfo.get(6090)));
								receiptDetails.put("bank_branch", PortalUtility.getString(mapFolderInfo.get(6095)));
								receiptDetails.put("reference_no", PortalUtility.getString(mapFolderInfo.get(7000)));
								receiptDetails.put("payment_schedule_log_id",PortalUtility.getString(mapFolderInfo.get(7005)));
							}
						} else {
							dataFetched = false;
							tinCheck = false;
							receiptDetails.put("errorMsg", "Invalid TIN ! Please provide a Valid TIN");
							receiptDetails.put("tinCheck", tinCheck);
						}

					} else {
						dataFetched = false;
					}
				} else {
					dataFetched = false;
				}
			} catch (Exception ex) {
				dataFetched = false;
				isExceptionOccured = true;
				exceptionClassName = ex.getClass().getName();
				exceptionTrace = ex.getMessage();
				ex.printStackTrace();
			}
			
			receiptDetails.put("folderType", PortalUtility.getString( folder != null ? folder.getFolderType() : ""));
			receiptDetails.put("DataFetched", dataFetched);
			receiptDetails.put("tinCheck", tinCheck);
			receiptDetails.put("isExceptionOccured", isExceptionOccured);
			receiptDetails.put("exceptionClassName", exceptionClassName);
			receiptDetails.put("exceptionTrace", exceptionTrace);
			receiptDetails.put("receiptFolderType", receiptFolderType);
			
			log.info("Printing... receiptDetails >>>"+receiptDetails.toString());
			log.info("Printing... Exiting >>>"+pageName);
			
			response.getWriter().write(receiptDetails.toString());
		}else if(receiptFolderType.equals("FR") || receiptFolderType.equals("FRCT")){
			/* FOR GETTING FOREIGN RECEIPT SEARCH RESULT */
			JSONObject receiptDetailsForForeignBulk 	= new JSONObject();
			JSONObject receiptDetailsForForeignCredit 	= new JSONObject();
			
			//receiptnumber = PortalUtility.getString(request.getParameter("receipt_number"));
			
			String 	BULK_RECEIPT_NUMBER 	= "",
					BULK_DATE				= "",
					BULK_RECV_FROM 			= "",
					BULK_ADDRESS 			= "",
					BULK_TIN 				= "",
					BULK_CURRENCY_TYPE 		= "",
					BULK_TAX_TYPE 			= "",
					BULK_AMOUNT 			= "",
					BULK_AMOUNT_WORD 		= "",
					BULK_PERIOD_FROM 		= "",
					BULK_PERIOD_TO 			= "",
					BULK_BANK 				= "",
					BULK_TAX_OFFICE 		= "",
					BULK_PAYMENT_REF 		= "",
					BULK_PAYMENT_DATE 		= "",
					BULK_ISSUING_OFFICER 	= "",
					BULK_IRNO 				= "",
					BULK_TAX_OFFICER 		= "";
			
			String 	CREDIT_RECEIPT_NUMBER 		= "",
					CREDIT_DATE					= "",
					CREDIT_RECV_FROM 			= "",
					CREDIT_BENEFICIARY_TIN		= "",
					CREDIT_CURRENCY_TYPE 		= "",
					CREDIT_ADDRESS 				= "",
					CREDIT_PAYER 				= "",
					CREDIT_PAYER_TIN			= "",
					CREDIT_TAX_TYPE 			= "",
					CREDIT_AMOUNT 				= "",
					CREDIT_AMOUNT_WORD 			= "",
					CREDIT_CONTRACT_AMOUNT		= "",
					CREDIT_WHT_RATE				= "",
					CREDIT_CONTRACT_DESC		= "",
					CREDIT_PERIOD_FROM			= "",
					CREDIT_PERIOD_TO 			= "",
					CREDIT_BANK 				= "",
					CREDIT_TAX_OFFICE 			= "",
					CREDIT_PAYMENT_REF 			= "",
					CREDIT_PAYMENT_DATE 		= "",
					CREDIT_ISSUING_OFFICER 		= "",
					CREDIT_BULK_IRNO 			= "",
					CREDIT_SUPERVISING_OFFICER 	= "";
			
			String receiptType = "";
			
			/* CHEACK FOR BULK OR CREDIT FROM RECEIPT NUMBER */
			WsTransactionRequest[] transactionRequests2 = new WsTransactionRequest[1];
			transactionRequests2[0] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
			
			WsTransactionResponse[] transactionResponse2 = PortalService.getWSClient().executeCustomTransaction(lid, 133, transactionRequests2);

			if (!PortalUtility.isNull(transactionResponse2)) {
				receiptType = PortalUtility.getString(transactionResponse2[0].getColumnValues(0));
			}
			
			if(receiptType.equals("FR")){
				/* BULK RECEIPT REPORT PARAMETER RETRIEVAL */
				try{
					String FOLDER_TIN = "";
					WsTransactionRequest[] transactionRequests5 = new WsTransactionRequest[1];
					transactionRequests5[0] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
					
					WsTransactionResponse[] transactionResponse5 = PortalService.getWSClient().executeCustomTransaction(lid, 137, transactionRequests5);
					if (!PortalUtility.isNull(transactionResponse5)) {
						FOLDER_TIN = PortalUtility.getString(transactionResponse5[0].getColumnValues(0)).trim();
					}
					
					if(FOLDER_TIN.equals(TIN)){
						WsTransactionRequest[] transactionRequests3 = new WsTransactionRequest[1];
						transactionRequests3[0] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
						
						WsTransactionResponse[] transactionResponse3 = PortalService.getWSClient().executeCustomTransaction(lid, 134, transactionRequests3);
						
						if (!PortalUtility.isNull(transactionResponse3)) {
							BULK_PAYMENT_REF 	= PortalUtility.getString(transactionResponse3[0].getColumnValues(0));
							BULK_PAYMENT_REF 	= BULK_PAYMENT_REF.toUpperCase().equals("NULL") ? "Not available" : BULK_PAYMENT_REF;
							
							BULK_BANK 			= PortalUtility.getString(transactionResponse3[1].getColumnValues(0));
							BULK_BANK 			= BULK_BANK.toUpperCase().equals("NULL") ? "Not available" : BULK_BANK;
							
							BULK_TIN 			= PortalUtility.getString(transactionResponse3[2].getColumnValues(0));
							BULK_TIN 			= BULK_TIN.toUpperCase().equals("NULL") ? "Not available" : BULK_TIN;
							
							BULK_CURRENCY_TYPE 	= PortalUtility.getString(transactionResponse3[3].getColumnValues(0));
							BULK_CURRENCY_TYPE 	= BULK_CURRENCY_TYPE.toUpperCase().equals("NULL") ? "Not available" : BULK_CURRENCY_TYPE;
							
							BULK_TAX_TYPE 		= PortalUtility.getString(transactionResponse3[4].getColumnValues(0));
							BULK_TAX_TYPE 		= BULK_TAX_TYPE.toUpperCase().equals("NULL") ? "Not available" : BULK_TAX_TYPE;
							
							BULK_RECV_FROM 		= PortalUtility.getString(transactionResponse3[7].getColumnValues(0));
							BULK_RECV_FROM 		= BULK_RECV_FROM.toUpperCase().equals("NULL") ? "Not available" : BULK_RECV_FROM;
							
							BULK_TAX_OFFICE 	= PortalUtility.getString(transactionResponse3[8].getColumnValues(0));
							BULK_TAX_OFFICE 	= BULK_TAX_OFFICE.toUpperCase().equals("NULL") ? "Not available" : BULK_TAX_OFFICE;
							
							BULK_ADDRESS 		= PortalUtility.getString(transactionResponse3[9].getColumnValues(0));
							BULK_ADDRESS 		= BULK_ADDRESS.toUpperCase().equals("NULL") ? "Not available" : BULK_ADDRESS;
							
							BULK_PERIOD_FROM 	= PortalUtility.getString(transactionResponse3[10].getColumnValues(0));
							BULK_PERIOD_FROM 	= BULK_PERIOD_FROM.toUpperCase().equals("NULL") ? "Not available" : BULK_PERIOD_FROM;
							
							BULK_PERIOD_TO 		= PortalUtility.getString(transactionResponse3[11].getColumnValues(0));
							BULK_PERIOD_TO 		= BULK_PERIOD_TO.toUpperCase().equals("NULL") ? "Not available" : BULK_PERIOD_TO;
							
							BULK_PAYMENT_DATE 	= PortalUtility.getString(transactionResponse3[12].getColumnValues(0));
							BULK_PAYMENT_DATE 	= BULK_PAYMENT_DATE.toUpperCase().equals("NULL") ? "Not available" : BULK_PAYMENT_DATE;
							
							BULK_AMOUNT 		= PortalUtility.getString(transactionResponse3[16].getColumnValues(0));
							BULK_AMOUNT 		= BULK_AMOUNT.toUpperCase().equals("NULL") ? "Not available" : BULK_AMOUNT;
							
							/* PARSING FLOAT VALUE FROM STRING */
							float f_BULK_AMOUNT = 0;
							f_BULK_AMOUNT 		= Float.parseFloat(BULK_AMOUNT);
							BULK_AMOUNT_WORD 	= PortalService.getAmountInWordByCurrencyKey(f_BULK_AMOUNT,BULK_CURRENCY_TYPE);
						}
						
						receiptDetailsForForeignBulk.put("BULK_RECEIPT_NUMBER", receiptNumberInHexa);
						receiptDetailsForForeignBulk.put("BULK_PAYMENT_REF", BULK_PAYMENT_REF);
						receiptDetailsForForeignBulk.put("BULK_BANK", BULK_BANK);
						receiptDetailsForForeignBulk.put("BULK_TIN", BULK_TIN);
						receiptDetailsForForeignBulk.put("BULK_CURRENCY_TYPE", BULK_CURRENCY_TYPE);
						receiptDetailsForForeignBulk.put("BULK_TAX_TYPE", BULK_TAX_TYPE);
						receiptDetailsForForeignBulk.put("BULK_RECV_FROM", BULK_RECV_FROM);
						receiptDetailsForForeignBulk.put("BULK_TAX_OFFICE", BULK_TAX_OFFICE);
						receiptDetailsForForeignBulk.put("BULK_ADDRESS", BULK_ADDRESS);
						receiptDetailsForForeignBulk.put("BULK_PERIOD_FROM", BULK_PERIOD_FROM);
						receiptDetailsForForeignBulk.put("BULK_PERIOD_TO", BULK_PERIOD_TO);
						receiptDetailsForForeignBulk.put("BULK_PAYMENT_DATE", BULK_PAYMENT_DATE);
						receiptDetailsForForeignBulk.put("BULK_AMOUNT", BULK_AMOUNT);
						receiptDetailsForForeignBulk.put("BULK_AMOUNT_WORD", BULK_AMOUNT_WORD);
						receiptDetailsForForeignBulk.put("receiptFolderType", receiptFolderType);
						receiptDetailsForForeignBulk.put("receiptType", receiptType);
						
					}else{
						dataFetched = false;
						tinCheck = false;
						receiptDetailsForForeignBulk.put("errorMsg", "Invalid TIN ! Please provide a Valid TIN");
						receiptDetailsForForeignBulk.put("tinCheck", tinCheck);
					}
					
				}catch(Exception ex){
					dataFetched = false;
					isExceptionOccured = true;
					exceptionClassName = ex.getClass().getName();
					exceptionTrace = ex.getMessage();
					ex.printStackTrace();
				}
				
				log.info("Printing... receiptDetailsForForeignBulk >>>"+receiptDetailsForForeignBulk.toString());
				log.info("Printing... Exiting >>>"+pageName);
				
				response.getWriter().write(receiptDetailsForForeignBulk.toString());
			}else if(receiptType.equals("FRCT")){
				/* CREDIT RECEIPT REPORT PARAMETER RETRIEVAL */
				try{
					String FOLDER_TIN = "";
					WsTransactionRequest[] transactionRequests5 = new WsTransactionRequest[1];
					transactionRequests5[0] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
					
					WsTransactionResponse[] transactionResponse5 = PortalService.getWSClient().executeCustomTransaction(lid, 137, transactionRequests5);
					if (!PortalUtility.isNull(transactionResponse5)) {
						FOLDER_TIN = PortalUtility.getString(transactionResponse5[0].getColumnValues(0)).trim();
					}
					
					if(FOLDER_TIN.equals(TIN)){
						WsTransactionRequest[] transactionRequests4 = new WsTransactionRequest[1];
						transactionRequests4[0] = new WsTransactionRequest("argReceiptNumber",String.valueOf(receiptnumber));
						
						WsTransactionResponse[] transactionResponse4 = PortalService.getWSClient().executeCustomTransaction(lid, 135, transactionRequests4);
						
						if (!PortalUtility.isNull(transactionResponse4)) {
								CREDIT_RECEIPT_NUMBER 		= PortalUtility.getString(transactionResponse4[1].getColumnValues(0));
								CREDIT_RECEIPT_NUMBER 		= CREDIT_RECEIPT_NUMBER.toUpperCase().equals("NULL") ? "Not available" : CREDIT_RECEIPT_NUMBER;
								
								CREDIT_DATE					= PortalUtility.getString(transactionResponse4[0].getColumnValues(0));
								CREDIT_DATE 				= CREDIT_DATE.toUpperCase().equals("NULL") ? "Not available" : CREDIT_DATE;
								
								CREDIT_RECV_FROM 			= PortalUtility.getString(transactionResponse4[2].getColumnValues(0));
								CREDIT_RECV_FROM 			= CREDIT_RECV_FROM.toUpperCase().equals("NULL") ? "Not available" : CREDIT_RECV_FROM;
								
								CREDIT_BENEFICIARY_TIN		= PortalUtility.getString(transactionResponse4[3].getColumnValues(0));
								CREDIT_BENEFICIARY_TIN 		= CREDIT_BENEFICIARY_TIN.toUpperCase().equals("NULL") ? "Not available" : CREDIT_BENEFICIARY_TIN;
								
								CREDIT_ADDRESS 				= PortalUtility.getString(transactionResponse4[4].getColumnValues(0));
								CREDIT_ADDRESS 				= CREDIT_ADDRESS.toUpperCase().equals("NULL") ? "Not available" : CREDIT_ADDRESS;
								
								CREDIT_PAYER 				= PortalUtility.getString(transactionResponse4[5].getColumnValues(0));
								CREDIT_PAYER 				= CREDIT_PAYER.toUpperCase().equals("NULL") ? "Not available" : CREDIT_PAYER;
								
								CREDIT_PAYER_TIN			= PortalUtility.getString(transactionResponse4[6].getColumnValues(0));
								CREDIT_PAYER_TIN 			= CREDIT_PAYER_TIN.toUpperCase().equals("NULL") ? "Not available" : CREDIT_PAYER_TIN;
								
								CREDIT_TAX_TYPE 			= PortalUtility.getString(transactionResponse4[7].getColumnValues(0));
								CREDIT_TAX_TYPE 			= CREDIT_TAX_TYPE.toUpperCase().equals("NULL") ? "Not available" : CREDIT_TAX_TYPE;
								
								CREDIT_AMOUNT 				= PortalUtility.getString(transactionResponse4[8].getColumnValues(0));
								CREDIT_AMOUNT 				= CREDIT_AMOUNT.toUpperCase().equals("NULL") ? "Not available" : CREDIT_AMOUNT;
								
								CREDIT_CURRENCY_TYPE 		= PortalUtility.getString(transactionResponse4[18].getColumnValues(0));
								CREDIT_CURRENCY_TYPE 		= CREDIT_CURRENCY_TYPE.toUpperCase().equals("NULL") ? "Not available" : CREDIT_CURRENCY_TYPE;
								
								/* PARSING FLOAT VALUE FROM STRING */
								float f_CREDIT_AMOUNT 	= 0;
								f_CREDIT_AMOUNT 		= Float.parseFloat(CREDIT_AMOUNT);
								CREDIT_AMOUNT_WORD 		= PortalService.getAmountInWordByCurrencyKey(f_CREDIT_AMOUNT,CREDIT_CURRENCY_TYPE);
										
								CREDIT_CONTRACT_AMOUNT		= PortalUtility.getString(transactionResponse4[9].getColumnValues(0));
								CREDIT_CONTRACT_AMOUNT 		= CREDIT_CONTRACT_AMOUNT.toUpperCase().equals("NULL") ? "Not available" : CREDIT_CONTRACT_AMOUNT;
								
								CREDIT_WHT_RATE				= PortalUtility.getString(transactionResponse4[10].getColumnValues(0));
								CREDIT_WHT_RATE 			= CREDIT_WHT_RATE.toUpperCase().equals("NULL") ? "Not available" : CREDIT_WHT_RATE;
								
								CREDIT_CONTRACT_DESC		= PortalUtility.getString(transactionResponse4[11].getColumnValues(0));
								CREDIT_CONTRACT_DESC 		= CREDIT_CONTRACT_DESC.toUpperCase().equals("NULL") ? "Not available" : CREDIT_CONTRACT_DESC;
								
								CREDIT_PERIOD_FROM			= PortalUtility.getString(transactionResponse4[12].getColumnValues(0));
								CREDIT_PERIOD_FROM 			= CREDIT_PERIOD_FROM.toUpperCase().equals("NULL") ? "Not available" : CREDIT_PERIOD_FROM;
								
								CREDIT_PERIOD_TO 			= PortalUtility.getString(transactionResponse4[13].getColumnValues(0));
								CREDIT_PERIOD_TO 			= CREDIT_PERIOD_TO.toUpperCase().equals("NULL") ? "Not available" : CREDIT_PERIOD_TO;
								
								CREDIT_BANK 				= PortalUtility.getString(transactionResponse4[14].getColumnValues(0));
								CREDIT_BANK 				= CREDIT_BANK.toUpperCase().equals("NULL") ? "Not available" : CREDIT_BANK;
								
								CREDIT_TAX_OFFICE 			= PortalUtility.getString(transactionResponse4[15].getColumnValues(0));
								CREDIT_TAX_OFFICE 			= CREDIT_TAX_OFFICE.toUpperCase().equals("NULL") ? "Not available" : CREDIT_TAX_OFFICE;
								
								CREDIT_PAYMENT_REF 			= PortalUtility.getString(transactionResponse4[16].getColumnValues(0));
								CREDIT_PAYMENT_REF 			= CREDIT_PAYMENT_REF.toUpperCase().equals("NULL") ? "Not available" : CREDIT_PAYMENT_REF;
								
								CREDIT_PAYMENT_DATE 		= PortalUtility.getString(transactionResponse4[17].getColumnValues(0));
								CREDIT_PAYMENT_DATE 		= CREDIT_PAYMENT_DATE.toUpperCase().equals("NULL") ? "Not available" : CREDIT_PAYMENT_DATE;
								
								CREDIT_ISSUING_OFFICER 		= "";
								CREDIT_BULK_IRNO 			= "";
								CREDIT_SUPERVISING_OFFICER 	= "";
						}
						
						receiptDetailsForForeignCredit.put("CREDIT_RECEIPT_NUMBER", receiptNumberInHexa);
						receiptDetailsForForeignCredit.put("CREDIT_DATE", CREDIT_DATE);
						receiptDetailsForForeignCredit.put("CREDIT_RECV_FROM", CREDIT_RECV_FROM);
						receiptDetailsForForeignCredit.put("CREDIT_BENEFICIARY_TIN", CREDIT_BENEFICIARY_TIN);
						receiptDetailsForForeignCredit.put("CREDIT_ADDRESS", CREDIT_ADDRESS);
						receiptDetailsForForeignCredit.put("CREDIT_PAYER", CREDIT_PAYER);
						receiptDetailsForForeignCredit.put("CREDIT_PAYER_TIN", CREDIT_PAYER_TIN);
						receiptDetailsForForeignCredit.put("CREDIT_TAX_TYPE", CREDIT_TAX_TYPE);
						receiptDetailsForForeignCredit.put("CREDIT_AMOUNT", CREDIT_AMOUNT);
						receiptDetailsForForeignCredit.put("CREDIT_AMOUNT_WORD", CREDIT_AMOUNT_WORD);
						receiptDetailsForForeignCredit.put("CREDIT_CONTRACT_AMOUNT", CREDIT_CONTRACT_AMOUNT);
						receiptDetailsForForeignCredit.put("CREDIT_WHT_RATE", CREDIT_WHT_RATE);
						receiptDetailsForForeignCredit.put("CREDIT_CONTRACT_DESC", CREDIT_CONTRACT_DESC);
						receiptDetailsForForeignCredit.put("CREDIT_PERIOD_FROM", CREDIT_PERIOD_FROM);
						receiptDetailsForForeignCredit.put("CREDIT_PERIOD_TO", CREDIT_PERIOD_TO);
						receiptDetailsForForeignCredit.put("CREDIT_BANK", CREDIT_BANK);
						receiptDetailsForForeignCredit.put("CREDIT_TAX_OFFICE", CREDIT_TAX_OFFICE);
						receiptDetailsForForeignCredit.put("CREDIT_PAYMENT_REF", CREDIT_PAYMENT_REF);
						receiptDetailsForForeignCredit.put("CREDIT_PAYMENT_DATE", CREDIT_PAYMENT_DATE);
						receiptDetailsForForeignCredit.put("CREDIT_ISSUING_OFFICER", CREDIT_ISSUING_OFFICER);
						receiptDetailsForForeignCredit.put("CREDIT_CURRENCY_TYPE", CREDIT_CURRENCY_TYPE);
						receiptDetailsForForeignCredit.put("CREDIT_BULK_IRNO", CREDIT_BULK_IRNO);
						receiptDetailsForForeignCredit.put("CREDIT_SUPERVISING_OFFICER", CREDIT_SUPERVISING_OFFICER);
						receiptDetailsForForeignCredit.put("receiptFolderType", receiptFolderType);
						receiptDetailsForForeignCredit.put("receiptType", receiptType);
					}else{
						dataFetched = false;
						tinCheck = false;
						receiptDetailsForForeignCredit.put("errorMsg", "Invalid TIN ! Please provide a Valid TIN");
						receiptDetailsForForeignCredit.put("tinCheck", tinCheck);
					}
					
				}catch(Exception ex){
					dataFetched = false;
					isExceptionOccured = true;
					exceptionClassName = ex.getClass().getName();
					exceptionTrace = ex.getMessage();
					ex.printStackTrace();
				}
				
				log.info("Printing... receiptDetailsForForeignCredit >>>"+receiptDetailsForForeignCredit.toString());
				log.info("Printing... Exiting >>>"+pageName);
				response.getWriter().write(receiptDetailsForForeignCredit.toString());
			}
		}
	}
	
%>