<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@include file="include/includeconstant.jsp"%>
<%!
	private final int TAX_OFFICE_INFOCODE = 5090;
	List<String> newAddressDropdown = new ArrayList<String>();
%>	
<%
	/*Prepairing New Address Dropdown*/
	WsValidInfoValue[] infoValues=null;
	infoValues = PortalService.getWSClient().getValidInfoValues(lid, TAX_OFFICE_INFOCODE);
	if(infoValues != null && infoValues.length>0){
		for(WsValidInfoValue infoValue:infoValues){
			String dropdownItem = infoValue.getInfoValue();
			
			newAddressDropdown.add(dropdownItem);
		}
	}
	/*Prepairing Year Array*/
	int maxYears = 4;
	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	String yearsArr[] = new String[maxYears];
	for(int i=0;i<maxYears;i++){
		yearsArr[i]=new Integer(currentYear--).toString();
	}
	
	WsPeople people = (WsPeople) session.getAttribute("people");
	String TIN = "";
	if(!PortalUtility.isNull(people)){
		TIN	= PortalUtility.getString(people.getLicenceNumber());
	}
%>
	<div>
	<div class="container-fluid">
	  <h4 align="center">Application for Tax Clearance Certificate</h4>
	  <!-- <ol class="breadcrumb hidden-xs">
	    <li id="crumb1" class="active">Enter Registration Details</li>
	    <li id="crumb2">Complete Registration</a></li>
	  </ol> -->
	</div>
	
	<div class="container-fluid" id="tccapp_form">
		<div style="margin-bottom:30px;">
			<p class="text-danger">
				<sup class="text-danger">*</sup><sup class="text-danger">*</sup>For
				any mandatory field like amount kindly put 0.00 in case there is no
				information available.
			</p>
		</div>
		<div style="padding:0px 20px 0px 20px;" class="panel panel-default">
			<div class="row hide" id="applyFormValidationAlert">
				<div class="col-xs-12">
					<br/>
					<p class="alert alert-warning"><i class="glyphicon glyphicon-alert"></i>&nbsp;&nbsp;<span id="formPageAlert">Please fill the mandatory fields!</span></p>
					<p class="well" id="missingMandatoryItemsOuter" style="padding:5px 10px;" >
						<span style="margin-right:10px;">Missing Mandatory Items: </span><span id="missingInputsContainer"></span>
					</p>
				</div>
			</div>
<%-- 			<h5>1.Particular of Application<br><br></h5>
			
			
			<div class="">
				<div class="row custom_row">
					<div class="form-group">
			    		<label class="col-md-3 col-lg-3 custom_label" for="registration_type">Registration for:<sup class="text-danger">*</sup></label>
			    		<div class="col-md-9 col-lg-9">
			    			<label class="col-md-3 col-lg-3 custom_label" for="registration_type_company">
			    				<input type="radio" selected value="company" aria-role="Registration Type" checked id="registration_type_company" name="registration_type">
				    			&nbsp &nbsp&nbsp Company
				    		</label>
				    		<label class="col-md-3 col-lg-3 custom_label" for="registration_type_individual">
				    			<input type="radio" value="individual" aria-role="Registration Type"  id="registration_type_individual" name="registration_type">
				    			&nbsp &nbsp&nbsp Individual
				    		</label>
				    		<div class="error" id=""></div>
			    		</div>
		 			</div>
	 			</div>
	 			<input type="hide" name="reg_name" class="hide" style="position:fixed;top:-1000px;" id="reg_name"/>
	    		<div class="row custom_row">
					<div class="form-group">
			    		<label class="col-md-3 col-lg-3 col-xs-12 custom_label" for="tax_id">Tax Identification No<sup class="text-danger">*</sup></label>
			    		<div class="col-md-4 col-lg-4 col-xs-6">
				    			<input type="text" class="form-control input-sm custom_input" aria-role="Tax Identification Number" title="Please input numbers and/or [-]. This is a mandatory field." id="tax_id" pattern="TIN" data-required="required" maxlength="20" >
				    			<div class="error" id=""></div>
			    		</div>
			    		<div class="col-md-4 col-lg-4 col-xs-6">
			    			<button class="btn btn-primary custom_btn" style="margin-top:-2px;" data-toggle="tooltip" id="searchInfoByTIN"><i style="margin-right:4px; font-size:12px;" class="glyphicon glyphicon-search"></i><span id="searchInfoByTINText" class="text-content">Search</span></button>
			    			<button class="btn btn-primary custom_btn hide" data-toggle="tooltip" id="clearTINInfo"><span class="text-content">Reset</span></button>
			    		</div>
		 			</div>
	 			</div>
 				<div class="text-center" id="entity_alert_message">
 					<div class="alert alert-info" >
 						<p>Please use the "Search" button above to retrieve your information based on your Tax Identification Number.</p>
 					</div>
 				</div>
 				<div class="row text-center hide" id="entity_error_message">
 					<div class="alert alert-warning col-md-10 col-xs-12 col-md-offset-1">
 						<p>No results found. Please try again.</p>
 					</div>
 				</div>
 				<div class="row hide" id="entity_personal_info">
 					<div class="col-xs-12">
 						<div class="jumbotron custom-jumbotron">
 							<h5>Tax Identification Number: <span class="text-success" id="tccPreview_TIN"></span></h5>
 						</div>
 					</div>
 					<div class="col-xs-12 col-md-6">
 						<div class="jumbotron custom-jumbotron" style="min-height:305px;">
	 						<h4 class="text-info">Existing Tax Information</h4>
	 						<hr/>
	 						<p>
		 						<label>
		 							<span class="margin-right-small">Name of <span id="entity_preview_type">Company</span></span>:
		 							<span><b><span class="small" id="tccPreview_name"></span></b></span>
		 						</label>
		 						<br/>
	 						</p>
	 						<p>
	 						<p>
	 							<label>
		 							<span class="margin-right-small"><b>Email: </b></span>
		 							<span id="tccPreview_email" class="small"></span>
		 						</label>
	 						</p>
	 						<p>
	 							<label>
		 							<span class="margin-right-small"><b>Mobile: </b></span>
		 							<span id="tccPreview_mobile" class="small"></span>
		 						</label>
	 						</p>
	 						<p>
	 							<label>
		 							<span class="margin-right-small"><b>Tax Office Name: </b></span>
		 							<span id="tccPreview_taxOfficeName" class="small"></span>
		 						</label>
	 						</p>
	 						<p>
	 							<label>
		 							<span class="margin-right-small"><b>Address: </b></span>
		 							<span id="tccPreview_address" class="small"></span>
		 						</label>
	 						</p>
 						</div>
 					</div>
 					<div class="col-xs-12 col-md-6">
 						<div class="jumbotron custom-jumbotron">
	 						<h4 class="text-info">Update Tax Information</h4>
	 						<hr/>
	 						<div class="row">
	 							<div class="col-md-6 col-xs-12 form-group errorplace">
	 								<label>E-mail</label>
	 								<input type="text" class="form-control input-sm custom_input" aria-role="Email" id="tccUpdate_email" pattern="email" >
					    			<div class="error" id="" style="top:0px;"></div>
	 							</div>
	 							<div class="col-md-6 col-xs-12 form-group errorplace">
	 								<label>Mobile</label>
	 								<input type="text" class="form-control input-sm custom_input" aria-role="Phone Number" placeholder="Enter your Phone Number" id="tccUpdate_phone" onkeyup="phoneMask(this.id);" value="" onblur="validPhoneMask(this.id);" maxlength="21" >
	 								<div class="error" id="" style="top:0px;"></div>
	 							</div>
	 						</div>
	 						<div class="row">
	 							<div class="col-md-6 col-xs-12 form-group errorplace">
	 								<label>Tax Office Name</label>
	 								<select class="form-control input-sm custom_input" id="tccUpdate_taxOfficeName">
	 									<% 
	 									if(newAddressDropdown != null && newAddressDropdown.size()>0){
	 									for(String dropdownItem:newAddressDropdown)
	 									 {
	 									%>
	 									<option><%=dropdownItem%></option>
	 									<%
	 										}
	 									}else{
	 										%>	
	 										<option default></option>
	 									<%}%>
	 								</select>
	 								<div class="error" id=""></div>
	 							</div>
	 						</div>
	 						<div class="row">
	 							<div class="col-xs-12 form-group errorplace">
	 								<label>Address</label>
	 								<input class="form-control input-sm custom_input" aria-role="Address" placeholder="Please provide an address" 
	 									pattern="address" maxlength="150" id="tcc_updateAddress"/>
	 								<div class="error" id=""></div>
	 							</div>
	 						</div>
 						</div>
 					</div>
	 			</div>
			</div> --%>
			<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="trade_nature">1.Nature of Trade/Business<sup class="text-danger">*</sup></label>
		    		<div class="col-md-9 col-lg-9">
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Nature of Trade/Business" id="trade_nature" maxlength="150" data-required="required"
			    		 name="currency" class="biginput">
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		<br>
	 		<span style="font-weight:200;">2.Turnover of the company in last 3 years in N<sup class="text-danger">*</sup></span><br><br>
	 		
	 		<div class="row custom_row">
				<div class="form-group" style="margin-bottom:0px">
		    		<label class="col-md-1 col-lg-1 custom_label" for="turnover-year-1"><%=yearsArr[1]%><sup class="text-danger">*</sup></label>
		    		<div class="col-md-3 col-lg-3"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company-1" id="turnover-year-1" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 			<div class="form-group" style="margin-bottom:0px">
		    		<label class="col-md-1 col-lg-1 custom_label" for="turnover-year-2"><%=yearsArr[2]%><sup class="text-danger">*</sup></label>
		    		<div class="col-md-3 col-lg-3"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company-2" id="turnover-year-2" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 			<div class="form-group" style="margin-bottom:0px">
		    		<label class="col-md-1 col-lg-1 custom_label" for="turnover-year-3"><%=yearsArr[3]%><sup class="text-danger">*</sup></label>
		    		<div class="col-md-3 col-lg-3"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company-3" id="turnover-year-3" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		<div class="row custom_row hide">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="">2.Turnover of the company in the latest tax return file with the Revenue N<sup class="text-danger">*</sup></label>
		    		<div class="col-md-9 col-lg-9"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company.." id="" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		<br>
			<span style="font-weight:200;">3.Particular of previous Tax Clearance Certificate (if Any):</span><br><br>
			<!-- <div class="alert alert-info" id="verifyTCCPrerequisiteAlert">Please verify your TIN to enable TCC verification.</div> -->
			<div class="row custom_row" id="verifyTCCContainer">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="oldtcc_num">Number:</label>
		    		<div class="col-md-4 col-lg-4">
		    			<input type="text" class="form-control input-sm custom_input input-with-btn" aria-role="Particular of previous Tax clearanace" style="float:left;padding-right:25px;" id="oldtcc_num" maxlength="20" pattern="alphanumeric&specialWithSpacesLG">
						<span id="verifyTCCLoader" class="hide inline-loader"><img src="images/loader.gif" alt="" height="20px" width="20px" style="margin-top:5px;"></span>
						<button class="btn btn-primary custom_btn input-addon-btn" id="verifyTCCBtn"><i style="margin-right:4px; font-size:12px;" class="glyphicon glyphicon-search"></i><span id="verifyTCCText" class="text-content">Verify</span></button>	    			
		    			<div class="error" id=""></div>
		    		</div>
		    		<label class="col-md-2 col-lg-2 custom_label" for="tcc_date">Date:</label>
		    		<div class="col-md-3 col-lg-3">
		    			<input type="text" class="form-control input-sm custom_input" disabled="disabled" aria-role="Date" id="tcc_date" pattern="alphanumeric&specials">
		    			<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		<br>
	 		<span style="font-weight:200;">4.Has the Company Paid all assessments raised up to the immediate previous Year of Assessment.<sup class="text-danger">*</span></sup><br>
			<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="pre_year_check"><input type="radio" aria-role="Previous year/s assessment payment status" name="pre_year_check" id="pre_year_check" value="true" checked="checked">
		    		&nbsp &nbsp&nbsp Yes
		    		</label>
		    		<label class="col-md-3 col-lg-3 custom_label" for="pre_year_uncheck"><input type="radio" aria-role="Previous year/s assessment payment status" name="pre_year_check" id="pre_year_uncheck" value="false">
		    		&nbsp &nbsp&nbsp No
		    		</label>
	 			</div>
	 		</div>
	 		<br>
	 		<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="year">5.If 'No' state the amount outstanding and the Year(s) affected:<sup class="text-danger">*</sup></label>
		    		<div class="col-md-4 col-lg-4">
		    			<input type="text" class="form-control input-sm custom_input" aria-role="Year for amount outstanding" id="year" pattern="alphanumeric&specialWithSpacesLG" disabled="disabled">
		    			<div class="error" id=""></div>
		    		</div>
		    		<label class="col-md-2 col-lg-2 custom_label" for="amount">Amount:</label>
		    		<div class="col-md-3 col-lg-3">
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Amount outstanding" onkeypress="return onlyNosAndDecimal(event,this)" id="amount" pattern="numbers" disabled="disabled">
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		
	 		<span style="font-weight:200;">6.Any Special arrangement made with the revenue to liquidate the amount outstanding?<sup class="text-danger">*</sup></span>
	 		<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="amount_out_check"><input type="radio" value="true" aria-role="Special Arrangements for outstanding amount" id="amount_out_check" name="amount_check">
		    		&nbsp &nbsp&nbsp Yes
		    		</label>
		    		<label class="col-md-3 col-lg-3 custom_label" for="amount_out_uncheck"><input type="radio" value="false" aria-role="Special Arrangements for outstanding amount" id="amount_out_uncheck" name="amount_check" checked="checked">
		    		&nbsp &nbsp&nbsp No
		    		</label>
	 			</div>
	 		</div>
	 		
	 		<div class="hide" id="spc_arg" style="border:1px solid black;margin-top:25px;padding:10px 10px 10px 10px;">
		    	<div class="row custom_row">
					<div class="form-group">
			    		<label class="col-md-3 col-lg-3 custom_label" for="arrange_type">Type of Arrangements<sup class="text-danger">*</sup></label>
			    		<div class="col-md-4 col-lg-4">
				    		<input type="text" class="form-control input-sm custom_input" aria-role="Type of arrangement" id="arrange_type" pattern="alphanumeric&specialWithSpacesLG" >
				    		<div class="error" id=""></div>
			    		</div>
		 			</div>
	 			</div>
		    	
		    	<div class="row custom_row">
					<div class="form-group">
			    		<label class="col-md-3 col-lg-3 custom_label" for="support_copy">Support Copy approval letter<sup class="text-danger">*</sup></label>
			    		<div class="col-md-6 col-lg-6">
				    		<b><span id="support_copy_preview" class="text-info" style="margin-right:5px;"></span></b>
				    		<button type="button" onclick="$('#support_copy').trigger('click');" id="support_copy_trigger" class="btn custom_btn btn-primary">Browse</button>
				    		<input type="file" class="hide" style="position:fixed;top:-1000px;" aria-role="Support Copy of approval" accept="image/jpeg, image/gif,application/pdf" id="support_copy">
				    		<div class="error" id="" style="margin-top:2px;"></div>
			    		</div>
			    		<div class="col-md-4 col-lg-4"></div>
		 			</div>
	 			</div>
	 			
	    	</div>
	    	<br>
	    	
	    	<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="other_doc">7.Any Other Relevant Information</label>
		    		<div class="col-md-9 col-lg-9">
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Other relevant Information" id="other_doc" pattern="AddressLG" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		
	 		<div class="">
				<label class="checkbox-inline custom_label" for="confirm_form"><input type="checkbox" id="confirm_form">
				I certify that the information given above is correct in respect and confirm that to the best
				of my knowledge and belief, there are no other facts the omission of which would be
				misleading.

				</label>
				<div class="text-center" style="margin-bottom:10px;">
					<span id="formButtonContainer">
						<button class="btn btn-default custom_btn" id="submit_form" disabled="disabled" TIN=<%=TIN %>>Submit</button>&nbsp&nbsp
						<button class="btn btn-default custom_btn" id="form_reset" onclick="location.reload();">Cancel</button>&nbsp&nbsp&nbsp
					</span>	
					<div class="loader-container hide" id="loader">
						<p class="alert alert-success">Please wait, Submitting form data.. <img src="images/loader.gif" alt="" height="20px" width="20px"></p>
					</div>
				</div>
				<div class="hide text-danger text-center" style="margin:5px 0px;font-style:italic" id="applyTCCFormError">
				</div>
			</div>
 		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default hide" id="ConfirmationResponseStatus">
			<div class="" style="padding: 60px 0 60px 0;">
					<h5 class="text-center"><b>THANK YOU!</b></h5>
					<h5 class="text-center">TCC application has been submitted successfully.</h5>
					<p class="text-center">Your application no. is <strong> <span id="successApplicationNumberDisplay"></span></strong></p>
					<div class="text-center">
						<%-- <b><a class="text-info" href="<%=request.getContextPath()%>">Home Page</a></b> --%>
					</div>
			</div>
		</div>
	</div>
</div>