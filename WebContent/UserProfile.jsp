<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Map"%>
<%@include file="include/includeconstant.jsp"%>
<%@include file="include/common/includeCss.jsp"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%!
	
%>
<%
	if(PortalUtility.isNull(request.getSession(false).getAttribute("people"))){
		response.sendRedirect("logoff.jsp");
		return;
	}else{
		WsPeople people = (WsPeople)request.getSession(false).getAttribute("people");
		if(PortalUtility.getInt(people.getPeopleCode()) != 3 && false){
			response.sendRedirect("logoff.jsp");
			return;
		}else{
			System.out.println(new Date().toString()+"User Logged in>>> "+String.valueOf(people.getPeopleRSN()));
		}
	}

    boolean applicationFlag = true;
    
    String[] monthArray = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
    String[] yearArray = PortalService.getYearArray(10);
%>
	
<!DOCTYPE html>
<html style="position: relative;min-height: 100%;">
  <head>
    <meta charset="utf-8">
    <title>FIRS | Welcome User</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<%=CSS_PORTAL%>Custom.css" async>
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="dist/js/site.min.js"></script>
    <%@include file="include/common/includeJs.jsp"%>
    
    <style>
		.list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover{
			background-color: #E6E9ED;
	    	border-color: #E6E9ED;
		}
	</style>

  </head>
  <body style="margin-bottom: 15px!important;">
  
  	<!-- Nav Bar -->
    <jsp:include page="includeNavBar.jsp" />
    
    
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><i class="glyphicon glyphicon-align-justify"></i> <b>User DashBoard</b></li>
                <li class="list-group-item active"><a href="javascript:" data-target="tcc_profile_details"><i class="glyphicon glyphicon-user"></i>Profile Details</a></li>
                <li class="list-group-item hide"><a href="javascript:" data-target="apply_tcc"><i class="glyphicon glyphicon-file"></i>Apply for TCC</a></li>
                <li class="list-group-item hide"><a href="javascript:" data-target="download_tcc"><i class="glyphicon glyphicon-download-alt"></i>Download TCC</a></li>
                <li class="list-group-item"><a href="javascript:" data-target="search_receipt"><i class="fa fa-search" aria-hidden="true"></i>Search Receipt</a></li>
             </ul>
          </div>
          
           <!-- Profile -->
		   <jsp:include page="UserProfileDetails.jsp" />
			
           <div class="col-xs-12 col-sm-9 content showHideDiv hide" id="apply_tcc">
            
            <%
            WsPeople people = (WsPeople)session.getAttribute("people");
		 	%>	
		  	<div class="panel panel-default">
              <div class="panel-heading">
				<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a>Apply for TCC
				</h3>
			 </div>
             <div class="panel-body">
                 <div class="content-row">
                  <div class="container-fluid">
					<div class="container-fluid" id="application-review-exist">
		        		<div class="panel panel-default">
			        		<div class="col-lg-12g text-center panel-body" style="padding:40px 20px 40px 20px;margin-top:20px;">
				        		<p class=""><strong><u>Application has been submitted Sucessfully!</u></strong></p>
				        		<p class="">Application Number:<span id="application-number">&nbsp&nbsp&nbsp&nbsp<strong></strong></span></p>
				        		<p class="">Application Status:<span id="application-number">&nbsp&nbsp<strong></strong></span></p>
			        		</div>
		        		</div>
          			</div>
				  </div>
                </div>
              </div>
             </div>
			<%%>
			
            <div class="panel panel-default">
              <div class="panel-heading">
				<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a>Apply for TCC
				</h3>
			 </div>
             <div class="panel-body">
                 <div class="content-row">
                  <div class="container-fluid">
					<jsp:include page="TccApplicationForm.jsp" />
				  </div>
                </div>
              </div>
             </div>
             <%%>
            </div>
            <div class="col-xs-12 col-sm-9 col-lg-10 content showHideDiv hide" id="download_tcc">
            	<div class="panel panel-default">
             	 <div class="panel-heading">
					 <h3 class="panel-title">
					 <a href="javascript:void(0);" class="toggle-sidebar"><span
						class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
						title="Maximize Panel"></span></a>Download TCC
					</h3>
			    </div>
                <div class="panel-body">
                  <div class="content-row row">
                   <div class="col-lg-12">
					 <jsp:include page="DownloadTCC.jsp" />
					</div>
                  </div>
                </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-lg-10 content showHideDiv hide" id="search_receipt">
            	<div class="panel panel-default">
             	 <div class="panel-heading">
					 <h3 class="panel-title">
					 <a href="javascript:void(0);" class="toggle-sidebar"><span
						class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
						title="Maximize Panel"></span></a>Search Receipt
					</h3>
			    </div>
                <div class="panel-body">
                  <div class="content-row row">
                   	<div class="col-lg-12">
                   		<p style="color: #303641;font-size: 12px;">Search Receipts here by Providing below Inputs</p>
					</div>
                  </div>
                  <div class="row content-row">
                  	<div class="col-lg-12">
                  		<p style="font-size: 12px;">- Please choose type here<sup class="text-danger">*</sup></p>
                  		<label class="radio-inline">
                  			<input type="radio" name="corporate-receipt-type-key" checked="checked" value="RCPT">Payment
                  		</label>
						<label class="radio-inline">
							<input type="radio" name="corporate-receipt-type-key" value="RCTI">Credit Note
						</label>
                  	</div>
                  </div>
                  <div class="row content-row">
                  	<div class="col-lg-12">
                  		<p style="font-size: 12px;">- Please choose Monthly / Daily here<sup class="text-danger">*</sup></p>
                  		<label class="radio-inline">
                  			<input type="radio" name="corporate-month-year-key" checked="checked" value="Monthly" onclick="handleClick(this);">Monthly
                  		</label>
						<label class="radio-inline">
							<input type="radio" name="corporate-month-year-key" value="Daily" onclick="handleClick(this);">Daily
						</label>
                  	</div>
                  </div>
                  <div class="row content-row">
                  	<div class="col-lg-7 col-sm-7">
                  		<div class="row" id="receiptInputContainer">
                  			<div class="col-lg-1 col-sm-1">
                  				<p style="line-height: 26px;">Year<sup class="text-danger">*</sup></p>
                  			</div>
                  			<div class="col-lg-3 col-sm-3">
                  				<select class="form-control input-sm custom_input" id="corporateYearInput" style="font-size: 11px;height: 28px;">
                  					<option value="" selected>Year</option>
                  					<%for(int i = 0; i < yearArray.length; i++) {%>
                  						<option value="<%=yearArray[i]%>"><%=yearArray[i]%></option>
                  					<%} %>
                  				</select>
                  			</div>
                  			<div class="col-lg-1 col-sm-1">
                  				<p style="line-height: 26px;">Month<sup class="text-danger">*</sup></p>
                  			</div>
                  			<div class="col-lg-3 col-sm-3">
                  				<select class="form-control input-sm custom_input" id="corporateMonthInput" style="font-size: 11px;height: 28px;">
                  					<option value="" selected>Month</option>
                  					<%for(int i = 0; i < monthArray.length; i++) {%>
                  						<option value="<%=monthArray[i]%>"><%=monthArray[i]%></option>
                  					<%} %>
                  				</select>
                  			</div>
                  			<div class="col-lg-1 col-sm-1">
                  				<p style="line-height: 26px;">Day<sup class="text-danger">*</sup></p>
                  			</div>
                  			<div class="col-lg-2 col-sm-2">
                  				<select class="form-control input-sm custom_input" id="corporateDayInput" style="font-size: 11px;height: 28px;" disabled="disabled">
                  					<option value="" selected>Day</option>
                  				</select>
                  			</div>
                  		</div>
                  	</div>
                  	<div class="col-lg-5 col-sm-5">
                  		<div class="row">
                  			<div class="col-lg-12 text-center">
                  				<button class="btn btn-default custom_btn"  id="searchCorporateReceipt" style="font-size:12px;">
                  					Search&nbsp;<i class="fa fa-search" aria-hidden="true"></i>
                  				</button>
			 					<button type="button" id="resetReceiptInputs"  class="btn btn-default custom_btn" style="font-size:12px;">
			 						Reset&nbsp;<i class="fa fa-times" aria-hidden="true"></i>
			 					</button>
                  			</div>
                  		</div>
                  	</div>
                  	<div class="row content-row hide" id="receiptSearchLoader">
                  		<div class="col-lg-12 pull-left">
                  			<span class="" style="font-size: 13px;color: #7990ae;font-weight: 600">Please Wait Searching. . .</span>
                  			<img class="img-responsive" src="images/loader-hor.gif" style="height: 50px;width: 200px;display:inline-block;">
                  		</div>
                  	</div>
                  	<div class="col-lg-offset-1 col-lg-10 col-sm-12 hide" id="corporateReceiptSearchResultContainer">
                  		<p style="color: #303641;font-size: 12px;display: inline-block;padding: 5px 10px 5px 10px;margin-left: -15px;background-color: #e6e9ed;">
                  			Search Result : 
                  		</p>
                  		<div class="row" style="margin-top:20px;">
                  			<div class="table-responsive">          
							  <table class="table table-hover table-bordered table-striped" id="receiptSearchTable" style="font-size: 11px;font-family: sans-serif;">
							    <thead style="background-color: #2c3e50; color: #ffffff96;">
							      <tr>
							     	<th>S/N</th>
							        <th>Payment Reference</th>
							        <th>Payment Amount</th>
							        <th>Payment Date</th>
							        <th>Download</th>
							      </tr>
							    </thead>
							    <tbody align="center">
							      
							    </tbody>
							  </table>
							</div>
                  		</div>
                  	</div>
                  </div>
                </div>
               </div>
            </div>
         </div><!-- panel body -->
       </div>
	   
	   <!--footer-->
	   <jsp:include page="includeFooter.jsp" />
		
    <script>
    $(document).ready(function(){
		var applicationFlag = localStorage.getItem('applicationFlag');
		if(applicationFlag){
			$.each($(".list-group li a"), function(obj){
				if($(this).attr("data-target") == "apply_tcc"){
					$(".list-group li").removeClass("active");
					$(this).parent("li").addClass("active");
					var curDiv = $(this).attr("data-target");
					$(".showHideDiv").hide();
					$(".showHideDiv").removeClass("hide");
					$("#"+curDiv).show();
				}
			})
			localStorage.removeItem('applicationFlag');
		}
		$(".list-group li a").on("click", function(){
			$(".list-group li").removeClass("active");
			$(this).parent("li").addClass("active");
			var curDiv = $(this).attr("data-target");
			
			if(curDiv == "apply_tcc" && applicationFlag){
				window.location.reload();
				return false;
			}
			
			$(".showHideDiv").hide();
			$(".showHideDiv").removeClass("hide");
			$("#"+curDiv).show();
		})
	});
	</script>
	<script>
		$(document).ready(function(){
			/* Getting Days on chnage of Month & Year */
			$("#corporateYearInput").on("change",function(){
				$year = $("#corporateYearInput");
				$month = $("#corporateMonthInput");
				$day = $("#corporateDayInput");
				
				debugger;
				$month.val("");
				
				$day.html("");
				$day.append("<option value='' selected>Day</option>");
				
			});
			
			$("#corporateMonthInput").on("change",function(){
				$year = $("#corporateYearInput");
				$month = $("#corporateMonthInput");
				$day = $("#corporateDayInput");
				
				clearDayInput($day);
				
				if($year.val() == ""){
					return;
				}
				
				var numbersOfDay = getNumbersOfDays($year.val(),$month.val());
				
				for(var i = 1; i <= numbersOfDay; i++){
					$day.append("<option value="+i+">"+i+"</option>");
				};
			});
			
			/* Resetting Receipt Inputs */
			$("#resetReceiptInputs").on("click",function(){
				$("#corporateReceiptSearchResultContainer").addClass("hide");
				$year = $("#corporateYearInput");
				$month = $("#corporateMonthInput");
				$day = $("#corporateDayInput");
				
				$year.val("");
				$month.val("");
				$day.html("");
				$day.append("<option value='' selected>Day</option>");
				
				$('#receiptSearchTable').DataTable().clear();
			});
			
			$("#searchCorporateReceipt").on("click",function(){
				$year = $("#corporateYearInput");
				$month = $("#corporateMonthInput");
				$day = $("#corporateDayInput");
				$monthYearKey = $("input[type='radio'][name='corporate-month-year-key']:checked");
				$corporateReceiptTypeKey = $("input[type='radio'][name='corporate-receipt-type-key']:checked");
				
				console.log($monthYearKey.val());
				debugger;
				
				$("#corporateReceiptSearchResultContainer").addClass("hide");
				var validateResObj = validateReceiptInputs($year.val(),$month.val(),$day.val(),$monthYearKey.val(),$corporateReceiptTypeKey.val());
				
				if(validateResObj.emptyField > 0){
					$("html, body").animate({ scrollTop: 100 }, "slow");
					alert("Marked Fields are mandatory","error");
					alert("Please fill these missing INPUTs / INPUT","error");
					
					for(var i = 0; i < validateResObj.emptyFieldIDArray.length; i++){
						console.log("empty field>>>"+validateResObj.emptyFieldIDArray[i]);
						
						raiseInputError(validateResObj.emptyFieldIDArray[i],"receiptInputContainer");
					}
					
					return;
				}
				
				//TODO:Disable all the Inputs
				$("#searchCorporateReceipt").attr("disabled","disabled");
				$("#resetReceiptInputs").attr("disabled","disabled");
				
				$("#receiptSearchLoader").removeClass("hide");
				/* AJAX Call for Getting Receipt history - STARTS*/
				
				var url = "SearchReceiptProfileAJAX.jsp";
				var formObject = new Object();
				formObject["cmd"] = "searchCorporateReceipt";
				formObject["year"] = $year.val();
				formObject["month"] = $month.val();
				formObject["day"] = $day.val();
				formObject["corporateMonthYearKey"] = $monthYearKey.val();
				formObject["corporateReceiptTypeKey"] = $corporateReceiptTypeKey.val();
				formObject["TIN"] = "<%=PortalUtility.getString(people.getLicenceNumber())%>";
				
				var dataSet = new Array();
				
				$.ajax({
					url: url,
					type: "POST",
					dataType: "json",
					async:true,
					data:formObject,
					success: function(res){
						if(!res.isExceptionOccured){
							if(JSON.stringify(res.arrayOfReceiptObj).match(/\[/)){
								var receiptObjectArray = res.arrayOfReceiptObj;
								//console.log(receiptObjectArray);
								
								for(var i = 0; i < receiptObjectArray.length; i++){
									var dataArray = new Array();
									
									dataArray.push((i+1).toString());
									dataArray.push(receiptObjectArray[i]["PAYMENT_REFERENCE"]);
									dataArray.push(receiptObjectArray[i]["SUM_OF_AMT"]);
									dataArray.push(receiptObjectArray[i]["PAYMENT_DATE"]);
									dataArray.push("<a href =\""+prepareDownloadLink(receiptObjectArray[i],$corporateReceiptTypeKey.val())+"\" target='_blank'><i class='fa fa-download' aria-hidden='true'></i></a>");
									
									dataSet.push(dataArray);
								}
							}else{
								var receiptObject = res.arrayOfReceiptObj;
								var dataArray = new Array();
								
								dataArray.push("1");
								dataArray.push(receiptObject["PAYMENT_REFERENCE"]);
								dataArray.push(receiptObject["SUM_OF_AMT"]);
								dataArray.push(receiptObjectArray["PAYMENT_DATE"]);
								dataArray.push("<a href =\""+prepareDownloadLink(receiptObject,$corporateReceiptTypeKey.val())+"\" target='_blank'><i class='fa fa-download' aria-hidden='true'></i></a>");
								
								dataSet.push(dataArray);
							}
							
							console.log(dataSet);
							
							/* Version Conflict between two J QUERY version */
							//var $j = jQuery.noConflict(true);
							$('#receiptSearchTable').DataTable().clear();
							$("#receiptSearchTable").DataTable({
								data: dataSet,
								destroy: true,
							        columns: [
							        	{ title: "S/N" },
							            { title: "Payment Reference" },
							            { title: "Payment Amount" },
							            { title: "Payment Date" },
							            { title: "Download" }
							        ]
							});
							
							$("#receiptSearchLoader").addClass("hide");
							$("#corporateReceiptSearchResultContainer").removeClass("hide");	
						}else{
							$("#receiptSearchLoader").addClass("hide");
							alert("Exception Occured Please try again","error");
						}
						
						$("#searchCorporateReceipt").removeAttr("disabled","disabled");
						$("#resetReceiptInputs").removeAttr("disabled","disabled");
			   		},
					error: function(res){
						$("#searchCorporateReceipt").removeAttr("disabled","disabled");
						$("#resetReceiptInputs").removeAttr("disabled","disabled");
						alert("ERROR Occured","error");
						$("#receiptSearchLoader").addClass("hide");
					}
				});
				/* AJAX Call for Getting Receipt history - ENDS*/
				
			});
			
			/* Testing for server side PROGRAMMING in J QUERY Data Table - Starts*/
			$("#searchCorporateReceipt1").on("click",function(){
				$year = $("#corporateYearInput");
				$month = $("#corporateMonthInput");
				$day = $("#corporateDayInput");
				$monthYearKey = $("input[type='radio'][name='corporate-month-year-key']:checked");
				$corporateReceiptTypeKey = $("input[type='radio'][name='corporate-receipt-type-key']:checked");
				
				console.log($monthYearKey.val());
				debugger;
				
				$("#corporateReceiptSearchResultContainer").addClass("hide");
				var validateResObj = validateReceiptInputs($year.val(),$month.val(),$day.val(),$monthYearKey.val(),$corporateReceiptTypeKey.val());
				
				if(validateResObj.emptyField > 0){
					$("html, body").animate({ scrollTop: 100 }, "slow");
					alert("Marked Fields are mandatory","error");
					alert("Please fill these missing INPUTs / INPUT","error");
					
					for(var i = 0; i < validateResObj.emptyFieldIDArray.length; i++){
						console.log("empty field>>>"+validateResObj.emptyFieldIDArray[i]);
						
						raiseInputError(validateResObj.emptyFieldIDArray[i],"receiptInputContainer");
					}
					
					return;
				}
				
				//TODO:Disable all the Inputs
				$("#receiptSearchLoader").removeClass("hide");
				
				var formObject = new Object();
				formObject["cmd"] = "searchCorporateReceipt";
				
				$('#receiptSearchTable').dataTable().fnDestroy();
				dataTableObj = $('#receiptSearchTable').DataTable({
			        //"stateSave": true,
			        "processing": true,
			        "serverSide": true,
			        //"filter": false,
			        "ajax": {
			        	'type': 'POST',
			            'url': "http://localhost:8080/FIRS/"+'SearchReceiptProfileAJAX.jsp',
			            'error': function(xhr, status, error) {}
			        },
			        "columns": [
			            { "data": "receiptNumber", "defaultContent": "" },
			            { "data": "payerName", "defaultContent": "" },
			            { "data": "taxAmount", "defaultContent": "" },
			            { "data": "date", "defaultContent": "" },
			            { "data": "taxOn", "defaultContent": "" },
			            { "data": "reference", "defaultContent": "" },
			            { "data": "downloadLink", "defaultContent": "" }
			        ]
				});
				
				dataTableObj.column( 0 ).visible(false);
				
				$("#receiptSearchLoader").addClass("hide");
				$("#corporateReceiptSearchResultContainer").removeClass("hide");
			});
			/* Testing for server side PROGRAMMING in J QUERY Data Table - Ends*/
			
		});	
		
		/* Function for getting number of days from Year and Month */
		function getNumbersOfDays(year,month){
			debugger;
	   		var isYearALeap = false;
			var numbersOfDay = 0;
			
			if(year % 4 === 0){
				isYearALeap = true;
			}
			
			if(month == "Feb"){
				if(isYearALeap){
					numbersOfDay = 29;
				}else{
					numbersOfDay = 28;
				}
			}
			
			if(month == "Apr" || month == "Jun" || month == "Sept" || month == "Nov"){
				numbersOfDay = 30;
			}
			
			if(month == "Jan" || month == "Mar" || month == "May" || month == "Jul" || month == "Aug" || month == "Oct" || month == "Dec"){
				numbersOfDay = 31;
			}
			
			return numbersOfDay;
		}
			
	   	/* Clearing Day Input */
		function clearDayInput($day){
			$day.html("");
			$day.append("<option value='' selected>Day</option>");
		}
	    	
	   	function validateReceiptInputs(year,month,day,monthYearKey,corporateReceiptTypeKey){
	   		var emptyField = 0;
	   		var validateResObj = new Object();
	   		var emptyFieldIDArray = new Array();
	   		
	   		if(year == ""){
	   			emptyField++;
	   			emptyFieldIDArray.push("corporateYearInput");
	   		}
			if(month == ""){
				emptyField++;
				emptyFieldIDArray.push("corporateMonthInput");
			}
			if(monthYearKey == "Daily"){
				debugger;
				if(day == ""){
					emptyField++;
					emptyFieldIDArray.push("corporateDayInput");
				}	
			}
			
			validateResObj["emptyFieldIDArray"] = emptyFieldIDArray;
			validateResObj["emptyField"] = emptyField;
			
			return validateResObj;
	   	}
	    	
	   	function raiseInputError(divID,parentID){
	   		$("#"+parentID).find("#"+divID).addClass("custom-error-input");
	   		setTimeout(function() {
	   			$("#"+parentID).find("#"+divID).removeClass("custom-error-input");
	   		}, 5000);
	   	}
	   	
	   	function handleClick(myRadio){
	   		//console.log(myRadio);
	   		if(myRadio.value == "Monthly"){
	   			clearDayInput($("#corporateDayInput"));
	   			$("#corporateDayInput").attr("disabled","disabled");
	   		}else{
	   			$("#corporateDayInput").removeAttr("disabled","disabled");
	   		}
	   	}
	   	
	   	function prepareDownloadLink(receiptObject,folderType){
	   		var params = "";
	   		var downloadURL = "";
	   		for(var key in receiptObject){
	   			if(key == "WHR_RATE"){
	   				params = params+"&"+key+"="+encodeURIComponent(parseFloat(receiptObject[key]).toString());
	   			}else{
		   			params = params+"&"+key+"="+encodeURIComponent(receiptObject[key]);
	   			}
	   		}
	   		
	   		downloadURL = "ExportProfileSearchedReceiptAJAX.jsp?"+"&key=viewReport&folderType="+folderType+params;
	   		return downloadURL.replace('"', '');
	   	}
	   	
   </script>
  </body>
 	<script type="text/javascript" src="<%=JS_API%>jquery.autocomplete.min.js"></script>
	<script type="text/javascript" src="<%=JS_API%>bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="<%=JS_PORTAL%>template.js"></script>
	<script type="text/javascript" src="<%=JS_PORTAL%>tradenature-autocomplete.js"></script>
		
	<script type="text/javascript" src="<%=JS_API%>jquery.growl.js" ></script>
	<script type="text/javascript" src="<%=JS_PORTAL%>notification.js"></script>
	<!-- Codes for AUTO Re - validation -->
	<script>
		var tinData = new Object();
		var TIN = "<%=PortalUtility.getString(people.getLicenceNumber())%>";
		var isProfileRevalided = false;
		
		tinData["TIN"] = TIN.trim();
		tinData["command"] = "updateDataInAmanda";
		
		var temp_isProfileRevalided = <%= session.getAttribute("isProfileRevalided")%>;
		//console.log("temp_isProfileRevalided");
		//console.log(temp_isProfileRevalided);
		
		temp_isProfileRevalided == null ? isProfileRevalided = false : isProfileRevalided = true; 
		//console.log("isProfileRevalided");
		//console.log(isProfileRevalided);
		
		/* if(!isProfileRevalided){
			autoUpdatePeopleRecord(tinData);
		} */
	</script>
</html>
