<%@page import="java.util.Map"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="org.apache.commons.lang.RandomStringUtils"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsSearchCriteria"%>
<%@ include file="include/includeconstant.jsp"%>
<%
	request.setAttribute("pageName","forgotpassword");

	int returnFlag = 0;
	String emailAddress = request.getParameter("email");
	String phoneNumber 	= PortalService.getUnmask(request.getParameter("phone"));
	String NINIANumber 	= request.getParameter("NINIA");
	String pageCommand 	= request.getParameter("command");
	String pageFrom		= request.getParameter("pageFrom");
	String fromEmailAddress = "fromaddress@senegalportal.com";
	String CCemailAddress = "";
	
	WsPeople[] wsPeoples = new WsPeople[1];
	
	if(!PortalUtility.isNull(emailAddress)){
		WsSearchCriteria[] scs = new WsSearchCriteria[1];
		scs[0] = new WsSearchCriteria();
		scs[0].setFieldName("People.emailAddress");
		scs[0].setTableName("People");
		scs[0].setOperator(PortalService.getClientValidOperators().getEQUAL());
		scs[0].setValue(new String[]{emailAddress.trim()});
		wsPeoples = PortalService.getWSClient().searchPeople(lid, scs, 0, 1, null);
	}else if(!PortalUtility.isNull(phoneNumber)){
		WsSearchCriteria[] scs = new WsSearchCriteria[1];
		scs[0] = new WsSearchCriteria();
		scs[0].setFieldName("People.phone1");
		scs[0].setTableName("People");
		scs[0].setOperator(PortalService.getClientValidOperators().getEQUAL());
		scs[0].setValue(new String[]{phoneNumber.trim()});
		wsPeoples = PortalService.getWSClient().searchPeople(lid, scs, 0, 1, null);
	}else if(!PortalUtility.isNull(NINIANumber)){
		WsSearchCriteria[] scs = new WsSearchCriteria[1];
		scs[0] = new WsSearchCriteria();
		scs[0].setFieldName("People.SocialSecurityNumber");
		scs[0].setTableName("People");
		scs[0].setOperator(PortalService.getClientValidOperators().getEQUAL());
		scs[0].setValue(new String[]{NINIANumber.trim()});
		wsPeoples = PortalService.getWSClient().searchPeople(lid, scs, 0, 1, null);
	}
	
		
	if("checkUser".equals(pageCommand)){
		if(wsPeoples != null && wsPeoples.length>0){
			WsPeople people = wsPeoples[0];
			String internetPassword = people.getInternetPassword();
			if(internetPassword == null || "".equals(internetPassword)){
				returnFlag = 1;
			}else{
				returnFlag = 2;
			}
		}else{
			returnFlag = 0;
		}
	}else if("checkUserByNINIA".equals(pageCommand)){
		if(wsPeoples != null && wsPeoples.length>0){
			returnFlag = 1;
		}else{
			returnFlag = 0;
		}
	}else if("setupPass".equals(pageCommand)){
		boolean flag = false;
		String TempPassword = RandomStringUtils.random(8, true, true);
		
		WsPeople people = wsPeoples[0];
		people.setInternetPassword(TempPassword);
		people.setInternetAccess("A");
		try{
			flag = PortalService.getWSClient().updatePeople(lid, people);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			if(flag)
			flag = PortalService.getWSClient().sendHtmlEmail(lid, emailAddress, fromEmailAddress, CCemailAddress, resourceBundle.getJsText("MESSAGE_MAIL_SUBJET_1"), resourceBundle.getText("MESSAGE_MAIL_BODY1_1")+" <br/>"+resourceBundle.getText("MESSAGE_MAIL_BODY2_1")+"<br/><br/>"+resourceBundle.getText("MESSAGE_MAIL_BODY3")+" <strong>"+TempPassword+"</strong>");
		}catch(Exception e){
			e.printStackTrace();
		}
		if(flag)
		returnFlag = 1;
	}else if("checkUserByEmailOrPhone".equals(pageCommand)){
		JSONObject jsonObject = new JSONObject();
		
		if(wsPeoples != null && wsPeoples.length>0){
			WsPeople people = wsPeoples[0];
			WsPeopleInfo[] peopleInfos = PortalService.getWSClient().getPeopleInfo(lid, PortalUtility.getInt(people.getPeopleRSN()));
			
			jsonObject.put("peopleRSN", PortalUtility.getInt(people.getPeopleRSN()));
			jsonObject.put("peopleContactTitle", PortalUtility.getString(people.getNameTitle()));
			jsonObject.put("peopleContact", PortalUtility.getString(people.getNameFirst()));
			jsonObject.put("peopleTelephone", PortalService.getMask(PortalUtility.getString(people.getPhone1()), PHONE_MASK_FORMAT, PHONE_MASK_FORMAT_CHAR));
			jsonObject.put("peopleFax", PortalService.getMask(PortalUtility.getString(people.getPhone2()), PHONE_MASK_FORMAT, PHONE_MASK_FORMAT_CHAR));
			jsonObject.put("peopleCountry", PortalUtility.getString(people.getAddrCountry()));
			jsonObject.put("peopleCity", PortalUtility.getString(people.getAddrCity()));
			jsonObject.put("peopleAddress", PortalUtility.getString(people.getAddressLine1()));
			jsonObject.put("peopleEnterpriseName", PortalUtility.getString(people.getOrganizationName()));
			jsonObject.put("peopleEmail", PortalUtility.getString(people.getEmailAddress()));
			jsonObject.put("clientType", PortalUtility.getInt(people.getPeopleCode()));
			jsonObject.put("NiniaNumber", PortalUtility.getString(people.getSocialSecurityNumber()));
			jsonObject.put("BirthDate", PortalUtility.getDate(people.getBirthDate()));
			jsonObject.put("peopleSigile", PortalUtility.getString(people.getNameLast()));
			jsonObject.put("peoplePostal", PortalUtility.getString(people.getAddrPostal()));
			
			
			
			Map<Integer, String> peopleInfoMap = PortalService.getPeopleInfoInMap(peopleInfos);
			if(peopleInfoMap != null){
				
				if(pageFrom != null && "directoryAdd".equals(pageFrom)){
					jsonObject.put("peopleWebAddress", peopleInfoMap.get(6034));
					jsonObject.put("LegalStatus", peopleInfoMap.get(6031));
					jsonObject.put("Sector", peopleInfoMap.get(196));
					jsonObject.put("Activity", peopleInfoMap.get(197));
					jsonObject.put("NumberEmployee", peopleInfoMap.get(6017));
					
					jsonObject.put("CapitalSocial", peopleInfoMap.get(6018));
					jsonObject.put("NumberEmploye", peopleInfoMap.get(6031));
					jsonObject.put("AnnualTurnover", peopleInfoMap.get(6033));
					jsonObject.put("Turnover", peopleInfoMap.get(6019));
					jsonObject.put("Investment", peopleInfoMap.get(6023));
					jsonObject.put("TradeRegNumber", peopleInfoMap.get(6022));
					jsonObject.put("SocialSecurityFundNo", peopleInfoMap.get(6021));
					jsonObject.put("AccountTaxPayerNo", peopleInfoMap.get(161));
					jsonObject.put("EstablishmentNo", peopleInfoMap.get(6023));
					jsonObject.put("IPRFSNumber", peopleInfoMap.get(6024));
					jsonObject.put("CUCINumber", peopleInfoMap.get(6025));
				}else{
					jsonObject.put("peopleWebAddress", peopleInfoMap.get(7003));
					jsonObject.put("peopleWorkLanguage", peopleInfoMap.get(7006));
				}
				
			}			
			
		}else{
			jsonObject.put("peopleRSN", "0");
		}
		
		out.print(jsonObject);
		return;
	}
	out.print((returnFlag > 0 ? returnFlag : 0));
%>