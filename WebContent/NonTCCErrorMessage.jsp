<%@include file= "header.jsp" %>
	<div>
		<div id="container-fluid">
			<!-- User Registration Page Starts -->
    <div class="hid" id="registration-sucess-info">
		<div class="row panel panel-default" style="margin: 0px 0px 0px 0px;">
			<div class="col-lg-offset-2 col-lg-4 text-center"
				style="padding: 30px 10px 30px 10px">
				<strong></strong>
				<p>
				<%=request.getAttribute("errorMessage") %>
				</p>
				<p>
					<button class="btn btn-warning" onclick="javascript:history.back()">Go Back</button>
				</p>
			</div>
			
			<div class="col-lg-4 text-center"
				style="padding: 30px 10px 30px 10px; border-left:1px solid #bbb">
				<strong>Is this your first TCC Application Request on this Portal?</strong>
				<p>
				Please Click on the Link below for fresh TCC Account<strong><span
						id="application-number"></span></strong>
				</p>
				<p>
					<a href="" data-toggle="modal" data-target="#myModal" style="text-decoration:underline;border:0;outline:none;"><button class="btn btn-primary">Click here to Register for a New TCC Account</button></a>
				</p>
			</div>
			
		</div>
	</div>
		</div>

	</div>

	<div class="container-fluid">
		<footer class="footer">
		<div class="container-fluid"
			style="font-family: sans-serif; font-size: 12px;">
			<p class="text-muted">
				<div class="text-center">
					&copy;&nbsp;Copyright 2017 <a href="#" target="_blank">Federal
						Inland Revenue Service. All Right Reserved </a>.
					<!-- <span class="text-right small"><b>Need help?&nbsp</b><a href="#" class="text-info"><u>Click here for LIVE CHAT</u></a></span> -->
				</div>
			</p>
		</div>
		</footer>


	</div>


</body>
</html>