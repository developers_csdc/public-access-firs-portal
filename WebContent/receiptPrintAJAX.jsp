<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="include/includeconstant.jsp"%>
<%
	boolean isFileAttachedFlag = false;
	String operation = "";
	ArrayList<Map<String, Object>> fileItems = new ArrayList<Map<String, Object>>();

	if(ServletFileUpload.isMultipartContent(request)){
		isFileAttachedFlag=true;
		List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
		for(FileItem item: multiparts){
			if (!item.isFormField()) {
				Map<String, Object> attachmentItem =new HashMap<String, Object>();
				attachmentItem.put("fileData", item.get());
				attachmentItem.put("fileName", item.getName());
				fileItems.add(attachmentItem);
			}else{
				if("operation".equals(item.getFieldName())){
					operation = new String(item.get());
				}
			}
		}
	}	
%>
