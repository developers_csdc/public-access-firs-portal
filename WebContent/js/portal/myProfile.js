

    $('#FreeServiceSubscriptionDataTable').DataTable( {
        "scrollY"		: "200px",
        "scrollCollapse": true,
        "paging"		: false,
        "bFilter"		: false,
        "bInfo"			: false
    } );

	var oldTitle="";
	var oldName="";
	var oldNameLast="";
	var oldEmailAddress="";
	var oldOrganization="";
	var oldAddress="";
	var oldCity="";
	var oldCountry="";
	var oldPhone = "";
	var oldFax = "";
	var oldPostalCode="";
	var oldWebAddress="";
	var oldNINIANumber="";
	var oldPassWord1="";
	
	var oldQuestion = "";
	var oldQuestionAnswer = "";
	
	var oldPeopleRSN="";   
	
	
	function keepOldValues() {
		oldTitle=$('#Title').val();
		oldName=$('#Name').val();
		oldNameLast=$('#NameLast').val();
		oldEmailAddress=$('#EmailAddress').val();
		oldAddress=$('#Address').val();
		oldCity=$('#City').val();
		oldCountry=$('#Country').val();
		oldPhone=$('#Phone').val();
		oldFax=$('#Fax').val();
		oldPostalCode=$('#PostalCode').val();
		oldQuestion = $('#Question').val();
		oldQuestionAnswer = $('#QuestionAnswer').val();
		/*
		oldOrganization=$('#CompanyName').val();
		oldWebAddress=$('#WebAddress').val();
		oldSecurityNumber=$('#SecurityNumber').val();
		oldPassWord1=$('#PassWord1').val();*/
		
		oldPeopleRSN=$('#peopleRSN').val();
	}
	
	function compare_oldvalue_with_newvalue(){
		
  	var check=((oldTitle==$('#Title').val()) && (oldName==$('#Name').val()) && (oldNameLast==$('#NameLast').val()) && 
  	         (oldEmailAddress==$('#EmailAddress').val()) && 
  	         (oldAddress==$('#Address').val()) && (oldCity==$('#City').val())&&
  	         (oldCountry==$('#Country').val()) && (oldPhone==$('#Phone').val()) &&
  	         (oldFax==$('#Fax').val()) && (oldPostalCode==$('#PostalCode').val()) && 
  	         (oldPeopleRSN==$('#peopleRSN').val()) && (oldQuestion==$('#Question').val()) &&
  	         (oldQuestionAnswer==$('#QuestionAnswer').val()));
      /* && (oldOrganization==$('#CompanyName').val()) && (oldWebAddress==$('#WebAddress').val()) && (oldSecurityNumber==$('#SecurityNumber').val())  	*/

                 if(!$("#ShowPassword").is(":checked") && check){
                     return false;
                 }else{
                     return true;
                 }    
      
    }
	
	//For Page Submit
	$(document).ready(function () {

		keepOldValues();
		
		$('#btnReset').on('click', function () {         
			$('#theform').each (function(){
			  	this.reset();
			});
		});

    });
	
	function showPasswordDiv(){
		if($("#ShowPassword").is(":checked")){
			$("#passwordMainDiv").show();
		}else{
			$("#passwordMainDiv").hide();
		}
	}

	function f_submitMyProfile() {
	    if(validate()){
	        	if(compare_oldvalue_with_newvalue()){//This Condition is when nothing to save. & 2nd condition is used to check if the Identification Document is Passport then the Document Number should not be less than 6 characters.
	          	  	var fullName = "&Title="+$('#Title').val()+"&Name="+$('#Name').val()+"&NameLast="+$('#NameLast').val();
	          		var contact 	= "&EmailAddress=" + $('#EmailAddress').val() + "&Phone1=" + $('#Phone').val() + "&Phone2=" + $('#Fax').val();
	          		var address 	= "&Address=" + $('#Address').val() + "&City=" + $('#City').val() + "&PostalCode=" + $('#PostalCode').val() + "&Country=" + $('#Country').val();
	          		var securityInfo= "&SecurityQuestion=" + $('#Question').val() + "&SecurityQuestionAnswer=" + $('#QuestionAnswer').val();
	          		var info 		= "";
	          		
	                var updatePasswordFlag = "&updatePasswordFlag="+$("#ShowPassword").is(":checked");
	                var datas = (fullName + contact + address + securityInfo + updatePasswordFlag + info).split("+").join("%2B");
	                var newurl ="updatePeopleAjax.jsp?pagename=myProfile&lid="+$('#lid').val();
	                $("#btnSend").attr('disabled','disabled');
			    	$("#btnSend").html('<span class="glyphicon glyphicon-refresh fa-spin"></span>&nbsp;'+LABEL_MYPROFILE_BUTTON_PLEASE_WAIT);
	                 $.ajax({
	                		url: newurl,
	                		type: "POST",
	                		data:datas,
	                		success: showMessage
	                	});
	                }
	            else{
	              	alert(JS_ALERT_NOTHING_TO_SAVE);
	               }
	      }
	}
	
	function prePopulate(responseXML) {
		var responseCity="";
		if(responseXML.getElementsByTagName("City")[0] && responseXML.getElementsByTagName("City")[0].firstChild != null){
			responseCity = responseXML.getElementsByTagName("City")[0].firstChild.nodeValue;
			$("#City").val(responseCity);
			oldCity = $("#City").val();
		}
	
		var responseCountry = "";
		if(responseXML.getElementsByTagName("Country")[0] && responseXML.getElementsByTagName("Country")[0].firstChild != null){
			responseCountry = responseXML.getElementsByTagName("Country")[0].firstChild.nodeValue;
			$("#Country").val(responseCountry);
			oldCountry = $("#Country").val();
		}
		
		
	}
	
	function showMessage(responseText) {
		$("#btnSend").html('<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;'+LABEL_MYPROFILE_BUTTON_UPDATE);
    	$("#btnSend").removeAttr('disabled','disabled');
    	
    	if(responseText.trim()=='success'){
    		alert(MESSAGE_PROFILE_UPDATE_SUCCESS);
    	}else if(responseText.trim()=='failed'){
    		alert(MESSAGE_PROFILE_UPDATE_FAIL);
    	}
		if($("#ShowPassword").is(":checked") || peopleAddedByAdmin){
			$("#theform").attr("action", "portal?cmd=home");
		}else{
			var lid = $("#lid").val();
			$("#theform").attr("action", "portal?cmd=myProfile");
		}
		document.theform.submit();
	} 
	
	//For Page Validation
	function validate(){
		var Title = $('#Title').val();
		var Name = $('#Name').val();
		var NameLast = $('#NameLast').val();
		var EmailAddress = $('#EmailAddress').val();
		var Address = $('#Address').val();
		var City = $('#City').val();
		var Phone = $('#Phone').val();
		var CompanyName = $('#CompanyName').val();
		var SecurityNumber=$('#SecurityNumber').val();
		
		if($('#ShowPassword').is(":checked")){
		var PassWord1 = $('#PassWord1').val();
		var PassWord2 = $('#PassWord2').val();
		}
		
		var Question = $('#Question').val();
		var QuestionAnswer = $('#QuestionAnswer').val();
		
		var retval = true;  
	
		var msg = "";
		var count = 0;
		var fieldOBJ;      	
		var bDate = 0;
		
		if(Name==""){     		
			if(count==0) fieldOBJ = $('#Name');
			msg += JS_ALERT_ERROR_FIRST_NAME+"\n";
			$('#Name').css("border", "1px solid red");
			retval =  false;
		}else{$('#Name').css("border", "1px solid #ccc");}
		
		if(NameLast==""){   		
			if(count==0) fieldOBJ = $('#NameLast');
			msg += JS_ALERT_ERROR_LAST_NAME+"\n";
			$('#NameLast').css("border", "1px solid red");
			retval =  false;
		}else{$('#NameLast').css("border", "1px solid #ccc");}
		
		if(Address==""){    		
			if(count==0) fieldOBJ = $('#Address');
			msg += JS_ALERT_ERROR_ADDRESS+"\n";
			$('#Address').css("border", "1px solid red");
			retval =  false;
		}else{$('#Address').css("border", "1px solid #ccc");}
	
		
		if(City==""){   		
			if(count==0) fieldOBJ = $('#Phone');
			msg += JS_ALERT_ERROR_CITY+"\n";
			$('#City').css("border", "1px solid red");
			retval =  false;
		}else{$('#City').css("border", "1px solid #ccc");}
	
		if(Phone==""){
			if(count==0) fieldOBJ = $('#Phone');
			msg += JS_ALERT_ERROR_PHONE1+"\n";
			$('#Phone').css("border", "1px solid red");
			retval =  false;
		}else{$('#Phone').css("border", "1px solid #ccc");}
	/*
		if(CompanyName==""){
			if(count==0) fieldOBJ = $('#Phone');
			msg += JS_ALERT_ERROR_COMPANY_NAME+"\n";
			$('#CompanyName').css("border", "1px solid red");
			retval =  false;
		}else{$('#CompanyName').css("border", "1px solid #ccc");}
		
		if(SecurityNumber==""){
			if(count==0) fieldOBJ = $('#Phone');
			msg += JS_ALERT_ERROR_SECURITY_NUMBER+"\n";
			$('#SecurityNumber').css("border", "1px solid red");
			retval =  false;
		}else{$('#SecurityNumber').css("border", "1px solid #ccc");}*/
		
		if(!f_checkEmail(EmailAddress)){
			if(count==0) fieldOBJ = $('#EmailAddress');
			msg += JS_ALERT_ERROR_EMAIL_ADDRESS+"\n";
			$('#EmailAddress').css("border", "1px solid red");
			retval =  false;
		}else{$('#EmailAddress').css("border", "1px solid #ccc");}
	
		if(retval && (PassWord1 != PassWord2)){
			alert(JS_ALERT_ERROR_MISMATCH); 
			$('#PassWord1').css("border", "1px solid red");
			$('#PassWord2').css("border", "1px solid red");
			retval =  false;
		}
		
		if(Question==""){
			if(count==0) fieldOBJ = $('#Question');
			msg += JS_ALERT_ERROR_QUESTION+"\n";
			$('#Question').css("border", "1px solid red");
			retval =  false;
		}else{$('#Question').css("border", "1px solid #ccc");}
	
		
		if(QuestionAnswer==""){
			if(count==0) fieldOBJ = $('#QuestionAnswer');
			msg += JS_ALERT_ERROR_QUESTION_ANSWER+"\n";
			$('#QuestionAnswer').css("border", "1px solid red");
			retval =  false;
		}else{$('#QuestionAnswer').css("border", "1px solid #ccc");}

		if($('#ShowPassword').is(":checked")){
	
			//var TempPassword = $('#TempPassword').val();
	        var newPassword = $('#newPassword').val();
	        var reTypePassword = $('#reTypePassword').val();
	        
	        //if(TempPassword==""){
	      		//alert(JS_ALERT_ENTER_TEMP_PASS);
	      		//$('#TempPassword').focus();
				//if(count==0) fieldOBJ = $('#TempPassword');
				//msg += (++count)+"."+JS_ALERT_ENTER_TEMP_PASS+"\n";
	      		//$('#TempPassword').css("border", "1px solid red");
	      		//retval =  false;
	      	//}else{$('#TempPassword').css("border", "1px solid #ccc");}
	        
	        
	        if(newPassword==""){
	      		//alert(JS_ALERT_ENTER_NEW_PASS);
	      		//$('#newPassword').focus();
				if(count==0) fieldOBJ = $('#newPassword');
				msg += JS_ALERT_ENTER_NEW_PASS+"\n";
	      		$('#newPassword').css("border", "1px solid red");
	      		retval = false;
	      	}else{$('#newPassword').css("border", "1px solid #ccc");}	      	
	
	      	
	        if(reTypePassword==""){
	      		//alert(JS_ALERT_RE_ENTER_NEW_PASS);
	      		//$('#reTypePassword').focus();
				if(count==0) fieldOBJ = $('#reTypePassword');
				msg += JS_ALERT_RE_ENTER_NEW_PASS+"\n";
	      		$('#reTypePassword').css("border", "1px solid red");
	      		retval =  false;
	      	}else{$('#reTypePassword').css("border", "1px solid #ccc");}
	
	      	if(newPassword!="" && reTypePassword!="" && msg==""){
		        if(!(reTypePassword==newPassword)){
		      		$('#reTypePassword').css("border", "1px solid red");
		      		$('#newPassword').css("border", "1px solid red");
		      		alert(JS_ALERT_PASS_MISMATCH);
		      		$('#newPassword').focus();
		      		$('#reTypePassword').focus();
		      		retval =  false;
		      	}else{
		      		$('#reTypePassword').css("border", "1px solid #ccc");
		      		$('#newPassword').css("border", "1px solid #ccc");
		      	}
	
		      	
		    	if(!f_checkPasswordStrength()){
		        	$('#newPassword').css("border", "1px solid red");
		            alert(JS_ALERT_PASSWORD_STRENGTH);
		            $('#newPassword').focus();
		            retval =  false;
		        }else $('#newPassword').css("border", "1px solid #ccc");
	      	}
	      	
	    }


		
	
	  
		if("" != msg){
		      alert(msg);
		      fieldOBJ.focus();
		      if(bDate==1)fieldOBJ.blur();
		      retval =  false;
		  }
		  
		return retval;
	}

    function f_checkEmail(email){
    	  var testresults;
    	  var str = email;
    	  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    	  if (filter.test(str)){
    	    testresults=true;
    	  }else{
    	    testresults=false;
    	  }

    	  return testresults;
    }

	function f_checkFields(){
		var TempPassword = $('#TempPassword').val();
        var newPassword = $('#newPassword').val();
        var reTypePassword = $('#reTypePassword').val();
        
        if(TempPassword==""){
      		$('#tempPassword').addClass('has-error has-feedback');
      		alert(JS_ALERT_ENTER_TEMP_PASS);
      		$('#TempPassword').focus();
      		return false;
      	}
      	
        if($('#ShowPassword').is(":checked")){
        if(newPassword==""){
      		$('#password1Div').addClass('has-error has-feedback');
      		alert(JS_ALERT_ENTER_NEW_PASS);
      		$('#newPassword').focus();
      		return false;
      	}
      	
        if(reTypePassword==""){
      		$('#passwordDiv').addClass('has-error has-feedback');
      		alert(JS_ALERT_RE_ENTER_NEW_PASS);
      		$('#reTypePassword').focus();
      		return false;
      	}
        if(!(reTypePassword==newPassword)){
      		$('#passwordDiv').addClass('has-error has-feedback');
      		$('#password1Div').addClass('has-error has-feedback');
      		alert(JS_ALERT_PASS_MISMATCH);
      		$('#reTypePassword').focus();
      		$('#newPassword').focus();
      		return false;
      	}
        if(!f_checkPasswordStrength()){
        	$('#password1Div').addClass('has-error has-feedback');
            alert(JS_ALERT_PASSWORD_STRENGTH);
            $('#newPassword').focus();
        	return false;
        }
        }
      	return true;
	}
    

    function f_checkPasswordStrength(){
        var password = $('#newPassword').val();
        //initial strength 
        var strength = 0;
        //if the password length is less than 6, return message. 
        if (password.length < 8) { 
            $('#password1Div').addClass('has-error has-feedback short');
            alert(JS_ALERT_ERROR_SHORT_PASSWORD);
            return false; 
            } 
        //if length is 8 characters or more, increase strength value 
        if (password.length > 7) strength += 1; 
        if (strength < 1) {
              return false;
            }
        else{
        return true;
        }
    }
    
    $("#Country").autocomplete({
    	width: "230px",
        serviceUrl: "AutoCompleteAJAX.jsp?cmd=myprofile&inputName=Country"
    });
    $("#City").autocomplete({
    	width: "230px",
        serviceUrl: "AutoCompleteAJAX.jsp?cmd=myprofile&inputName=City"
    });