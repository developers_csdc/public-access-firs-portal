/**
 * uploadReceipt.js
 */
$("#do-login").on("click", function () {
	var userID    = $("#username").val();
	var password  = $("#password").val();
	
	$("#do-login").attr("disabled","disabled");
	$("#do-login").html('Logging in..<i class="fa fa-circle-o-notch fa-spin"></i>');
	
	
	var checkFlag = validateUserIDAndPassword(userID,password);
	if(!checkFlag){
		alert("User ID or Password cannot left blank","error");
		$("#do-login").html('Login <i class="fa fa-key" aria-hidden="true"></i>');
		$("#do-login").removeAttr("disabled");
		return false;
	}else{
		$("#login_waiting_section").removeClass("hide");
		$("#login_section").addClass("hide");
		$("#login_buttons_section").addClass("hide");
		setTimeout(function(){
			data={"command":"loginUser","userID":userID,"password":password};
			$.ajax({
				url:"UserLogin_Ajax.jsp",
				type: "POST",
				dataType: "json",
				data: data,
				success: function(res){
					   if(res.success){
						   //$(location).attr('href',res.page).attr('target','_blank');
						   $(location).attr({"href": res.page , "target":"_blank"})
						   //window.open(res.page, '_blank');
						   }
					   else{
						   alert("Invalid UserID or Password.Please try Again","error");
						   $("#do-login").html('Login <i class="fa fa-key" aria-hidden="true"></i>');
						   $("#do-login").removeAttr("disabled");
						   $("#username").val("");
						   $("#password").val("");
					   }
					   $("#login_buttons_section").removeClass("hide");
				},
				error: function(res){
					 alert("An error has occured Please Try Again","error");
					 $("#do-login").html('Login <i class="fa fa-key" aria-hidden="true"></i>');
					 $("#do-login").removeAttr("disabled");
					 $("#username").val("");
					 $("#password").val("");
				}
			});
		}, 100);
	}
	
	
	
});

function validateUserIDAndPassword(userID,password){
	var checkFlag = false;
	if(userID !=="" && userID !== undefined && password !=="" && password !== undefined){
		checkFlag = true;
	}else{
		checkFlag = false;
	}
	return checkFlag;
}
