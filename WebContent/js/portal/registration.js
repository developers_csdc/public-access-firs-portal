/**
 JS for user Registration
 */

var formDataJson = {}, formData = new FormData();

$(document).ready(function(){
	//loader
	
	   $(document).ajaxStart(function(){
		   	$("#submit-registration-form").attr('disabled',true);
	        $("#wait").css("display", "block");
	    });
	    $(document).ajaxComplete(function(){
	        $("#wait").css("display", "none");
	    });
	resetNewDetails();
	//resetPrevDetails();
	
	/*Code for confirmation*/
	
	$("#confirm-registration").click(function(){
		if($("#confirm-registration").prop("checked") == true){   
			$("#submit-registration-form").attr('disabled',false);
		}else{
			$("#submit-registration-form").attr('disabled',true);
			}
	});
	
	$("#btnRetriveTIN").click(function(){
		var tinNumber=$('#tin').val().trim();
		var $email = $('#email-perview-hidden');
		var applicanttype=$('#applicant-type').val();
		if(applicanttype=='INDIVIDUAL')
			{
				$("#peopledob").hide();
			}
		
		//$('#invalidTINMsg').addClass("hide");
		$("#btnRetriveTIN").html("Please wait..");
		$("#btnRetriveTIN").attr("disabled","disabled");
		$email.attr("type","hidden");
		
		tinData={"command":"searchTin","searchMethod":"searchByREST","tinNumber":tinNumber};		
		if(tinNumber.length<=0){
			alert('Please enter TIN number before retrieval',"error");
			$("#btnRetriveTIN").html("Retrieve");
			$("#btnRetriveTIN").removeAttr("disabled","disabled");
		}else{			
		url="searchTin_ajax.jsp";
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				async:true,
				data:tinData,
				success: function(res){
					if(res.Success==true){
					$("#confirm-registration").prop("checked", false);
					$("#btnRetriveTIN").html("Retrieve");
					$("#btnRetriveTIN").removeAttr("disabled","disabled");
					//document.getElementById('preview-section-reg').style.display = "";
					$('#preview-section-reg').removeClass("hide");
					$('#name-preview').html(res.CompanyName);
					$('#name-preview-hidden').val(res.CompanyName);
					$('#tax-office-preview').html(res.TaxOfficeName);
					$('#tax-office-preview-hidden').val(res.TaxOfficeName);
					$('#taxOfficeId-preview-hidden').val(res.TaxOfficeId);
					
					$email.attr("pattern","email");
					$email.removeAttr("type");
					$email.val(res.EmailId);
					$('#email-perview').html(res.EmailId);
					
					$('#phone-preview').html(res.PhoneNumber);
					$('#phone-preview-hidden').val(res.PhoneNumber);
					$('#RCNo-preview-hidden').val(res.RCNo);
					$('#JTBTIN-preview-hidden').val(res.JTBTIN);
					$('#Address-preview-hidden').val(res.Address);
					$('#FIRSTIN-hidden').val(res.TIN);
					$('#taxPayerTypeFromTINDB').val(res.TaxpayerType);
					$("#btnPreviewReg").attr('disabled',false);					
					}else if(res.Success==false){
						if(res.exceptionOccured){
							$("#btnRetriveTIN").html("Retrieve");
							$("#btnRetriveTIN").removeAttr("disabled","disabled");
							resetPrevDetails();
							alert("<strong> Unable to Retrieve!</strong> Please Try Agian.","error");
						}else if(!res.isTINFound){
							$("#btnRetriveTIN").html("Retrieve");
							$("#btnRetriveTIN").removeAttr("disabled","disabled");
							resetPrevDetails();
							alert("<strong>TIN not found!</strong> Please enter a valid TIN.","error");
						}
						//$('#invalidTINMsg').addClass("alert alert-danger text-center");
						$("#btnPreviewReg").attr('disabled',true);
					}else if(res.Success=="Retrieved"){
						$("#btnRetriveTIN").html("Retrieve");
						$("#btnRetriveTIN").removeAttr("disabled","disabled");
						$('#invalidTINMsg').addClass("alert alert-danger text-center");
						$('#invalidTINMsg').append(res.Message);
					}
		   		},
				error: function(res){
					$("#btnRetriveTIN").html("Retrieve");
					$("#btnRetriveTIN").removeAttr("disabled","disabled");
					resetPrevDetails();
					alert('Some Error occured !!!!!',"error");
				}
			});	
		}
	});	
	
	/*Saving the registration details*/
	
	/*$("#submit-registration-form").click(function(){
		//var tinNumber=$('#save_tin').html();
		var tinNumber=$('#FIRSTIN-hidden').val();
		var peopleType=$('#save_peopleType').html();
		var name=$('#save_name').html();
		var taxOffice=$('#save_taxOffice').html();
		var email=$('#save_email').html();
		var phone=$('#save_phone').html();
		var phone=$('#save_phone').html();
		var newTaxOffice=$('#save_newTaxOffice').html();
		var newEmail=$('#save_newEmail').html();
		var newMobile1=$('#save_newMobile').html();
		Extracting phone number
		var newMobile = newMobile1.substring(6,newMobile1.length).split(" ").join("");
		
		var newAddress=$('#save_newAddress').html();
		var taxOfficeId=$('#TaxOfficeId').val();
		var taxOfficeIdExisting=$('#taxOfficeId-preview-hidden').val();
		var RCNo=$('#RCNo-preview-hidden').val();		
		//var JTBTIN=$('#JTBTIN-hiddenFIRSTIN-hidden').val();
		var JTBTIN=$('#JTBTIN-preview-hidden').val();
		var oldAddress=$('#Address').val();
		
		registrationdata={"command":"changePassword","tinNumber":tinNumber,"peopleType":peopleType,"name":name,
						  "taxOffice":taxOffice,"email":email,"phone":phone,"newTaxOffice":newTaxOffice,"newEmail":newEmail,
						  "newMobile":newMobile,"newAddress":newAddress,"RCNo":RCNo,"JTBTIN":JTBTIN,
						  "oldAddress":oldAddress,"taxOfficeId":taxOfficeId,"taxOfficeIdExisting":taxOfficeIdExisting};
		
		url="RegistrationAjax.jsp";		
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				async:true,
				data:registrationdata,
				success: function(res){
					if(res.Success){
						alert("Registration successful.", "success");
						$('#RegistrationPreviewDiv').addClass("hide");
						$('#application-number').html(res.folderRSN);
						$('#registration-sucess-info').removeClass("hide");
						//alert("Registration Successful.Your application no. is :"+res.folderRSN);
					   //goToBackPage(contextPath);
					}
		   		},
				error: function(res){
					resetPrevDetails();
					alert('Some Error occured !!!!!');
				}
			});	
		});*/
	
	$("#btnResetTIN").click(function(){		
		$('#tin').val('');
		$('#invalidTINMsg').addClass("hide");
		resetNewDetails();
		resetPrevDetails();
		
	});
	
	$("#btnResetNewTIN").click(function(){
		resetNewDetails();	
	});
	
	
	//Checkbox confirmition and activating the submit button on check
	$("#confirm-registration").click(function(){
		if($("#confirm-registration").prop("checked") == true) {   
			$("#submit-registration-form").attr('disabled',false);
		}else{
			$("#submit-registration-form").attr('disabled',true);
		}
	});

	//Going back to form for further editing on clicking back button
	$("#form_back").on("click", function(){
		$("#RegistrationPreviewDiv").fadeOut();
		$("#RegistrationFormDiv").fadeIn();
	})
	
	//Preview data after clicking preview button
	$("#btnPreviewReg").on("click", function(){
		var flag = validateFieldsByDiv("RegistrationFormDiv", formDataJson);
		if(flag){
			previewRegistration();
		}
		console.log(formDataJson);
	});
	
	/*Saving Registration details(without any Preview)*/
	$("#submit-registration-form").click(function(){
		//var tinNumber=$('#save_tin').html();
		var applicanttype=$('#applicant-type').val();
		var tinNumber=$('#FIRSTIN-hidden').val();
		var peopleType=$('#peopleType-preview-hidden').val();
		var name=$('#name-preview-hidden').val();
		var taxOffice=$('#tax-office-preview-hidden').val();
		var email=$('#email-perview-hidden').val();
		var phone=$('#phone-preview-hidden').val();
		var phone1=$('#save_phone').html();
		var newTaxOffice=$('#save_newTaxOffice').html();
		var newEmail=$('#save_newEmail').html();
		var newMobile1=$('#save_newMobile').html();
		
		/*added by puja on 06-10-2017 to add date of incorporation in people*/
		var temp_newdateofincor=$('#dateofinc-perview-hidden').val();
		var taxPayerTypeFromTINDB = $('#taxPayerTypeFromTINDB').val();
		
		/*ESS:Samarjit Bootstrap Date Picker has been initialize and date has been formatted(into DD/MM/YYYY from DD-MM-YYYY) accordingly*/
		String.prototype.replaceAll = function(f,r){return this.split(f).join(r);}
		var newdateofincor = temp_newdateofincor.replaceAll("-","/").trim();
		
		/*Extracting phone number*/
		var newMobile = newMobile1.substring(6,newMobile1.length).split(" ").join("");
		
		var newAddress=$('#save_newAddress').html();
		var taxOfficeId=$('#TaxOfficeId').val();
		var taxOfficeIdExisting=$('#taxOfficeId-preview-hidden').val();
		var RCNo=$('#RCNo-preview-hidden').val();		
		//var JTBTIN=$('#JTBTIN-hiddenFIRSTIN-hidden').val();
		var JTBTIN=$('#JTBTIN-preview-hidden').val();
		//var oldAddress=$('#Address').val();
		var oldAddress=$('#Address-preview-hidden').val();
		//var emailRegEx=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		var emailRegEx=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		$("#submit-registration-form").html("Please Wait..")
		$("#submit-registration-form").attr("disabled","disabled");
		
		debugger;
		//Codes for validating PEOPLETYPE selected By User and type belongs to TINDB
		var peopleTypeObj = new Object();
		peopleTypeObj["C"] = "CORPORATE";
		peopleTypeObj["I"] = "INDIVIDUAL";
		peopleTypeObj["F"] = "FOREX";
		
		if(peopleTypeObj[taxPayerTypeFromTINDB] !== applicanttype){
			var errorMsg = getPeopleTypeMismatchError(taxPayerTypeFromTINDB,applicanttype);
			alert(errorMsg,"error");
			return;
		}
		
		registrationdata={"command":"changePassword","tinNumber":tinNumber,"peopleType":peopleType,"name":name,
						  "taxOffice":taxOffice,"email":email,"phone":phone,"newTaxOffice":newTaxOffice,"newEmail":newEmail,
						  "newMobile":newMobile,"newAddress":newAddress,"RCNo":RCNo,"JTBTIN":JTBTIN,
						  "oldAddress":oldAddress,"taxOfficeId":taxOfficeId,"taxOfficeIdExisting":taxOfficeIdExisting,
						  "newdateofincor":newdateofincor,"applicanttype":applicanttype};
		
		if(applicanttype != 'INDIVIDUAL' && newdateofincor == ''){
			$("#submit-registration-form").html("Submit")
			$("#submit-registration-form").removeAttr("disabled","disabled");
			alert("Please enter date of incorporation","error");
		}
		
		else 
			{
			if(email !=null && email !=undefined && email !="" && emailRegEx.test(email)){ /*||email ==""*/
			url="RegistrationAjax.jsp";		
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				async:true,
				data:registrationdata,
				success: function(res){
					if(res.Success){
						alert("Registration successful.", "success");
						$("#submit-registration-form").html("Submit")
						$("#submit-registration-form").removeAttr("disabled","disabled");
						$('#RegistrationPreviewDiv').addClass("hide");
						$('#application-number').html(res.peopleRSN);
						$('#registration-sucess-info').removeClass("hide");
						$('#RegistrationFormDiv').addClass("hide");
						//alert("Registration Successful.Your application no. is :"+res.folderRSN);
					   //goToBackPage(contextPath);
					}else{
						$("#submit-registration-form").html("Submit")
						$("#submit-registration-form").removeAttr("disabled","disabled");
						alert(res.errorMessage,"error");
						resetPrevDetails();
					}
		   		},
				error: function(res){
					$("#submit-registration-form").html("Submit")
					$("#submit-registration-form").removeAttr("disabled","disabled");
					resetPrevDetails();
					alert('Some Error occured !!!!!');
				}
			});	
		}else{
			$("#submit-registration-form").html("Submit")
			$("#submit-registration-form").removeAttr("disabled","disabled");
			alert("Email Address not Found! Contact Your Tax Office","error");
		}
			}
		
		});
		
});

function resetNewDetails(){
	$('#mobile-new').val('');
	$('#tax-office-new').val($("#tax-office-new option:first").val());
	$('#email-new').val('');
	$('#address-new').val('');
	$("#confirm-registration").prop("checked", false);

}

function resetPrevDetails(){
	$('#name-preview').html('');
	$('#tax-office-preview').val('');
	$('#email-perview').val('');
	$('#phone-preview').val('');
	//$('#invalidTINMsg').addClass("hide");
	$('#preview-section-reg').addClass("hide");
	$("#confirm-registration").prop("checked", false);
	$('#dateofinc-perview-hidden').val('');

}

function goToBackPage(){
	window.history.back();
}

//Added by Samarjit:
//Setting of data from input fields into preview screen
function previewRegistration(){
	$.each(formDataJson, function(k, v){
		$("."+k).text(v);
	})
	$("#RegistrationFormDiv").hide();
	$("#RegistrationPreviewDiv").removeClass("hide");
	$("#RegistrationPreviewDiv").fadeIn();
}

function getPeopleTypeMismatchError(taxPayerTypeFromTINDB,applicantTypeByUser){
	var errorMsg 	= "";
	var peopleType 	= "";
	
	switch(taxPayerTypeFromTINDB) {
	    case "C":
	    	peopleType = "Corporate"
	        break;
	    case "I":
	    	peopleType = "Individual"
	        break;
	    case "F":
	    	peopleType = "Forex"
	        break;
	    default:
	    	//Default Message.
	}
	
	errorMsg = "The TaxPayerType found as "+peopleType+", Please select "+peopleType+" type during registration to get your TCC account";
	$("#submit-registration-form").html("Submit")
	$("#submit-registration-form").removeAttr("disabled","disabled");
	resetPrevDetails();
	
	return errorMsg;
}


