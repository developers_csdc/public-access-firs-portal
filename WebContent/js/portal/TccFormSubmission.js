/**
 * 
 */

var formSubmittedFlag = false;

function form_submit(){
	var error=0;
	var reg_type=$('input[name=registration_type]:checked').val();
	var tax_id_no = $("#tax_id").val();
	var email =  $("#email").val();
	var phone_no = $("#phone").val();
	/*Extracting phone number*/
	//phone_no = phone_no.substring(6,phone_no.length).split(" ").join("");
	
	var updatePhone_no = $("#tccUpdate_phone").val();
	//updatePhone_no = updatePhone_no.substring(6,updatePhone_no.length).split(" ").join("");
	var updateEmail = $("#tccUpdate_email").val();
	var tccUpdateTaxOfficeName = $("#tccUpdate_taxOfficeName").val();
	var tccUpdateAddress = $("#tcc_updateAddress").val();
	
	var trade_nature= $("#trade_nature").val();
	
	var turnover_company= $("#turnover").val();
	var turnover_year_1= $("#turnover-year-1").val();
	var turnover_year_2= $("#turnover-year-2").val();
	var turnover_year_3= $("#turnover-year-3").val();
	
	var oldtcc_num= $("#oldtcc_num").val();
	var tcc_date= $("#tcc_date").val();
	
	var pre_year_check='';//$('input[name=pre_year_check]:checked').val();
	
	var year_affected=$("#year").val();
	var amount=$("#amount").val();
	
	var amount_check=$('input[name=amount_check]:checked').val();
	
	var type_arrng=$("#arrange_type").val();
	
	var other_doc=$("#other_doc").val();
	
	var folderType = "";
	var taxPayerTypeHidden =$("#taxPayerTypeHidden").val();
	
	if(taxPayerTypeHidden == 3){
		folderType = "TCC";
	}
	
	if(taxPayerTypeHidden == 4){
		folderType = "ITCC";
	}
	
	if(taxPayerTypeHidden == 5){
		folderType = "FTCC";
	}
	console.log("This is folderType " + folderType);
	
	var tin = $("#tin").val();
	
	if(tin == ""){
		alert("Error Occured, please try again, and if this persists, kindly contact eservice support");
		return false;
	}
	
	var tccformData = {
		"operation" : "formregister",
		"folderType" : folderType,
		"reg_type" : reg_type,
		"tax_id_no" : tax_id_no,
		"email" : email,
		"phone_no" : phone_no,
		"trade_nature" : trade_nature,
		"turnover_company" : turnover_company,
		"oldtcc_num" : oldtcc_num,
		"tcc_date" : tcc_date,
		"pre_year_check" : pre_year_check,
		"year_affected" : year_affected,
		"amount" : amount,
		"amount_check" : amount_check,
		"type_arrng" : type_arrng,
		"other_doc" : other_doc,
		"updatePhone_no" : updatePhone_no,
		"updateEmail" : updateEmail,
		"tccUpdateTaxOfficeName" : tccUpdateTaxOfficeName,
		"tccUpdateAddress" : tccUpdateAddress,
		"turnover_year_1" : turnover_year_1,
		"turnover_year_2" : turnover_year_2,
		"turnover_year_3" : turnover_year_3,
		"tin":tin
	};
	for(var key in tccformData){
		tccformData[key]=isValueNull(tccformData[key]);
	}
	
	
	setLoadingState(true);
	$.ajax({
		//url: "TccApplicationFormAjax.jsp",
		url: "RegistrationHandler.jsp",
		type: "POST",
		dataType: "json",
		data: checkForSupportingDocumentUpload()?prepareFileUpload():tccformData,
		processData: checkForSupportingDocumentUpload()?false:true,
		contentType: checkForSupportingDocumentUpload()?false:"application/x-www-form-urlencoded; charset=UTF-8",
		success: function(res){
				setLoadingState(false);
				if(res.Success == true){
					formSubmittedFlag = true;
					localStorage.setItem('applicationFlag', true);
					tccFormSubmitSuccess = true;
					var applicationNumber = res.applicationNumber;
					$("#successApplicationNumberDisplay").text(applicationNumber);
					document.getElementById("tccapp_form").style.display="none";
					$("#ConfirmationResponseStatus").removeClass("hide");
					console.log("This is success " + res);
			   }else{
				   alert("Something went wrong1 ","error");
				   console.log("This is error " + res);
			   }
   		},
   		error:function(xhr){
   			alert("Error occured during submission! Please Try Again","error");
   			console.log("This block run " + res);
   			console.log(xhr.status+" "+xhr.statusText);
   		}
	});
	function prepareFileUpload(){
		var fileUploadData = new FormData();
		var $fileContainer=$("#support_copy");
		if($fileContainer[0] && $fileContainer[0].files.length>0){
			var file = $fileContainer[0].files[0];
			fileUploadData.append("operation","formregister");
			fileUploadData.append("reg_type",tccformData.reg_type);
			fileUploadData.append("tin",tccformData.tin);
			fileUploadData.append("tax_id_no",tccformData.tax_id_no);
			fileUploadData.append("email",tccformData.email);
			fileUploadData.append("phone_no",tccformData.phone_no);
			fileUploadData.append("trade_nature",tccformData.trade_nature);
			fileUploadData.append("turnover_company",tccformData.turnover_company);
			fileUploadData.append("turnover_year_1",tccformData.turnover_year_1);
			fileUploadData.append("turnover_year_2",tccformData.turnover_year_2);
			fileUploadData.append("turnover_year_3",tccformData.turnover_year_3);
			fileUploadData.append("oldtcc_num",tccformData.oldtcc_num);
			fileUploadData.append("tcc_date",tccformData.tcc_date);
			fileUploadData.append("pre_year_check",tccformData.pre_year_check);
			fileUploadData.append("year_affected",tccformData.year_affected);
			fileUploadData.append("amount",tccformData.amount);
			fileUploadData.append("amount_check",tccformData.amount_check);
			fileUploadData.append("type_arrng",tccformData.type_arrng);
			fileUploadData.append("other_doc",tccformData.other_doc);
			fileUploadData.append("support_copy",file);
			fileUploadData.append("updatePhone_no",tccformData.updatePhone_no);
			fileUploadData.append("updateEmail",tccformData.updateEmail);
			fileUploadData.append("tccUpdateTaxOfficeName",tccformData.tccUpdateTaxOfficeName);
			fileUploadData.append("tccUpdateAddress",tccformData.tccUpdateAddress);
			fileUploadData.append("folderType",tccformData.folderType);
		}
		return fileUploadData;
	}
	
}
/*Helper methods for attachment upload*/
function isValueNull(value){
	if(value!=null){
		return value.trim();
	}
	return '';
}
function checkForSupportingDocumentUpload(){
	return($('input[name=amount_check]:checked').val()=="true");
}

/*Helper methods for attachment upload -  END*/
function setLoadingState(loadingState){
	if(loadingState){
		//Hiding buttons
		$("#formButtonContainer").addClass("hide");
		$("#loader").removeClass("hide");
	}else{
		$("#formButtonContainer").removeClass("hide");
		$("#loader").addClass("hide");
	}
}

function breadcrumb_update(){
	$("#crumb1").removeClass("active");
	$("#crumb2").addClass("active");
}