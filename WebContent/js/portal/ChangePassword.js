/**
 * 
 */
$(document).ready(function(){
  
	$("#changePassword").on("click",function(){
		 var confirm_password = $("#confirm_password").val();
		    var new_password = $("#new_password").val();
		    var tin = $("#tin").val();
			
			if(tin == null){
				alert("Error Occured, please try again, and if this persists, kindly contact eservice support");
				return false;
			}
			
			if(new_password == ""){
				alert("New password Cannot be Empty", "error");
				return false;
			}
			
			if(confirm_password == ""){
				alert("Confirm password Cannot be Empty", "error");
				return false;
			}
			
			if(new_password != confirm_password){
				alert("New password and Confirm password do not match!!", "error");
				return false;
			}
			
			$.ajax({
				url:"ChangePasswordAjax.jsp",
				type:"POST",
				dataType:"json",
				data:{"operation":"changePassword",newPassword:new_password, confirmPassword:confirm_password, tin:tin},
				success:function(res){
					if(res.Success){
						alert("Password Reset Was Successful");
						window.location.href="PasswordChangedSuccessMessage.jsp";
					}else{
						alert(res.errorMessage, "error");
						$("#new_password").focus();
					}
				},error:function(){
					alert("Could not proceed due to network issue. Please try later");
				}
	          })
		
	})
		    
		
});