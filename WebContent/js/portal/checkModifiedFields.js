

	var modified = false;
	function checkModifiedFields() {
		var allInputs = $( ":input" );
		modified = false;
		for(var i =0 ; i < allInputs.length; i++){
			//	alert(allInputs[i].name);
			if(allInputs[i].type.indexOf("submit") == -1 || allInputs[i].type.indexOf("hidden") == -1){
				if(allInputs[i].type.indexOf("select") > -1 && !allInputs[i].options[allInputs[i].selectedIndex].defaultSelected && allInputs[i].id.indexOf("LicenseType") == -1 && allInputs[i].id.indexOf("LicenceStatus") == -1){
						modified = modified || true;
				}
				else if(allInputs[i].type.indexOf("select") > -1 && allInputs[i].options[allInputs[i].selectedIndex].defaultSelected && allInputs[i].id.indexOf("LicenseType") == -1 && allInputs[i].id.indexOf("LicenceStatus") == -1){
					modified = modified || false;
				}
				else if ( allInputs[i].type.indexOf("text") > -1 && allInputs[i].value != allInputs[i].defaultValue && allInputs[i].id.indexOf("date") == -1) {
					modified = modified || true;
				}
				else if( allInputs[i].type.indexOf("text") > -1 && allInputs[i].value == allInputs[i].defaultValue && allInputs[i].id.indexOf("date") == -1){
					modified = modified || false;
				}
				else if((allInputs[i].type.indexOf("checkbox") > -1 || allInputs[i].type.indexOf("radio") > -1) && allInputs[i].checked != allInputs[i].defaultChecked){
					modified = modified || true;
				}
				else if((allInputs[i].type.indexOf("checkbox") > -1 || allInputs[i].type.indexOf("radio") > -1) && allInputs[i].checked == allInputs[i].defaultChecked){
					modified = modified || false;
				}
			}	
		}	
	}