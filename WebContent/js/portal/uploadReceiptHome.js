/**
 * 
 */
$(document).ready(function(){
	$("#showUploadWHT").click(function() {
		var $chooseSectionContainer = $("#chooseSectionContainer");
		var $uploadSectionContainer = $("#uploadSectionContainer");
		var cmd = $(this).attr('id');
		var key = $(this).attr('key');
		showUploadSection($(this).attr('id'));
		$("#validateBtn").attr("key","WHT");
		$("#uploadBtn").attr("key","WHT");
	});
	
	$("#showUploadCREDIT").click(function() {
		showUploadSection($(this).attr('id'));
		$("#validateBtn").attr("key","CREDIT");
		$("#uploadBtn").attr("key","CREDIT");
	});
	
	$("#backToProfile").click(function() {
		var $chooseSectionContainer = $("#chooseSectionContainer");
		var $uploadSectionContainer = $("#uploadSectionContainer");
		
		$chooseSectionContainer.removeClass("hide");
		$uploadSectionContainer.addClass("hide");
	});
	
	$("#resetInputSection").click(function() {
		$input = $("#uploadExcelInput");
		resetInputSection($input)
	});
	
	$("#validateBtn").click(function() {
		debugger;
		var $validateBtn = $("#validateBtn");
		var $excelFile = $('#uploadExcelInput')[0].files[0];
		var cmd = "Validate"+$validateBtn.attr("key");
		
		var resData = validateExcel($excelFile);
		
		if(resData.isInputEmpty){
			alert("Input cannot left Blank",'error');
			return;
		}else if(!resData.isExcelValid){
			alert("Invalid Input please choose an EXCEL File",'error');
			$('#uploadExcelInput').val("");
			return;
		}
		
		loaderController("Validating...","show");
		
		var formData = new FormData();
		formData.append('cmd', cmd);
		// Attaching Excel here
		formData.append('excel', $('input[type=file]')[0].files[0]);
		var url = "UploadReceiptHomeAJAX.jsp";
		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:formData,
			contentType: false,
		    processData: false,
			success: function(res){
				loaderController("","hide");
				debugger;
				if(res.isExceptionOccured){
					//$("#uploadExcelInput").val("");
					alert("Exception Error",'error');
					return;
				}
				
				if(!res.isExcelContainsData){
					$("#uploadExcelInput").val("");
					alert("Your Excel do not have Data",'error');
					return;
				}
				
				if(res.isExcelDataExceedsLimit){
					$("#uploadExcelInput").val("");
					alert("Your Excel Exceeding MAX ROW LIMIT of 50",'error');
					return;
				}
				
				if(res.isExcelDataExceedsLimit){
					$("#uploadExcelInput").val("");
					alert("Your Excel Exceeding MAX ROW LIMIT of 50",'error');
					return;
				}
				
				alert("A valid Excel",'success');
				$("#validateBtn").attr("disabled","disabled");
				$("#uploadBtn").removeAttr("disabled","disabled");
				
	   		},
			error: function(res){
				console.log(res);
				loaderController("","hide");
				alert("Error",'error');
			}
		});
	});
	
	$("#uploadBtn").click(function() {
		debugger;
		var $uploadBtn = $("#uploadBtn");
		var $excelFile = $('#uploadExcelInput')[0].files[0];
		var cmd = "Upload"+$uploadBtn.attr("key");
		
		var resData = validateExcel($excelFile);
		
		if(resData.isInputEmpty){
			alert("Input cannot left Blank",'error');
			return;
		}else if(!resData.isExcelValid){
			alert("Invalid Input please choose an EXCEL File",'error');
			$('#uploadExcelInput').val("");
			return;
		}
		
		loaderController("Uploading...","show");
		
		var formData = new FormData();
		formData.append('cmd', cmd);
		// Attaching Excel here
		formData.append('excel', $('input[type=file]')[0].files[0]);
		var url = "UploadReceiptHomeAJAX.jsp";
		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:formData,
			contentType: false,
		    processData: false,
			success: function(res){
				loaderController("","hide");
				debugger;
				alert("Uploaded and Reports has been Generated",'success');
	   		},
			error: function(res){
				console.log(res);
				loaderController("","hide");
				alert("Error",'error');
			}
		});
	});
})

function showUploadSection(cmd){
	var $chooseSectionContainer = $("#chooseSectionContainer");
	var $uploadSectionContainer = $("#uploadSectionContainer");
	
	$chooseSectionContainer.addClass("hide");
	$uploadSectionContainer.removeClass("hide");
}

function resetInputSection($input){
	$input.val("");
	//$input.replaceWith( $input = $input.clone( true ) );
}

function validateExcel($excelFile){
	var validationData = new Object();
	var isExcelValid = false;
	var isInputEmpty = true;
	
	if($excelFile == undefined){
		console.log("Empty Input detected");
	}else{
		isInputEmpty = false;
		if($excelFile.type == "application/vnd.ms-excel"){
			console.log("EXCEL detected");
			isExcelValid = true;
		}
		console.log($excelFile);
	}
	validationData["isExcelValid"] = isExcelValid;
	validationData["isInputEmpty"] = isInputEmpty;
	
	return validationData;
}

function loaderController(loadingText,cmd){
	$loaderContainer = $("#loaderContainer");
	$loadingTextContainer = $("#loadingTextContainer");
	$loadingText = $("#loadingText");
	
	if(cmd == "show"){
		$loaderContainer.removeClass("hide");
		$loadingTextContainer.removeClass("hide");
		$loadingText.html(loadingText);
	}else if(cmd == "hide"){
		$loaderContainer.addClass("hide");
		$loadingTextContainer.addClass("hide");
		$loadingText.html(loadingText);
	}
}
