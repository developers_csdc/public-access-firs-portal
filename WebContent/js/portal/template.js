/**
 * Template
 */
var tccFormSubmitSuccess = false;
 $(document).ready(function () {
	 var dateNow = new Date();   
	 $('#ref_date,#tcc_date').datetimepicker({useCurrent:true,maxDate:new Date(), format: 'YYYY-MM-DD',dayViewHeaderFormat:'MMM-YYYY'}).val("");
	 //Setting up modal, for current path:
	 var $hrefLinks = $("a[href*='portal']");
	 $hrefLinks.on("click",pageHeaderClickListener);
 });
 
 function pageHeaderClickListener(evt){
	 var $modal = $("[data-remodal-id=website-icon]");
	 var href = $(this).attr("href");
	 //Check if current page is Apply TCC
	 var queryParams = getQueryParams();
	 evt.preventDefault();
	 if(checkIfApplyTCCPage(getQueryParams()) && !tccFormSubmitSuccess){
		 var modalInit = $modal.remodal({
			 closeOnEscape:true,
			 closeOnOutsideClick:true,
			 hashTracking:false
		 });
		 modalInit.open();
		 $(document).on('confirmation','.remodal',function(){
			 window.location = href;	
		 });
	 }else{
		 window.location = href;
	 }
	 
	 function checkIfApplyTCCPage(params){
		 if(params && params["cmd"] && params["cmd"].length>0){
			 if(params["cmd"][0]=="tccapplicationform"){
				 return true;
			 }
		 }
		 return false;
	 }
 }
 
 
 
/*$(document).ready(function() {
	 $(document).ajaxError(function(event, xhr, options, exc){
		 if(httpStatusCodes.indexOf(xhr.status) != -1){ 
			 BootstrapDialog.alert({
				title: "<i class='fa fa-exclamation-triangle'></i> "+LABEL_ALERT_BOX_TITLE,
				message: MESSAGE_ALERT_AJAX_ERROR,
				type: BootstrapDialog.TYPE_INFO, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
				closable: true,
				buttonLabel: LABEL_ALERT_BOX_OK_BUTTON,
				callback: function(result) {
					$(location).attr('href',"home.jsp"); 
				}
			 });
		 }
	 });
	
	
	
	
	$('[data-toggle="tooltip"]').tooltip();
	//$("#userWelcome").tooltip();
	$("#mainmenu").removeClass("mymenu").addClass("smymenu");
	$("#spanLanguage").removeClass("langSpan").addClass("scrollLangSpan");
	$(window).scroll(function(e){
		var stp = $(window).scrollTop();
		if(stp>=20){
			$("#mainmenu").removeClass("mymenu").addClass("smymenu");
		}else{
			$("#mainmenu").removeClass("smymenu").addClass("mymenu");
		}
		//console.log($(window).scrollTop());
	});
	
	$(".nineaNumber").on({//Prevent spaces in NINEA number
		  keydown: function(e) {
		    if (e.which === 32)
		      return false;
		  },
		  change: function() {
		    this.value = this.value.replace(/\s/g, "");
		  }
	});
});
*/

 function getQueryParams(){
	var qd = {};
	location.search.substr(1).split("&").forEach(function(item) {var s = item.split("="), k = s[0], v = s[1] && decodeURIComponent(s[1]); (qd[k] = qd[k] || []).push(v)})
	return qd;
}

function phoneMask(inputName) {
	try {
		$("#"+inputName).val(changeToPhoneMask($("#"+inputName).val()));
	} catch(e) {}
}

function validPhoneMask(inputName){
	console.log("Length of input is "+$("#"+inputName).val().length);
	console.log("Length of phone mask format is "+phoneMaskFormat.length);
	
	if($("#"+inputName).val().length != 0 && $("#"+inputName).val().length != phoneMaskFormat.length){
		$("#"+inputName).addClass('form-control-error');
		$("#"+inputName).next('.error').text("Please enter a valid phone number.");
		$("#"+inputName).val("");
	}else{
		$("#"+inputName).removeClass('form-control-error');
		$("#"+inputName).next('.error').text("");
	}
}

function changeToPhoneMask(value){ 
	
  	var numberPattern=/[0-9]/;
  	var newValue = "";

  	for (var vId = 0, mId = 0 ; mId < phoneMaskFormat.length ; ) {
    	if (mId >= value.length)
      		break;

        // Number expected but got a different value, store only the valid portion
        if (phoneMaskFormat[mId] == phoneMaskFormatChar && value[vId].match(numberPattern) == null) {
          	break;
        }

        // Found a literal
        while (phoneMaskFormat[mId].match(phoneMaskFormatLiteralPattern) == null) {
          	if (value[vId] == phoneMaskFormat[mId])
            	break;

        	newValue += phoneMaskFormat[mId++];
      	}

      	newValue += value[vId++];
      	mId++;
	  	//alert("masked "+mId);
    }
  	console.log(newValue.length);
  	return newValue;
}

/*
$('#newRegistration').on('click', function () { 
	if(lid=="" || lid=="NotLogon"){
		//return true;
	}else{
		//alert(MESSAGE_ALERT_WHEN_LOGGED_ON);
		//return false;
	}
});

function toggleLanguageCode(languageCode){
	
	if($("#languageToggleFlag").val()=='true' || cmd =='add_ieInfoDetail' || cmd =='add_tcInfoDetail'){
		alert("Can't change language in this time. / Ne peut pas changer la langue en ce moment.");
		if(langCode==1){
			$("#langToggleswitch").prop('checked', true);
		}else if(langCode==2){
			$("#langToggleswitch").prop('checked', false);
		}
		return false;
	}
	
	var languageType =(languageCode==langCode) ? "fr_ca": "en";
	var mainUrl = "toggleLanguageAjax.jsp?languageCode="+languageCode+"&PeopleRSN="+$("#TempPeopleRSN").val()+"&lid="+lid+"&from="+from+"&"+new Date().getTime();

	$.ajax({
		url: mainUrl,
		type: "GET",
		success: function(resText){
			//$("#message").text(resText.trim());
			//$('#myModal').modal({backdrop: 'static', keyboard: false})
			//$('#myModal').modal('show');
			
			var queryStringFolderRSN = ["MyServiceDetail", "OpportunityDetail", "edit_opportunity", "ielicense_payment", "tclicense_payment", "BOPaymentDetail", "BDPaymentDetail", "FtFtsPaymentDetail", "SEDSubscriptionPaymentDetail"];
			var queryStringPeopleRSN = ["directoryDetail", "edit_directory", "senegal_embassy_consulate_detail", "women_professional_detail", "hotel_services_detail", "fairs_exhibitions_detail", "emp_org_trade_union_detail", "support_stru_assistance_detail", "chamber_of_commerce_detail", "business_support_org_detail", "market_research_detail", "importers_exporters_detail", "call_for_tender_detail", "documentary_database_detail"];

			$("#theform").attr("action", "portal?cmd="+cmd);
			$("<input hidden='hidden' name='from' id='from' />").attr("value", resText.split("||")[1]).prependTo("#theform");
			
			if($.inArray(cmd, queryStringFolderRSN) > -1) {
				$("<input hidden='hidden' name='folderRSN' id='folderRSN' />").attr("value", folderRSN).prependTo("#theform");
			}else if($.inArray(cmd, queryStringPeopleRSN) > -1){
				$("<input hidden='hidden' name='peopleRSN' id='peopleRSN' />").attr("value", peopleRSN).prependTo("#theform");
				$("<input hidden='hidden' name='isRequireBack' id='isRequireBack' />").attr("value", isRequireBack).prependTo("#theform");
			}
			document.theform.submit();
		}
	});
}

$(function () {
	$('a[href="#Search"]').on('click', function(event) {
		event.preventDefault();
		$('#searchPanel').addClass('open');
		$('#searchPanel > form > input[type="search"]').focus();
	});

	$('#Close').on('click keyup', function(event) {
		//if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
		$('#searchPanel').removeClass('open');
		//}
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) { // Closing modal on hitting 'Esc' Key
			$('#searchPanel').removeClass('open');
		}
	});

	//Do not include! This prevents the form from submitting for DEMO purposes only!
	$('form').submit(function(event) {
		event.preventDefault();
		return false;
	})
});

//Code for jQuery autocomplete Search - Start
$(function() {
	$( "#inputSearch" ).autocomplete({
		source: availableTags,
		response: function(event, ui) {
			if (ui.content.length === 0) {
				$("#result").text(MESSAGE_SEARCH_NO_RESULT_FOUND);
			} else {
				$("#result").empty();
			}
		},
		select: function(event, ui){
			ajaxSearch(event, ui);
		}
	});
});

function ajaxSearch(event, ui) {
	var searchbox = ui.item.value;
	$("#result").html('<center><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></center>');
	$.ajax({
		url: "SearchResult.jsp",
		method: "GET",
		data: { searchbox : searchbox },
		dataType: "html",
		context: document.body
	}).done(function (data) { 
		$("#result").html(data);
	});		 
}



function f_checkEmailExistInPeople(email) {
	emailExistCount = 0;
	var url = "CheckEmailExistInPeople.jsp?email=" + email;
	$.ajax({
		url : url,
		dataType : 'text',
		async : false,
		type : "POST",
		success : function(res) {
			if (res.trim() > 0) {
				emailExistCount = 1;
			}

		}
	});
	return emailExistCount;
}



function f_checkEmail(email){
	  var testresults;
	  var str = email;
	  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

	  if (filter.test(str)){
	    testresults=true;
	  }else{
	    testresults=false;
	  }

	  return testresults;
	}




function onlyNos(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
}

function checkNum(eleObj){
	var regex = /[0-9]|\./;
	if($(eleObj).val() != "" && !regex.test($(eleObj).val())) {
		$(eleObj).val("");
		alert("Please Enter number only. / Veuillez saisir uniquement le numéro.");
	}
}

function isValidDate(str){
	// STRING FORMAT dd-mm-yyyy
	if(str=="" || str==null){return false;}								
	
	// m[1] is year 'DD' * m[2] is month 'MM' * m[3] is day 'YYYY'					
	var m = str.match(/(\d{2})-(\d{2})-(\d{4})/);

	
	// STR IS NOT FIT m IS NOT OBJECT
	if( m === null || typeof m !== 'object'){return false;}				
	
	// CHECK m TYPE
	if (typeof m !== 'object' && m !== null && m.size!==3){return false;}
				
	var ret = true; //RETURN VALUE						
	var thisYear = new Date().getFullYear(); //YEAR NOW
	var minYear = 1900; //MIN YEAR
	
	// YEAR CHECK
	if( (m[3].length < 4) || m[3] < minYear || m[3] > thisYear){
		ret = false;
	}
	// MONTH CHECK			
	if( ret && (m[2].length < 2) || m[2] < 1 || m[2] > 12){
		ret = false;
	}
	// DAY CHECK
	if(ret){
		var maxDay = 31;
		if(m[2] == 4 || m[2] == 6  || m[2] == 9 || m[2] == 11)	maxDay = 30;
		if(m[2] == 2){
			maxDay = 28;
			if(m[3]%4==0) maxDay = 29;	
		}
		if((m[1].length < 2) || m[1] < 1 || m[1] > maxDay){
			ret = false;
		}
	}
	
	return ret;			
}



//Code for jQuery autocomplete Search - End

function f_loadSubServices(id){
	window.location.href='home.jsp?subservice='+id+'&sectionId=features';
	window.location.href='home.jsp#features';
}
//Opening sub-menu on clicking menu booton in Feature section
function f_loadSubServices(id){
//	1 - BO, 2 - IE Cards, 3 - Trade cards, 4 - BD, 5- FT(Foreign Trade), 6-FTS(Foreign trade Statistics), 7- SED(Socio Economic Database)
	console.log('subser:'+id);
	f_hideSubServices();
	if(id==1){
		$("#TPSServiceList").css("display","none");
		$("#BOSubServiceList").css("display","");
		//alert("Showing sublist of services.");
	}else if(id==2){
		$("#TPSServiceList").css("display","none");
		$("#IESubServiceList").css("display","");
	}else if(id==3){
		$("#TPSServiceList").css("display","none");
		$("#TCSubServiceList").css("display","");
	}else if(id==4){
		$("#TPSServiceList").css("display","none");
		$("#BDSubServiceList").css("display","");
	}else if(id==5){
		$("#TPSServiceList").css("display","none");
		$("#FTSubServiceList").css("display","");
	}else if(id==6){
		$("#TPSServiceList").css("display","none");
		$("#FTSSubServiceList").css("display","");
	}else if(id==7){
		window.location.href='portal?cmd=sedsearch';
	}else if(id==99){
		$("#TPSServiceList").css("display","");
		f_hideSubServices();
	}
	else{
		alert("Implementation for this service is in progress.");
	}
	subserviceid = id;

}

function f_hideSubServices(){
	$("#BOSubServiceList").css("display","none");
	$("#IESubServiceList").css("display","none");
	$("#TCSubServiceList").css("display","none");
	$("#BDSubServiceList").css("display","none");
	$("#FTSubServiceList").css("display","none");
	$("#FTSSubServiceList").css("display","none");
}

//Expanding menu and submenu on mouse hover
	 $(document).ready(function(){
		$(".dropdown-toggle").hover(function(){
			$(this).toggleClass("active-toggle");
			$(this).next('.d-menu').toggle();
		});
		$(".dropdown-toggle").next('.d-menu').hover(function(){
			$(this).toggle();
		});		
	});
	 

		
	if(language == "fr"){
		$("#staticPageContent2").removeClass('hidden');
		$("#staticPageContent").addClass('hidden');
	}else{
		$("#staticPageContent").removeClass('hidden');
		$("#staticPageContent2").addClass('hidden');
	}*/