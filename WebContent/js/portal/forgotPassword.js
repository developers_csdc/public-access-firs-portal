/**
 FIRS Forgot Password Section has been start
 */	
$(document).ready(function(){
	
	$("#forgotPassword-submit").click(function(){
		
		$("#TIN").attr("disabled","disabaled");
		$("#forgotPassword-submit").attr("disabled","disabaled");
		$("#forgotPassword-submit").html("Please Wait..");
		
		var TIN = $("#TIN").val();
		if(TIN!=undefined && TIN!=""){
		
		var url = 'forgotPasswordAjax.jsp';
		var tinData;
		
		tinData={"command":"peopleChecK","TIN":TIN};	
		$("#TIN-Preview").val(TIN);
		$("#TINPreview").val(TIN);
		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:tinData,
			success: function(res){
				if(res.isTINValid){
					if(res.mailExist){
					$("#TIN").removeAttr("disabled","disabaled");
					$("#forgotPassword-submit").removeAttr("disabled","disabaled");
					$("#forgotPassword-submit").html("Submit");
					$("#TIN").val("");
					$('#forgotPassword-modal').modal('hide');
					$('#forgotPasswordOTP-modal').modal({backdrop: 'static', keyboard: false,show: 'false'});
					countdown(10);
					}else{
						alert("Email doesnot Exsist for this account","error");
						$("#TIN").val("");
						$("#TIN").removeAttr("disabled","disabaled");
						$("#forgotPassword-submit").removeAttr("disabled","disabaled");
						$("#forgotPassword-submit").html("Submit");
					}
				}else{
					alert(res.errorMessage,"error");
					$("#TIN").val("");
					$("#TIN").removeAttr("disabled","disabaled");
					$("#forgotPassword-submit").removeAttr("disabled","disabaled");
					$("#forgotPassword-submit").html("Submit");
				}
			},
			error: function(res){
				alert("Some error occured!");
				$("#TIN").val("");
				$("#TIN").removeAttr("disabled","disabaled");
				$("#forgotPassword-submit").removeAttr("disabled","disabaled");
				$("#forgotPassword-submit").html("Submit");
			}
		});
		
	}else{
		alert("TIN cannot be left Blank","error");
		$("#forgotPassword-submit").html("Submit");
		$("#forgotPassword-submit").removeAttr("disabled","disabaled");
		$("#TIN").removeAttr("disabled","disabaled");
		$("#TIN").val("");
		};
	});
	
	/*checking OTP*/
	$("#OTP-Submit").click(function(){
		
		$("#OTP").attr("disabled","disabaled");
		$("#OTP-Submit").attr("disabled","disabaled");
		$("#OTP-Submit").html("Please Wait..");
		
		var OTP = $("#OTP").val();
		if(OTP!=undefined && OTP!=""){
		
		var url = 'forgotPasswordAjax.jsp';
		var OTPData;
		
		OTPData={"command":"checkOTP","OTP":OTP};	
		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:OTPData,
			success: function(res){
				if(res.isOTPValidFlag){
					$('#forgotPasswordOTP-modal').modal('hide');
					$('#changePassword-modal').modal({backdrop: 'static', keyboard: false,show: 'false'});
				}else{
					if(res.ErrorMsg == "Invalid OTP"){
						alert("Invalid OTP","error");
					}else{
						alert("Incorrect OTP","error");
					}
				}
				$("#OTP").removeAttr("disabled","disabaled");
				$("#OTP-Submit").removeAttr("disabled","disabaled");
				$("#OTP-Submit").html("Submit");
				$("#OTP").val("");
			},
			error: function(res){
				alert("Some error occured!Please try Again");
				$("#OTP").removeAttr("disabled","disabaled");
				$("#OTP-Submit").removeAttr("disabled","disabaled");
				$("#OTP-Submit").html("Submit");
				$("#OTP").val("");
			}
		});
		
	}else{
		alert("OTP cannot be left Blank","error");
		$("#OTP").removeAttr("disabled","disabaled");
		$("#OTP-Submit").removeAttr("disabled","disabaled");
		$("#OTP-Submit").html("Submit");
		$("#OTP").val("");
		};
	});
	
	/*Re-sending OTP*/
	
	$("#resend-OTP").click(function(){
		
		$("#resend-OTP").html("Sending OTP..");
		$("#resend-OTP").attr("disabled","disabaled");
		$("#OTP-Submit").attr("disabled","disabaled");
		
		var url = 'forgotPasswordAjax.jsp';
		var OTPData;
		var TIN = $("#TIN-Preview").val();
		
		resendOTPData={"command":"resendOTP","TIN":TIN};	
		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:resendOTPData,
			success: function(res){
				if(res.result){
					$("#counter").removeClass("hide");
					stopTimer();
					$('#counter').html("");
					countdown(10);
					alert("New OTP sends successfully to your email.Please check","success");
					$("#OTP").removeAttr("disabled","disabaled");
					$("#resend-OTP").removeAttr("disabled","disabaled");
					$("#OTP-Submit").removeAttr("disabled","disabaled");
					$("#resend-OTP").html("Re-Send OTP");
				}else{
					alert("OTP Resend Fails","error");
				}
				
			},
			error: function(res){
				alert("Some error occured!");
			}
		});
	
	});
	
	/*Update Password Section*/
	$("#updatePassword-submit").click(function(){
		
	$("#updatePassword-submit").html("Please Wait..");
	$("#updatePassword-submit").attr("disabled","disabaled");
	
	var Password = $('#new-password').val();
	var confirmPassword = $('#confirm-password').val();
	var TIN = $("#TINPreview").val();
		
	if((Password!=undefined && Password!="" || confirmPassword!=undefined && confirmPassword!="")&&  passwordCheck(Password,confirmPassword)){
		var url = 'forgotPasswordAjax.jsp';
		var passwordData;
		
		passwordData={"command":"resetPassword","newPassword":Password,"confirmPassword":confirmPassword,"TIN":TIN};	
		
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:passwordData,
			success: function(res){
				if(res.result){
					alert("Password reset Sucessfully!","success");
					$("#updatePassword-submit").html("Submit");
					$("#updatePassword-submit").removeAttr("disabled","disabaled");
					$('#new-password').val("");
					$('#confirm-password').val("");
					$('#changePassword-modal').modal('hide');
				}else{
					alert("Password reset Unsucessful!","error");
					$("#updatePassword-submit").html("Submit");
					$("#updatePassword-submit").removeAttr("disabled","disabaled");
					$('#new-password').val("");
					$('#confirm-password').val("");
				}
			},
			error: function(res){
				alert("Some error occured!Please Try Again");
				$("#updatePassword-submit").html("Submit");
				$("#updatePassword-submit").removeAttr("disabled","disabaled");
				$('#new-password').val("");
				$('#confirm-password').val("");
			}
		});
		
	}else{
		alert("Password Mismatch","error");
		$("#updatePassword-submit").html("Submit");
		$("#updatePassword-submit").removeAttr("disabled","disabaled");
	};
	});
});

/*Function for Timer - JS*/
var timerObject;
function countdown(minutes) {
    var seconds = 60;
    var mins = minutes
    function tick() {
        var counter = document.getElementById("counter");
        var current_minutes = mins-1
        seconds--;
        counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
        if(current_minutes == 00 && seconds == 00){
        	$("#OTP").attr("disabled","disabaled");
        	$("#OTP-Submit").attr("disabled","disabaled");
        	$("#counter").addClass("hide");
        	alert("OTP is Invalid Please Re-Send OTP","error");
        }
        if( seconds > 0 ) {
        	timerObject = setTimeout(tick, 1000);
        } else {
            if(mins > 1){
                countdown(mins-1);           
            }
        }
    }
    tick();
}
function stopTimer(){
	 if (timerObject) {
         clearTimeout(timerObject);
         timerObject = 0;
     }
}

function passwordCheck(Password,confirmPassword){
	if(password==confirmPassword){
		return true;
	}return false;
}

