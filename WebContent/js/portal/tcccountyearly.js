/**
 * 
 */

function createReport(argYear){
	$.ajax({
 		url: "getJsonData.jsp?argYear="+argYear,
 		type: "POST",
 		dataType: "json",
 		success: function(response){
 			var chartData = generateChartData(response.data); 			
 			generateHighChart(chartData,argYear);
 		}
 	});
 	
 	//it expects a list of objects with attributes name, value, id, drilldown(array of arrays)
 	
 	function generateChartData(list){ 		
 		var output = new Object();
 		output.series = [{
 			name: 'TCC COUNT',
 			colorByPoint: true,
 			data: getAllSeries(list)
 		}];
 		output.drilldown = {
 			series: getAllDrillDownSeries(list)	
 		};
 		return output;
 		/*Helpers*/
 		function getAllSeries(items){
 			var output = new Array();
 			for(var item in items){
 				var temp = items[item];
 				output.push(new SeriesElement(temp["name"],temp["value"],temp["id"]));
 			}
 			return output;
 		}
 		function getAllDrillDownSeries(items){
 			var output = new Array();
 			for(var item in items){
 				//var temp = items[item];
 				var temp = items[item];
 				output.push(new DrillDownElement(temp["name"],temp["id"],temp["drilldown"]));
 			}
 			return output;
 		}
 		
 		/*Object generator*/
 		function SeriesElement(name,value,id){
 			this.name = name;
 			this.y = Number(value);
 			this.drilldown = id;
 		}
 		function DrillDownElement(name,id,drilldown){
 			this.name = name;
 			this.id = id;
 			this.colorByPoint = true;
 			this.data = drilldown;
 		}
 	}
 	function generateHighChart(chartData, argYear){ 
 		Highcharts.setOptions({lang: {noData: "No Data Found"}}); 		
 		//var chart =Highcharts.chart('container', {
 		Highcharts.setOptions({
 		    lang: {
 		        drillUpText: 'Back to {series.name}'
 		    }
 		});
 		var options = {
 		    chart: {
 		        type: 'column'
 		    },
 		    title: {
 		        text: 'Tax Office TCC Count For Year '+argYear
 		    },
 		    subtitle: {
 		        text: 'Source: FIRS TCC Count Tax Office Wise'
 		    },
 		    xAxis: {
 		        type: 'category',
 		        crosshair: true
 		    },
 		    yAxis: {
 		        min: 0,
 		        title: {
 		            text: 'TCC Count'
 		        }
 		    },

 		    legend: {
 		        enabled: true
 		    }, 		    

 		    plotOptions: {
 		        series: {
 		            borderWidth: 0,
 		            dataLabels: {
 		                enabled: true
 		            },
 		           shadow: false
 		        },
 		       pie: {
		            size: '100%'
		        },
 		        column: {
 		            pointPadding: 0.2,
 		            borderWidth: 0
 		        }
 		    },

 		    series: chartData.series,
 		    drilldown: {
 		    	drillUpButton: {
 		            relativeTo: 'spacingBox',
 		            position: {
 		            	y: 0,
 		                x: -50
 		            },
 		            theme: {
 		                fill: 'white',
 		                'stroke-width': 1,
 		                stroke: 'silver',
 		                r: 0,
 		                states: {
 		                    hover: {
 		                        fill: '#a4edba'
 		                    },
 		                    select: {
 		                        stroke: '#039',
 		                        fill: '#a4edba'
 		                    }
 		                }
 		            }

 		        },
 		    	series: chartData.drilldown.series
 		    },
 		   exporting: {
 		    	csv: {
 		        	columnHeaderFormatter: function (series, key) { 		        		
 		        		if(key=='y')
 		            	return 'TCC COUNT';
 		        		else{
 		        		return 'TAX OFFICE/MONTHS';
 		        		}
 		            }
 		        }
 		    }
 		    
 		//}); 		    
 		};
 		
 		// Column chart
 		options.chart.renderTo = 'container1';
 		options.chart.type = 'column';
 		var chart1 = new Highcharts.Chart(options);

 		// Pie
 		options.chart.renderTo = 'container2';
 		options.chart.type = 'pie';
 		var chart2 = new Highcharts.Chart(options);
 		
 	}
	// Create the chart	
 	
}

/*$(document).ready(function() {
 	
});*/

function generateReport(){
	var reportYear=$('#reportDate').val();
	if(reportYear==''){
		alert('Please select Year to proceed.');
	}else{
		createReport(reportYear);
	}
}