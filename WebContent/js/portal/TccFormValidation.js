var inputCheckFlag = false, formSubmitFlag = false;
$(document).ready(function(){
	var blockSubmit = false;
	var tccReady = false;
	updateVerifyTCCAuthority(false);
	$("#pre_year_uncheck").click(function() {
		$("#year").removeAttr("disabled");
		$("#amount").removeAttr("disabled");
		$("#year").attr("data-required","required");
		$("#amount").attr("data-required","required");
	});
	$("#pre_year_check").click(function() {
		$("#year").val("");
		$("#amount").val("");
		$("#year").attr("disabled","disabled");
		$("#amount").attr("disabled","disabled");
		$("#year").removeAttr("data-required","required");
		$("#amount").removeAttr("data-required","required");
		$("#year").next(".error").html("");
		$("#amount").next(".error").html("");
		$("#year").removeClass("form-control-error");
		$("#amount").removeClass("form-control-error");
		
		
	});
	
	$("#amount_out_check").click(function(){
		$("#spc_arg").removeClass("hide");
		$("#arrange_type").attr("data-required","required");
	});
	
	$("#amount_out_uncheck").click(function(){
		$("#spc_arg").addClass("hide");
		$("#arrange_type").removeAttr("data-required","required");
		$("#arrange_type").val("");
		$("#support_copy").val("");
		$("#support_copy_preview").text("");
	});
	
	$("#confirm_form").click(function(){
		{
		if($("#confirm_form").prop("checked") == true) {   
		$("#submit_form").attr('disabled',false);
		}else{
		 $("#submit_form").attr('disabled',true);
			}
		}
	});
	var loader = document.createElement("i");
	
	$("#verifyTCCBtn").on("click",function(){
		var $el = $("#oldtcc_num");
		var value = $el.val();
		var $btn = $(this);
		var $loader = $("#verifyTCCLoader");
		if(tccReady){
			console.log("tcc ready")
			resetTCC();
		}else{
			if(value!=null && value!="" && validateFormElement(value,$el.attr("pattern"))==null){
				blockSubmit = true;
				$loader.removeClass("hide");
				$el.attr("disabled",true);
				$btn.attr("disabled",true);
				$.ajax({
					url:"TccApplicationFormAjax.jsp",
					type:"POST",
					dataType:"json",
					data:{"operation":"verifyTCC",tcc:value},
					success:function(res){
						$loader.addClass("hide");
						$btn.attr("disabled",false);
						blockSubmit = false;
						if(res.Success){
							$("#tcc_date").val(res.issueDate);
							tccReady = true;
							$("#verifyTCCText").text("Reset");
						}else{
							$el.attr("disabled",false);
							$("#tcc_date").val("");
							$("#oldtcc_num").val("");
							$("#verifyTCCText").text("Verify");
							alert(res.errorMessage, "error");
							$("#oldtcc_num").focus();
						}
					},error:function(){
						$("#verifyTCCText").text("Verify");
						$el.attr("disabled",false);
						$loader.addClass("hide");
						$el.attr("disabled",false);
						$("#tcc_date").val("");
						$("#oldtcc_num").val("");
						alert("Could not proceed due to network issue. Please try later");
						blockSubmit = false;
					}
				});
			}
		}
	});
	function resetTCC(){
		tccReady = false;
		var $el = $("#oldtcc_num");
		var $loader = $("#verifyTCCLoader");
		$el.attr("disabled",false);
		$("#verifyTCCText").text("Verify");
		$("#verifyTCCText").text("Verify");
		$el.attr("disabled",false);
		$loader.addClass("hide");
		$el.attr("disabled",false);
		$("#tcc_date").val("");
		$("#oldtcc_num").val("");
	}
	/*-------------Handling File Upload for attachment--------------------------*/
	$("#support_copy").on("change",function(){
		var $fileContainer=$(this);
		if($fileContainer[0] && $fileContainer[0].files.length>0){
			var file = $fileContainer[0].files[0];
			if(file instanceof File && (file.size/1024/1024)<2){
				$("#support_copy_preview").text(file.name);
				$("#support_copy").next(".error").text("");
			}else{
				$("#support_copy").next(".error").html("Please do not exceed file size limit: 2 MB.");
				$("#support_copy").val("");
			}
		}
	});
	
	/*-------------------------------------------------------------------------*/
	setFormInfoStage("init");
	$("#searchInfoByTIN").on("click",function(){
		var $taxId=$("#tax_id");
		if(validateFormElement($taxId.val(),$taxId.attr("pattern"))==null && $taxId.val()!=''){
			$taxId.removeClass("form-control-error");
			$taxId.next(".error").text("");
			setFormInfoStage("getDataByTINInit");
			$.ajax({
				url: "TccApplicationFormAjax.jsp",
				type: "POST",
				dataType: "json",
				data:{"operation":"getDataByTIN","taxID":$taxId.val()},
				success:function(res){
					if(res.Success){
						$("#entity_personal_info").removeClass("hide");
						$('input[name=registration_type]').each(function(){
							$(this).attr("checked",false);
							if($(this).val()==res.entityType){
								$(this).attr("checked",true);
							}
						});
						updateVerifyTCCAuthority(true);
						populateExistingTCCPreview(res.dataExists,res.TIN,res.entityName, res.entityType,res.mobile,res.email,res.taxOfficeName,res.address);
						$("#reg_name").val(res.entityName);
						$("#entity_preview_type").text(res.entityType);
						$("#entity_preview_name").text(res.entityName);
						setFormInfoStage("getDataByTINSuccess");
					}else{
						updateVerifyTCCAuthority(false);
						setFormInfoStage("getDataByTINFailure","",new Object(),res.errorMessage);
					}
				}
			});
			
		}else{
			$taxId.addClass("form-control-error");
			if($taxId.val()==''){
				$taxId.next(".error").text("Please provide a TIN Number.");
			}else{
				$taxId.next(".error").text("Please enter only numbers and allowed special character[-].");
			}
		}
	});
	/*Implementing reset of TIN search*/
	$("#clearTINInfo").on("click",function(){
		var $tax_ID = $("#tax_id");
		setFormInfoStage("init");
		$tax_ID.val("");
		$tax_ID.removeClass("form-control-error");
		$tax_ID.next(".error").text("");
		$("#entity_personal_info").addClass("hide");
		$("#entity_contact_info").addClass("hide");
		$("#reg_name").val("");
		$("#entity_preview_type").text("");
		$("#entity_preview_name").text("");
		updateVerifyTCCAuthority(false);
	});
	
	$("#submit_form").click(function(){
		$("#applyFormValidationAlert").addClass("hide");
		
		/*The category of payers that can apply for TCC are:
   		-Taxpayers with TIN ending with “-0001”. E.g 11223344-0001 can apply for TCC whereas 11223344-0002 should not.
  		-Individuals TINs without “-“ in the TIN (not JTBTIN).*/
	
		var TIN = $(this).attr("tin").trim();
		//var TIN = prompt("Enter TIN");
		
		if(TIN.trim().includes("-")){
			if(!TIN.trim().includes("-0001")){
				alert("TCC cannot be applied for the TIN "+TIN+". Please contact your Administrator","error");
				return;
			} 
		}
		
		/*console.log("TIN is valid");
		return;*/
		
		if(tccform_validation()){
			form_submit();
		}else{
			$("#applyFormValidationAlert").removeClass("hide");
			$("#confirm_form").prop("checked",false);
			$("#submit_form").attr("disabled",true);
			$("html, body").animate({ scrollTop: 0 }, "slow");
		}
	});
	
	$('#form_reset').click(function() {
		$("#applyTCCFormError").addClass('hide');
		$("#entity_error_message").addClass("hide");
		$("#ref_date").val("");
		$("#tax_id").val("");
		$("#email").val("");
		$("#phone").val("");
		
		$("#trade_nature").val("");
		
		$("#turnover").val("");
		
		$("#oldtcc_num").val("");
		$("#tcc_date").val("");
		
		$('input[name=pre_year_check]:checked').val();
		
		$("#year").val();
		$("#amount").val();
		
		$('input[name=amount_check]:checked').val();
		
		$("#arrange_type").val();
		$("#support_copy").val("");
		$(".error").each(function(){
			$(this).text("");
		});
		$("input").each(function(){
			if($(this).hasClass("form-control-error")){
				$(this).removeClass("form-control-error");
			}
		});
		$("#entity_personal_info").addClass("hide");
		$("#entity_contact_info").addClass("hide");
		
		$("#other_doc").val();
	    $("html, body").animate({ scrollTop: 0 }, "slow");
	    /*location.reload();*/
	});
	function updateVerifyTCCAuthority(state){
		if(!state){
			resetTCC();
			$("#verifyTCCPrerequisiteAlert").removeClass("hide");
			//$('#verifyTCCBtn').attr("disabled",true);
			//$("#oldtcc_num").attr("disabled",true);
			//$("#verifyTCCContainer").addClass("hide");
		}else{
			$("#verifyTCCPrerequisiteAlert").addClass("hide");
			$('#verifyTCCBtn').attr("disabled",false);
			$("#oldtcc_num").attr("disabled",false);
			$("#verifyTCCContainer").removeClass("hide");
		}
		$("#oldtcc_num").val("");
		$("#tcc_date").val("");
	}
});

function populateExistingTCCPreview(dataExsists,tin,name,type,mobile,email,taxOfficeName,address){
	var errorText = "No Data Found";
	var $tccPreview_name = $("#tccPreview_name");
	var $tccPreview_email = $("#tccPreview_email");
	var $tccPreview_mobile = $("#tccPreview_mobile");
	var $tccPreview_taxOfficeName = $("#tccPreview_taxOfficeName");
	var $tccPreview_address = $("#tccPreview_address");
	var $tccPreview_TIN = $("#tccPreview_TIN");
	tccPreview_TIN
	if(dataExsists){
		(tin === undefined || tin == null || tin.length <= 0)?$tccPreview_TIN.html(errorText):$tccPreview_TIN.html(tin);
		(name === undefined || name == null || name.length <= 0)?$tccPreview_name.html(errorText):$tccPreview_name.html(name);
		(email === undefined || email == null || email.length <= 0)?$tccPreview_email.html(errorText):$tccPreview_email.html(email);
		(mobile === undefined || mobile == null || mobile.length <= 0)?$tccPreview_mobile.html(errorText):$tccPreview_mobile.html(mobile);
		(taxOfficeName === undefined || taxOfficeName == null || taxOfficeName.length <= 0) ? $tccPreview_taxOfficeName.html(errorText)
				: $tccPreview_taxOfficeName.html(taxOfficeName);
		(address === undefined || address == null || address.length <= 0)?$tccPreview_address.html(errorText):$tccPreview_address.html(address);
		
	}else{
		$tccPreview_name.html(errorText);
		$tccPreview_email.html(errorText);
		$tccPreview_mobile.html(errorText);
		$tccPreview_taxOfficeName.html(errorText);
		$tccPreview_address.html(errorText);
	}
}
function tccform_validation(){
	var formPageErrorMessage="Please fill the mandatory fields!";
	var TINMessage =  null;
	var missingInputs=new Object();
	var errorCount=0;
	var formElements = {
		company_name:$("#company_name"),
		reg_address:$("#reg_addr"),
		inc_ref_no:$("#ref_no"),
		app_date:$("#ref_date"),
		tax_id_no:$("#tax_id"),
		email:$("#email"),
		phone_no:$("#phone"),
		trade_nature:$("#trade_nature"),
		turnover_company:$("#turnover"),
		turnover_company_1:$("#turnover-year-1"),
		turnover_company_2:$("#turnover-year-2"),
		turnover_company_3:$("#turnover-year-3"),
		oldtcc_num:$("#oldtcc_num"),
		tcc_date:$("#tcc_date"),
		pre_year_check:$('input[name=pre_year_check]:checked'),
		year_affected:$("#year"),
		amount:$("#amount"),
		amount_check:$('input[name=amount_check]:checked'),
		type_arrng:$("#arrange_type"),
		other_doc:$("#other_doc")
	};
	for(var key in formElements){
		if(formElements.hasOwnProperty(key) && typeof key!="function"){
			var element =  formElements[key];
			if(element.attr("data-required")=="required"){
				var value=null;
				switch(element.attr('type')){
					case "radio":{
						$('input [name='+element.attr("name")+']').each(function(){
							if($(this).prop(checked)==true){
								value =  $(this).val();
							}
						})
						break;
					}
					case "text":{
						value=element.val();
						break;
					}
					
					default:{
						//Add code for select/textarea
					}
				}
				var errorStatus=validateFormElement(value,element.attr("pattern"));
				if(errorStatus!=null){
					missingInputs[element.attr("id")]=element.attr("aria-role");
					element.next(".error").text(errorStatus);
					element.addClass('form-control-error');
					errorCount++;
				}else{
					element.next(".error").text('');
					element.removeClass('form-control-error');
				}
			}
		}
	}
	if($('input[name=amount_check]:checked').val()=="true"){
		if(!$("#support_copy")[0] || $("#support_copy")[0].files.length<=0 || !($("#support_copy")[0].files[0] instanceof File)){
			errorCount++;
			missingInputs["support_copy_trigger"]=$("#support_copy").attr("aria-role");
			$("#support_copy").next(".error").text("Please provide a file to upload");
		}
	}
	if($('#reg_name').val()==''){
		formPageErrorMessage = "Please click on \"Search\" to check your TIN before you proceed.";
		errorCount++;
	}
	if(errorCount==0){
		return true;
	}else{
		setFormInfoStage("ValidationErrorMandatory",formPageErrorMessage,missingInputs,TINMessage);
	}
	return false;
}

//Adding code to consolidate code for showing form-level alerts to user

function setFormInfoStage(state,formMessage,missingInputs,TINMessage){
	/*Currying methods to control alerts*/
	function getFormAlertByID(id){
		var $alert = $("#"+id);
		var defaultMessage= $alert.find(".alert").text();
		function showAlert(status,message){
			if(status){
				$alert.removeClass("hide");
			}else{
				$alert.addClass("hide");
			}
			$alert.find(".alert").text(message || defaultMessage);
		}
		return showAlert;
	}
	
	
	/*Currying methods for TIN buttons*/
	function getButtonControlByID(id){
		var $button = $("#"+id);
		var defaultText = $button.find(".text-content").text();
		function setButtonState(state,visibility,text){
			if(visibility){
				$button.removeClass("hide");
			}else{
				$button.addClass("hide");
			}
			$button.attr("disabled",!state);
			$button.find(".text-content").text(text || defaultText);
		}
		return setButtonState;
	}
	
	/*Currying methods for Form Elements in transitional stages during TIN search*/
	function getInputControlByID(id){
		var $input = $("#"+id);
		function setState(state){
			if($input.attr("type")=="radio" || $input.attr("type")=="checkbox"){
				$("input[name="+$input.attr("name")+"]").attr("disabled",!state);
			}else{
				$input.attr("disabled",!state);
			}
		}
		return setState;
	}
	/*Generating Methods using currying*/
	var showFormAlert = getFormAlertByID("applyFormValidationAlert");
	var showTINAlert = getFormAlertByID("entity_alert_message");
	var showTINError = getFormAlertByID("entity_error_message");
	var enableRegistrationType = getInputControlByID("registration_type_company");
	var enableTIN = getInputControlByID("tax_id");
	var enableSearchByTIN = getButtonControlByID("searchInfoByTIN");
	var enableClearSearchByTIN =  getButtonControlByID("clearTINInfo");
	/**/
	
	
	var $taxID=$("#tax_id");
	switch(state){
		case "init":{
			showFormAlert(false);
			enableRegistrationType(true);
			enableTIN(true);
			showTINError(false);
			enableSearchByTIN(true,true,"Search");
			enableClearSearchByTIN(false,false);
			showTINAlert(true,"Please use the \"Search\" button above to retrieve your information based on your Tax Identification Number");
			break;
		}
		case "getDataByTINInit":{
			showTINError(false);
			showFormAlert(false);
			showTINAlert(false);
			enableRegistrationType(false);
			enableTIN(false);
			enableSearchByTIN(false,true,"Loading..");
			enableClearSearchByTIN(false,false);
			break;
		}
		case "getDataByTINSuccess":{
			showTINError(false);
			showFormAlert(false);
			showTINAlert(false);
			enableRegistrationType(false);
			enableTIN(false);
			enableSearchByTIN(false,false,"Search");
			enableClearSearchByTIN(true,true,"Reset");
			break;
		}
		case "getDataByTINFailure":{
			showFormAlert(false);
			showTINAlert(false);
			enableRegistrationType(true);
			enableTIN(true);
			enableClearSearchByTIN(false,false,"Reset");
			enableSearchByTIN(true,true,"Search");
			showTINError(true, TINMessage || "No Results Found. Please try a valid TIN.");
			enableClearSearchByTIN(false,false);
			$taxID.val("");
			$taxID.focus();
			break;
		}
		case "ValidationErrorMandatory":{
			showFormAlert(true,formMessage);
			showTINError(false);
			if($("#reg_name").val()!=null && $("#reg_name").val()!=''){
				showTINAlert(true);
			}
			$("#missingInputsContainer").empty();
			if(missingInputs!=null && Object.keys(missingInputs).length>0){
				$("#missingMandatoryItemsOuter").removeClass("hide");
				for(var key in missingInputs){
					var missingItem = document.createElement("button");
					missingItem.appendChild(document.createTextNode(missingInputs[key]));
					missingItem.className= "text-info small btn-link btn";
					missingItem.style["margin-right"]="2px";
					missingItem.style["font-weight"]="bold";
					missingItem.setAttribute("data-scroll-target",key);
					missingItem.innerHTML=missingInputs[key];
					missingItem.addEventListener("click",function(){
						$('html, body').animate({
							scrollTop: ($("#"+$(this).attr("data-scroll-target")).offset().top) - 100
					    }, 500);
						$("#"+$(this).attr("data-scroll-target")).focus();
					});
					$("#missingInputsContainer").append(missingItem);
				}
			}else{
				$("#missingMandatoryItemsOuter").addClass("hide");
			}
		}
	}
}


/*function setTINSearchStage(stage,result){
	var $alertMessage=$("#entity_alert_message");
	var $alertError=$("#entity_error_message");
	var $taxID=$("#tax_id");
	$alertError.addClass("hide");
	switch(stage){
		case "init":{
			$alertMessage.removeClass("hide");
			$alertMessage.siblings().addClass("hide");
			break;
		}
		case "loading":{
			$("#searchInfoByTIN").find("#searchInfoByTINText").text("Loading..");
			$("#searchInfoByTIN").attr("disabled",true);
			$alertMessage.addClass("hide");
			break;
		}
		case "loadingComplete":{
			$alertMessage.addClass("hide");
			$("#searchInfoByTIN").find("#searchInfoByTINText").text("Search");
			$("#searchInfoByTIN").attr("disabled",false);
			if(!result){
				$taxID.val("");
				$alertError.removeClass("hide");
			}
			
		}
		
		
	}
}*/