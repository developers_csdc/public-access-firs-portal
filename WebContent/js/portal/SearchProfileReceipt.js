$(document)
		.ready(
				function() {
					/* Getting Days on chnage of Month & Year */
					$("#individualYearInput").on("change", function() {
						$year = $("#individualYearInput");
						$month = $("#individualMonthInput");
						$day = $("#individualDayInput");

						$month.val("");

						$day.html("");
						$day.append("<option value='' selected>Day</option>");

					});

					$("#individualMonthInput").on(
							"change",
							function() {
								$year = $("#individualYearInput");
								$month = $("#individualMonthInput");
								$day = $("#individualDayInput");

								clearDayInput($day);

								if ($year.val() == "") {
									return;
								}

								var numbersOfDay = getNumbersOfDays(
										$year.val(), $month.val());

								for (var i = 1; i <= numbersOfDay; i++) {
									$day.append("<option value=" + i + ">" + i
											+ "</option>");
								}
								;
							});

					/* Resetting Receipt Inputs */
					$("#resetReceiptInputs")
							.on(
									"click",
									function() {
										$(
												"#individualReceiptSearchResultContainer")
												.addClass("hide");
										$year = $("#individualYearInput");
										$month = $("#individualMonthInput");
										$day = $("#individualDayInput");

										$year.val("");
										$month.val("");
										$day.html("");
										$day
												.append("<option value='' selected>Day</option>");
										$('#receiptSearchTable').DataTable()
												.clear();
									});

					$("#searchIndividualReceipt")
							.on(
									"click",
									function() {
										$year = $("#individualYearInput");
										$month = $("#individualMonthInput");
										$day = $("#individualDayInput");
										$monthYearKey = $("input[type='radio'][name='individual-month-year-key']:checked");
										$individualReceiptTypeKey = $("input[type='radio'][name='individual-receipt-type-key']:checked");

										$(
												"#individualReceiptSearchResultContainer")
												.addClass("hide");
										var validateResObj = validateReceiptInputs(
												$year.val(), $month.val(), $day
														.val(), $monthYearKey
														.val(),
												$individualReceiptTypeKey.val());

										if (validateResObj.emptyField > 0) {
											$("html, body").animate({
												scrollTop : 100
											}, "slow");
											alert(
													"Marked Fields are mandatory",
													"error");
											alert(
													"Please fill these missing INPUTs / INPUT",
													"error");

											for (var i = 0; i < validateResObj.emptyFieldIDArray.length; i++) {
												console
														.log("empty field"
																+ validateResObj.emptyFieldIDArray[i]);

												raiseInputError(
														validateResObj.emptyFieldIDArray[i],
														"receiptInputContainer");
											}

											return;
										}

										// TODO:Disable all the Inputs
										$("#searchIndividualReceipt").attr(
												"disabled", "disabled");
										$("#resetReceiptInputs").attr(
												"disabled", "disabled");

										$("#receiptSearchLoader").removeClass(
												"hide");
										/*
										 * AJAX Call for Getting Receipt history -
										 * STARTS
										 */

										var url = "SearchReceiptProfileAJAX.jsp";
										var formObject = new Object();
										formObject["cmd"] = "searchIndividualReceipt";
										formObject["year"] = $year.val();
										formObject["month"] = $month.val();
										formObject["day"] = $day.val();
										formObject["individualMonthYearKey"] = $monthYearKey
												.val();
										formObject["individualReceiptTypeKey"] = $individualReceiptTypeKey
												.val();
										formObject["TIN"] = document.getElementById("tin").value;

										var dataSet = new Array();

										$
												.ajax({
													url : url,
													type : "POST",
													dataType : "json",
													async : true,
													data : formObject,
													success : function(res) {
														if (!res.isExceptionOccured) {
															if (JSON
																	.stringify(
																			res.arrayOfReceiptObj)
																	.match(/\[/)) {
																var receiptObjectArray = res.arrayOfReceiptObj;
																// console.log(receiptObjectArray);

																for (var i = 0; i < receiptObjectArray.length; i++) {
																	var dataArray = new Array();

																	dataArray
																			.push((i + 1)
																					.toString());
																	dataArray
																			.push(receiptObjectArray[i]["PAYMENT_REFERENCE"]);
																	dataArray
																			.push(receiptObjectArray[i]["SUM_OF_AMT"]);
																	dataArray
																			.push(receiptObjectArray[i]["PAYMENT_DATE"]);
																	dataArray
																			.push("<a href =\""
																					+ prepareDownloadLink(
																							receiptObjectArray[i]["RECEIPTNUMBER"],document.getElementById("tin").value)
																					+ "\" target='_blank'><i class='fa fa-download' aria-hidden='true'></i></a>");

																	dataSet
																			.push(dataArray);
																}
															} else {
																var receiptObject = res.arrayOfReceiptObj;
																var dataArray = new Array();

																dataArray
																		.push("1");
																dataArray
																		.push(receiptObject["PAYMENT_REFERENCE"]);
																dataArray
																		.push(receiptObject["SUM_OF_AMT"]);
																dataArray
																		.push(receiptObject["PAYMENT_DATE"]);
																dataArray
																.push("<a href =\""
																		+ prepareDownloadLink(
																				receiptObjectArray[i]["RECEIPTNUMBER"],document.getElementById("tin").value)
																		+ "\" target='_blank'><i class='fa fa-download' aria-hidden='true'></i></a>");

																dataSet
																		.push(dataArray);
															}

															console
																	.log(dataSet);

															/*
															 * Version Conflict
															 * between two J
															 * QUERY version
															 */
															// var $j =
															// jQuery.noConflict(true);
															$(
																	'#receiptSearchTable')
																	.DataTable()
																	.clear();
															$(
																	"#receiptSearchTable")
																	.DataTable(
																			{
																				data : dataSet,
																				destroy : true,
																				columns : [
																						{
																							title : "S/N"
																						},
																						{
																							title : "Payment Reference"
																						},
																						{
																							title : "Payment Amount"
																						},
																						{
																							title : "Payment Date"
																						},
																						{
																							title : "Download"
																						} ]
																			});

															$(
																	"#receiptSearchLoader")
																	.addClass(
																			"hide");
															$(
																	"#individualReceiptSearchResultContainer")
																	.removeClass(
																			"hide");
														} else {
															$(
																	"#receiptSearchLoader")
																	.addClass(
																			"hide");
															alert(
																	"Exception Occured Please try again",
																	"error");
														}

														$(
																"#searchIndividualReceipt")
																.removeAttr(
																		"disabled",
																		"disabled");
														$("#resetReceiptInputs")
																.removeAttr(
																		"disabled",
																		"disabled");
													},
													error : function(res) {
														$(
																"#searchIndividualReceipt")
																.removeAttr(
																		"disabled",
																		"disabled");
														$("#resetReceiptInputs")
																.removeAttr(
																		"disabled",
																		"disabled");
														alert("ERROR Occured",
																"error");
														$(
																"#receiptSearchLoader")
																.addClass(
																		"hide");
													}
												});
										/*
										 * AJAX Call for Getting Receipt history -
										 * ENDS
										 */

									});
				});

/* Function for getting number of days from Year and Month */
function getNumbersOfDays(year, month) {
	var isYearALeap = false;
	var numbersOfDay = 0;

	if (year % 4 === 0) {
		isYearALeap = true;
	}

	if (month == "Feb") {
		if (isYearALeap) {
			numbersOfDay = 29;
		} else {
			numbersOfDay = 28;
		}
	}

	if (month == "Apr" || month == "Jun" || month == "Sept" || month == "Nov") {
		numbersOfDay = 30;
	}

	if (month == "Jan" || month == "Mar" || month == "May" || month == "Jul"
			|| month == "Aug" || month == "Oct" || month == "Dec") {
		numbersOfDay = 31;
	}

	return numbersOfDay;
}

/* Clearing Day Input */
function clearDayInput($day) {
	$day.html("");
	$day.append("<option value='' selected>Day</option>");
}

function validateReceiptInputs(year, month, day, monthYearKey,
		individualReceiptTypeKey) {
	var emptyField = 0;
	var validateResObj = new Object();
	var emptyFieldIDArray = new Array();

	if (year == "") {
		emptyField++;
		emptyFieldIDArray.push("individualYearInput");
	}
	if (month == "") {
		emptyField++;
		emptyFieldIDArray.push("individualMonthInput");
	}
	if (monthYearKey == "Daily") {
		if (day == "") {
			emptyField++;
			emptyFieldIDArray.push("individualDayInput");
		}
	}

	validateResObj["emptyFieldIDArray"] = emptyFieldIDArray;
	validateResObj["emptyField"] = emptyField;

	return validateResObj;

}

function raiseInputError(divID, parentID) {
	$("#" + parentID).find("#" + divID).addClass("custom-error-input");
	setTimeout(function() {
		$("#" + parentID).find("#" + divID).removeClass("custom-error-input");
	}, 5000);
}

function handleClick(myRadio) {
	//console.log(myRadio);
	if (myRadio.value == "Monthy") {
		clearDayInput($("#individualDayInput"));
		$("#individualDayInput").attr("disabled", "disabled");
	} else {
		$("#individualDayInput").removeAttr("disabled", "disabled");
	}
}

function prepareDownloadLink(receiptNumber, tin) {
	var downloadURL = "";
	downloadURL = "ExportForeignReceiptAjax.jsp?"
			+ "&TIN=" + tin + "&ReceiptNumber="+receiptNumber;
	return downloadURL.replace('"', '');
}