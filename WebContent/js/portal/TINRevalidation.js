/**JS for TIN Revalidation from Profile.**/
/*console.log("TINRevalidation.js is running! Yes")*/
$(document).ready(
		function() {

			var $dataRetrieveBtn = $("#revalidateTIN");
			$("#revalidateTIN").click(
					function() {
						//alert("Btn Clicked");
						loadingState("Please Wait Retrieving Data..",
								"disabled", $dataRetrieveBtn);
						var $tin = $(this).attr("tin").trim();

						var tinData = {};
						tinData['TIN'] = $tin;
						tinData['command'] = 'fetchTaxPayerData';
						console.log(tinData);
						fetchTINData(tinData);
					});
			
			$("#updateTINData").click(
					function() {
						var $updateTINDataBtn = $("#updateTINData");
						var $cancelUpdateTINDataBtn = $("#cancelUpdateTINData");

						loadingState("Updatinging Data...Please Wait", "disabled",
								$updateTINDataBtn);
						$cancelUpdateTINDataBtn.attr("disabled", "disabled");

						var $tin = $(this).attr("tin").trim();
						var tinData = {};
						tinData['TIN'] = $tin;
						tinData['command'] = 'updateData';
						console.log("Firing Now Yes!!! " + tinData);

						updatePeopleRecord(tinData);
			});

			$("#redirectToHome").click(function() {
				location.reload();
			});

		});




function updatePeopleRecord(tinData){
	var url = "TINRevalidationAJAX.jsp";
	var $updateTINDataBtn = $("#updateTINData");
	var $cancelUpdateTINDataBtn = $("#cancelUpdateTINData");

	$.ajax({
		url: url,
		type: "POST",
		dataType: "json",
		async:true,
		data:tinData,
		success: function(res){
			//debugger;
			console.log(tinData);
			$("#dataPreviewSection").addClass("hide");
			$("#successDiv").removeClass("hide");
			$("#redirectToHome").removeClass("hide");
			$("#updateTINData").addClass("hide");
			
			$("#redirectToHome").html("Home");
		},
		error: function(res){
			alert("Error","Network Error");
			
			$("#redirectToHome").removeClass("hide");
			$("#updateTINData").addClass("hide");
			$("#redirectToHome").html("Reload The page and Try Again");
		}
	});	
}



function fetchTINData(tinData) {
	var url = "TINRevalidationAJAX.jsp";
	var $dataRetrieveBtn = $("#revalidateTIN");

	$.ajax({
		url : url,
		type : "POST",
		dataType : "json",
		async : true,
		data : tinData,
		success : function(res) {
			if (res.Success) {
				//if(res.isTINValid){
				//debugger;
				loadingState("Click this for Re-validation", "enable",
						$dataRetrieveBtn);
				populateDateToPreview(res);
				$('#tinDatailsModal').modal({
					backdrop : 'static',
					keyboard : false
				});
				/*}else{
					alert("TIN conflicts","error");
					loadingState("Click this for Re-validation","enable",$dataRetrieveBtn);
				}*/
				alert("Success", "success")

			} else {
				alert("Data not Found", "error");
				loadingState("Click this for Re-validation", "enable",
						$dataRetrieveBtn);
			}
		},
		error : function(res) {
			alert("Error", "Network Error, Try Again");
			loadingState("Click this for Re-validation", "enable",
					$dataRetrieveBtn);
		}
	});
}

function populateDateToPreview(res) {
	for ( var key in res) {
		$("#" + key).html(res[key]);
	}

	//$("#companyName").
}

function loadingState(text, state, buttonId) {
	$btn = buttonId;

	if (state == "disabled") {
		$btn.attr("disabled", "disabled");
	} else if (state == "enable") {
		$btn.removeAttr("disabled", "disabled");
	}

	$btn.html(text);
}

function handleClick(myRadio) {
	$("#updateTINData").removeAttr("disabled", "disabled");
}

/*Adding Event-Listener for Re-validate Button*/
/*$("#revalidateTIN").click(function(){
	
	var $dataRetrieveBtn = $("#revalidateTIN")
	loadingState("Please Wait Retrieving Data..","disabled",$dataRetrieveBtn);
	var $tin = $(this).attr("tin").trim();
	
	var tinData = {};
	tinData['TIN'] = $tin;
	tinData['command'] = "fetchTaxPayerData";
	
	fetchTINData(tinData);
});

$("#updateTINData").click(function(){
	var $updateTINDataBtn = $("#updateTINData");
	var $cancelUpdateTINDataBtn = $("#cancelUpdateTINData");
	
	loadingState("Updatinging Data...Please Wait","disabled",$updateTINDataBtn);
	$cancelUpdateTINDataBtn.attr("disabled","disabled");
	
	var $tin = $(this).attr("tin").trim();
	var tinData = {};
	tinData['TIN'] = $tin;
	tinData['command'] = "updateDataInAmanda";
	
	updatePeopleRecord(tinData);
});

$("#redirectToHome").click(function(){
	location.reload();
});

});


function fetchTINData(tinData){
var url = "TINRevalidationAJAX.jsp";
var $dataRetrieveBtn = $("#revalidateTIN");

$.ajax({
	url: url,
	type: "POST",
	dataType: "json",
	async:true,
	data:tinData,
	success: function(res){
		if(res.Success){
			if(res.isTINValid){
				debugger;
				loadingState("Click this for Re-validation","enable",$dataRetrieveBtn);
				populateDateToPreview(res);
				$('#tinDatailsModal').modal({backdrop: 'static', keyboard: false});
			}else{
				alert("TIN conflicts","error");
				loadingState("Click this for Re-validation","enable",$dataRetrieveBtn);
			}
			
		}else{
			alert("Data not Found","error");
			loadingState("Click this for Re-validation","enable",$dataRetrieveBtn);
		}
	},
	error: function(res){
		alert("Error","error");
		loadingState("Click this for Re-validation","enable",$dataRetrieveBtn);
	}
});	
}

function updatePeopleRecord(tinData){
var url = "TINRevalidationAJAX.jsp";
var $updateTINDataBtn = $("#updateTINData");
var $cancelUpdateTINDataBtn = $("#cancelUpdateTINData");

$.ajax({
	url: url,
	type: "POST",
	dataType: "json",
	async:true,
	data:tinData,
	success: function(res){
		debugger;
		if(res.isTINValid){
			if(res.peopleUpdateFlag && res.updatePeopleInfoFlag){
				$("#dataPreviewSection").addClass("hide");
				$("#successDiv").removeClass("hide");
				$("#redirectToHome").removeClass("hide");
				$("#updateTINData").addClass("hide");
				
				$("#redirectToHome").html("Home");
			
			}else{
				alert("Data Not Saved.");
				$("#redirectToHome").removeClass("hide");
				$("#updateTINData").addClass("hide");
				
				$("#redirectToHome").html("Try Again");
			}
		}else{
			alert("TIN conflicts","error");
			$("#redirectToHome").removeClass("hide");
			$("#updateTINData").addClass("hide");
			
			$("#redirectToHome").html("Try Again");
		}
		
	},
	error: function(res){
		alert("Error","error");
		
		$("#redirectToHome").removeClass("hide");
		$("#updateTINData").addClass("hide");
		$("#redirectToHome").html("Try Again");
	}
});	
}

function populateDateToPreview(res){
for(var key in res){
	$("#"+key).html(res[key]);
}
}

function loadingState(text,state,buttonId){
$btn = buttonId;

if(state == "disabled"){
	$btn.attr("disabled","disabled");
} else if(state == "enable"){
	$btn.removeAttr("disabled","disabled");
}

$btn.html(text);
}

function autoUpdatePeopleRecord(tinData){
var url = "TINRevalidationAJAX.jsp";
var isProfileRevalided = false;

$.ajax({
	url: url,
	type: "POST",
	dataType: "json",
	async:true,
	data:tinData,
	timeout:120000,
	success: function(res){
		debugger;
		if(res.isTINValid){
			if(res.peopleUpdateFlag && res.updatePeopleInfoFlag){
				alert("Auto - Revalidation has been done in Your Profile","success");
			}else{
				alert("Auto - Revalidation Failed Please Refresh Your Page","error");
			}
		}else{
			alert("TIN conflicts Auto - Revalidation Failed Please Refresh Your Page","error");
		}
		
	},
	error: function(xhr){
		alert("ERROR Auto - Revalidation Failed Please Refresh Your Page","error");
		console.log(xhr.status+" "+xhr.statusText);
		if(xhr.statusText == "timeout"){
			console.log("AJAX Request has been timed out and Terminated");
		}
	}
	
});
}
 */