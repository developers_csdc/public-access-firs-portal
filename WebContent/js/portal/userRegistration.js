/**
 * 
 */

var currentEmailId, emailExistCount;

function afterValidation() {
	if (validate()) {
		continueToRegistration();
	}
}

function processPayment() {
	var paymentMode = $("#paymentMode").val();
	if (paymentMode == "") {
		alert('Payment Mode Cannot be blanked.');
		return;
	}
	continueToRegistration();
	if ($("#PayAndContinue") != undefined)
		$("#PayAndContinue").html('<i class="fa fa-cog fa-spin"></i>&nbsp;'+LABEL_PAYMENT_PROCESSING);
}

function continueToRegistration() {
	
	var fullName 	= "&Title=" + $('#Title').val() + "&Name=" + $('#Name').val() + "&NameLast=" + $('#NameLast').val();
	var contact 	= "&EmailAddress=" + $('#EmailAddress').val() + "&Phone1=" + $('#Phone1').val() + "&Phone2=" + $('#Phone2').val();
	var address 	= "&Address=" + $('#Address').val() + "&City=" + $('#City').val() + "&PostalCode=" + $('#PostalCode').val() + "&Country=" + $('#Country').val();
	var securityInfo= "&SecurityQuestion=" + $('#Question').val() + "&SecurityQuestionAnswer=" + $('#QuestionAnswer').val();
	var info 		= "";
	
	var datas = (fullName + contact + address + securityInfo + info).split("+").join("%2B");
	var newurl = "updatePeopleAjax.jsp?command=" + $('#command').val();
	$("#btnSend").attr('disabled', 'disabled');
	$("#btnSend").html(
			'<span class="glyphicon glyphicon-refresh fa-spin"></span>&nbsp;'
					+ LABEL_SUBSCRIBER_REGISTRATION_BUTTON_PLEASE_WAIT);
	$.ajax({
		url : newurl,
		type : "POST",
		data : datas,
		success : function(responseText) {
			$("#btnSend").html(
					'<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;'
							+ LABEL_SUBSCRIBER_REGISTRATION_BUTTON_SEND);
			$("#btnSend").removeAttr('disabled', 'disabled');
			if (responseText.trim() == 'success') {
				showMessage(MESSAGE_PROFILE_INSERT_SUCCESS);
				$("#theform").attr("action", "portal?cmd=logon");
				document.theform.submit();
			} else if (responseText.trim() == 'failed')
				showMessage(MESSAGE_PROFILE_INSERT_FAIL);
		}
	});
}

function f_checkEmailExistInPeople(email) {
	emailExistCount = 0;
	var url = "CheckEmailExistInPeople.jsp?email=" + email;
	$.ajax({
		url : url,
		dataType : 'text',
		async : false,
		type : "POST",
		success : function(res) {
			if (res.trim() > 0) {
				emailExistCount = 1;
			}

		}
	});
	return emailExistCount;
}

function f_checkEmailAndNineaCombination() {
	var emailAndNineaNumberCombinationExist = false;
	var email = $('#EmailAddress').val();
	var nineaNumber = $('#NumberNINEA').val();
	var param = "email=" + email + "&nineaNumber=" + nineaNumber;
	var url = "TPS/bo/CheckEmailAndNineaCombination.jsp?" + param;
	$.ajax({
		url : url,
		dataType : 'text',
		async : false,
		type : "POST",
		success : function(res) {
			if (res.trim() > 0) {
				emailAndNineaNumberCombinationExist = true;
			}

		}
	});
	return emailAndNineaNumberCombinationExist;
}

//For Page Submit
$(document).ready(function() {
	/* if($('#peopleRSN').val()!=0){
		var url = "prePopulatePeople.jsp?PeopleRSN="+$('#peopleRSN').val();
		$.ajax({
	   		url: url,
	   		type: "POST",
	   		success: prePopulate
	   	});
	} */

	$('#btnReset').on('click', function() {
		$('#theform').each(function() {
			this.reset();
		});
	});

	var inputDate = $("#BirthDate");
	var changeYearButtons = function() {
		setTimeout(
				function() {
					var widgetHeader = inputDate.datepicker(
							"widget").find(
							".ui-datepicker-header");
					//you can opt to style up these simple buttons tho
					var prevYrBtn = $('<button title="PrevYr" style="font-size:11px">&lt;&lt; '
							+ CALENDAR_BUTTON_PREV_YEAR
							+ '</button>');
					prevYrBtn.bind("click", function() {
						$.datepicker._adjustDate(inputDate, -1,
								'Y');
					});
					var nextYrBtn = $('<button title="NextYr" style="font-size:11px">'
							+ CALENDAR_BUTTON_NEXT_YEAR
							+ ' &gt;&gt;</button>');
					nextYrBtn.bind("click", function() {
						$.datepicker._adjustDate(inputDate, +1,
								'Y');

					});
					prevYrBtn.appendTo(widgetHeader);
					nextYrBtn.appendTo(widgetHeader);

				}, 1);
	};
	var olddate = new Date("1/1/1800");
	var currentdate = new Date();
	$("#SubscriptionDate").datepicker({
	    language: language,
		format : "dd-mm-yyyy",
		startDate : "01/01/1980",
		endDate : currentdate,
		autoclose : true
	});

	$('#SubscriptionPickDate').click(function() {
		$('#SubscriptionDate').datepicker("show");
	});

	$("#CreationDate").datepicker({
	    language: language,
		format : "dd-mm-yyyy",
		startDate : "01/01/1980",
		endDate : currentdate,
		autoclose : true
	});

	$('#CreationPickDate').click(function() {
		$('#CreationDate').datepicker("show");
	});

	$("#BusinessOpprCheck, #BusinessDirCheck, #ForeignTradeCheck, #SocioEconomicCheck").change(
			function() {
				var dateStr;
				var nowStr;
				if ($("#BusinessOpprCheck").prop('checked') || $("#BusinessDirCheck").prop('checked') 
						|| $("#ForeignTradeCheck").prop('checked') || $("#SocioEconomicCheck").prop('checked')) {
					var now = new Date();
					nowStr =  now.getDate() + "-"
							+ (now.getMonth() + 1) + "-"
							+ now.getFullYear();
					now.setDate(now.getDate() + 365);
					dateStr = now.getDate() + "-"
							+ (now.getMonth() + 1) + "-"
							+ now.getFullYear();
					$("#divSubscriptionDate").css("display","");
					$("#SubscriptionDate").val(nowStr);
					$("#divPaymentType").css("display","");
					$("#divBusinessOpprExpDate").css("display","");
				} else {
					dateStr = "DD-MM-YYYY";
					$("#divSubscriptionDate").css("display","none");
					$("#divPaymentType").css("display","none");
					$("#divBusinessOpprExpDate").css("display","none");
					$("#SubscriptionDate").val("");
				}
				$("#BusinessOpprExpirydate").val(dateStr);
			});

});

//Checking for user is registered/added by admin
$("#EmailAddress").on( "focusout", function(){
	currentEmailId = $(this).val();
	if(f_checkEmail($(this).val())){
		$("#emailAddressDiv").removeClass('has-error has-feedback');
		$("#ajaxLoaderDiv").show();
		var result = checkUserName($('#EmailAddress').val());
		$("#ajaxLoaderDiv").hide();
		if(result == 1){//For unregistered user...
			$("#userCheckModalBody").html(MESSAGE_USER_EXIST_NEEDS_PASSWORD+" <br/><br/><button id='btnContinue' onclick='f_continue();' type='button' class='btn btn btn-success pull-right'>"+LABEL_BUTTON_CONTINUE+"&nbsp;&nbsp;&nbsp;<i class='fa fa-caret-right'></i></button>");         
			$('#userCheckModal').modal({backdrop: 'static', keyboard: false});
			$("#userCheckModal").modal('show');
			$("#btnContinue").bind('click');
		}if(result == 2){//For registered user...
			$("#userCheckModalBody").html(MESSAGE_USER_EXIST+" <br/><br/><br/><a href='portal?cmd=logon' id='btnLogin' class='btn btn btn-warning pull-right' style='color:white;'>"+LABEL_BUTTON_LOGIN+"&nbsp;&nbsp;&nbsp;<i class='fa fa-sign-in'></i></a>");   
			$('#userCheckModal').modal({backdrop: 'static', keyboard: false});      
			$("#userCheckModal").modal('show');
		}
	}else{
		$("#emailAddressDiv").addClass('has-error has-feedback');
		alert(JS_ALERT_INVALID_EMAIL)
		//$(this).focus();
	}
});

function f_continue(){//Sending temporary password to user's email id.
	$("#btnContinue").addClass("disabled");
	$("#btnContinue").html(LABEL_SUBSCRIBER_REGISTRATION_BUTTON_PLEASE_WAIT+"&nbsp;&nbsp;&nbsp;<i class='fa fa-spin fa-refresh'></i>");
	var setupFlag = setupPassword(currentEmailId);
	$("#btnContinue").html("Continue&nbsp;&nbsp;&nbsp;<i class='fa fa-caret-right'></i>");
	$("#btnContinue").removeClass("disabled");
	if(setupFlag == 1){
		$("#userCheckModalBody").html(MESSAGE_PASSWORD_SETUP_SUCCESS+"<br/><br/><a href='portal?cmd=logon' id='btnLogin' class='btn btn btn-warning pull-right' style='color:white;'>"+LABEL_BUTTON_LOGIN+"&nbsp;&nbsp;&nbsp;<i class='fa fa-sign-in'></i></a>");
	}else{
		$("#userCheckModalBody").html(MESSAGE_PASSWORD_SETUP_FAIL+"<br/><br/><br/><br/><a href='portal?cmd=logon' id='btnLogin' class='btn btn btn-warning pull-right' style='color:white;'>"+LABEL_BUTTON_LOGIN+"&nbsp;&nbsp;&nbsp;<i class='fa fa-sign-in'></i></a>");
	}
}

//For checking existence of email address in people table for the user added by admin.
function checkUserName(email) {
	emailExistCount = 0;
	var url = "CheckUserAJAX.jsp?command=checkUser&email=" + email;
	$.ajax({
		url : url,
		dataType : 'text',
		async : false,
		type : "POST",
		success : function(res) {
			var responseFlag = res.trim();
			if (responseFlag > 0) {
				emailExistCount = responseFlag;
			}
		}
	});
	return emailExistCount;
}

//For setting up temporary password for the user added by admin.
function setupPassword(email) {
	var setupFlag = 0;
	var url = "CheckUserAJAX.jsp?command=setupPass&email=" + email;
	$.ajax({
		url : url,
		dataType : 'text',
		async : false,
		type : "POST",
		success : function(res) {
			var responseFlag = res.trim();
			if (responseFlag > 0) {
				setupFlag = responseFlag;
			}
		}
	});
	return setupFlag;
}

function f_submitMyRegistration() {
	if (validate()) {
		continueToRegistration();
	}
}

//Checking for duplicate NINIA number.
function f_checkNineaNumber(NINIANumber) {
	var NINIAExistFlag = false;
	var url = "CheckUserAJAX.jsp?command=checkUserByNINIA&NINIA=" + NINIANumber;
	$.ajax({
		url : url,
		dataType : 'text',
		async : false,
		type : "POST",
		success : function(res) {
			var responseFlag = res.trim();
			if (responseFlag > 0) {
				NINIAExistFlag = true;
			}
		}
	});
	return NINIAExistFlag;
}

function showMessage(responseText) {
	alert(responseText.trim());
	$("#theform").attr("action", "portal?cmd=logon");
	$("#theform").submit();

}

function getActualPhone() {
	var sd = $("#Phone1").val().replace(/[^0-9]/gi, '');
	var number = parseInt(sd, 10);
	return number;
}

//For Page Validation
function validate() {
	var EmailAddress = $('#EmailAddress').val();
	var Name = $('#Name').val();
	var NameLast = $('#NameLast').val();
	var Address = $('#Address').val();
	var City = $('#City').val();
	var Phone1 = $('#Phone1').val();
	var Question = $('#Question').val();
	var QuestionAnswer = $('#QuestionAnswer').val();
	/*
	var CompanyName = $('#CompanyName').val();
	var Title = $('#Title').val();
	var Phone2 = $('#Phone2').val();
	var CreationDate = $('#CreationDate').val();
	var SubscriptionDate = $('#SubscriptionDate').val();
	var PostalCode = $('#PostalCode').val();
	var PaymentType = $('#PaymentType').val();
	var NineaNumer = $('#NumberNINEA').val();*/
	
	
	var retval = true;

	var msg = "";
	var count = 0;
	var fieldOBJ;
	var bDate = 0;
	
	if (!f_checkEmail(EmailAddress)) {
		if (count == 0)	fieldOBJ = $('#EmailAddress');
		msg = JS_ALERT_ERROR_EMAIL_ADDRESS + "\n";
		$('#emailAddressDiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#emailAddressDiv').removeClass('has-error has-feedback');
	}
	
	if (Name == "") {
		if (count == 0)	fieldOBJ = $('#Name');
		msg += JS_ALERT_ERROR_FIRST_NAME + "\n";
		$('#nameDiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#nameDiv').removeClass('has-error has-feedback');
	}
	
	if (NameLast == "") {
		if (count == 0)	fieldOBJ = $('#NameLast');
		msg += JS_ALERT_ERROR_LAST_NAME + "\n";
		$('#nameLastDiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#nameLastDiv').removeClass('has-error has-feedback');
	}
	

	if (Address == "") {
		if (count == 0)	fieldOBJ = $('#Address');
		msg += JS_ALERT_ERROR_ADDRESS + "\n";
		$('#addressDiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#addressDiv').removeClass('has-error has-feedback');
	}

	if (City == "") {
		if (count == 0)	fieldOBJ = $('#City');
		msg += JS_ALERT_ERROR_CITY + "\n";
		$('#cityDiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#cityDiv').removeClass('has-error has-feedback');
	}

	if (Phone1 == "") {
		if (count == 0)	fieldOBJ = $('#Phone1');
		msg += JS_ALERT_ERROR_PHONE1 + "\n";
		$('#phone1Div').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#phone1Div').removeClass('has-error has-feedback');
	}
	
	if(Question==""){
		if(count==0) fieldOBJ = $('#Question');
		msg += JS_ALERT_ERROR_QUESTION+"\n";      		
		$('#questionDiv').addClass('has-error has-feedback');
		retval =  false;
	}else{
		$('#questionDiv').removeClass('has-error has-feedback');
	}

	
	if(QuestionAnswer==""){
		if(count==0) fieldOBJ = $('#QuestionAnswer');
		msg += JS_ALERT_ERROR_QUESTION_ANSWER+"\n";      		
		$('#questionAnswerDiv').addClass('has-error has-feedback');
		retval =  false;
	}else{
		$('#questionAnswerDiv').removeClass('has-error has-feedback');
	}
	
	/*
	if (CompanyName == "") {
		if (count == 0)	fieldOBJ = $('#CompanyName');
		msg += JS_ALERT_ERROR_COMPANY_NAME + "\n";
		$('#companyNameDiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#companyNameDiv').removeClass('has-error has-feedback');
	}
	
	if (NineaNumer == "") {
		if (count == 0) fieldOBJ = $('#NumberNINEA');
		msg += JS_ALERT_NINEA_NUMEBR + "\n";
		$('#numberNINEADiv').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#numberNINEADiv').removeClass('has-error has-feedback');
	}

	if (PassWord1 == "") {
		//alert("Mot de passe ne peut pas être balnk");
		//$('#PassWord1').focus();
		if (count == 0)
			fieldOBJ = $('#PassWord1');
		msg += JS_ALERT_ERROR_PASSWORD1 + "\n";
		$('#passWord1Div').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#passWord1Div').removeClass('has-error has-feedback');
	}

	if (PassWord2 == "") {
		//alert("Confirmer que mot de passe ne peut pas être balnk");
		//$('#PassWord2').focus();
		if (count == 0)
			fieldOBJ = $('#PassWord2');
		msg += JS_ALERT_ERROR_PASSWORD2 + "\n";
		$('#passWord2Div').addClass('has-error has-feedback');
		retval = false;
	} else {
		$('#passWord2Div').removeClass('has-error has-feedback');
	}
	
	if ($("#BusinessOpprCheck").prop('checked') || $("#BusinessDirCheck").prop('checked')) {
		if (PaymentType == "") {
			//alert("Confirmer que mot de passe ne peut pas être balnk");
			//$('#PassWord2').focus();
			if (count == 0)
				fieldOBJ = $('#PaymentType');
			msg += JS_ALERT_ERROR_PAYMENT_TYPE + "\n";
			$('#paymentTypeDiv').addClass('has-error has-feedback');
			retval = false;
		} else {
			$('#paymentTypeDiv').removeClass('has-error has-feedback');
		}
	}*/

	if ("" != msg) {
		alert(msg);
		fieldOBJ.focus();
		if (bDate == 1)
			fieldOBJ.blur();
		retval = false;
	} 

	return retval;
}

//Moved to js/common/validator.js
/*  function f_checkEmail(email){
  var testresults;
  var str = email;
  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

  if (filter.test(str)){
    testresults=true;
  }else{
    testresults=false;
  }

  return testresults;
}
 */

function f_checkPasswordStrength() {
	var password = $('#PassWord1').val();
	//initial strength 
	var strength = 0;
	//if the password length is less than 6, return message. 
	if (password.length < 8) {
		$('#PassWord1').addClass('has-error has-feedback short');
		alert(JS_ALERT_ERROR_SHORT_PASSWORD);
		return false;
	}
	//length is ok, lets continue. 
	//if length is 8 characters or more, increase strength value 
	if (password.length > 7)
		strength += 1;
	//if password contains both lower and uppercase characters, increase strength value 
	//if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) strength += 1; 
	//if it has numbers and characters, increase strength value 
	//if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) strength += 1; 
	//if it has one special character, increase strength value 
	//if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1; 
	//now we have calculated strength value, we can return messages 
	//if value is less than 2 
	if (strength < 1) {
		return false;
	} else {
		return true;
	}
}



function f_gotoHome() {
	window.location.href = "home.jsp";
}
$(function() {
	$('form').each(function() {
		this.reset()
	});
});

function checkValidate() {
	var CompanyName = $('#CompanyName').val();
	var NameLast = $('#NameLast').val();
	var City = $('#City').val();
	var Community = $('#Community').val();
	var Province = $('#Province').val();
	var Phone1 = $('#Phone1').val();
	var EmailAddress = $('#EmailAddress').val();
	var BirthDate = $('#BirthDate').val();
	var IdentificatioDocument = $('#IdentificatioDocument').val();
	var IdentificatioDocumentNumber = $('#IdentificatioDocumentNumber').val();
	var Question = $('#Question').val();
	var QuestionAnswer = $('#QuestionAnswer').val();
	var NumberNINEA = $('#NumberNINEA').val();
	var PaymentType = $('#PaymentType').val();
	var Address = $('#Address').val();
	var Name = $('#Name').val();
	var EmailAddress = $('#EmailAddress').val();
	
	if (CompanyName != "") {
		$('#companyNameDiv').removeClass('has-error has-feedback');
		$('#CompanyName').removeClass('req');
	}else{
		$('#CompanyName').addClass('req');
	}

	if (NameLast != "") {
		$('#nameLastDiv').removeClass('has-error has-feedback');
		$('#NameLast').removeClass('req');
	}else{
		$('#NameLast').addClass('req');
	}

	if (City != "") {
		$('#cityDiv').removeClass('has-error has-feedback');
		$('#City').removeClass('req');
	}else{
		$('#City').addClass('req');
	}

	if (Community != "") {
		$('#communityDiv').removeClass('has-error has-feedback');
	}

	if (Province != "") {
		$('#provinceDiv').removeClass('has-error has-feedback');
	}

	if (Phone1 != "") {
		$('#phone1Div').removeClass('has-error has-feedback');
		$('#Phone1').removeClass('req');
	}else{
		$('#Phone1').addClass('req');
	}

	if (f_checkEmail(EmailAddress)) {
		$('#emailAddressDiv').removeClass('has-error has-feedback');
		$('#EmailAddress').removeClass('req');
	}else{
		$('#EmailAddress').addClass('req');
	}

	if (BirthDate != "") {
		$('#birthDateDiv').removeClass('has-error has-feedback');
	}

	if (Question != "") {
		$('#questionDiv').removeClass('has-error has-feedback');
		 $('#Question').removeClass('req');
	}else{
		$('#Question').addClass('req');
	}

	if (QuestionAnswer != "") {
		$('#questionAnswerDiv').removeClass('has-error has-feedback');
		$('#QuestionAnswer').removeClass('req');
	}else{
		$('#QuestionAnswer').addClass('req');
	}

	if (NumberNINEA != "") {
		$('#numberNINEADiv').removeClass('has-error has-feedback');
		$('#NumberNINEA').removeClass('req');
	}else{
		$('#NumberNINEA').addClass('req');
	}

	if (PaymentType != "") {
		$('#paymentTypeDiv').removeClass('has-error has-feedback');
	}

	if (Address != "") {
		$('#addressDiv').removeClass('has-error has-feedback');
		$('#Address').removeClass('req');
	}else{
		$('#Address').addClass('req');
	}

	if (Name != "") {
		$('#nameDiv').removeClass('has-error has-feedback');
		$('#Name').removeClass('req');
	}else{
		$('#Name').addClass('req');
	}
	if(EmailAddress!="" && Name != "" && Address!="" 
		&& NumberNINEA != "" && QuestionAnswer != "" 
			&& Question != "" && Phone1 != "" 
				&& City != "" && CompanyName != "")
		$('#btnSend').prop('disabled', false);
	
	else
		$('#btnSend').prop('disabled', true);

}


$("#Country").autocomplete({
    serviceUrl: "AutoCompleteAJAX.jsp?cmd=userRegistration&inputName=country"
});