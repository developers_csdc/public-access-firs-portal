
$(document).ready(function(){
	$("#btnRedirectSearchTCC").on("click",function(){
		window.location.href="portal?cmd=searchtcc";
	});
	/*--------------------ESS: 53691 -  Check for query params and redirect if true (Redirect by QR CODE)--------------------------*/
	var queryParams = getQueryParams();
	if(queryParams && "tccNumber" in queryParams){
		console.log(queryParams);
		redirectIfTCCFound(queryParams);
	}
});

function searchTCC_validation(){
	var tcc_number=removeSpaces(document.getElementById('tcc_number').value);
	
	if(tcc_number=='' || tcc_number==null || tcc_number.length==0){
		alert("TCC Number cannot left blank","error");
    }else{
    	//searchTCCReceipt();
    	searchNewTccReceipt();
    }
}

function searchNewTccReceipt(){
	alert("Downloading TCC","success");
	var tcc_number=$("#tcc_number").val();
	downloadTCC(tcc_number);
}


function downloadTCC(tcc_number) {
	setLoadingState(true);
	var url = "SearchNewTCCAjax.jsp";
	var receiptData = {
			"tccNumber" : tcc_number
		};
	var response;
	$
			.ajax({
				url : url,
				type : "POST",
				dataType : "json",
				async : true,
				data : receiptData,
				success : function(res) {
					if (res === 0) {
						alert("TCC not found or not ready. Please verify your credentials.","error");
					} else {
						alert("Downloading TCC...","success");
						window.location.href = "ExportForeignReceiptAjax.jsp?tccNumber="
								+ tcc_number;
					}

				},
				error : function(e) {
					alert("Some error occoured, please try again","error");
				}
			});
}


function searchTCCReceipt(){
	var tcc_number=$("#tcc_number").val();
	searchByTCC(tcc_number);
 }

function reset_submit(){
	$("#tcc_number").val("");
	$("#error_Search").html("");
	$("#tcc_loader").addClass("hide");
	$("#submit_button").removeClass("disabled");
}

function mapFreeformDataIntoView(infoList,formType){
	var $corporateTable=$("#tcc-search-result-corporate");
	switch(formType){
		case "individual":{
			$("#tcc-result-individual").removeClass("hide");
			$("#tcc-result-corporate").addClass("hide");
			for(var i=infoList.length-1,counter=1;i>=0;i--,counter++){
				$("#ind_year_"+counter).val(infoList[i]["year"]);
				$("#ind_totalIncome_"+counter).val(infoList[i]["totalIncome"]);
				$("#ind_taxPaid_"+counter).val(infoList[i]["taxPaid"]);
			}
			break;
		}
		case "corporate":{
			$("#tcc-result-individual").addClass("hide");
			$("#tcc-result-corporate").removeClass("hide");
			for(var i=infoList.length-1,counter=1;i>=0;i--,counter++){
				$("#corporate_year_"+counter).val(infoList[i]["year"]);
				$("#corporate_turnover_"+counter).val(infoList[i]["turnover"]);
				$("#corporate_assessableProfit_"+counter).val(infoList[i]["assessableProfit"]);
				$("#corporate_taxPaid_"+counter).val(infoList[i]["taxPaid"]);
			}
			break;
		}
	}
	
}

function searchByTCC(tcc_number){
	var tccData = {"tcc_number":tcc_number.trim()};
	var url = "SearchTCCAjax.jsp";
	loderController("on");
	$.ajax({
		url: url,
		type: "POST",
		dataType: "json",
		async:true,
		data:tccData,
		success: function(res){
			   if(res.DataFetched == true){
				   $("#tccnumber").val(res.tccnumber);	
				   $("#tccstatus").val(res.tccstatus);					   
				   $("#tccissue").val(res.tccIssueDate);
				   $("#tccexpiry").val(res.tccExpiry);
				   $("#tccApplicantTIN").val(res.tccApplicantTIN);;
				   $("#tccApplicantName").html(res.tccApplicantName);
				   $("#tcc_search").addClass("hide");
				   $("#tcc_search_result").removeClass("hide");
				   $("#tcc_loader").addClass("hide");
				   
				   /*Showing information using freeform data*/
				   mapFreeformDataIntoView(res.freeformInfo,res.applicantType);
				   
				   /*Showing information for FOREX TCC verification*/
				   if(res.applicantType == "forex"){
					   $("#remittanceAmountContainer").removeClass("hide");
					   $("#remittanceAmount").val(res.freeformInfo[0]["remmitanceAmountApproved"]);
				   }
				   
			   }else{
				   	loderController("off");
				   	alert('SORRY THE TCC NUMBER IS NOT AVAILABLE IN OUR DATABASE',"error");
			   }
   		},
   		error: function(res){
			alert("Some error occured");
			loderController("off");
		}
	});
}

function redirectIfTCCFound(params){
	if (params["cmd"][0]=='searchtcc'){
		if(params["tccNumber"]){
			var tcc = params["tccNumber"].join("");
			$("#tcc_number").val(tcc);
			searchByTCC(tcc);
		}
	}
}
function loderController(cmd){
	if(cmd=="on"){
		$("#submit_button").attr("disabled","disabled");
		$("#submit_button").html("Retrieving..");
		$("#tcc_number").attr("disabled","disabled");
		}
	else if(cmd=="off"){
		$("#tcc_number").removeAttr("disabled","disabled");
		$("#submit_button").removeAttr("disabled","disabled");
		$("#submit_button").html("Submit");
		$("#tcc_number").val("");
		DrawBotBoot();
    	$("#expression-result").val("");
	}
	
}


