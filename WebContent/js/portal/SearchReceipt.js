$(document).ready(function() {
	$("#btnRedirectSearchRceceipt").on("click", function() {
		window.location.href = "portal?cmd=searchreceipt";
	});

	$("#RedirectSearchRceceipt").on("click", function() {
		window.location.href = "portal?cmd=searchreceipt";
	});

	/*---Check for query params and redirect if true (Redirect by QR CODE)---*/
	var queryParams = getQueryParams();
	if (queryParams && "receiptNumber" && "tinNumber" in queryParams) {
		console.log(queryParams);
		redirectIfReceiptFound(queryParams);
	}
});

function SearchReceipt_Validation() {
	if ($("#expression-result").val() !== null
			&& $("#expression-result").val() !== "") {
		if (ValidBotBoot()) {
			// debugger;
			var receipt_number = removeSpaces(document
					.getElementById('receipt_no').value);
			var TIN = removeSpaces(document.getElementById('TIN').value);
			var receiptType = $(
					"input[type='radio'][name='receiptTypeName']:checked")
					.val();

			// return;
			if (receipt_number == '' || receipt_number == null
					|| receipt_number.length == 0 || TIN == '' || TIN == null
					|| TIN.length == 0) {
				alert("Receipt Number / TIN cannot left Blank", "error");
				// $("#error_Search").html('<b>! Enter Receipt Number /
				// TIN</b>');
			} else {
				$("#submit_button").attr("disabled", "disabled");
				searchReceipt(receipt_number, TIN);
			}

			/*
			 * if(receiptType == "NONFOREIGN" && receiptType != null){
			 * if(receipt_number == '' || receipt_number == null ||
			 * receipt_number.length == 0||TIN == '' || TIN == null ||
			 * TIN.length == 0){ alert("Receipt Number / TIN cannot left
			 * Blank","error"); //$("#error_Search").html('<b>! Enter Receipt
			 * Number / TIN</b>'); }else{
			 * $("#submit_button").attr("disabled","disabled");
			 * searchReceipt(receipt_number,TIN); } }else if(receiptType ==
			 * "FOREIGN" && receiptType != null){ if(receipt_number == '' ||
			 * receipt_number == null || receipt_number.length == 0){
			 * alert("Receipt Number cannot left Blank","error");
			 * //$("#error_Search").html('<b>! Enter Receipt Number / TIN</b>');
			 * }else{ //alert("FOREIGN RECEIPT SEARCH FUNCTIONALITY YET TO BE
			 * AVAILABE FOR USE..."); //return;
			 * $("#submit_button").attr("disabled","disabled");
			 * searchReceipt(receipt_number,TIN); } }else{
			 * //console.log("RECEIPT TYPE NOT FOUND"); }
			 */
		} else {
			alert("Captcha Mismatch", "error");
			DrawBotBoot();
			$("#expression-result").val("");
		}
	} else {
		alert("Captcha cannot be Empty", "error");
	}
};

function searchReceipt(receipt_number, TIN) {
	debugger;
	/* CHECKING AS RECEIPT IS FOREIGN OR NOT */
	// var receiptType = $('input[name=receiptTypeName]:checked').val().trim();
	// return;
	// $("#receipt_loader").removeClass("hide");
	$("#submit_button").html("Retrieving...");
	var receiptData = {
		"receipt_number" : receipt_number.trim(),
		"TIN" : TIN.trim()
	};
	// var receiptData =
	// {"receipt_number":receipt_number.trim(),"TIN":TIN.trim(),"receiptType":receiptType};
	var url = "SearchReceiptAjax.jsp";
	setLoadingState(true);
	$
			.ajax({
				url : url,
				type : "POST",
				dataType : "json",
				async : true,
				data : receiptData,
				success : function(res) {
					// return;
					setLoadingState(false);
					if (!(res.receiptFolderType == "FR" || res.receiptFolderType == "FRCT")) {
						if (res.DataFetched == true) {
							$("#submit_button").html("Submit");
							$("#receipt_loader").addClass("hide");
							$("#Receipt_Search").addClass("hide");
							$("#Receipt_Details").removeClass("hide");
							$("#Receipt_No").html(receipt_number);

							if (res.folderType == "RCPT") {
								$("#Name").html(
										decodeURIComponent(res.TaxPayerName
												.replace(/\+/g, '%20')));
							} else if (res.folderType == "RCTI") {
								$("#Name").html(
										decodeURIComponent(res.beneficiaryName
												.replace(/\+/g, '%20')));
							}

							$("#Amount").html(res.transaction_amount);
							$("#Payment_date").html(res.paymentdate);
							/* TriggerDownload */
							// alert(JSON.stringify(res));
							params = "&RECEIPT_NO=" + receipt_number + "&DATE="
									+ res.paymentdate + "&BILL_NO="
									+ res.bill_no + "&RECEIVED_FORM="
									+ res.TaxPayerName.replace("&", "%26")
									+ "&BENEFICIARY_TIN=" + res.benificiary_tin
									+ "&ADDRESS=" + res.payer_address
									+ "&PAYER=" + res.payer_name
									+ "&address_ben=" + res.ben_address
									+ "&PAYER_TIN=" + res.payer_tin
									+ "&TAX_TYPE=" + res.tax_type_name
									+ "&SUM_OF_AMT=" + res.transaction_amount
									+ "&SUM_OF_AMT_IN_WORD="
									+ res.transaction_amount_in_word
									+ "&TAX_ON=" + res.tax_type_code
									+ "&CONTRACT_DESC=" + "" + "&WHR_RATE="
									+ res.rate + "&PERIOD_FROM="
									+ res.assessmentPeriodFrom + "&PERIOD_TO="
									+ res.assessmentPeriodTo + "&BANK="
									+ res.bank + "&TAX_OFFICE="
									+ res.tax_office_name
									+ "&PAYMENT_REFERENCE="
									+ res.paymentReference + "&PAYMENT_DATE="
									+ res.paymentdate + "&folderType="
									+ res.folderType + "&paymentResponse="
									+ res.paymentResponse + "&ben_address="
									+ res.ben_address + "&fee_description="
									+ res.fee_description
									+ "&contractDescription="
									+ res.contractDescription
									+ "&contractAmount=" + res.contractAmount
									+ "&beneficiaryName=" + res.beneficiaryName
									+ "&receiptFolderType="
									+ res.receiptFolderType;
							$("#export_Receipt").attr("href",
									"ExportReceiptAjax.jsp?" + params);
						} else {
							if (!res.tinCheck) {
								alert(res.errorMsg, "error");
							} else if (res.isExceptionOccured) {
								alert("Error Occured.Please Try again", "error");
							} else {
								// $("#receipt_loader").addClass("hide");
								// $("#error_Search").html('The Receipt number
								// you entered is invalid.');
								alert(
										"The Receipt number you entered is invalid.",
										"error");
							}

						}
					} else if (res.receiptFolderType == "FR"
							|| res.receiptFolderType == "FRCT") {
						if (res.receiptType == "FR") {
							$("#submit_button").html("Submit");
							$("#receipt_loader").addClass("hide");
							$("#Receipt_Search").addClass("hide");
							$("#Receipt_Details").removeClass("hide");
							$("#Receipt_No").html(receipt_number);
							$("#Name").html(
									decodeURIComponent(res.BULK_RECV_FROM
											.replace(/\+/g, '%20')));
							$("#Amount").html(res.BULK_AMOUNT);
							$("#Payment_date").html(res.BULK_PAYMENT_DATE);

							params = "&BULK_PAYMENT_REF="
									+ res.BULK_PAYMENT_REF + "&BULK_BANK="
									+ res.BULK_BANK + "&BULK_TIN="
									+ res.BULK_TIN + "&BULK_TAX_TYPE="
									+ res.BULK_TAX_TYPE + "&BULK_RECV_FROM="
									+ res.BULK_RECV_FROM + "&BULK_TAX_OFFICE="
									+ res.BULK_TAX_OFFICE + "&BULK_ADDRESS="
									+ res.BULK_ADDRESS + "&BULK_PERIOD_FROM="
									+ res.BULK_PERIOD_FROM + "&BULK_PERIOD_TO="
									+ res.BULK_PERIOD_TO
									+ "&BULK_PAYMENT_DATE="
									+ res.BULK_PAYMENT_DATE + "&BULK_AMOUNT="
									+ res.BULK_AMOUNT + "&BULK_AMOUNT_WORD="
									+ res.BULK_AMOUNT_WORD
									+ "&receiptFolderType="
									+ res.receiptFolderType + "&receiptType="
									+ res.receiptType + "&BULK_RECEIPT_NUMBER="
									+ res.BULK_RECEIPT_NUMBER
									+ "&BULK_CURRENCY_TYPE="
									+ res.BULK_CURRENCY_TYPE;

							$("#export_Receipt").attr("href",
									"ExportReceiptAjax.jsp?" + params);
						} else if (res.receiptType == "FRCT") {
							$("#submit_button").html("Submit");
							$("#receipt_loader").addClass("hide");
							$("#Receipt_Search").addClass("hide");
							$("#Receipt_Details").removeClass("hide");
							$("#Receipt_No").html(receipt_number);
							$("#Name").html(
									decodeURIComponent(res.CREDIT_RECV_FROM
											.replace(/\+/g, '%20')));
							$("#Amount").html(res.CREDIT_AMOUNT);
							$("#Payment_date").html(res.CREDIT_PAYMENT_DATE);

							params = "&CREDIT_RECEIPT_NUMBER="
									+ res.CREDIT_RECEIPT_NUMBER
									+ "&CREDIT_DATE=" + res.CREDIT_DATE
									+ "&CREDIT_RECV_FROM="
									+ res.CREDIT_RECV_FROM
									+ "&CREDIT_BENEFICIARY_TIN="
									+ res.CREDIT_BENEFICIARY_TIN
									+ "&CREDIT_ADDRESS=" + res.CREDIT_ADDRESS
									+ "&CREDIT_PAYER=" + res.CREDIT_PAYER
									+ "&CREDIT_PAYER_TIN="
									+ res.CREDIT_PAYER_TIN
									+ "&CREDIT_TAX_TYPE=" + res.CREDIT_TAX_TYPE
									+ "&CREDIT_AMOUNT=" + res.CREDIT_AMOUNT
									+ "&CREDIT_AMOUNT_WORD="
									+ res.CREDIT_AMOUNT_WORD
									+ "&CREDIT_WHT_RATE=" + res.CREDIT_WHT_RATE
									+ "&CREDIT_CONTRACT_DESC="
									+ res.CREDIT_CONTRACT_DESC
									+ "&CREDIT_PERIOD_FROM="
									+ res.CREDIT_PERIOD_FROM
									+ "&CREDIT_PERIOD_TO="
									+ res.CREDIT_PERIOD_TO + "&CREDIT_BANK="
									+ res.CREDIT_BANK + "&CREDIT_TAX_OFFICE="
									+ res.CREDIT_TAX_OFFICE
									+ "&CREDIT_PAYMENT_REF="
									+ res.CREDIT_PAYMENT_REF
									+ "&CREDIT_PAYMENT_DATE="
									+ res.CREDIT_PAYMENT_DATE
									+ "&CREDIT_ISSUING_OFFICER="
									+ res.CREDIT_ISSUING_OFFICER
									+ "&CREDIT_BULK_IRNO="
									+ res.CREDIT_BULK_IRNO
									+ "&CREDIT_CURRENCY_TYPE="
									+ res.CREDIT_CURRENCY_TYPE
									+ "&CREDIT_SUPERVISING_OFFICER="
									+ res.CREDIT_SUPERVISING_OFFICER
									+ "&receiptFolderType="
									+ res.receiptFolderType + "&receiptType="
									+ res.receiptType;

							$("#export_Receipt").attr("href",
									"ExportReceiptAjax.jsp?" + params);
						}
					}

					$("#submit_button").html("Submit");
					$("#receipt_no").val("");
					$("#TIN").val("");
					$("#submit_button").removeAttr("disabled", "disabled");
					DrawBotBoot();
					$("#expression-result").val("");

				},
				error : function(res) {
					$("#submit_button").html("Submit");
					$("#receipt_no").val("");
					$("#TIN").val("");
					alert("Error Occured Please try again", "error");
					$("#submit_button").removeAttr("disabled", "disabled");
					DrawBotBoot();
					$("#expression-result").val("");
				}
			});
}

function foreignReceiptValidation() {
	setLoadingState(true);
	var url = "SearchForeignReceiptAjax.jsp";
	var receipt_number = removeSpaces(document.getElementById('receipt_no').value);
	var TIN = removeSpaces(document.getElementById('TIN').value);
	var receiptData = {
		"receipt_number" : receipt_number.trim(),
		"TIN" : TIN.trim()
	};
	if (receipt_number.trim().length < 1) {
		alert("Please, enter a receipt number","error");
		return;
	}
	if (TIN.trim().length < 1) {
		alert("Please, enter a TIN number","error");
		return;
	}
	if ($("#expression-result").val() === null
			&& $("#expression-result").val() === "") {
		alert("Captcha can not be empty","error");
		return;

	}
	if (!ValidBotBoot()) {
		alert("Please enter a correct captcha","error");
		DrawBotBoot();
		$("#expression-result").val("");
		return;
	}
	var response;
	$
			.ajax({
				url : url,
				type : "POST",
				dataType : "json",
				async : true,
				data : receiptData,
				success : function(res) {
					if (res === 0) {
						alert("Receipt not found or not ready. Please verify your credentials.","error");
					} else {
						alert("Downloading Receipt...","success");
						window.open( "ExportForeignReceiptAjax.jsp?ReceiptNumber="
								+ receipt_number.trim() + "&TIN=" + TIN.trim());
					}

				},
				error : function(e) {
					alert("Some error occoured, please try again","error");
				}
			});
}

function localReceiptValidation() {
	setLoadingState(true);
	var url = "SearchForeignReceiptAjax.jsp";
	var receipt_number = removeSpaces(document.getElementById('receipt_no').value);
	var TIN = removeSpaces(document.getElementById('TIN').value);
	var receiptData = {
		"receipt_number" : receipt_number.trim(),
		"TIN" : TIN.trim()
	};
	if (receipt_number.trim().length < 1) {
		alert("Please, enter a receipt number","error");
		return;
	}
	if (TIN.trim().length < 1) {
		alert("Please, enter a TIN number","error");
		return;
	}
	if ($("#expression-result").val() === null
			&& $("#expression-result").val() === "") {
		alert("Captcha can not be empty","error");
		return;

	}
	if (!ValidBotBoot()) {
		alert("Please enter a correct captcha","error");
		DrawBotBoot();
		$("#expression-result").val("");
		return;
	}
	var response;
	$
			.ajax({
				url : url,
				type : "POST",
				dataType : "json",
				async : true,
				data : receiptData,
				success : function(res) {
					if (res === 0) {
						alert("Receipt not found or not ready. Please verify your credentials.","error");
					} else {
						alert("Downloading Receipt...","success");
						window.open("ExportForeignReceiptAjax.jsp?ReceiptNumber="
								+ receipt_number.trim() + "&TIN=" + TIN.trim());
					}

				},
				error : function(e) {
					alert("Some error occoured, please try again","error");
				}
			});
}

function redirectIfReceiptFound(params) {
	if (params["cmd"][0] == 'searchreceipt') {
		var receipt, tin;
		if (params["receiptNumber"]) {
			receipt = params["receiptNumber"].join("");
		}
		if (params["tinNumber"]) {
			tin = params["tinNumber"].join("");
		}
		$("#receipt_no").val(receipt);
		$("#TIN").val(tin);
		searchReceipt(receipt, tin);
	}
}
