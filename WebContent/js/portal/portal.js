//Holds all the auto complete field definition in this array
g_AutoCompleteArray = new Array();

g_AutoCompleteCache = new Object();

//Basic UA detection
isIE = document.all ? true : false;

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };

//Attachs the autocomplete object to a form element. Sets onkeypress event on
//the form element.
function AutoComplete_Create(id, columnName, tableName, whereClause) {
	g_AutoCompleteArray[id] = {
			'columnName' :columnName,
			'tableName' :tableName,
			'whereClause' :whereClause,
			'isVisible' :false,
			'element' :document.getElementById(id),
			'dropdown' :null,
			'highlighted' :null
	};
	if(g_AutoCompleteArray[id]['element']){
		g_AutoCompleteArray[id]['element'].setAttribute('autocomplete', 'off');
		g_AutoCompleteArray[id]['element'].onkeydown = function(e) {
			return AutoComplete_KeyDown(this.getAttribute('id'), e);
		}
		g_AutoCompleteArray[id]['element'].onkeyup = function(e) {
			return AutoComplete_KeyUp(this.getAttribute('id'), e);
		}
		g_AutoCompleteArray[id]['element'].onkeypress = function(e) {
			if (!e)
				e = window.event;
			if (e.keyCode == 13)
				return false;
		}
		g_AutoCompleteArray[id]['element'].ondblclick = function() {
			AutoComplete_ShowDropdown(this.getAttribute('id'));
		}
		g_AutoCompleteArray[id]['element'].onclick = function(e) {
			if (!e)
				e = window.event;
			e.cancelBubble = true;
			e.returnValue = false;
		}
	}
	// Hides the dropdowns when document clicked
	var docClick = function() {
		for (id in g_AutoCompleteArray) {
			AutoComplete_HideDropdown(id);
		}
	}

	if (document.addEventListener) {
		document.addEventListener('click', docClick, false);
	} else if (document.attachEvent) {
		document.attachEvent('onclick', docClick, false);
	}

	// Max number of items shown at once
	var maxitems = 10;
	g_AutoCompleteArray[id]['maxitems'] = maxitems;
	g_AutoCompleteArray[id]['firstItemShowing'] = 0;
	g_AutoCompleteArray[id]['lastItemShowing'] = maxitems - 1;

	// AutoComplete_CreateDropdown(id);

	// Prevent select dropdowns showing thru
	if (isIE) {
		g_AutoCompleteArray[id]['iframe'] = document.createElement('iframe');
		g_AutoCompleteArray[id]['iframe'].id = id + '_iframe';
		g_AutoCompleteArray[id]['iframe'].style.position = 'absolute';
		g_AutoCompleteArray[id]['iframe'].style.top = '0';
		g_AutoCompleteArray[id]['iframe'].style.left = '0';
		g_AutoCompleteArray[id]['iframe'].style.width = '0px';
		g_AutoCompleteArray[id]['iframe'].style.height = '0px';
		g_AutoCompleteArray[id]['iframe'].style.zIndex = '98';
		g_AutoCompleteArray[id]['iframe'].style.visibility = 'hidden';
		if(g_AutoCompleteArray[id]['element']){
			g_AutoCompleteArray[id]['element'].parentNode.insertBefore(
					g_AutoCompleteArray[id]['iframe'],
					g_AutoCompleteArray[id]['element']);
		}
	}
}

//Sets the width of the dropdown field
function AutoComplete_SetWidth(id, width) {
	g_AutoCompleteArray[id]['width'] = width;
}
//ESS:Dipti 2009-05-25(15047) For processdeficiency ajax contenets should be set within a user defined height otherwise this will overlap underlying buttonbar
//Sets the Height of the dropdown field
function AutoComplete_SetHeight(id, height) {
	g_AutoCompleteArray[id]['height'] = height;
}

//ESS:Dipti 2009-08-18 CallCenterPeople Detail AddrStreet AutoComplete feature implementation.
function AutoComplete_SetTop(id, top) {
	g_AutoCompleteArray[id]['top'] = top;
}


//Creates the dropdown layer
function AutoComplete_CreateDropdown(id) {
	// alert('AutoComplete_CreateDropdown');
	if(!g_AutoCompleteArray[id]['element'])return;
	var left = AutoComplete_GetLeft(g_AutoCompleteArray[id]['element']);
	var top = AutoComplete_GetTop(g_AutoCompleteArray[id]['element'])
	+ g_AutoCompleteArray[id]['element'].offsetHeight;
	var width = g_AutoCompleteArray[id]['element'].offsetWidth;
	if (g_AutoCompleteArray[id]['width']) {
		width = g_AutoCompleteArray[id]['width'];
	}

	g_AutoCompleteArray[id]['dropdown'] = document.createElement('div');
	g_AutoCompleteArray[id]['dropdown'].className = 'autocomplete'; // Don't use
	// setAttribute()

	g_AutoCompleteArray[id]['element'].parentNode.insertBefore(
			g_AutoCompleteArray[id]['dropdown'],
			g_AutoCompleteArray[id]['element']);

	// Position it
	g_AutoCompleteArray[id]['dropdown'].style.left = left + 'px';
	g_AutoCompleteArray[id]['dropdown'].style.top = top + 'px';
	g_AutoCompleteArray[id]['dropdown'].style.width = width + 'px';
	g_AutoCompleteArray[id]['dropdown'].style.zIndex = '99';
	g_AutoCompleteArray[id]['dropdown'].style.visibility = 'hidden';
}

//Gets left coord of given element
function AutoComplete_GetLeft(element) {
	// a hack for admin windows as it uses relative positioning
	if (document.getElementById('mainDiv')) {
		return element.offsetLeft;
	}
	var curNode = element;
	var left = 0;

	do {
		left += curNode.offsetLeft;
		curNode = curNode.offsetParent;
		// alert(curNode.tagName.toLowerCase());
	} while (curNode.tagName.toLowerCase() != 'body'
		&& curNode.tagName.toLowerCase() != 'html'
			&& curNode.tagName.toLowerCase() != 'table');//Aspire 2009/08/11 - Auto complete position is not correct when table is used

	left = left + 276;	//Hongli 2010.1.21
	return left;
}

//Gets top coord of given element
function AutoComplete_GetTop(element) {
	// a hack for admin windows as it uses relative positioning
	if (document.getElementById('mainDiv')) {
		return element.offsetTop;
	}
	var curNode = element;
	var top = 0;

	do {
		top += curNode.offsetTop;
		curNode = curNode.offsetParent;
	} while (curNode.tagName.toLowerCase() != 'body'
		&& curNode.tagName.toLowerCase() != 'html'
			&& curNode.tagName.toLowerCase() != 'table');//Aspire 2009/08/11 - Auto complete position is not correct when table is used

	top = top + 438;	//Hongli 2010.1.21

	return top;
}

//Shows the dropdown layer
function AutoComplete_ShowDropdown_Field(id) {
	// alert('AutoComplete_ShowDropdown_Field');
	if (!g_AutoCompleteArray[id]['dropdown']) {
		// alert('Creating dropdown');
		AutoComplete_CreateDropdown(id);
	}
	// alert("0");

	AutoComplete_HideAll();

	var value = (g_AutoCompleteArray[id]['element'])?g_AutoCompleteArray[id]['element'].value:"";
	// var toDisplay = new Array();
	var newDiv = null;
	var text = null;
	var numItems = g_AutoCompleteArray[id]['dropdown'].childNodes.length;

	// Remove all child nodes from dropdown
	while (g_AutoCompleteArray[id]['dropdown'].childNodes.length > 0) {
		g_AutoCompleteArray[id]['dropdown']
		.removeChild(g_AutoCompleteArray[id]['dropdown'].childNodes[0]);
	}

	// alert("1");
	// Go thru data searching for matches
	/*
	 * for (i = 0; i < g_AutoCompleteArray[id]['data'].length; ++i) { if
	 * (g_AutoCompleteArray[id]['data'][i].substr(0, value.length).toLowerCase() ==
	 * value.toLowerCase()) { toDisplay[toDisplay.length] =
	 * g_AutoCompleteArray[id]['data'][i]; } }
	 */
	var toDisplay = AutoComplete_Filter_Default(id);

	// alert("2");
	// No matches?
	if (toDisplay.length == 0) {
		// alert('Nothing to dislay');
		AutoComplete_HideDropdown(id);
		return;
	}

	// Add data to the dropdown layer
	for (i = 0; i < toDisplay.length; ++i) {
		newDiv = document.createElement('div');
		newDiv.className = 'autocomplete_item'; // Don't use setAttribute()
		newDiv.setAttribute('id', 'autocomplete_item_' + i);
		newDiv.setAttribute('index', i);
		newDiv.style.zIndex = '99';

		// Scrollbars are on display ?
		if (toDisplay.length > g_AutoCompleteArray[id]['maxitems']
		&& navigator.userAgent.indexOf('MSIE') == -1) {
			if(g_AutoCompleteArray[id]['element'])
				newDiv.style.width = g_AutoCompleteArray[id]['element'].offsetWidth - 22 + 'px';
		}

		newDiv.onmouseover = function() {
			AutoComplete_HighlightItem(g_AutoCompleteArray[id]['element']
			.getAttribute('id'), this.getAttribute('index'));
		};
		newDiv.onclick = function() {
			AutoComplete_SetValue_Default(g_AutoCompleteArray[id]['element']
			.getAttribute('id'));
			AutoComplete_HideDropdown(g_AutoCompleteArray[id]['element']
			.getAttribute('id'));
			f_getHouseNumber();
		}

		text = document.createTextNode(toDisplay[i]);
		newDiv.appendChild(text);

		g_AutoCompleteArray[id]['dropdown'].appendChild(newDiv);
	}
	// alert("3");
	// Too many items?
	if (toDisplay.length > g_AutoCompleteArray[id]['maxitems']) {
		//ESS:Dipti 2009-05-25(15047) For processdeficiency ajax contenets should be set within a user defined height otherwise this will overlap underlying buttonbar
		if(g_AutoCompleteArray[id]['height'])
			g_AutoCompleteArray[id]['dropdown'].style.height = g_AutoCompleteArray[id]['height'];
		else
			g_AutoCompleteArray[id]['dropdown'].style.height = (g_AutoCompleteArray[id]['maxitems'] * 15) + 2 + 'px';
	} else {
		g_AutoCompleteArray[id]['dropdown'].style.height = '';
	}
	// alert("4");

	// Set left/top in case of document movement/scroll/window resize etc
	g_AutoCompleteArray[id]['dropdown'].style.left = AutoComplete_GetLeft(g_AutoCompleteArray[id]['element']);
	//ESS:Dipti 2009-08-18 CallCenterPeople Detail AddrStreet AutoComplete feature implementation.
	if(g_AutoCompleteArray[id]['top'])
		g_AutoCompleteArray[id]['dropdown'].style.top = g_AutoCompleteArray[id]['top'];
	else
		g_AutoCompleteArray[id]['dropdown'].style.top = AutoComplete_GetTop(g_AutoCompleteArray[id]['element'])
		+ g_AutoCompleteArray[id]['element'].offsetHeight;

	// Show the iframe for IE
	if (isIE) {
		g_AutoCompleteArray[id]['iframe'].style.top = g_AutoCompleteArray[id]['dropdown'].style.top;
		g_AutoCompleteArray[id]['iframe'].style.left = g_AutoCompleteArray[id]['dropdown'].style.left;
		g_AutoCompleteArray[id]['iframe'].style.width = g_AutoCompleteArray[id]['dropdown'].offsetWidth;
		g_AutoCompleteArray[id]['iframe'].style.height = g_AutoCompleteArray[id]['dropdown'].offsetHeight;
		g_AutoCompleteArray[id]['iframe'].style.visibility = 'visible';
	}

	// Show dropdown
	if (!g_AutoCompleteArray[id]['isVisible']) {
		// alert('About to show dropdown');
		g_AutoCompleteArray[id]['dropdown'].style.visibility = 'visible';
		g_AutoCompleteArray[id]['isVisible'] = true;
	}

	// If now showing less items than before, reset the highlighted value
	if (g_AutoCompleteArray[id]['dropdown'].childNodes.length != numItems) {
		g_AutoCompleteArray[id]['highlighted'] = null;
	}
}

//Hides the dropdown layer
function AutoComplete_HideDropdown(id) {
	if (g_AutoCompleteArray[id]['iframe']) {
		g_AutoCompleteArray[id]['iframe'].style.visibility = 'hidden';
	}

	//Venkat 2009/04/14 added this condition to ignore if this variable is not initialized
	if (g_AutoCompleteArray[id]['dropdown']) {
		g_AutoCompleteArray[id]['dropdown'].style.visibility = 'hidden';
	}
	g_AutoCompleteArray[id]['highlighted'] = null;
	g_AutoCompleteArray[id]['isVisible'] = false;
}

//Hides all dropdowns
function AutoComplete_HideAll() {
	for (id in g_AutoCompleteArray) {
		AutoComplete_HideDropdown(id);
	}
}

//Highlights a specific item
function AutoComplete_HighlightItem(id, index) {
	if (g_AutoCompleteArray[id]['dropdown'].childNodes[index]) {
		for ( var i = 0; i < g_AutoCompleteArray[id]['dropdown'].childNodes.length; ++i) {
			if (g_AutoCompleteArray[id]['dropdown'].childNodes[i].className == 'autocomplete_item_highlighted') {
				g_AutoCompleteArray[id]['dropdown'].childNodes[i].className = 'autocomplete_item';
			}
		}

		g_AutoCompleteArray[id]['dropdown'].childNodes[index].className = 'autocomplete_item_highlighted';
		g_AutoCompleteArray[id]['highlighted'] = index;
	}
}

//Highlights the menu item with the given index
function AutoComplete_Highlight(id, index) {
	// Out of bounds checking
	if (index == 1
			&& g_AutoCompleteArray[id]['highlighted'] == g_AutoCompleteArray[id]['dropdown'].childNodes.length - 1) {
		g_AutoCompleteArray[id]['dropdown'].childNodes[g_AutoCompleteArray[id]['highlighted']].className = 'autocomplete_item';
		g_AutoCompleteArray[id]['highlighted'] = null;
	} else if (index == -1 && g_AutoCompleteArray[id]['highlighted'] == 0) {
		g_AutoCompleteArray[id]['dropdown'].childNodes[0].className = 'autocomplete_item';
		g_AutoCompleteArray[id]['highlighted'] = g_AutoCompleteArray[id]['dropdown'].childNodes.length;
	}

	// Nothing highlighted at the moment
	if (g_AutoCompleteArray[id]['highlighted'] == null) {
		g_AutoCompleteArray[id]['dropdown'].childNodes[0].className = 'autocomplete_item_highlighted';
		g_AutoCompleteArray[id]['highlighted'] = 0;
	} else {
		if (g_AutoCompleteArray[id]['dropdown'].childNodes[g_AutoCompleteArray[id]['highlighted']]) {
			g_AutoCompleteArray[id]['dropdown'].childNodes[g_AutoCompleteArray[id]['highlighted']].className = 'autocomplete_item';
		}

		var newIndex = g_AutoCompleteArray[id]['highlighted'] + index;
		if (g_AutoCompleteArray[id]['dropdown'].childNodes[newIndex]) {
			g_AutoCompleteArray[id]['dropdown'].childNodes[newIndex].className = 'autocomplete_item_highlighted';
			g_AutoCompleteArray[id]['highlighted'] = newIndex;
		}
	}
}

//Sets the input to a given value
function AutoComplete_SetValue_Default(id) {
	if (window.f_AutoComplete_SetValue) {
		var data = g_AutoCompleteArray[id]['data'][g_AutoCompleteArray[id]['highlighted']];
		f_AutoComplete_SetValue(id, data);
	} else {
		g_AutoCompleteArray[id]['element'].value = g_AutoCompleteArray[id]['dropdown'].childNodes[g_AutoCompleteArray[id]['highlighted']].innerHTML;
		// don't invoke onchange event if there is nothing set
		if (g_AutoCompleteArray[id]['element'].onchange) {
			g_AutoCompleteArray[id]['element'].onchange();
		}
	}
}

//Checks if the dropdown needs scrolling
function AutoComplete_ScrollCheck(id) {
	// Scroll down, or wrapping around from scroll up
	if (g_AutoCompleteArray[id]['highlighted'] > g_AutoCompleteArray[id]['lastItemShowing']) {
		g_AutoCompleteArray[id]['firstItemShowing'] = g_AutoCompleteArray[id]['highlighted']
		- (g_AutoCompleteArray[id]['maxitems'] - 1);
		g_AutoCompleteArray[id]['lastItemShowing'] = g_AutoCompleteArray[id]['highlighted'];
	}

	// Scroll up, or wrapping around from scroll down
	if (g_AutoCompleteArray[id]['highlighted'] < g_AutoCompleteArray[id]['firstItemShowing']) {
		g_AutoCompleteArray[id]['firstItemShowing'] = g_AutoCompleteArray[id]['highlighted'];
		g_AutoCompleteArray[id]['lastItemShowing'] = g_AutoCompleteArray[id]['highlighted']
		+ (g_AutoCompleteArray[id]['maxitems'] - 1);
	}

	g_AutoCompleteArray[id]['dropdown'].scrollTop = g_AutoCompleteArray[id]['firstItemShowing'] * 15;
}

//Function which handles the keypress event
function AutoComplete_KeyDown(id) {
	// Mozilla
	if (arguments[1] != null) {
		event = arguments[1];
	}

	var keyCode = event.keyCode;

	switch (keyCode) {

	// Return/Enter
	case 13:
		if (g_AutoCompleteArray[id]['highlighted'] != null) {
			AutoComplete_SetValue_Default(id);
			AutoComplete_HideDropdown(id);
		}

		event.returnValue = false;
		event.cancelBubble = true;

		f_getHouseNumber();
		break;

		// Escape
	case 27:
		AutoComplete_HideDropdown(id);
		event.returnValue = false;
		event.cancelBubble = true;
		break;

		// Up arrow
	case 38:
		if (!g_AutoCompleteArray[id]['isVisible']) {
			AutoComplete_ShowDropdown(id);
		}

		AutoComplete_Highlight(id, -1);
		AutoComplete_ScrollCheck(id, -1);
		return false;
		break;

		// Tab
	case 9:
		if (g_AutoCompleteArray[id]['isVisible']) {
			AutoComplete_HideDropdown(id);
		}
		return;

		// Down arrow
	case 40:
		if (!g_AutoCompleteArray[id]['isVisible']) {
			AutoComplete_ShowDropdown(id);
		}

		AutoComplete_Highlight(id, 1);
		AutoComplete_ScrollCheck(id, 1);
		return false;
		break;
	}
}

//Function which handles the keyup event
function AutoComplete_KeyUp(id) {
	// Mozilla
	if (arguments[1] != null) {
		event = arguments[1];
	}

	var keyCode = event.keyCode;

	switch (keyCode) {
	case 13:
		event.returnValue = false;
		event.cancelBubble = true;
		break;

	case 27:
		AutoComplete_HideDropdown(id);
		event.returnValue = false;
		event.cancelBubble = true;
		break;

	case 38:
	case 40:
		return false;
		break;

	default:
		AutoComplete_ShowDropdown(id);
	break;
	}
}

//Returns whether the dropdown is visible
function AutoComplete_isVisible(id) {
	return g_AutoCompleteArray[id]['dropdown'].style.visibility == 'visible';
}

function AutoComplete_ShowDropdown(id,showAll) {
	var startsWith = g_AutoCompleteArray[id]['element'].value;
	var lid = document.getElementById("lid").value;
	var url = 'common/AJAX_AutoComplete.jsp?lid=' + lid;
	var http_request = false;
	if (window.XMLHttpRequest) { // Mozilla, Safari,...
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/plain');
		}
	} else if (window.ActiveXObject) { // IE
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
			}
		}
	}
	if (!http_request) {
		// alert('Giving up :( Cannot create an XML HTTP instance');
		return;
	}
	var tableName = g_AutoCompleteArray[id]['tableName'];
	var columnName = g_AutoCompleteArray[id]['columnName'];
	var whereClause = g_AutoCompleteArray[id]['whereClause'];
	if (window.f_AutoComplete_GetWhere) {
		whereClause = f_AutoComplete_GetWhere(id);
	}
	if(showAll == true)
		startsWith = '';
	var parameters = "tableName=" + tableName + "&columnName=" + columnName
	+ "&where=" + whereClause + "&startsWith="
	+ encodeURIComponent(startsWith);
	if (g_AutoCompleteCache[parameters]) {
		//alert('Found in cache');
		AutoComplete_SetData(id, g_AutoCompleteCache[parameters]);
	} else {
		http_request.onreadystatechange = function() {
			AutoComplete_SetResponse(id, http_request, parameters);
		};
		http_request.open('POST', url, true);
		http_request.setRequestHeader("Content-type",
		"application/x-www-form-urlencoded");
		http_request.send(parameters);
	}
}

function AutoComplete_SetResponse(id, http_request, parameters) {
	//alert("Got response from AJAX request: " + http_request.responseText.trim());
	if (http_request.readyState == 4) {
		if (http_request.status == 200) {
			var data = http_request.responseText.trim();
			if (data.substr(0, 6) == "Error:") {
				// alert(data);
			} else {
				// g_AutoCompleteArray[id]['data'] = data.split("\n");
				try {
					var jsonData = eval('(' + data + ')');
					AutoComplete_SetData(id, jsonData);
					g_AutoCompleteCache[parameters] = jsonData;
				} catch (e) {
					alert("Error while parsing JSON response: "+e.message);
				}
			}
		}
	}
}

function AutoComplete_SetData(id, data) {
	g_AutoCompleteArray[id]['data'] = data;
	//alert("JSON evaluation is successful");
	AutoComplete_ShowDropdown_Field(id);
}


function AutoComplete_Filter_Default(id) {
	//alert("AutoComplete_Filter_Default");
	var toDisplay = new Array();
	var value = g_AutoCompleteArray[id]['element'].value;
	var columnNames = g_AutoCompleteArray[id]['columnName'].split(",");
	var columnName = columnNames[0].trim();
	// Go thru data searching for matches
	for (i = 0; i < g_AutoCompleteArray[id]['data'].length; ++i) {
		if (window.f_AutoComplete_ToDisplay) {
			toDisplay[toDisplay.length] = f_AutoComplete_ToDisplay(id, g_AutoCompleteArray[id]['data'][i]);
		} else {
			if (g_AutoCompleteArray[id]['data'][i][columnName].substr(0, value.length).toLowerCase() == value.toLowerCase()) {
				toDisplay[toDisplay.length] = g_AutoCompleteArray[id]['data'][i][columnName];
			}
		}
	}
	return toDisplay;
}

function f_AutoComplete_ToDisplay(id, data) {
	if(id == 'PropStreet') {
		var display = data['PropStreet'] + " " + data['PropStreetType'] + (data['PropStreetDirection'] && data['PropStreetDirection'] != '' ? " " + data['PropStreetDirection'] : "") ;
		return display;
	}
}

function f_AutoComplete_SetValue(id, data) {
	if(id == 'PropStreet') {
		document.getElementById('PropStreet').value = data['PropStreet'];
		document.getElementById('PropStreetType').value = data['PropStreetType'];
		document.getElementById('PropStreetDirection').value = data['PropStreetDirection'];
//		f_getHouseNumber();
	}
}

var posX = 0;
var posY = 0;

function init() {
	if (window.Event) {
		document.captureEvents(Event.MOUSEMOVE);
	}
	document.onmousemove = getXY;
}

function f_submitTarget(argUrl, argTarget) {
	document.theform.action = argUrl;
	document.theform.target = argTarget;
	document.theform.submit();
}

function f_submit(argUrl) {
	document.theform.action = argUrl;
	document.theform.target = '_top';
	document.theform.submit();
}

function f_new_window(argUrl) {
	document.theform.action = argUrl;
	document.theform.target = '_blank';
	document.theform.submit();
}

function f_getNumber(argString) {
	var theChar, theFinal;
	theFinal="";
	if ( argString.length > 0 ) {
		for(var i=0; i < (argString.length); i++){
			theChar = argString.substring(i, i+1);
			if(theChar >= '0' && theChar <= '9') {
				theFinal += theChar;
			}
		}
	}
	return theFinal;
}

function f_isNumber(argString) {
	var theChar;
	if ( argString.length > 0 ) {
		for(var i=0; i < (argString.length); i++){
			theChar = argString.substring(i, i+1);
			if ((theChar < '0' || theChar > '9') && theChar != '.') {
				return false;
			}
		}
	}
	return true;
}

function f_LTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(0)) != -1) {
		var j=0, i = s.length;
		while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
			j++;
		s = s.substring(j, i);
	}
	return s;
}

function f_RTrim(str)
{
	var whitespace = new String(" \t\n\r");
	var s = new String(str);
	if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
		var i = s.length - 1;
		while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
			i--;
		s = s.substring(0, i+1);
	}
	return s;
}

function f_Trim(str)
{
	return f_RTrim(f_LTrim(str));
}

//recursively replaces multiple occurances of text with by
function f_Replace(string,text,by) {
	var strLength = string.length, txtLength = text.length;
	if ((strLength == 0) || (txtLength == 0)) return string;
	var i = string.indexOf(text);
	if ((!i) && (text != string.substring(0,txtLength))) return string;
	if (i == -1) return string;
	var newstr = string.substring(0,i) + by;
	if (i+txtLength < strLength) {
		newstr += f_Replace(string.substring(i+txtLength,strLength),text,by);
	}
	return newstr;
}

//Note, this is 1 offset, so the first character is argStart=1
//using JavaScript slice(startPositionIndex[, endPositionIndex]) might be faster.
//using JavaScript substr(startPositionIndex[, length]) might be faster.
function f_mid(argString, argStart, argLength) {
	argStart = argStart - 1;
	if (argStart > argString.length) { return ""; }
	if (!argLength) {
		return argString.substring(argStart, argString.length);
	}
	var e = argStart + argLength;
	if (e > argString.length) {e = argString.length;}
	return argString.substring(argStart, e);
}

function f_left(argString, argLength) {
	if (argLength > argString.length) {argLength = argString.length;}
	return argString.substring(0, argLength);
}

function f_checkNumeric(ctrl) {
	if (ctrl.value > " ") {
		if (!f_isNumber(ctrl.value)) {
			alert('Invalid number. Non numeric text is not allowed.');
			ctrl.value = '';
			ctrl.focus();
			return;
		}
	}
}

function f_checkDate(ctrl) {
	if (ctrl.value > " ") {
		if (parseDate(ctrl.value) == null) {
			alert('Invalid Date. The value is not compatible with any standard date format.');
			ctrl.value = '';
			ctrl.focus();
			return;
		}
	}
}

function f_messagebox(argIcon, argButton, argHeight, argWidth) {
	MessageboxIcon = argIcon;
	MessageboxButton = argButton;

	if (argHeight == '0') { argHeight = '500'; }
	window.showModalDialog("messagebox.htm",window,"dialogHeight: " + argHeight + "px; dialogWidth: " + argWidth + "px; dialogTop: 200px; dialogLeft: 200px; edge: Raised; center: Yes; help: No; resizable: No; status: No;"); 

	if (document.theform.messageboxReturn.value > ' ') {
		buttonname = document.theform.buttonname.value;
		theButton = document.all.item(buttonname);
		theButton.onclick();
	}
}

function f_messagebox_error() {
	MessageboxIcon = "";
	MessageboxButton = "Cancel";
	window.showModalDialog("messageboxError.htm",window,"dialogHeight: 100px; dialogWidth: 100px; dialogTop: 200px; dialogLeft: 200px; edge: Raised; center: Yes; help: No; resizable: No; status: No;"); 
}

//dw[0] = new f_DW('Property', 1, 40);
function f_DW(argName, argRowCount, argColumnCount) {
	this.name = argName;
	this.rowCount = argRowCount;
	this.columnCount = argColumnCount;
	this.modified = false;
	this.row = new Array(argRowCount + 1);
	this.column = new Array(argColumnCount + 1);
}

//dw[0].column[1] = new f_COLUMN('Property', 'PropHouse', 'string');
function f_COLUMN(argTableName, argColumnName, argColumnType, argPrimaryKey) {
	this.tableName = argTableName;
	this.columnName = argColumnName;
	this.columnType = argColumnType;
	this.primaryKey = argPrimaryKey;
}

//dw[0].row[40] = new f_ROW(40, 'W0404');
function f_ROW(argColumnCount, argPrimaryKey) {
	this.modified = false;
	this.primaryKey = argPrimaryKey;
	this.data = new Array(argColumnCount + 1);
	this.type = "U";
	this.selected = false;
	this.tr = null;
	this.backgroundColor = "#FFFFFF";
}

function f_DATA(argOldValue) {
	this.modified = false;
	this.oldValue = argOldValue;
	this.newValue = argOldValue;
}

function f_ClickChoose(argSpan) {
	var infovalue = document.getElementById('InfoValue_' + chooseRow);
	infovalue.value = argSpan.innerHTML;

	infovalue.onchange();

	var chooseDiv = document.getElementById('choose_' + chooseRow);
	chooseDiv.style.visibility='hidden';
}

function f_PickChoose(argRow) {
	f_Hidden_DataChoose();
	chooseRow = argRow;
	var chooseDiv = document.getElementById("choose_" + argRow);

	chooseDiv.style.visibility='visible';
	if (navigator.appName == "Microsoft Internet Explorer") chooseDiv.focus();
}

function f_Hidden_DataChoose() {
	if (navigator.appName == "Microsoft Internet Explorer") return;

	var els = document.getElementsByTagName('div');
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+"DataChoose"+"(\\s|$)");
	for (i = 0; i < elsLen; i++) {
		if (pattern.test(els[i].className) ) {
			els[i].style.visibility='hidden';
		}
	}
}

function f_pickUnSelectRow(obj,argInfoType){
}
function f_pickSelectRow(obj,argInfoType){
}

function getXY(e) {
	posX = (window.Event) ? e.pageX : event.clientX;
	posY = (window.Event) ? e.pageY : event.clientY;
}

function getPageEventCoords(evt) {
	var coords = {left:0, top:0};
	if (evt.pageX) {
		coords.left = evt.pageX;
		coords.top = evt.pageY;
	} else if (evt.clientX) {
		coords.left = 
			evt.clientX + document.body.scrollLeft - document.body.clientLeft;
		coords.top = 
			evt.clientY + document.body.scrollTop - document.body.clientTop;
		// include html element space, if applicable
		if (document.body.parentElement && document.body.parentElement.clientLeft) {
			var bodParent = document.body.parentElement;
			coords.left += bodParent.scrollLeft - bodParent.clientLeft;
			coords.top += bodParent.scrollTop - bodParent.clientTop;
		}
	}
	return coords;
}

function getPositionedEventCoords(evt) {
	var elem = (evt.target) ? evt.target : evt.srcElement;
	var coords = {left:0, top:0};
	if (evt.layerX) {
		var borders = {left:parseInt(getElementStyle("progressBar", 
				"borderLeftWidth", "border-left-width")),
				top:parseInt(getElementStyle("progressBar", 
						"borderTopWidth", "border-top-width"))};
		coords.left = evt.layerX - borders.left;
		coords.top = evt.layerY - borders.top;
	} else if (evt.offsetX) {
		coords.left = evt.offsetX;
		coords.top = evt.offsetY;
	}
	evt.cancelBubble = true;
	return coords;
}

var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');

function LZ(x) {return(x<0||x>9?"":"0")+x;}

function isDate(val,format) {
	var date=getDateFromFormat(val,format);
	if (date==0) { return false; }
	return true;
}

//-------------------------------------------------------------------
//compareDates(date1,date1format,date2,date2format)
//Compare two date strings to see which is greater.
//Returns:
//1 if date1 is greater than date2
//0 if date2 is greater than date1 of if they are the same
//-1 if either of the dates is in an invalid format
//-------------------------------------------------------------------
function compareDates(date1,dateformat1,date2,dateformat2) {
	var d1=getDateFromFormat(date1,dateformat1);
	var d2=getDateFromFormat(date2,dateformat2);
	if (d1==0 || d2==0) {
		return -1;
	}
	else if (d1 > d2) {
		return 1;
	}
	return 0;
}

//------------------------------------------------------------------
//formatDate (date_object, format)
//Returns a date in the output format specified.
//The format string uses the same abbreviations as in getDateFromFormat()
//------------------------------------------------------------------
function formatDate(date,format) {
	format=format+"";
	var result="";
	var i_format=0;
	var c="";
	var token="";
	var y=date.getYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var E=date.getDay();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	// Convert real date parts into formatted versions
	var value=new Object();
	if (y.length < 4) {y=""+(y-0+1900);}
	value["y"]=""+y;
	value["yyyy"]=y;
	value["yy"]=y.substring(2,4);
	value["M"]=M;
	value["MM"]=LZ(M);
	value["MMM"]=MONTH_NAMES[M-1];
	value["NNN"]=MONTH_NAMES[M+11];
	value["d"]=d;
	value["dd"]=LZ(d);
	value["E"]=DAY_NAMES[E+7];
	value["EE"]=DAY_NAMES[E];
	value["H"]=H;
	value["HH"]=LZ(H);
	if (H==0){value["h"]=12;}
	else if (H>12){value["h"]=H-12;}
	else {value["h"]=H;}
	value["hh"]=LZ(value["h"]);
	if (H>11){value["K"]=H-12;} else {value["K"]=H;}
	value["k"]=H+1;
	value["KK"]=LZ(value["K"]);
	value["kk"]=LZ(value["k"]);
	if (H > 11) { value["a"]="PM"; }
	else { value["a"]="AM"; }
	value["m"]=m;
	value["mm"]=LZ(m);
	value["s"]=s;
	value["ss"]=LZ(s);
	while (i_format < format.length) {
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
		}
		if (value[token] != null) { result=result + value[token]; }
		else { result=result + token; }
	}
	return result;
}

//------------------------------------------------------------------
//Utility functions for parsing in getDateFromFormat()
//------------------------------------------------------------------
function _isInteger(val) {
	var digits="1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i))==-1) { return false; }
	}
	return true;
}

function _getInt(str,i,minlength,maxlength) {
	for (var x=maxlength; x>=minlength; x--) {
		var token=str.substring(i,i+x);
		if (token.length < minlength) { return null; }
		if (_isInteger(token)) { return token; }
	}
	return null;
}

//------------------------------------------------------------------
//getDateFromFormat( date_string , format_string )

//This function takes a date string and a format string. It matches
//If the date string matches the format string, it returns the 
//getTime() of the date. If it does not match, it returns 0.
//------------------------------------------------------------------
function getDateFromFormat(val,format) {
	val=val+"";
	format=format+"";
	var i_val=0;
	var i_format=0;
	var c="";
	var token="";
	var token2="";
	var x,y;
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=1;
	var hh=now.getHours();
	var mm=now.getMinutes();
	var ss=now.getSeconds();
	var ampm="";

	while (i_format < format.length) {
		// Get next token from format string
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
		}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year=_getInt(val,i_val,x,y);
			if (year==null) { return 0; }
			i_val += year.length;
			if (year.length==2) {
				if (year > 70) { year=1900+(year-0); }
				else { year=2000+(year-0); }
			}
		}
		else if (token=="MMM"||token=="NNN"){
			month=0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name=MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
					if (token=="MMM"||(token=="NNN"&&i>11)) {
						month=i+1;
						if (month>12) { month -= 12; }
						i_val += month_name.length;
						break;
					}
				}
			}
			if ((month < 1)||(month>12)){return 0;}
		}
		else if (token=="EE"||token=="E"){
			for (var i=0; i<DAY_NAMES.length; i++) {
				var day_name=DAY_NAMES[i];
				if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
					i_val += day_name.length;
					break;
				}
			}
		}
		else if (token=="MM"||token=="M") {
			month=_getInt(val,i_val,token.length,2);
			if(month==null||(month<1)||(month>12)){return 0;}
			i_val+=month.length;}
		else if (token=="dd"||token=="d") {
			date=_getInt(val,i_val,token.length,2);
			if(date==null||(date<1)||(date>31)){return 0;}
			i_val+=date.length;}
		else if (token=="hh"||token=="h") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>12)){return 0;}
			i_val+=hh.length;}
		else if (token=="HH"||token=="H") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>23)){return 0;}
			i_val+=hh.length;}
		else if (token=="KK"||token=="K") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>11)){return 0;}
			i_val+=hh.length;}
		else if (token=="kk"||token=="k") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>24)){return 0;}
			i_val+=hh.length;hh--;}
		else if (token=="mm"||token=="m") {
			mm=_getInt(val,i_val,token.length,2);
			if(mm==null||(mm<0)||(mm>59)){return 0;}
			i_val+=mm.length;}
		else if (token=="ss"||token=="s") {
			ss=_getInt(val,i_val,token.length,2);
			if(ss==null||(ss<0)||(ss>59)){return 0;}
			i_val+=ss.length;}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
			else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
			else {return 0;}
			i_val+=2;}
		else {
			if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
			else {i_val+=token.length;}
		}
	}
	// If there are any trailing characters left in the value, it doesn't match
	if (i_val != val.length) { return 0; }
	// Is date valid for month?
	if (month==2) {
		// Check for leap year
		if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
			if (date > 29){ return 0; }
		}
		else { if (date > 28) { return 0; } }
	}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return 0; }
	}
	// Correct hours value
	if (hh<12 && ampm=="PM") { hh=hh-0+12; }
	else if (hh>11 && ampm=="AM") { hh-=12; }
	var newdate=new Date(year,month-1,date,hh,mm,ss);
	return newdate.getTime();
}

//------------------------------------------------------------------
//parseDate( date_string [, prefer_euro_format] )

//This function takes a date string and tries to match it to a
//number of possible date formats to get the value. It will try to
//match against the following international formats, in this order:
//y-M-d   MMM d, y   MMM d,y   y-MMM-d   d-MMM-y  MMM d
//M/d/y   M-d-y      M.d.y     MMM-d     M/d      M-d
//d/M/y   d-M-y      d.M.y     d-MMM     d/M      d-M
//A second argument may be passed to instruct the method to search
//for formats like d/M/y (european format) before M/d/y (American).
//Returns a Date object or null if no patterns match.
//------------------------------------------------------------------
function parseDate(val) {
	var preferEuro=(arguments.length==2)?arguments[1]:false;
	generalFormats=new Array('y/m/d','y-M-d','MMM d, y','MMM d,y','MMM d y','y-MMM-d','d-MMM-y','MMM d');
	monthFirst=new Array('M/d/y','M-d-y','M.d.y','MMM-d','M/d','M-d');
	dateFirst =new Array('d/M/y','d-M-y','d.M.y','d-MMM','d/M','d-M');
	var checkList=new Array('generalFormats',preferEuro?'dateFirst':'monthFirst',preferEuro?'monthFirst':'dateFirst');
	var d=null;

	for (var i=0; i<checkList.length; i++) {
		var l=window[checkList[i]];
		for (var j=0; j<l.length; j++) {
			d=getDateFromFormat(val,l[j]);
			if (d>0) { return new Date(d); }
		}
	}
	return null;
}

function trim(str)
{
	return str.replace(/^\s*|\s*$/g,"");
}

var top_v, left_v, screenH = window.screen.height, x;
var vWinCal = true;

function entrCalendar(str_target, str_datetime, event) {
	var browser=navigator.appName;

	left_v = event.clientX;
	top_v = event.clientY + 70;
	x = screenH - top_v;

	if (x < 232) top_v = screenH - 232;
	if (!vWinCal.closed && vWinCal.location) vWinCal.close();

	show_calendar(str_target, str_datetime);	
}

function show_calendar(str_target, str_datetime) {	
	var arr_months = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
	var week_days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
	var n_weekstart = 0; // day week starts from (normally 0 or 1)

	str_datetime = convertDateFormat(str_datetime);

	var dt_datetime = (str_datetime == null || str_datetime =="" ?  new Date() : str2dt(str_datetime));
	var dt_prev_month = new Date(dt_datetime);
	dt_prev_month.setMonth(dt_datetime.getMonth()-1);
	var dt_next_month = new Date(dt_datetime);
	dt_next_month.setMonth(dt_datetime.getMonth()+1);
	var dt_firstday = new Date(dt_datetime);
	dt_firstday.setDate(1);
	dt_firstday.setDate(1-(7+dt_firstday.getDay()-n_weekstart)%7);
	var dt_lastday = new Date(dt_next_month);
	dt_lastday.setDate(0);

	// html generation (feel free to tune it for your particular application)
	// print calendar header
	var str_buffer = new String (
			"<html>\n"+
			"<head>\n"+
			"	<title>Calendar</title>\n"+
			"</head>\n"+
			"<body bgcolor=\"White\" >\n"+
			"<table class=\"clsOTable\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n"+
			"<tr><td bgcolor=\"#4682B4\">\n"+
			"<table cellspacing=\"1\" cellpadding=\"3\" border=\"0\" width=\"100%\">\n"+
			"<tr>\n	<td bgcolor=\"#4682B4\"><a href=\"javascript:window.opener.show_calendar('"+
			str_target+"', '"+ dt2dtstr(dt_prev_month)+"'," + top_v + "," + left_v + ");\">"+
			"<img src=\"images/prev.gif\" width=\"16\" height=\"16\" border=\"0\""+
			" title=\"previous month\"></a></td>\n"+
			"	<td bgcolor=\"#4682B4\" colspan=\"5\">"+
			"<font color=\"white\" face=\"verdana\" size=\"1\">"
			+arr_months[dt_datetime.getMonth()]+" "+dt_datetime.getFullYear()+"</font></td>\n"+
			"	<td bgcolor=\"#4682B4\" align=\"right\"><a href=\"javascript:window.opener.show_calendar('"
			+str_target+"', '"+dt2dtstr(dt_next_month)+"'," + top_v + "," + left_v + ");\">"+
			"<img src=\"images/next.gif\" width=\"16\" height=\"16\" border=\"0\""+
			" title=\"next month\"></a></td>\n</tr>\n"
	);

	var dt_current_day = new Date(dt_firstday);
	// print weekdays titles
	str_buffer += "<tr>\n";
	for (var n=0; n<7; n++)
		str_buffer += "	<td bgcolor=\"#87CEFA\">"+
		"<font color=\"white\" face=\"verdana\" size=\"1\">"+
		week_days[(n_weekstart+n)%7]+"</font></td>\n";
	// print calendar table
	str_buffer += "</tr>\n";
	while (dt_current_day.getMonth() == dt_datetime.getMonth() ||
			dt_current_day.getMonth() == dt_firstday.getMonth()) {
		// print row heder
		str_buffer += "<tr>\n";
		for (var n_current_wday=0; n_current_wday<7; n_current_wday++) {
			if (dt_current_day.getDate() == dt_datetime.getDate() &&
					dt_current_day.getMonth() == dt_datetime.getMonth())
				// print current date
				str_buffer += "	<td bgcolor=\"#FFB6C1\" align=\"right\">";
			else if (dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
				// weekend days
				str_buffer += "	<td bgcolor=\"#DBEAF5\" align=\"right\">";
			else
				// print working days of current month
				str_buffer += "	<td bgcolor=\"white\" align=\"right\">";

			if (dt_current_day.getMonth() == dt_datetime.getMonth())
				// print days of current month
				str_buffer += "<a href=\"javascript:window.opener."+str_target+
				".value='"+dt2dtstr(dt_current_day)+"'; window.close();\">"+
				"<font color=\"black\" face=\"tahoma, verdana\" size=\"1\">";
			else 
				// print days of other months
				str_buffer += "<a href=\"javascript:window.opener."+str_target+
				".value='"+dt2dtstr(dt_current_day)+"'; window.close();\">"+
				"<font color=\"gray\" face=\"verdana\" size=\"1\">";
			str_buffer += dt_current_day.getDate()+"</font></a></td>\n";
			dt_current_day.setDate(dt_current_day.getDate()+1);
		}
		// print row footer
		str_buffer += "</tr>\n";
	}
	// print calendar footer
	str_buffer +=
		"<form name=\"cal\">\n<tr><td colspan=\"7\" bgcolor=\"#87CEFA\">"+
		"<font color=\"White\" face=\"tahoma, verdana\" size=\"1\"></form>\n" +
		"</table>\n" +
		"</tr>\n</td>\n</table>\n" +
		"</body>\n" +
		"</html>\n";
	vWinCal = window.open("", "Calendar", "width=200,height=180,status=no,resizable=no,top=" + top_v + ",left=" + left_v);
	var calc_doc = vWinCal.document;
	calc_doc.write (str_buffer);
	calc_doc.close();
}

function str2dt (str_datetime) {
	var re_date = /^(\d+)\-(\d+)\-(\d+)+$/;
	if (!re_date.exec(str_datetime))
		return alert("Invalid Datetime format: "+ str_datetime);
	return (new Date (RegExp.$3, RegExp.$2-1, RegExp.$1, RegExp.$4, RegExp.$5, RegExp.$6));
}

function dt2dtstr (dt_datetime) {
	var month_v = "";

	if (dt_datetime.getMonth()+1 == 1) {
		month_v = "Jan";
	} 
	else if (dt_datetime.getMonth()+1 == 2) {
		month_v = "Feb";
	}
	else if (dt_datetime.getMonth()+1 == 3) {
		month_v = "Mar";
	}
	else if (dt_datetime.getMonth()+1 == 4) {
		month_v = "Apr";
	}
	else if (dt_datetime.getMonth()+1 == 5) {
		month_v = "May";
	}
	else if (dt_datetime.getMonth()+1 == 6) {
		month_v = "Jun";
	}
	else if (dt_datetime.getMonth()+1 == 7) {
		month_v = "Jul";
	}
	else if (dt_datetime.getMonth()+1 == 8) {
		month_v = "Aug";
	}
	else if (dt_datetime.getMonth()+1 == 9) {
		month_v = "Sep";
	}
	else if (dt_datetime.getMonth()+1 == 10) {
		month_v = "Oct";
	}
	if (dt_datetime.getMonth()+1 == 11) { 
		month_v = "Nov";
	}
	else if (dt_datetime.getMonth()+1 == 12) {
		month_v = "Dec";
	}

	return (new String (month_v + " " + (dt_datetime.getDate()) + ", " + dt_datetime.getFullYear()));
}

function dt2tmstr (dt_datetime) {
	return (new String (dt_datetime.getHours()+":"+dt_datetime.getMinutes()+":"+dt_datetime.getSeconds()));
}

function convertDateFormat(dateArg) {

	if (dateArg.length >= 11) { 
		var month_v = "";
		var indx = dateArg.indexOf(",");
		var day_v = dateArg.substr(4,indx-4);
		var year_v = dateArg.substr(indx+2);
		var found = true;

		if (dateArg.substr(0,3) == "Jan") {
			month_v = "1";
		}
		else if (dateArg.substr(0,3) == "Feb") {
			month_v = "2"; 
		}
		else if (dateArg.substr(0,3) == "Mar") {
			month_v = "3"; 
		}
		else if (dateArg.substr(0,3) == "Apr") {
			month_v = "4"; 
		}
		else if (dateArg.substr(0,3) == "May") {
			month_v = "5"; 
		}
		else if (dateArg.substr(0,3) == "Jun") {
			month_v = "6"; 
		}
		else if (dateArg.substr(0,3) == "Jul") {
			month_v = "7"; 
		}
		else if (dateArg.substr(0,3) == "Aug") {
			month_v = "8"; 
		}
		else if (dateArg.substr(0,3) == "Sep") {
			month_v = "9";
		}
		else if (dateArg.substr(0,3) == "Oct") {
			month_v = "10";
		}
		else if (dateArg.substr(0,3) == "Nov") {
			month_v = "11";
		}
		else if (dateArg.substr(0,3) == "Dec") {
			month_v = "12";
		}
		else {
			found = false;
		}

		if (found) dateArg = day_v + "-" + month_v + "-" + year_v;
	}

	return(dateArg);
}

function compareDates_ (value1, value2) {
	var date1, date2;
	var month1, month2;
	var year1, year2;

	month1 = value1.substring (0, value1.indexOf ("-"));
	date1 = value1.substring (value1.indexOf ("-")+1, value1.lastIndexOf ("-"));
	year1 = value1.substring (value1.lastIndexOf ("-")+1, value1.length);

	month2 = value2.substring (0, value2.indexOf ("-"));
	date2 = value2.substring (value2.indexOf ("-")+1, value2.lastIndexOf ("-"));
	year2 = value2.substring (value2.lastIndexOf ("-")+1, value2.length);

	if (year1 > year2) return true;
	else if (year1 < year2) return false;
	else if (month1 > month2) return true;
	else if (month1 < month2) return false;
	else if (date1 > date2) return true;
	else if (date1 < date2) return false;
	else return false;
} 

function entrCalendar_insp(str_target, str_datetime, event, days) {
	var browser=navigator.appName;

	left_v = event.clientX;
	top_v = event.clientY + 70;
	x = screenH - top_v;

	if (x < 232) top_v = screenH - 232;
	if (!vWinCal.closed && vWinCal.location) vWinCal.close();

	show_calendar_insp(str_target, str_datetime, days);	
}

function show_calendar_insp(str_target, str_datetime, days) {	
	var arr_months = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
	var week_days = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"];
	var n_weekstart = 0; // day week starts from (normally 0 or 1)

	str_datetime = convertDateFormat(str_datetime);

	var dt_datetime = (str_datetime == null || str_datetime =="" ?  new Date() : str2dt(str_datetime));
	var dt_prev_month = new Date(dt_datetime);
	dt_prev_month.setMonth(dt_datetime.getMonth()-1);
	var dt_next_month = new Date(dt_datetime);
	dt_next_month.setMonth(dt_datetime.getMonth()+1);
	var dt_firstday = new Date(dt_datetime);
	dt_firstday.setDate(1);
	dt_firstday.setDate(1-(7+dt_firstday.getDay()-n_weekstart)%7);
	var dt_lastday = new Date(dt_next_month);
	dt_lastday.setDate(0);

	// html generation (feel free to tune it for your particular application)
	// print calendar header
	var str_buffer = new String (
			"<html>\n"+
			"<head>\n"+
			"	<title>Calendar</title>\n"+
			"</head>\n"+
			"<body bgcolor=\"White\" >\n"+
			"<table class=\"clsOTable\" cellspacing=\"0\" border=\"0\" width=\"100%\">\n"+
			"<tr><td bgcolor=\"#4682B4\">\n"+
			"<table cellspacing=\"1\" cellpadding=\"3\" border=\"0\" width=\"100%\">\n"+
			"<tr>\n	<td bgcolor=\"#4682B4\"><a href=\"javascript:window.opener.show_calendar_insp('"+
			str_target+"', '"+ dt2dtstr(dt_prev_month) + "'," + days + ");\">"+
			"<img src=\"images/prev.gif\" width=\"16\" height=\"16\" border=\"0\""+
			" title=\"previous month\"></a></td>\n"+
			"	<td bgcolor=\"#4682B4\" colspan=\"5\">"+
			"<font color=\"white\" face=\"verdana\" size=\"1\">"
			+arr_months[dt_datetime.getMonth()]+" "+dt_datetime.getFullYear()+"</font></td>\n"+
			"	<td bgcolor=\"#4682B4\" align=\"right\"><a href=\"javascript:window.opener.show_calendar_insp('"
			+str_target+"', '"+dt2dtstr(dt_next_month) + "'," + days + ");\">"+
			"<img src=\"images/next.gif\" width=\"16\" height=\"16\" border=\"0\""+
			" title=\"next month\"></a></td>\n</tr>\n"
	);

	var dt_current_day = new Date(dt_firstday);

	myDate = new Date();
	myDate.setDate(myDate.getDate() + days - 1);

	// print weekdays titles
	str_buffer += "<tr>\n";
	for (var n=0; n<7; n++)
		str_buffer += "	<td bgcolor=\"#87CEFA\">"+
		"<font color=\"white\" face=\"verdana\" size=\"1\">"+
		week_days[(n_weekstart+n)%7]+"</font></td>\n";
	// print calendar table
	str_buffer += "</tr>\n";
	while (dt_current_day.getMonth() == dt_datetime.getMonth() ||
			dt_current_day.getMonth() == dt_firstday.getMonth()) {
		// print row heder
		str_buffer += "<tr>\n";
		for (var n_current_wday=0; n_current_wday<7; n_current_wday++) {
			if (dt_current_day.getDate() == dt_datetime.getDate() &&
					dt_current_day.getMonth() == dt_datetime.getMonth())
				// print current date
				str_buffer += "	<td bgcolor=\"#FFB6C1\" align=\"right\">";
			else if (dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
				// weekend days
				str_buffer += "	<td bgcolor=\"#DBEAF5\" align=\"right\">";
			else
				// print working days of current month
				str_buffer += "	<td bgcolor=\"white\" align=\"right\">";

			if (dt_current_day.getMonth() == dt_datetime.getMonth()) {
				// print days of current month
				if (myDate.getTime() >= dt_current_day.getTime() || dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
					str_buffer += "<font color=\"black\" face=\"tahoma, verdana\" size=\"1\">";
				else
					str_buffer += "<a href=\"javascript:window.opener."+str_target+
					".value='"+dt2dtstr(dt_current_day)+"'; window.close();\">"+
					"<font color=\"black\" face=\"tahoma, verdana\" size=\"1\">";
			} else {
				// print days of other months
				if (myDate.getTime() > dt_current_day.getTime()|| dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
					str_buffer += "<font color=\"gray\" face=\"verdana\" size=\"1\">";
				else
					str_buffer += "<a href=\"javascript:window.opener."+str_target+
					".value='"+dt2dtstr(dt_current_day)+"'; window.close();\">"+
					"<font color=\"gray\" face=\"verdana\" size=\"1\">";
			}

			if (myDate.getTime() > dt_current_day.getTime() || dt_current_day.getDay() == 0 || dt_current_day.getDay() == 6)
				str_buffer += dt_current_day.getDate()+"</font></td>\n";
			else
				str_buffer += dt_current_day.getDate()+"</font></a></td>\n";
			dt_current_day.setDate(dt_current_day.getDate()+1);
		}
		// print row footer
		str_buffer += "</tr>\n";
	}
	// print calendar footer
	str_buffer +=
		"<form name=\"cal\">\n<tr><td colspan=\"7\" bgcolor=\"#87CEFA\">"+
		"<font color=\"White\" face=\"tahoma, verdana\" size=\"1\"></form>\n" +
		"</table>\n" +
		"</tr>\n</td>\n</table>\n" +
		"</body>\n" +
		"</html>\n";
	vWinCal = window.open("", "Calendar", "width=200,height=180,status=no,resizable=no,top=" + top_v + ",left=" + left_v);
	var calc_doc = vWinCal.document;
	calc_doc.write (str_buffer);
	calc_doc.close();
}

function createXMLHttpRequest()
{
	var newXmlHttp;

	if (window.ActiveXObject){
		newXmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else{
		newXmlHttp = new XMLHttpRequest();
	}  
	return newXmlHttp;  
}

function f_logon(){
	f_submit('logon.jsp');
}

function f_logoff(){
	f_submit('logoff.jsp');
}

function f_print() {
	window.print();
}

function f_onload() {

}

function f_help() {
	if (document.theform.HelpLink) {
		var HelpLink = document.theform.HelpLink.value;

		if (HelpLink > "")
			f_submitTarget('common/Help.html#' + HelpLink,'Help');
		else
			f_submitTarget('common/Help.html','Help');
	} else
		f_submitTarget('common/Help.html','Help');
}

function f_submit1(argUrl) {
	theform.action = argUrl;
	theform.submit();
}

//ESS: Subhashis Enhancement Id: 40582 03/19/2015 - Added for Validating mandatory fields

function validateMandatoryFields(){
	var inputArraySize = document.getElementsByTagName("input");
	var selectTagArraySize = document.getElementsByTagName("select");
	for(var i=0; i < inputArraySize.length; i++){
		if(document.getElementsByTagName("input")[i].className == "mandatory"){
			document.getElementsByTagName("input")[i].onblur = (function(){if(this.value != ""){
				this.style.backgroundColor = "white";
			}
			else{
				this.style.backgroundColor = "#FFDAEC";
			}
			});
		}
	}
	for(var i=0; i < selectTagArraySize.length; i++){
		if(document.getElementsByTagName("select")[i].className == "mandatory"){
			document.getElementsByTagName("select")[i].onblur = (function(){if(this.value != ""){
				this.style.backgroundColor = "white";
			}
			else{
				this.style.backgroundColor = "#FFDAEC";
			}
			});
		}
	}
}

function Captcha(){
	var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','1','2','3','4','5','6','7','8');
	var i;
	for (i=0;i<6;i++){
		var a = alpha[Math.floor(Math.random() * alpha.length)];
		var b = alpha[Math.floor(Math.random() * alpha.length)];
		var c = alpha[Math.floor(Math.random() * alpha.length)];
		var d = alpha[Math.floor(Math.random() * alpha.length)];
		var e = alpha[Math.floor(Math.random() * alpha.length)];
		var f = alpha[Math.floor(Math.random() * alpha.length)];
		var g = alpha[Math.floor(Math.random() * alpha.length)];
	}
	var code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e + ' '+ f + ' ' + g;
	document.getElementById("mainCaptcha").style.fontStyle = 'italic';
	document.getElementById("mainCaptcha").innerHTML = code;
}
function ValidCaptcha(){
	var string1 = removeSpaces(document.getElementById('mainCaptcha').innerHTML);
	var string2 = removeSpaces(document.getElementById('txtInput').value);
	if (string1 == string2){
		return true;
	}
	else{        
		return false;
	}
}
function removeSpaces(string){
	return string.split(' ').join('');
}
