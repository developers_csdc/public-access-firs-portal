var formDataJson = {}, formData = new FormData(), tccReady = false;
var formSubmittedFlag = false;

//Preview data after clicking preview button
$("#preview_form").on("click", function(){
	var flag = validateFieldsByDiv("tccapp_form", formDataJson);
	
	var WithholdingTax = $("input:radio[name=WithholdingTax]:checked").val();
	//Adding recent WithholdingTaxFlag to formDataJson.
	formDataJson["WithholdingTax"] = WithholdingTax;
	
	if(flag){
		previewTCCApplication();
	}
	//console.log(formDataJson);
})

//Inserting attachment into FormData attachment before form submitting
function submitAttachments(){
	$("[type = file]").each(function(){
		if(!$(this).val() == ""){
			formData.append($(this).attr("id"), $(this)[0].files[0]);
		}
	});

}

//Setting of data from input fields into preview screen
function previewTCCApplication(){
	$.each(formDataJson, function(k, v){
		$("."+k).text(v);
	})

	$("#ForexProfileFormDiv").hide();
	$("#PreviewDiv").removeClass("hide");
	$("#PreviewDiv").fadeIn();
}

//Checkbox confirmition and activating the submit button on check
$("#confirm_form").click(function(){
	if($("#confirm_form").prop("checked") == true) {   
		$("#submit_forex_form").attr('disabled',false);
	}else{
		$("#submit_forex_form").attr('disabled',true);
	}
});

//Submitting form
$("#submit_forex_form").on("click", function(e){
	
	$(this).attr("disabled", "disabled");
	$(this).text("Please wait..");
	
	submitAttachments();
	formData.append("inputData", JSON.stringify(formDataJson));
	
	var url = "ForexTCCApplicationAjax.jsp?"
		
	$.ajax({
	    url: url,
	    data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        datatype:'json',
	    success: function(responseJson){
	    	if(responseJson.result == "success"){
	    		alert("TCC application Submitted.", "success");
	    		formSubmittedFlag = true;
				localStorage.setItem('applicationFlag', true);
	    		$("#successApplicationNumberDisplay").text(responseJson.folderRefNo);
	    		$("#PreviewDiv").hide();
	    		$("#ConfirmationResponseStatus").removeClass("hide");;
	    	}
	    	
	    }
	});
});

//Verifying previous TCC No.
$("#verifyTCCButton").on("click", function() {
    var $el = $("#PrevTCCNo");
    var value = $el.val();
    var $btn = $(this);
    var $loader = $("#verifyTCCLoader");
    if (tccReady) {
        resetTCC();
    } else {
        if (value != null && value != "" && validateFormElement(value, $el.attr("pattern")) == null) {
            blockSubmit = true;
            $loader.removeClass("hide");
            $el.attr("disabled", true);
            $btn.attr("disabled", true);
            $.ajax({
                url: "TccApplicationFormAjax.jsp",
                type: "POST",
                dataType: "json",
                data: {
                    "operation": "verifyTCC",
                    tcc: value
                },
                success: function(res) {
                    $loader.addClass("hide");
                    $btn.attr("disabled", false);
                    blockSubmit = false;
                    if (res.Success) {
                        $("#TCCDate").val(res.issueDate);
                        tccReady = true;
                        $("#verifyTCCText").text("Reset");
                    } else {
                        $el.attr("disabled", false);
                        $("#TCCDate").val("");
                        $("#PrevTCCNo").val("");
                        $("#verifyTCCText").text("Verify");
                        alert(res.errorMessage, "error");
                        $("#PrevTCCNo").focus();
                    }
                },
                error: function() {
                    $("#verifyTCCText").text("Verify");
                    $el.attr("disabled", false);
                    $loader.addClass("hide");
                    $el.attr("disabled", false);
                    $("#TCCDate").val("");
                    $("#PrevTCCNo").val("");
                    alert("Could not proceed due to network issue. Please try later");
                    blockSubmit = false;
                }
            });
        }
    }
});

//Reset TCC
function resetTCC() {
    tccReady = false;
    var $el = $("#PrevTCCNo");
    var $loader = $("#verifyTCCLoader");
    $el.attr("disabled", false);
    $("#verifyTCCText").text("Verify");
    $("#verifyTCCText").text("Verify");
    $el.attr("disabled", false);
    $loader.addClass("hide");
    $el.attr("disabled", false);
    $("#TCCDate").val("");
    $("#PrevTCCNo").val("");
}

//Navigations for show hide sections
$(document).ready(function(){
	var applicationFlag = localStorage.getItem('applicationFlag');
	if(applicationFlag){
		$.each($(".list-group li a"), function(obj){
			if($(this).attr("data-target") == "ApplicationFormDiv"){
				$(".list-group li").removeClass("active");
				$(this).parent("li").addClass("active");
				var curDiv = $(this).attr("data-target");
				$(".showHideDiv").hide();
				$(".showHideDiv").removeClass("hide");
				$("#"+curDiv).show();
			}
		})
		localStorage.removeItem('applicationFlag');
	}
	$(".list-group li a").on("click", function(){
		$(".list-group li").removeClass("active");
		$(this).parent("li").addClass("active");
		var curDiv = $(this).attr("data-target");
		
		if(curDiv == "ApplicationFormDiv" && formSubmittedFlag){
			window.location.reload();
			return false;
		}
		
		$(".showHideDiv").hide();
		$(".showHideDiv").removeClass("hide");
		$("#"+curDiv).show();
	})
})

//Showing the fields on payer is different
$("#isPayerDifferent").on("change", function(){
	if($(this).prop("checked")){
		$(".isPayerDifferentDiv").fadeIn();
	}else{
		$(".isPayerDifferentDiv").fadeOut();
	}
})

//Showing and hiding attachment according to radio button select
$("[name='WithholdingTax']").on("change", function(){
	if($(this).val() == "Yes"){
		$(".isWithholdingTax").fadeIn();
		$("#PaymentEvidence").addClass("required");
	}else{
		$(".isWithholdingTax").fadeOut();
		$("#PaymentEvidence").removeClass("required");
		$("#PaymentEvidence").val("");
		$("#PaymentEvidence").next(".error").html("");
	}
})

//Going back to form for further editing on clicking back button
$("#form_back").on("click", function(){
	$("#PreviewDiv").fadeOut();
	$("#ForexProfileFormDiv").fadeIn();
})