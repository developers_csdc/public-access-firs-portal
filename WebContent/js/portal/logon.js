/*
$( document ).ready(function() {
    $('#showMessageNeedToLogin').hide();
    
    if(showMsgInLogonPage=="y")
    	$('#showMessageNeedToLogin').show();
});

function showForgotPasswordLink(flag){
	if(flag==true){
		$("#forgotPassword").html('<a href="javaScript:" onclick="f_checkForgotPassword()" style="font-size: 14px;margin-right: 10px">'+LABEL_MENU_FORGOT_PASSWORD+'</a>');
	}else{
		$("#forgotPassword").html('<div><span><i class="fa-li fa fa-spinner fa-spin" style="position: initial;"></i>'+LABEL_BTN_PLEASE_WAIT+'</span></div>');
	}
}

function checkEmail(){
showForgotPasswordLink(false)
	var testresults;
	var str = $('#UserName').val();
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

	if (filter.test(str)){
		testresults=true;
	}else{
		$('#userName').addClass('has-error has-feedback');
		alert(MESSAGE_ALERT_ON_INCORRECT_EMAIL);
		$('#UserName').focus();
		//document.getElementById("EmailAddress").focus();
		testresults=false;
		showForgotPasswordLink(true)
	}

	return (testresults);
}

function f_login(){
	if(f_checksubmit()){
		var url = 'logonAction.jsp';
		if(reqFrom!='')
			url += '?from='+reqFrom;
		f_submit(url);
	}	
}
function f_checksubmit() {

	if (document.getElementById("UserName").value.length == 0) {
		$('#userName').addClass('has-error has-feedback');
		alert(MESSAGE_ALERT_ON_EMPTY_USERNAME);
		document.getElementById("UserName").focus();
		return false;
	}
	else{
		$('#userName').removeClass('has-error has-feedback');
	}

	if (document.getElementById("PassWord").value.length == 0) {
		$('#passWord').addClass('has-error has-feedback');
		alert(MESSAGE_ALERT_ON_EMPTY_PASSWORD);
		document.getElementById("PassWord").focus();
		return false;
	}
	else{
		$('#passWord').removeClass('has-error has-feedback');
	}

	return true;
}

function f_checkForgotPassword(){
showForgotPasswordLink(false)
	var UserName = $('#UserName').val();
	if(UserName==""){
		$('#userName').addClass('has-error has-feedback');
		alert(MESSAGE_ALERT_FOR_FORGOT_PASSWORD);
		$('#UserName').focus();
		showForgotPasswordLink(true)
		return false;
	}
	else if(checkEmail()){
		$.ajax({
			url: "forgotPasswordAjax.jsp?pageName=logon&emailId="+UserName,
			type: "POST",
			dataType: "json",
			success: function(jsondata){
				if("success"==jsondata.responseText){
					$("#theform").attr("action", "portal?cmd=forgot_password&LogonName="+UserName);
					document.theform.submit();
				}
				else if("failed"==jsondata.responseText){
					showForgotPasswordLink(true)
					alert("Invalid Username Provided.");
				}
			}
		});
	}
}*/
/* FIRS Portal Log-in starts*/
var tccData;
var url;
$(document).ready(function(){
	
	$("#btnLogin").click(function(){
		if($("#expression-result").val()!==null && $("#expression-result").val()!==""){
		if (ValidBotBoot()){
        	var $userName,$password;
    		clearLoginSection();
    		
    		$username = $("#username_FP_Login").val();
    		$password = $("#password_FP_Login").val();
    		
    		$("#login_FP_error").html("");
    		var checkFlag = mandatoryFieldCheck($username,$password);
    		if(!checkFlag){
    			alert("TIN/Password cannot left blank","error");
    		}else{
    			submitRegistration($username,$password);
    		}
        }else{
        	alert("Captcha Mismatch","error");
        	DrawBotBoot();
        	$("#expression-result").val("");
        }
		}else{
			alert("Captcha cannot left blank","error");
		}
	});
	/*Change password section starts*/
	$("#update-password").click(function(){
		var $newPassword,$confirmNewPassword;
		var errorMessage;
		
		$("#change-password-bottons").removeClass("hide");
		
		$("#password-error").addClass("hide");
		$("#password-error").html("");
		
		$newPassword = $("#new-password").val();
		$confirmNewPassword = $("#confirm-new-password").val();
		if(($newPassword !=null && $newPassword !="")||($confirmNewPassword !=null && $confirmNewPassword !="")){
			var passwordCheckFlag = passwordCheck($newPassword,$confirmNewPassword);
		}else{
			errorMessage = "Fields cann't be left blank";
			$("#password-error").removeClass("hide");
			$("#password-error").html(errorMessage);
			$("#change-password-bottons").removeClass("hide");
		}
		if(passwordCheckFlag != undefined){
		if(passwordCheckFlag){
			updatePassword($newPassword,$confirmNewPassword);
		}else{
			errorMessage = "Password didnot match.Try Again";
			$("#password-error").removeClass("hide");
			$("#password-error").html(errorMessage);
			$("#change-password-bottons").removeClass("hide");
		}
		}
	});
});

function mandatoryFieldCheck(userName,password){
	if((userName!="")&&(userName!=null)){
		if((password!="")&&(password!=null)){
			return true;
		}
	}return false;
}
function clearLoginSection(){
	$("#login_FP_error").html("");
	$("#username_FP_Login").removeClass("form-control-error");
	$("#password_FP_Login").removeClass("form-control-error");
}
function submitRegistration(userName,password){
	
	$("#btnLogin").attr("disabled","disabled");
	$("#resetLoginFrontPage").attr("disabled","disabled");
	$("#username_FP_Login").attr("disabled","disabled");
	$("#password_FP_Login").attr("disabled","disabled");
	$("#btnLogin").html("Logging in..")
	//$("#logon-loader-section").removeClass("hide");
	//$("#logon-form-section").addClass("hide");
	
	tccData={"command":"loginUser","userName":userName,"password":password};	
	url="login_ajax.jsp";
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:tccData,
			success: function(res){
				$("#username_FP_Login").val("");
				$("#password_FP_Login").val("");
				if(res.result){
					   if(res.result=="2"){
						   	//$("#logon-loader-section").addClass("hide");
						   	//$("#logon-form-section").removeClass("hide");
						   	alert(res.errorMessage,"error");
						   	$("#username_FP_Login").removeAttr("disabled","disabled");
							$("#password_FP_Login").removeAttr("disabled","disabled");
						   	$("#btnLogin").removeAttr("disabled","disabled");
							$("#resetLoginFrontPage").removeAttr("disabled","disabled");
							$("#btnLogin").html("Login")
							$("#expression-result").val("");
				        	DrawBotBoot();
					   }else{
						   $(location).attr('href',res.page);	
					   }
				   }else{
					   //$("#logon-loader-section").addClass("hide");
					   //$("#logon-form-section").removeClass("hide");
					   alert("Some error occured");
					   $("#username_FP_Login").removeAttr("disabled","disabled");
					   $("#password_FP_Login").removeAttr("disabled","disabled");
					   $("#btnLogin").removeAttr("disabled","disabled");
					   $("#resetLoginFrontPage").removeAttr("disabled","disabled");
					   $("#btnLogin").html("Login")
					  //location.reload();
				   }
	   		},
			error: function(res){
				//$("#logon-loader-section").addClass("hide");
			   	//$("#logon-form-section").removeClass("hide");
				alert("Some error occured");
				$("#username_FP_Login").removeAttr("disabled","disabled");
				$("#password_FP_Login").removeAttr("disabled","disabled");
				$("#btnLogin").removeAttr("disabled","disabled");
				$("#resetLoginFrontPage").removeAttr("disabled","disabled");
				$("#btnLogin").html("Login")
				//location.reload();
			}
		});
}

function passwordCheck(password,confirmPassword){
	if(password==confirmPassword){
		return true;
	}return false;
}

function updatePassword(password,confirmPassword){
	passwordData={"command":"changePassword","newPassword":password};	
	url="login_ajax.jsp";
	
	$("#change-password-bottons").addClass("hide");
	$("#change-password-loader").removeClass("hide");
	
	$("#new-password").attr("disabled","disabled");
	$("#confirm-new-password").attr("disabled","disabled");
	
		$.ajax({
			url: url,
			type: "POST",
			dataType: "json",
			async:true,
			data:passwordData,
			success: function(res){	
				if(res.result){
					   if(res.result=="2"){
						   alert("Some error occured Please try Again","error");
						   location.reload();
					   }else{
						   $(location).attr('href',res.page);	
					   }
				   }else{
					   alert("Some error occured Please try Again","error");
					   location.reload();
				   }
	   		},
			error: function(res){
				alert("Some error occured Please try Again","error");
				location.reload();
			}
		});
}

function resetLoginProcess(){
	$("#login_FP_error").html("");
	$("#username_FP_Login").removeClass("form-control-error");
	$("#password_FP_Login").removeClass("form-control-error");
	$("#username_FP_Login").val("");
	$("#password_FP_Login").val("");
	$("#login_FP_error").html("");
	$("#login_FP_error").show();
}

function resetChangePasswordSection(){
	$("#new-password").val("");
	$("#confirm-new-password").val("");
	$("#change-password-loader").addClass("hide");
	$("#password-error").addClass("hide");
	$("#password-error").html("");
}






















