<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderPeople"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page	import="com.csdcsystems.amanda.client.stub.WsDisplayInfoOptionFlag"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.client.AmandaClient"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@include file="include/includeconstant.jsp"%>
<%@ page language="java" contentType="application/x-www-form-urlencoded;charset=utf-8" pageEncoding="ISO-8859-1"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>

<%!
	String getPeopleType(int code) {
		switch (code) {
		case 3:
			return "Corporate";
		case 4:
			return "Individual";
		case 5:
			return "Forex";
		default:
			return "";
		}
	}

	private boolean isAccountActive(WsPeople people) {
		boolean flag = false;
		if (PortalUtility.getString(people.getSmsFlag()).equalsIgnoreCase("Y")) {
			flag = true;
		}
		return flag;
	}
	
	private void updateDateOfIncorporation(String INCDate, String peopleRSN, String lid, int DATE_OF_INC_CUSTOM_CODE) throws Exception {
		/* Getting PAYMENTREFERENCE from ACCOUNTPAYMNT.PAYMENTRESPONSE1 by compairing PAYMENTRESPONSE2 of both folder and its Parent Folder */
		WsTransactionRequest[] transactionRequests2 = new WsTransactionRequest[2];
		transactionRequests2[0] = new WsTransactionRequest("argDateOfincorporation",INCDate);
		transactionRequests2[1] = new WsTransactionRequest("argPeopleRSN",peopleRSN);		
		WsTransactionResponse[] transactionResponse2 = PortalService.getWSClient().executeCustomTransaction(lid, DATE_OF_INC_CUSTOM_CODE, transactionRequests2);
		System.out.print(new Date().toString()+" printing updateDateOfIncorporation Result >>>"+String.valueOf(transactionResponse2[0].getColumnValues().toString()));
		
	}
%>
<%
	log.info("Entering-->RegistrationAjax.jsp");
	System.out.println(new Date().toString()+"  Entering >>> RegistrationAjax.jsp");
	
	String tinNumber = PortalUtility.getString(request.getParameter("tinNumber"));
	String peopleType = PortalUtility.getString(request.getParameter("peopleType"));
	String name = PortalUtility.getString(request.getParameter("name"));
	String taxOffice = PortalUtility.getString(request.getParameter("taxOffice"));
	String taxOfficeId = PortalUtility.getString(request.getParameter("taxOfficeId"));
	String email = PortalUtility.getString(request.getParameter("email"));
	String phone = PortalUtility.getString(request.getParameter("phone"));
	String newTaxOffice = PortalUtility.getString(request.getParameter("newTaxOffice"));
	String newEmail = PortalUtility.getString(request.getParameter("newEmail"));
	String newMobile = PortalUtility.getString(request.getParameter("newMobile"));
	String newAddress = PortalUtility.getString(request.getParameter("newAddress"));
	String RCNo = PortalUtility.getString(request.getParameter("RCNo"));
	String JTBTIN = PortalUtility.getString(request.getParameter("JTBTIN"));
	String oldAddress = PortalUtility.getString(request.getParameter("oldAddress"));
	String taxOfficeIdExisting = PortalUtility.getString(request.getParameter("taxOfficeIdExisting"));
	String dateOfincorporation =PortalUtility.getString(request.getParameter("newdateofincor")+" 06:00:00");
	String applicanttype=request.getParameter("applicanttype");
	
	/* Added By ESS:Samarjit for getting path for Activation Link */
	StringBuffer requestURL = request.getRequestURL();
	
	String t_tempURLWithGetURL = requestURL.toString();
	String t_contextURL = t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));

	/*added by puja on 06-10-2017 to add date of incorporation in people*/
	/*SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	Date peopledob = formatter.parse(request.getParameter("newdateofincor"));
	System.out.println(request.getParameter("newdateofincor"));
	Calendar cal = Calendar.getInstance();
	cal.setTime(peopledob);
	System.out.println(cal.getTime());*/
	//String newdateofincor = PortalUtility.getDate(cal);

	JSONObject res = new JSONObject();
	int folderRSN;
	int peopleRSN;
	boolean peolpleExist = false;
	boolean folderExist = false;
	boolean isAccountActiveFlag = false;

	final int TAX_OFFICE_EXISTING_INFOCODE = 5090;
	final int EMAIL_INFOCODE = 5025;
	final int PHONE_INFOCODE = 5030;
	final int DATE_OF_INC_CUSTOM_CODE = 120;
	
	//Samarjit:26.03.2018 If people is a CORPORATE,a INDIVIDUAL or a FOREX then that is a CORE one.
	boolean isPeopleACoreType = false;

	/*Prepairing peopleInfoMap*/
	Map<Integer, String> peopleInfoMap = new HashMap<Integer, String>();

	peopleInfoMap.put(TAX_OFFICE_EXISTING_INFOCODE, taxOfficeIdExisting);
	peopleInfoMap.put(EMAIL_INFOCODE, email);
	peopleInfoMap.put(PHONE_INFOCODE, phone);

	try {
		/*
		WsFolder existingFolder=PortalService.getREGDFolderByTIN(tinNumber);
			
		if(existingFolder != null){
		res.put("Success", false);
		if(existingFolder.getStatusCode() == 1){
			res.put("errorMessage","You Application Registration With File No."+existingFolder.getFolderRSN()+" Is Under Review.");
		}else if(existingFolder.getStatusCode() == 3){
			res.put("errorMessage","You Application Is Already Registered With File No."+existingFolder.getFolderRSN()+".");
		}else if(existingFolder.getStatusCode() == 6){
			res.put("errorMessage","You Registration Application Is Rejected.Kindly Contact Your Tax Office.");
		}else if(existingFolder.getStatusCode() == 8){
			res.put("errorMessage","You Registration Application Is Already Registered And Is In-Progress.Kindly Contact Your Tax Office.");
		}
		}else{
		*/
		
		WsPeople existingPeople = PortalService.getPeopleByTIN(tinNumber);

		if (!PortalUtility.isNull(existingPeople)) {
			int peopleCode = PortalUtility.getInt(existingPeople.getPeopleCode());
			
			peolpleExist = PortalUtility.getInt(existingPeople.getPeopleCode()) > 1 ? true : false;
			isPeopleACoreType = peopleCode >= 3 && peopleCode <= 5 ? true : false;
			isAccountActiveFlag = isAccountActive(existingPeople);
		}
		if (!peolpleExist) {
			/*Getting peopleInfos Object*/
			WsPeopleInfo[] peopleinfos = PortalService.getInfosObject(peopleInfoMap);

			WsPeople people = new WsPeople();
			people.setOrganizationName(name);
			people.setAddressLine1(newAddress);
			people.setAddressLine2(oldAddress);

			if (peopleType.equalsIgnoreCase("Corporate")) {
				people.setPeopleCode(3);
			} else if (peopleType.equalsIgnoreCase("Individual")) {
				people.setPeopleCode(4);
			} else if (peopleType.equalsIgnoreCase("Forex")) {
				people.setPeopleCode(5);
			}

			people.setReferenceFile(RCNo);
			//people.setBirthDate(IncorporationDate);//Not available in TIN Web Service
			people.setLicenceNumber(tinNumber);
			people.setEmailAddress(email);
			people.setPhone1(phone);
			//people.setPhone1Desc("Old Phone No.");
			people.setPhone2(newMobile);
			//Adding JTBTIN to people
			people.setSocialSecurityNumber(JTBTIN);
			//people.setPhone2Desc("New Phone No.");
			//people.setBirthDate(PortalUtility.getCalendarObject(request.getParameter("newdateofincor")+" 00:00:00"));
			//added by puja to set the birth date of people

			//Generating one random String and setting password in internetAnswer column
			String randomString = PortalService.getRandomString(8);
			people.setInternetAnswer(randomString);

			peopleRSN = PortalService.getWSClient().addPeople(lid, people);
			System.out.println(new Date().toString()+" printing peopleRSN >>>"+String.valueOf(peopleRSN));
			
			boolean updatePeopleInfoFlag = PortalService.getWSClient().updatePeopleInfo(lid, peopleRSN,peopleinfos);
			System.out.println(new Date().toString()+" printing updatePeopleInfoFlag >>>"+String.valueOf(updatePeopleInfoFlag));
			
			System.out.println(dateOfincorporation);
			if(applicanttype.equals("CORPORATE") || applicanttype.equals("FOREX")){
				//Updating Incorporation Date
				updateDateOfIncorporation(dateOfincorporation, String.valueOf(peopleRSN), lid, DATE_OF_INC_CUSTOM_CODE);
			}
			
			String fromEmailAddress = "eservices@firs.gov.ng";

			//Sending Email to user with Password and Activation Link.
			boolean mailFlag = PortalService.getWSClient().sendHtmlEmail(lid, email, fromEmailAddress, "",
					"User Account Activation",
					"Dear "+name+",<br/><br/>Your first time login-password is :<br/><br/>Password : <b style='color:red;'>"
							+ randomString + "</b><br><br>"
							+ "<br/><br/>Activate your account by Clicking This link:<br/><br/><a href="
							+ PortalService.getActivationURL(Integer.toString(peopleRSN), randomString,t_contextURL)
							+ ">"
							+ PortalService.getActivationURL(Integer.toString(peopleRSN), randomString,t_contextURL)
							+ "</a><br><br>"
							+ "<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."
							+ "<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."
							+ "The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."
							+ "The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."
							+ "Before opening and accessing the attachment, if any, please check and scan for virus.</div>");
			
			System.out.println(new Date().toString()+" printing mailFlag >>>"+String.valueOf(mailFlag));
			res.put("updatePeopleInfoFlag", updatePeopleInfoFlag);
			res.put("peopleRSN", peopleRSN);
			res.put("type", getPeopleType(people.getPeopleCode()));
			res.put("Success", true);
			log.info("peopleRSN :" + peopleRSN);
		} else {
			/* IF people is not belonging to (People Code 3,4,5) then updating the PEOPLE as well as PEOPLEINFO */
			if (!isAccountActiveFlag || !isPeopleACoreType) {
				/*Getting peopleInfos Object*/
				WsPeopleInfo[] peopleinfos = PortalService.getInfosObject(peopleInfoMap);

				WsPeople people = new WsPeople();
				people.setOrganizationName(name);
				people.setAddressLine1(newAddress);
				people.setAddressLine1(oldAddress);

				if (peopleType.equalsIgnoreCase("Corporate")) {
					people.setPeopleCode(3);
				} else if (peopleType.equalsIgnoreCase("Individual")) {
					people.setPeopleCode(4);
				} else if (peopleType.equalsIgnoreCase("Forex")) {
					people.setPeopleCode(5);
				}

				people.setReferenceFile(RCNo);
				//people.setBirthDate(IncorporationDate);//Not available in TIN Web Service
				people.setLicenceNumber(tinNumber);
				people.setEmailAddress(email);
				people.setPhone1(phone);
				//people.setPhone1Desc("Old Phone No.");
				people.setPhone2(newMobile);
				//Adding JTBTIN to people
				people.setSocialSecurityNumber(JTBTIN);
				//people.setPhone2Desc("New Phone No.");
				//people.setBirthDate(PortalUtility.getCalendarObject(request.getParameter("newdateofincor")+" 00:00:00"));
				//added by puja to set the birth date of people

				people.setPeopleRSN(PortalUtility.getInt(existingPeople.getPeopleRSN()));

				String randomString = PortalService.getRandomString(8);
				people.setInternetAnswer(randomString);

				boolean updatePeopleFlag = PortalService.getWSClient().updatePeople(lid, people);
				System.out.println(new Date().toString()+" printing updatePeopleFlag >>>"+String.valueOf(updatePeopleFlag));
				
				peopleRSN = PortalUtility.getInt(existingPeople.getPeopleRSN());
				System.out.println(new Date().toString()+" printing peopleRSN >>>"+String.valueOf(peopleRSN));
				
				boolean updatePeopleInfoFlag = PortalService.getWSClient().updatePeopleInfo(lid, peopleRSN,peopleinfos);
				System.out.println(new Date().toString()+" printing updatePeopleInfoFlag >>>"+String.valueOf(updatePeopleInfoFlag));
				
				if(applicanttype.equals("CORPORATE") || applicanttype.equals("FOREX")){
					//Updating Incorporation Date
					updateDateOfIncorporation(dateOfincorporation, String.valueOf(peopleRSN), lid, DATE_OF_INC_CUSTOM_CODE);
				}
				
				String fromEmailAddress = "eservices@firs.gov.ng";

				//Sending Email to user with Password and Activation Link.
				boolean mailFlag = PortalService.getWSClient().sendHtmlEmail(lid, email, fromEmailAddress, "",
						"User Account Activation",
						"Dear "+name+",<br/><br/>Your first time login-password is :<br/><br/>Password : <b style='color:red;'>"
								+ randomString + "</b><br><br>"
								+ "<br/><br/>Activate your account by Clicking This link**:<br/><br/><a href="
								+ PortalService.getActivationURL(Integer.toString(peopleRSN), randomString,t_contextURL)
								+ ">"
								+ PortalService.getActivationURL(Integer.toString(peopleRSN), randomString,t_contextURL)
								+ "</a><br><br>"
								+ "<br/><br/>**NOTE: If the above link is not clickable, please copy the link and open in a separate window for account activation.<br/><br/>"
								+ "<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."
								+ "<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."
								+ "The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."
								+ "The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."
								+ "Before opening and accessing the attachment, if any, please check and scan for virus.</div>");

				System.out.println(new Date().toString()+" printing mailFlag >>>"+String.valueOf(mailFlag));						
				res.put("updatePeopleFlag", updatePeopleFlag);
				res.put("updatePeopleInfoFlag", updatePeopleInfoFlag);
				res.put("peopleRSN", peopleRSN);
				res.put("type", getPeopleType(people.getPeopleCode()));
				res.put("Success", true);
				log.info("peopleRSN :" + peopleRSN);
			} else {
				WsPeople people = new WsPeople();

				people.setOrganizationName(name);
				people.setAddressLine1(newAddress);
				people.setAddressLine1(oldAddress);

				if (peopleType.equalsIgnoreCase("Corporate")) {
					people.setPeopleCode(3);
				} else if (peopleType.equalsIgnoreCase("Individual")) {
					people.setPeopleCode(4);
				} else if (peopleType.equalsIgnoreCase("Forex")) {
					people.setPeopleCode(5);
				}

				people.setReferenceFile(RCNo);
				//people.setBirthDate(IncorporationDate);//Not available in TIN Web Service
				people.setLicenceNumber(tinNumber);
				people.setEmailAddress(email);
				people.setPhone1(phone);
				//people.setPhone1Desc("Old Phone No.");
				people.setPhone2(newMobile);
				//Adding JTBTIN to people
				people.setSocialSecurityNumber(JTBTIN);
				//people.setPhone2Desc("New Phone No.");
				//people.setBirthDate(PortalUtility.getCalendarObject(request.getParameter("newdateofincor")));
				//added by puja to set the birth date of people
				people.setPeopleRSN(PortalUtility.getInt(existingPeople.getPeopleRSN()));
				//System.out.println(PortalUtility.getCalendarObject(request.getParameter("newdateofincor")));
				boolean updatePeopleFlag = PortalService.getWSClient().updatePeople(lid, people);
				System.out.println(new Date().toString()+" printing updatePeopleFlag >>>"+String.valueOf(updatePeopleFlag));
				
				peopleRSN = PortalUtility.getInt(existingPeople.getPeopleRSN());
				System.out.println(new Date().toString()+" printing peopleRSN >>>"+String.valueOf(peopleRSN));
				
				if(applicanttype.equals("CORPORATE") || applicanttype.equals("FOREX")){
					//Updating Incorporation Date
					updateDateOfIncorporation(dateOfincorporation, String.valueOf(peopleRSN), lid, DATE_OF_INC_CUSTOM_CODE);
				}
				res.put("Success", false);
				res.put("updatePeopleFlag", updatePeopleFlag);
				res.put("errorMessage","You Account is Active. Please login or try forgot password. Ref Number - "+ existingPeople.getPeopleRSN() + ".");
			}

		}

		/*
			WsFolderPeople folderPeople=new WsFolderPeople();
			folderPeople.setPeopleRSN(peopleRSN);
			folderPeople.setPeopleCode(1);
			if(peopleType.equalsIgnoreCase("Corporate")){
				folderPeople.setPeopleCode(3);
			}else if(peopleType.equalsIgnoreCase("Individual")){
				folderPeople.setPeopleCode(4);
			}else if(peopleType.equalsIgnoreCase("Forex")){
				folderPeople.setPeopleCode(5);
			}
			folderPeople.setPeopleDesc(peopleType);
			WsFolderPeople[] folderPeoples=new WsFolderPeople[1];
			folderPeoples[0]=folderPeople;
		*/

		/*
			//Tax Office Old tax office
			WsFolderInfo folderinfo5090=new WsFolderInfo();
			folderinfo5090.setInfoCode(5090);
			folderinfo5090.setInfoValue(taxOfficeIdExisting);
			folderinfo5090.setInfoDesc(taxOffice);
			
			//Tax Office New tax office
			WsFolderInfo folderinfo5091=new WsFolderInfo();
			folderinfo5091.setInfoCode(5091);
			folderinfo5091.setInfoValue(newTaxOffice);
			//folderinfo5091.setInfoDesc("New TaxOffice");
			
			//Email Address Old Email Address
			WsFolderInfo folderinfo5025=new WsFolderInfo();
			folderinfo5025.setInfoCode(5025);
			folderinfo5025.setInfoValue(email);
			
			//Email Address New Email Address
			WsFolderInfo folderinfo5026=new WsFolderInfo();
			folderinfo5026.setInfoCode(5026);
			folderinfo5026.setInfoValue(newEmail);
			
			//Old Phone No.
			WsFolderInfo folderinfo5030=new WsFolderInfo();
			folderinfo5030.setInfoCode(5030);
			folderinfo5030.setInfoValue(phone);
			
			//New Phone No.
			WsFolderInfo folderinfo5031=new WsFolderInfo();
			folderinfo5031.setInfoCode(5031);
			folderinfo5031.setInfoValue(newMobile);
			
			//New Address
			WsFolderInfo folderinfo5006=new WsFolderInfo();
			folderinfo5006.setInfoCode(5006);
			folderinfo5006.setInfoValue(newAddress); 
			
			//Inserting Folder Info	
			WsFolderInfo[] infos=new WsFolderInfo[7];
			infos[0]=folderinfo5090;
			infos[1]=folderinfo5091;
			infos[2]=folderinfo5025;
			infos[3]=folderinfo5026;
			infos[4]=folderinfo5030;
			infos[5]=folderinfo5031;
			infos[6]=folderinfo5006;
			
			//Inseting folder
			WsFolder folder=new WsFolder();
			folder.setFolderType("REGD");
			if(peopleType.equalsIgnoreCase("Corporate")){
				folder.setSubCode(1);
			}else if(peopleType.equalsIgnoreCase("Individual")){
				folder.setSubCode(2);
			}else if(peopleType.equalsIgnoreCase("Forex")){
				folder.setSubCode(3);
			}
			folder.setReferenceFile(RCNo);
			folder.setStatusCode(1);
			folder.setReferenceFile2(tinNumber);
			folder.setReferenceFile5(JTBTIN);
			
			//Inserting Folder
			folderRSN=PortalService.getWSClient().addNewFolder(lid,folder,peopleRSN,null,null,null,null);
			log.info("folderRSN :"+folderRSN);
			PortalService.getWSClient().updateFolderInfo(lid, folderRSN, infos);
			res.put("folderRSN",folderRSN);
			res.put("type",getPeopleType(people.getPeopleCode()));
			res.put("Success", true);
		*/
		/*}*/

	} catch (Exception e) {
		res.put("Success", false);
		res.put("errorMessage", "Some error Occured!Please try Again");
		res.put("exceptionMsg", e.toString());
		e.printStackTrace();
	}
	
	response.getWriter().print(res.toString());
	System.out.println(new Date().toString()+" printing res >>>"+res.toString());
	
	log.info("Leaving-->RegistrationAjax.jsp");
	System.out.println(new Date().toString()+"   Exiting >>> RegistrationAjax.jsp");
%>

