<div class="row hide" style="margin-top:30px;" id="activation-success">
			<div class="panel panel-default col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-sm-10">
      			<div class="panel-body">
        		<center>
	          		<img src="https://upload.wikimedia.org/wikipedia/commons/c/cb/B-check.svg" style="width:50px; height: 40px;">
	          		<p class="desc"><font color="darkgreen" style="font-weight:bold"><%=request.getAttribute("email") %></font>
	          		<br>Your Password was Successfully Sent to your email. You can login using your credentials. 
	          		 <br><br><a href="index.jsp"><button class="btn btn-default" style="border-radius:0px;">Home</button></a> 
	          		</p>
        		</center>        
      			</div>
			</div>
		</div>