<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="java.text.Format"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.csdcsystems.amanda.client.service.ReportService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderProcessAttempt"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.BufferedOutputStream"%>
<%!
	FileInputStream fis=null;
	String encodedTCC;
	String encodedTIN;
	private static String QR_CODE_IMAGE_FOLDER = "tempQRImage";
	String filePath;
	File qrFile=null;
	String FIRS_QRCODE;
	String reportFileName;
%>
<%
	log.info("Entering"+pageName);
	System.out.println(new Date().toString()+"  Entering >>>"+pageName);
	
	String receiptnumber = PortalUtility.getString(request.getParameter("RECEIPT_NO"));
	String tinnumber = PortalUtility.getString(request.getParameter("PAYER_TIN"));
	String folderType = PortalUtility.getString(request.getParameter("folderType"));
	Map<String, Object> params = new HashMap<String, Object>();
	Calendar cal = Calendar.getInstance();
	StringBuilder pdfFileName = new StringBuilder();
	
	if(folderType.equalsIgnoreCase("RCPT")){
		reportFileName = "FIRS_VAT_WHT_NGN.jrxml";
	}else if(folderType.equalsIgnoreCase("RCTI")){
		reportFileName = "FIRS_BEN_WHT_NGN.jrxml";
	}
	
	pdfFileName.append(PortalUtility.getString(request.getParameter("RECEIPT_NO")));
	
	params.put("ADDRESS",request.getParameter("ADDRESS"));
	params.put("RECEIPT_NO",PortalUtility.getString(request.getParameter("RECEIPT_NO")));
	params.put("DATE",PortalUtility.getDateByPattern(cal,"dd-MM-yyyy"));
	params.put("BILL_NO",PortalUtility.getString(request.getParameter("BILL_NO")));
	params.put("RECEIVED_FORM",PortalUtility.getString(request.getParameter("RECEIVED_FORM")));
	params.put("BENEFICIARY_TIN",PortalUtility.getString(request.getParameter("BENEFICIARY_TIN")));
	params.put("PAYER",PortalUtility.getString(request.getParameter("PAYER")));
	params.put("PAYER_TIN",PortalUtility.getString(request.getParameter("PAYER_TIN")));
	params.put("TAX_TYPE",PortalUtility.getString(request.getParameter("TAX_TYPE")));
	
	double d_sumOfAmount = 
			Double.valueOf(PortalUtility.getString(request.getParameter("SUM_OF_AMT")).equals("") ? "0.0" : PortalUtility.getString(request.getParameter("SUM_OF_AMT")));
	String string_sumOfAmount = String.format("%,.2f", d_sumOfAmount);
	params.put("SUM_OF_AMT","NGN "+string_sumOfAmount);
	params.put("SUM_OF_AMT_IN_WORD",PortalService.getConvertCurrencyToWord(d_sumOfAmount));

	double d_contractAmount = 
			Double.valueOf(PortalUtility.getString(request.getParameter("TAX_ON")).equals("") ? "0.0" : PortalUtility.getString(request.getParameter("TAX_ON")));
	String string_contractAmount = String.format("%,.2f", d_contractAmount);
	params.put("TAX_ON","Contract Amount: NGN "+string_contractAmount);
	
	params.put("CONTRACT_DESC",PortalUtility.getString(request.getParameter("CONTRACT_DESC")));
	params.put("WHR_RATE",PortalUtility.getString(request.getParameter("WHR_RATE")));
	params.put("PERIOD_FROM",PortalUtility.getString(request.getParameter("PERIOD_FROM")));
	params.put("PERIOD_TO",PortalUtility.getString(request.getParameter("PERIOD_TO")));
	params.put("BANK",PortalUtility.getString(request.getParameter("BANK")));
	params.put("TAX_OFFICE",PortalUtility.getString(request.getParameter("TAX_OFFICE")));
	params.put("PAYMENT_REFERENCE",PortalUtility.getString(request.getParameter("PAYMENT_REFERENCE")));
	
	String s_tempPaymentDate = PortalUtility.getString(request.getParameter("PAYMENT_DATE"));
	System.out.print(s_tempPaymentDate);
	//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	Date date_paymentDate = (Date)formatter.parse(s_tempPaymentDate);
	Format formatter1 = new SimpleDateFormat("dd-MM-yyyy");
	String string_paymentDate = formatter1.format(date_paymentDate);
	params.put("PAYMENT_DATE",PortalUtility.getString(string_paymentDate));
	
	params.put("ISSUING_OFFICER","System Generated");
	params.put("IRNO","");
	params.put("SUPERVISING_OFFICER","");
	params.put("SIGNATURE","");
	
	StringBuffer requestURL = request.getRequestURL();
	
	String t_tempURLWithGetURL = requestURL.toString();
	String t_contextURL = t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));
	
	/*QR CODE TEXT GENERATION*/
	StringBuilder QRCodeLinkBuilder = new StringBuilder();
	QRCodeLinkBuilder.append(t_contextURL);
	QRCodeLinkBuilder.append("/?cmd=searchreceipt&receiptNumber=");
	//TODO: Encode URL incase special characters are used in TCC
	try{
		encodedTCC = URLEncoder.encode(receiptnumber,"UTF-8")
						.replaceAll("\\+", "%20")
	                    .replaceAll("\\%21", "!")
	                    .replaceAll("\\%27", "'")
	                    .replaceAll("\\%28", "(")
	                    .replaceAll("\\%29", ")")
	                    .replaceAll("\\%7E", "~");
	}catch(UnsupportedEncodingException e){
		encodedTCC=receiptnumber;
	}
	QRCodeLinkBuilder.append(encodedTCC);
	
	QRCodeLinkBuilder.append("&tinNumber=");
	try{
		encodedTIN = URLEncoder.encode(tinnumber,"UTF-8")
						.replaceAll("\\+", "%20")
	                    .replaceAll("\\%21", "!")
	                    .replaceAll("\\%27", "'")
	                    .replaceAll("\\%28", "(")
	                    .replaceAll("\\%29", ")")
	                    .replaceAll("\\%7E", "~");
	}catch(UnsupportedEncodingException e){
		encodedTIN=tinnumber;
	}
	QRCodeLinkBuilder.append(encodedTIN);
	
	String qrCodeText=QRCodeLinkBuilder.toString();
	/*QR CODE TEXT GENERATION - END*/

	/*Generating QR CODE*/
	int size = 125;
	String fileType = "png";
	StringBuilder filePathBuilder = new StringBuilder();
	filePathBuilder
		.append(request.getRealPath(QR_CODE_IMAGE_FOLDER))
		//.append(pathName)
		.append("\\")
		.append(receiptnumber)
		.append("QRImage");
	filePath = filePathBuilder.toString();
	//qrFile = new File(filePath);
	/*for Local Testing:*/
	qrFile = new File("img");
	//System.out.println(qrFile.getAbsolutePath());
	ServletContext context = request.getServletContext();
	String realPath = context.getRealPath("/");
	File bgFile = new File(reportImagePath+"TCC_BACKGROUND.png");
	byte[] byteArrayBG =null;
	
	if(bgFile.exists()){
		byteArrayBG =  IOUtils.toByteArray(new FileInputStream(bgFile));
	}
	
	if(!qrFile.exists()){
		PortalService.createQRImage(qrFile, qrCodeText, size, fileType);
	}
	fis = new FileInputStream(qrFile);
	byte[] byteArrayQR =  IOUtils.toByteArray(fis);
    FIRS_QRCODE=DatatypeConverter.printBase64Binary(byteArrayQR);
	/*Generating QR CODE - END*/

	params.put("QR_CODE",FIRS_QRCODE);
	//System.out.println("Coming before download "+reportFileName+" "+ReportService.PDF_REPORT_TYPE+" "+params);
	
	List<Object> listData = new ArrayList<Object>();
	Map<String, Object> colectionDataSource = new HashMap<String, Object>();
	listData.add(colectionDataSource);
	params.put("dataSource", listData);
	byte[] reportByteContent = ReportService.getReportContent(params, "dataSource", ReportService.PDF_REPORT_TYPE, reportFileName);
	//System.out.println("after BYTE CONTENT download try"+ reportByteContent);
	response.setContentType("application/pdf");			
	response.setHeader("Content-Length", String.valueOf((reportByteContent.length)));
	//response.setHeader("Content-Disposition", "attachment;filename=\"Receipt.pdf\"");
	
	response.setHeader("Content-Disposition", "inline;filename="+pdfFileName.toString()+".pdf"+"");
			
	BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
	//System.out.println("Coming after download");
	try{
		//System.out.println("Coming after download try");
		bos.write(reportByteContent);
		//System.out.println("Coming after download try end");
	}catch(Exception e){
		throw e;
	}finally{
		if(bos!=null){
			bos.flush();
			bos.close();
		}
		if(fis!=null){
			fis.close();
			log.info("File has been deleted at "+pageName);
		}
		if(qrFile!=null){
			log.info("File delete status at: "+pageName+" : "+qrFile.delete());
		}
		response.getOutputStream().flush();
		response.getOutputStream().close();
		
		/*Logging Receipt DOWNLOAD details */
		Map<String,String> receiptDataForLogging = new HashMap<String,String>();
		
		int receiptNumber = 0;
		String receiptNumberInHexa = "";
		String receiptType = "";
		
		receiptNumberInHexa = PortalUtility.getString(request.getParameter("RECEIPT_NO"));
		receiptNumber = PortalUtility.hex2decimal(receiptNumberInHexa);
		
		if(PortalUtility.getString(request.getParameter("folderType")).equals("RCPT")){
			receiptType = "BULK";
		}else if(PortalUtility.getString(request.getParameter("folderType")).equals("RCTI")){
			receiptType = "CreditNotes";
		}else{
			
		}
		
		receiptDataForLogging.put("receiptNumber",String.valueOf(receiptNumber));
		receiptDataForLogging.put("receiptType",receiptType);
		receiptDataForLogging.put("paymentReference",PortalUtility.getString(request.getParameter("PAYMENT_REFERENCE")));
		receiptDataForLogging.put("downloadMedium","ReceiptPortal");
		
		Map<String,Object> responseObject = PortalService.doReceipAuditLogging(lid,receiptDataForLogging);
		System.out.println(new Date().toString()+"  loggingResponseObject >>>"+responseObject);
		
		System.out.println(new Date().toString()+"  Exiting >>>"+pageName);
	}
%>
