<%@ page language="java" buffer="8kb" import="java.util.*, com.csdcsystems.amanda.common.*" autoFlush="true" isThreadSafe="true" isErrorPage="false" session="true" %>

<%
	session.invalidate();
	session = request.getSession(true);
  	response.sendRedirect("index.jsp");
%>

