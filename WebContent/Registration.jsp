<!-- User Registration Page Starts -->
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page
	import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@include file="include/includeconstant.jsp"%>
<%
	String t_contextURL = PortalService.getROOTURL(request);
%>

<script>
 var contextPath = "<%=path%>";
 
 $( function() {
 	/* $( "#dateofinc-perview-hidden" ).datepicker({
	     changeMonth: true,
	     changeYear: true
 	}); */
 	$('#dateofinc-perview-hidden').datetimepicker({
		useCurrent:false,
		//maxDate:new Date().getDate() + 1, 
		format: 'MM-DD-YYYY',
		dayViewHeaderFormat:'MMM-YYYY'
	});
 });
 
</script>

<div class="container-fluid">
	<div id="RegistrationFormDiv">
		<p class="alert alert-info"
			style="border-radius: 0px; background-color: rgba(119, 119, 119, 0.16); border-color: rgba(85, 85, 85, 0.19); color: #333;">
			Registration for
			<%=request.getParameter("type") != null ? request.getParameter("type").toUpperCase() : ""%>
			Account
		</p>
		<div class="row">
			<div class="col-lg-12">
				<span class="text-info-custom">* marked fields are mandatory.</span>
			</div>
		</div>
		<div class="row custom-row-margin">
			<div
				class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-3">
				<label class="control-label custom_label" for="applicant-type">Applicant
					Type:<sup class="text-danger">*</sup>
				</label>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<input class="form-control input-sm custom_input required"
					id="applicant-type" type="text"
					value="<%=request.getParameter("type").toUpperCase()%>"
					disabled="disabled" pattern="" data-required="">
				<div class="error" id=""></div>
			</div>
		</div>
		<div class="row custom-row-margin">
			<div
				class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-3">
				<label class="control-label custom_label" for="tin">Tax
					Identification Number:<sup class="text-danger">*</sup>
				</label>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<input class="form-control input-sm custom_input" id="tin"
					name="tin" type="text" placeholder="Enter your TIN" pattern=""
					data-required="">
				<div class="error" id=""></div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<button type="button" id="btnRetriveTIN"
					class="btn btn-default custom_btn">Retrieve</button>
				&nbsp
				<button id="btnResetTIN" type="button"
					class="btn btn-default custom_btn">Reset</button>
			</div>
			<div
				class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8"
				id="invalidTINMsg" style="margin-top: 10px;">
				<!-- <div class="alert alert-danger text-center">
					<strong>Invalid TIN!</strong> Please enter a valid TIN.
				</div> -->
			</div>
		</div>
		<div id="preview-section-reg" class="hide">
			<div class="jumbotron custom-row-margin"
				style="border-radius: 0px; padding: 10px 5px 5px 5px;">
				<div class="row">
					<div
						class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-3">
						<label class="control-label custom_label" for="name-preview">Name
							of Company/Individual:</label>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<span id="name-preview"></span> <input
							class="form-control input-sm custom_input"
							id="name-preview-hidden" name="name-preview-hidden" type="hidden"
							value="">
						<div class="error" id=""></div>
					</div>
				</div>
				<div class="row custom-row-margin">
					<div
						class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-offset-1 col-sm-3">
						<label class="control-label custom_label" for="tax-office-preview">Tax
							Office:</label>
						<h5 id="tax-office-preview"></h5>
						<input class="form-control input-sm custom_input"
							id="tax-office-preview-hidden" name="tax-office-preview-hidden"
							type="hidden" value="">
						<div class="error" id=""></div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<label class="control-label custom_label" for="email-perview">E
							Mail:</label>
						<h5 id="email-perview" class=""></h5>
						<input class="form-control input-sm custom_input"
							id="email-perview-hidden" name="email-perview-hidden"
							type="hidden" value="">
						<div class="error" id=""
							style="margin-top: 26px; font-style: normal;"></div>
					</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
						<label class="control-label custom_label" for="phone-preview">Phone
							Number:</label>
						<h5 id="phone-preview"></h5>
						<input class="form-control input-sm custom_input"
							id="phone-preview-hidden" name="phone-preview-hidden"
							type="hidden" value="">
						<div class="error" id=""></div>

					</div>
					<!-- added by puja on 06-10-2017 to add date of incorporation in people -->
					<div class="col-lg-2 col-md-2 col-sm-2" id="peopledob">
						<label class="control-label custom_label" for="dateofinc-perview">Date of Incorporation:</label>
						
						<input class="form-control input-sm custom_input" id="dateofinc-perview-hidden" name="dateofinc-perview-hidden"
							type="text" value="">
						<div class="error" id="" style="margin-top:26px;font-style: normal;"></div>	
					</div>


				</div>

			</div>

			<!-- Added by samarjit new Regd submit  -->
			<div class="row">
				<div
					class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10">
					<p>
						<label><input type="checkbox" value=""
							id="confirm-registration"></label> I confirm that the above
						information is correct and current for further transactions.
					</p>
				</div>
			</div>
			<div class="row" style="margin-top: 10px; margin-bottom: 10px;">
				<div class="col-lg-12 text-danger" style="font-size: 13px;">
					<strong>Note&nbsp:&nbsp&nbsp</strong>Please contact your Tax Office
					to update the above information.Without a valid E Mail ID you
					cannot register.
				</div>
			</div>

			<div class="row">
				<div class="col-lg-12 text-center">
					<button type="button" class="btn btn-default custom_btn"
						id="form_back" onClick="window.location.reload();">Cancel</button>
					<button type="button" class="btn btn-default custom_btn"
						id="submit-registration-form" disabled="disabled">Submit</button>
				</div>
			</div>

			<!-- END  -->

			<div class="hide">
				<label class="control-label">New Details:</label>
			</div>
			<div class="panel panel-default hide"
				style="border-radius: 0px; padding: 2px 10px 2px 10px;">
				<div class="row custom-row-margin">
					<div
						class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
						<label class="control-label custom_label" for="mobile-new">New
							Mobile:</label>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<input class="form-control input-sm custom_input" id="mobile-new"
							name="mobile-new" type="text" value=""
							onkeyup="phoneMask(this.id);" value=""
							onblur="validPhoneMask(this.id);" maxlength="21">
						<div class="error" id=""></div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2">
						<label class="control-label custom_label hide"
							for="tax-office-new">New Tax Office:</label>
					</div>
					<%
						//Code For fetching Tax Office
						WsTransactionRequest[] transactionRequests = new WsTransactionRequest[2];
						WsTransactionResponse[] transactionResponse = PortalService.getWSClient().executeCustomTransaction(lid, 101,
								transactionRequests);
						String taxOfficeDesc[] = null;
						String taxOfficeShort[] = null;

						if (!PortalUtility.isNull(transactionResponse)) {

							taxOfficeDesc = new String[transactionResponse[0].getColumnValues().length];
							taxOfficeShort = new String[transactionResponse[0].getColumnValues().length];

							for (int i = 0; i < transactionResponse[0].getColumnValues().length; i++) {
								taxOfficeShort[i] = transactionResponse[0].getColumnValues(i);
								taxOfficeDesc[i] = transactionResponse[1].getColumnValues(i);
							}
						}

						//Code For fetching Tax Office
					%>
					<div class="col-lg-3 col-md-3 col-sm-3  hide">
						<select class="form-control input-sm custom_input taxofficeselect"
							id="tax-office-new" name="tax-office-new">
							<option value="">Select</option>
							<%
								if (taxOfficeShort != null) {
									for (int i = 0; i < taxOfficeShort.length; i++) {
							%>
							<option value=<%=taxOfficeShort[i]%>><%=taxOfficeDesc[i]%></option>
							<%
								}
								}
							%>
						</select>
						<div class="error" id=""></div>
					</div>
				</div>
				<div class="row custom-row-margin">
					<div
						class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
						<label class="control-label custom_label" for="email-new">New
							Email:</label>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<input class="form-control input-sm custom_input" id="email-new"
							name="email-new" type="text" value="" pattern="email">
						<div class="error" id=""></div>
					</div>
				</div>
				<div class="row custom-row-margin hide" style="margin-bottom: 10px">
					<div
						class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-offset-1 col-sm-2">
						<label class="control-label custom_label" for="address-new">New
							Address:</label>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<input class="form-control input-sm custom_input" id="address-new"
							name="address-new" type="text" value="">
						<div class="error" id=""></div>
					</div>
				</div>
			</div>
			<input type="hidden" name="peopleType-preview-hidden" id="peopleType-preview-hidden" value="<%=request.getParameter("type").toUpperCase()%>"> 
			<input type="hidden" id="taxOfficeId-preview-hidden" name="TaxOfficeId" value=""> 
			<input type="hidden" id="RCNo-preview-hidden" name="RCNo" value=""> 
			<input type="hidden" id="JTBTIN-preview-hidden" class="JTBTIN-preview-hidden" name="JTBTIN" value=""> 
			<input type="hidden" id="Address-preview-hidden" name="Address" value=""> 
			<input type="hidden" id="FIRSTIN-hidden" class="FIRSTIN-hidden" value="">
			<!-- Samarjit 27032018 Added to get taxPayerType from TINDB -->
			<input type="hidden" id="taxPayerTypeFromTINDB" class="" value="">
			<div class="row custom-row-margin hide">
				<div class="col-lg-12 text-center">
					<button type="button" class="btn btn-default custom_btn"
						id="btnResetNewTIN">Reset</button>
					<button id="btnPreviewReg" class="btn btn-default custom_btn"
						disabled="disabled">Preview</button>
					<!-- <span>Loading..<img alt="" src="images/CUBE-loading.gif" height="50px" width="50px"></span> -->
				</div>
			</div>
		</div>
	</div>
	<div class="hide" id="RegistrationPreviewDiv">
		<p class="alert alert-success text-center">
			<strong>Preview</strong>
		</p>
		<div class="panel panel-default" id="preview-background"
			style="padding: 30px 10px 55px 10px">
			<p class="alert"
				style="font-size: 15px; font-family: sans-serif; font-weight: 700;">A.&nbsp&nbspApplication
				Details:</p>
			<div class="row custom-row-margin"
				style="padding: 0px 10px 0px 20px;">
				<div
					class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
					<span class="preview-lable">Application Type:<sup
						class="text-danger">*</sup></span> <span id="save_peopleType"
						class="applicant-type"></span>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<span class="preview-lable">Tax Identification Number:<sup
						class="text-danger">*</sup></span> <span id="save_tin" class="tin"></span>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<span class="preview-lable">Name:</span> <span
						class="name-preview-hidden" id="save_name"></span>
				</div>
			</div>
			<p class="alert"
				style="font-size: 15px; font-family: sans-serif; font-weight: 700;">B.&nbsp&nbspExisting
				Details:</p>
			<div class="custom-row-margin jumbotron"
				style="padding: 20px 0px 40px 0px;">
				<div
					class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
					<span class="preview-lable">Tax Office:</span> <span
						class="tax-office-preview-hidden" id="save_taxOffice"></span>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<span class="preview-lable">E mail:</span> <span
						class="email-perview-hidden" id="save_email"></span>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<span class="preview-lable">Mobile:</span> <span
						class="phone-preview-hidden" id="save_phone"></span>
				</div>
			</div>
			<p class="alert"
				style="font-size: 15px; font-family: sans-serif; font-weight: 700;">C.&nbsp&nbspNew
				Details:</p>
			<div class="jumbotron" style="padding: 20px 0px 20px 20px;">
				<div class="row custom-row-margin">
					<!-- <div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
					<span class="preview-lable">New Tax Office:</span>
					<span  class="tax-office-new"  id="save_newTaxOffice"></span>
				</div> -->
					<div
						class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
						<span class="preview-lable">New E-Mail:</span> <span
							class="email-new" id="save_newEmail"></span>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<span class="preview-lable">New Mobile:</span> <span
							class="mobile-new" id="save_newMobile"></span>
					</div>
				</div>
				<!-- <div class="row custom-row-margin">
				<div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3 hide">
					<span class="preview-lable">New Address:</span>
					<span  class="address-new"  id="save_newAddress"></span>
				</div>
			</div> -->
			</div>
		</div>
		<input type="hidden" id="TaxOfficeId" value=""> <input
			type="hidden" id="RCNo" value=""> <input type="hidden"
			id="JTBTIN-preview-hidden" value=""> <input type="hidden"
			id="Address" value=""> <input type="hidden"
			id="FIRSTIN-hidden" value="">
		<div class="row">
			<div class="col-lg-12 text-center">
				<p>
					<label><input type="checkbox" value=""
						id="confirm-registration"></label> I certify that the information
					given above is correct in respect and confirm that to the best of
					my knowledge and belief, there are no other facts the omission of
					which would be misleading
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 text-center">
				<button type="button" class="btn btn-default custom_btn"
					id="form_back">Back</button>
				<button type="button" class="btn btn-default custom_btn"
					id="submit-registration-form" disabled="disabled">Submit</button>
			</div>
		</div>
		<div class="loader-section preloaderReg hide" id="wait">
			<p>
				Loading...<i class="custom-loader"></i>
			</p>
		</div>
	</div>
	<div class="hide" id="registration-sucess-info">
		<div class="row panel panel-default" style="margin: 0px 0px 0px 0px;">
			<div class="col-lg-offset-4 col-lg-4 text-center"
				style="padding: 30px 10px 30px 10px">
				<strong>Please check your mail to complete your
					registration. Registration Successful.</strong>
				<p>
					Your application number is :<strong><span
						id="application-number"></span></strong>
				</p>
				<p>
					<a href="<%=t_contextURL%>">Home Page</a><span></span>
				</p>
			</div>
		</div>
	</div>
</div>

