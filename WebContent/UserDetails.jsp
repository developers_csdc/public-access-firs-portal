<%@include file= "header.jsp" %>

	<div>
	<%          
				String message = (String)request.getAttribute("error");
				System.out.println("Tin User Already Registered" + message);
				if(message!=null){
				%>
			    <p class="alert alert-warning" style="text-align:center; margin-top:60px;  border-radius:0;  border: 1px solid #faebcc; background-color:transparent;">This TIN has already been registered! Kindly login using your TIN and password.<br> Should you have further complain, please contact <b>eservicesupport@firs.gov.ng</b> 
			      
			     </p>
			     
			    <% } %>
		<div id="container-fluid">
			<!-- User Registration Page Starts -->
		    
			<div class="container-fluid">
				<%
				People tin = (People) request.getAttribute("data");
				String applicationType = request.getParameter("applicationType");
			    %>
			    
				
				<div class="container" style="margin-top: 20px">
					<div class="col-md-6">
						<ul class="list-group">
							<li class="list-group-item">Registration For : <span>TCC</span></li>
							<li class="list-group-item">Tax Identification Number: <span><%=tin.getTIN()%></span></li>
							<li class="list-group-item">Name of Company: <span><%=tin.getTaxPayerName() %></span></li>
							<li class="list-group-item">Tax Office ID : <span><%=tin.getTaxOfficeID()%></span></li>
							<li class="list-group-item">Address: <span><%=tin.getAddress()%></span></li>
							<li class="list-group-item">Tax Office Type : <span><%=tin.getTaxPayerType()%></span></li>
							<li class="list-group-item">RC Number : <span><%=tin.getRCNumber()%></span></li>
							<li class="list-group-item">JTBTIN : <span><%=tin.getJTBTIN() %></span></li>
							<li class="list-group-item">Email Address: <span><%=tin.getEmail()%></span></li>
							<li class="list-group-item">Phone Number: <span><%=tin.getPhone()%></span></li>
						</ul>
					</div>
					<form action="registrationControllerService" method="post">
						<div class="col-md-6">
							<p style="background: #6092ab; color: #fff; padding: 7px;">
								Registration for
								<%=applicationType.toUpperCase() %>
								account
							</p>
							<%
							if(!applicationType.equals("Individual")){
								
							%>
							<p>
								<small>*Email is required*</small>
							</p>
							<%} %>
							
							<div class="form-group">
								<input name="email" type="text" class="form-control"
									value="<%=tin.getEmail()%>" placeholder="Enter a valid Email">
							</div>
							<div class="form-group">
							<p>
								<small>*TIN is required*</small>
							</p>
								<input name="ftin" type="text" class="form-control"
									value="<%=tin.getTIN()%>" readonly>
							</div>
							
							<%
							if(!applicationType.equals("Individual")){
								
							%>
							<div class="form-group">
							<p>
								<small>*Date of Incorporation is required*</small>
							</p>
								<input name="doi" type="text" id="datetimepicker1"
									placeholder="Enter Date of Incorporation" class="form-control" required>
							</div>
							<%
							}
							%>
							
							
							<input name="applicationType" type="hidden" value="<%=applicationType%>">
								<input name="taxFinalOfficeName" type="hidden" value="<%=(tin.getTaxOfficeName()).replace("'","''") %>"> 
								<input name="taxOfficeID" type="hidden"
									value="<%=tin.getTaxOfficeID()%>"> <input
										name="taxPayerType" type="hidden"
										value="<%=tin.getTaxPayerType()%>"> <input
											name="RCNumber" type="hidden" value="<%=tin.getRCNumber()%>">
											 <input
											name="taxPayerName" type="hidden" value="<%=(tin.getTaxPayerName()).replace("'","''") %>">
											
												<input name="phone" type="hidden"
												value="<%=tin.getPhone()%>"> <input name="address"
													type="hidden" value="<%=(tin.getAddress()).replace("'","''")%>">
							<button type="submit" name="command" value="AddUser"
								class="btn btn-default custom_btn">Submit</button>
						</div>
					</form>
					<% //} %>
				</div>
			</div>

		</div>
	</div>


	</div>

	</div>

	<div class="container-fluid">
		<footer class="footer">
		<div class="container-fluid"
			style="font-family: sans-serif; font-size: 12px;">
			<p class="text-muted">
				<div class="text-center">
					&copy;&nbsp;Copyright 2017 <a href="#" target="_blank">Federal
						Inland Revenue Service. All Right Reserved </a>.
					<!-- <span class="text-right small"><b>Need help?&nbsp</b><a href="#" class="text-info"><u>Click here for LIVE CHAT</u></a></span> -->
				</div>
			</p>
		</div>
		</footer>


	</div>
	<div data-remodal-id="website-icon">
		<h3>
			<i><i class="glyphicon glyphicon-question"
				style="margin-right: 5px;"></i>Are you sure you want to navigate
				away from this page?</i>
		</h3>
		<p class="text-warning">
			<i>All form changes will be lost</i>
		</p>
		<br />
		<button data-remodal-action="cancel" class="remodal-cancel">No,
			stay on this page</button>
		<button data-remodal-action="confirm" class="remodal-confirm">Yes,
			navigate away from this page</button>
	</div>
<<script type="text/javascript">
$(function() {
	  $('#datetimepicker1').datepicker({
		    format: 'dd/mm/yyyy hh:mm:ss',
		    startDate: '-3d'
		});
	});
</script>
<!--

//-->
</script>
</body>
</html>