	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10">
				<div class="panel custom_panel panel-profile">
			  		<div class="panel-heading">
			  		Download TCC
			  		</div>
			  		<div class="panel-body">
				  		<p style="margin-bottom: 40px;">Download your Tax Clearance Certificate by providing the details below</p>
				  		<div class="row custom_row">
					  		<div class="form-group">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="TCC_Input">TCC Number</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<input type="text" class="form-control input-sm custom_input" data-required="required" errorMessage="Please provide a valid TCC number" pattern ="TINInput" maxlength="20" id="TCC_Input" placeholder="Enter TCC Number">
						    		<div class="error" id=""></div>
						    		<br/>
					    		</div>
				 			</div>
				 			<%--<div class="form-group">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="TIN_Input">TIN Number</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<input type="text" class="form-control input-sm custom_input" data-required="required" errorMessage="Please provide a valid TIN number" pattern =""  maxlength="20" id="TIN_Input" placeholder="Enter TIN Number">
						    		<div class="error" id=""></div>
						    		<br/>
					    		</div>
				 			</div>
				 			<div class="form-group">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="application_Input">Application Number</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<input type="text" class="form-control input-sm custom_input" pattern="numbersLg" errorMessage="Please provide a valid Application Number" data-required="required"  maxlength="30" id="application_Input" placeholder="Enter Application Number">
						    		<div class="error" id=""></div>
						    		<br/>
					    		</div>
				 			</div> --%>
			 			</div>
			 			<div class="row custom-row">
			 				<div class="col-xs-12">
			 					<h5 class="text-warning text-center" ><i id="downloadTCCErrorDisplay"></i></h5>
			 				</div>
			 			</div>
			 			<div class="text-center" style="margin-bottom: 50px;">
			 				<button class="btn btn-default custom_btn "  id="export_TCC">Download</button>
			 				<button type="button" id="reset_DownloadTCC"  class="btn btn-default custom_btn ">Reset</button>
			 			</div>
			  		</div>
				</div>
			</div>
		</div>
		
	</div>