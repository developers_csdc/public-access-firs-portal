<%@page import="com.csdcsystems.amanda.common.CsdcUtility"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderFreeform"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@page import="com.csdcsystems.amanda.client.service.ReportService"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> 
</head>
<body>
<%

	final String reportFileName = "FIRS_Tax_Clearance_Certificate_For_Remittance_Of_Funds_Outside_Nigeria.jrxml";
	Map<String, Object> params = new HashMap<String, Object>(); 
	List<Object> listData = new ArrayList<Object>();

	Map<String, Object> colectionDataSource = new HashMap<String, Object>();
	listData.add(colectionDataSource);	
	params.put("dataSource", listData);		
	
	WsFolder folder = new WsFolder();
	WsPeople people = new WsPeople();
	WsFolderFreeform freeform = new WsFolderFreeform();
	
	String rank = "20144";
	String F03_WORD = "";
	String TCC_ISSUANCE_DATE = "25-02-2017";
	String ATTEMPT_USER = "Santosh Kumar Mohapatra";
	
	/* Start Set Sample Value */
	folder.setFolderRSN(12345);
	people.setNameFirst("Jyoti Ranjan Behera");
	people.setOrganizationName("Enterprise System Solutions (P) Limited, Infocity, Bhubaneswar, Odisha-751024, Fax : +91-674-7106050, Phone : +91-674-7106000, E-Mail : ess@esspl.com");
	freeform.setD01(PortalUtility.toDate("20-05-2016"));
	freeform.setC01("OPD Registration Fee");
	freeform.setF01(4562.45);
	freeform.setN01(2017);
	freeform.setF02(5445.45);
	freeform.setF03(55448875.50);
	freeform.setF04(3344.00);
	/*End of setting sample value*/
	
	
	
	F03_WORD = PortalService.getConvertCurrencyToWord(freeform.getF03());
	
	params.put("TCC_R", "");
	params.put("FOLDER_RSN", String.valueOf(folder.getFolderRSN()));
	params.put("PEOPLE_NAME", PortalUtility.getPeopleName(people));
	params.put("ORGANIZATION_NAME", people.getOrganizationName());
	params.put("D01", PortalUtility.getDate(freeform.getD01()) );
	params.put("C01", freeform.getC01());
	params.put("F01", CsdcUtility.formatMoney(freeform.getF02()));
	params.put("N01", String.valueOf(freeform.getN01()));
	params.put("F02", CsdcUtility.formatMoney(freeform.getF02()));
	params.put("F03", CsdcUtility.formatMoney(freeform.getF03()));
	params.put("F03_WORD", F03_WORD);
	params.put("F04", CsdcUtility.formatMoney(freeform.getF04()));
	params.put("RANK", rank);
	params.put("TCC_ISSUANCE_DATE", TCC_ISSUANCE_DATE);
	params.put("ATTEMPT_USER", ATTEMPT_USER);
	params.put("BENEFICIARY_NAME", "");
	params.put("BENEFICIARY_ADDRESS", "");

	byte[] reportByteContent = ReportService.getReportContent(params, "dataSource", ReportService.PDF_REPORT_TYPE, reportFileName);
	
	response.setContentType("application/pdf");
	
	BufferedOutputStream bos = new BufferedOutputStream(
	        response.getOutputStream());
	    bos.write(reportByteContent);
	    bos.flush();
	    bos.close();
	    response.getOutputStream().flush();
	    response.getOutputStream().close();
%>
</body>
</html>