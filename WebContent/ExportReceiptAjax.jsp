<%@page import="com.csdcsystems.amanda.client.service.ReportService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderProcessAttempt"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.BufferedOutputStream"%>
<%!
	FileInputStream fis=null;
	String encodedTCC;
	String encodedTIN;
	private static String QR_CODE_IMAGE_FOLDER = "tempQRImage";
	String filePath;
	File qrFile=null;
	String FIRS_QRCODE;
	String reportFileName;
%>
<%
	log.info("Entering"+pageName);

	String receiptFolderType = PortalUtility.getString(request.getParameter("receiptFolderType"));
	
	if(!(receiptFolderType.equals("FR") || receiptFolderType.equals("FRCT"))){
		String receiptnumber 		= PortalUtility.getString(request.getParameter("RECEIPT_NO"));
		String tinnumber 			= PortalUtility.getString(request.getParameter("PAYER_TIN"));
		String folderType 			= PortalUtility.getString(request.getParameter("folderType"));
		Map<String, Object> params 	= new HashMap<String, Object>();
		Calendar cal 				= Calendar.getInstance();
		StringBuilder pdfFileName 	= new StringBuilder();
		
		if(folderType.equalsIgnoreCase("RCPT")){
			reportFileName = "FIRS_VAT_WHT_NGN.jrxml";
			params.put("ADDRESS",request.getParameter("ADDRESS"));
			params.put("RECEIVED_FORM",request.getParameter("RECEIVED_FORM"));
		}else if(folderType.equalsIgnoreCase("RCTI")){
			reportFileName = "FIRS_BEN_WHT_NGN.jrxml";
			params.put("ADDRESS",request.getParameter("address_ben"));
			params.put("RECEIVED_FORM",request.getParameter("beneficiaryName"));
		}
		
		params.put("RECEIPT_NO",request.getParameter("RECEIPT_NO"));
		params.put("DATE",PortalUtility.getDateByPattern(cal,"dd-MM-yyyy"));
		params.put("BILL_NO",request.getParameter("BILL_NO"));
		
		pdfFileName.append(PortalUtility.getString(request.getParameter("RECEIPT_NO")));
		
		params.put("BENEFICIARY_TIN",request.getParameter("BENEFICIARY_TIN"));
		params.put("PAYER",request.getParameter("PAYER"));
		params.put("PAYER_TIN",request.getParameter("PAYER_TIN"));
		params.put("TAX_TYPE",request.getParameter("fee_description"));
		params.put("SUM_OF_AMT","NGN "+request.getParameter("SUM_OF_AMT"));
		params.put("SUM_OF_AMT_IN_WORD",request.getParameter("SUM_OF_AMT_IN_WORD"));
		params.put("TAX_ON",request.getParameter("contractAmount"));
		params.put("CONTRACT_DESC",request.getParameter("contractDescription"));
		params.put("WHR_RATE",request.getParameter("WHR_RATE"));
		params.put("PERIOD_FROM",request.getParameter("PERIOD_FROM"));
		params.put("PERIOD_TO",request.getParameter("PERIOD_TO"));
		params.put("BANK",request.getParameter("BANK"));
		params.put("TAX_OFFICE",request.getParameter("TAX_OFFICE"));
		params.put("PAYMENT_REFERENCE",request.getParameter("PAYMENT_REFERENCE"));
		params.put("PAYMENT_DATE",request.getParameter("PAYMENT_DATE"));
		params.put("ISSUING_OFFICER","System Generated");
		params.put("IRNO","");
		params.put("SUPERVISING_OFFICER","");
		params.put("SIGNATURE","");
		
		/*Adding QR code to Receipt report*/
		StringBuffer requestURL = request.getRequestURL();
		
		String t_tempURLWithGetURL 	= requestURL.toString();
		String t_contextURL 		= t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));
		
		/*QR CODE TEXT GENERATION*/
		StringBuilder QRCodeLinkBuilder = new StringBuilder();
		QRCodeLinkBuilder.append(t_contextURL);
		QRCodeLinkBuilder.append("/?cmd=searchreceipt&receiptNumber=");
		//TODO: Encode URL incase special characters are used in TCC
		try{
			encodedTCC = URLEncoder.encode(receiptnumber,"UTF-8")
							.replaceAll("\\+", "%20")
		                    .replaceAll("\\%21", "!")
		                    .replaceAll("\\%27", "'")
		                    .replaceAll("\\%28", "(")
		                    .replaceAll("\\%29", ")")
		                    .replaceAll("\\%7E", "~");
		}catch(UnsupportedEncodingException e){
			encodedTCC=receiptnumber;
		}
		
		QRCodeLinkBuilder.append(encodedTCC);
		QRCodeLinkBuilder.append("&tinNumber=");

		try{
			encodedTIN = URLEncoder.encode(tinnumber,"UTF-8")
							.replaceAll("\\+", "%20")
		                    .replaceAll("\\%21", "!")
		                    .replaceAll("\\%27", "'")
		                    .replaceAll("\\%28", "(")
		                    .replaceAll("\\%29", ")")
		                    .replaceAll("\\%7E", "~");
		}catch(UnsupportedEncodingException e){
			encodedTIN=tinnumber;
		}
		
		QRCodeLinkBuilder.append(encodedTIN);
		String qrCodeText=QRCodeLinkBuilder.toString();
		/*QR CODE TEXT GENERATION - END*/

		/*Generating QR CODE*/
		int size = 125;
		String fileType = "png";
		StringBuilder filePathBuilder = new StringBuilder();
		filePathBuilder
			.append(request.getRealPath(QR_CODE_IMAGE_FOLDER))
			//.append(pathName)
			.append("\\")
			.append(receiptnumber)
			.append("QRImage");
		filePath = filePathBuilder.toString();
		//qrFile = new File(filePath);
		/*for Local Testing:*/
		qrFile = new File("img");
		//System.out.println(qrFile.getAbsolutePath());
		ServletContext context = request.getServletContext();
		String realPath = context.getRealPath("/");
		File bgFile = new File(reportImagePath+"TCC_BACKGROUND.png");
		byte[] byteArrayBG =null;
		
		if(bgFile.exists()){
			byteArrayBG = IOUtils.toByteArray(new FileInputStream(bgFile));
		}
		
		if(!qrFile.exists()){
			PortalService.createQRImage(qrFile, qrCodeText, size, fileType);
		}
		
		fis = new FileInputStream(qrFile);
		byte[] byteArrayQR = IOUtils.toByteArray(fis);
	    FIRS_QRCODE = DatatypeConverter.printBase64Binary(byteArrayQR);
		/*Generating QR CODE - END*/

		params.put("QR_CODE",FIRS_QRCODE);
		//System.out.println("Coming before download "+reportFileName+" "+ReportService.PDF_REPORT_TYPE+" "+params);
		
		List<Object> listData = new ArrayList<Object>();
		Map<String, Object> colectionDataSource = new HashMap<String, Object>();
		listData.add(colectionDataSource);
		params.put("dataSource", listData);
		byte[] reportByteContent = ReportService.getReportContent(params, "dataSource", ReportService.PDF_REPORT_TYPE, reportFileName);
		//System.out.println("after BYTE CONTENT download try"+ reportByteContent);
		response.setContentType("application/pdf");			
		response.setHeader("Content-Length", String.valueOf((reportByteContent.length)));
		response.setHeader("Content-Disposition", "attachment;filename="+pdfFileName.toString()+".pdf"+"");
		BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
		//System.out.println("Coming after download");
		try{
			//System.out.println("Coming after download try");
			bos.write(reportByteContent);
			//System.out.println("Coming after download try end");
		}catch(Exception e){
			throw e;
		}finally{
			if(bos!=null){
				bos.flush();
				bos.close();
			}
			
			if(fis!=null){
				fis.close();
				log.info("File has been deleted at "+pageName);
			}
			
			if(qrFile!=null){
				log.info("File delete status at: "+pageName+" : "+qrFile.delete());
			}
			
			response.getOutputStream().flush();
			response.getOutputStream().close();
			
			/*Logging Receipt DOWNLOAD details */
			Map<String,String> receiptDataForLogging = new HashMap<String,String>();
			
			int receiptNumber = 0;
			String receiptNumberInHexa = "";
			String receiptType = "";
			
			receiptNumberInHexa = PortalUtility.getString(request.getParameter("RECEIPT_NO"));
			receiptNumber 		= PortalUtility.hex2decimal(receiptNumberInHexa);
			
			if(PortalUtility.getString(request.getParameter("folderType")).equals("RCPT")){
				receiptType = "BULK";
			}else if(PortalUtility.getString(request.getParameter("folderType")).equals("RCTI")){
				receiptType = "CreditNotes";
			}else{
				
			}
			
			receiptDataForLogging.put("receiptNumber",String.valueOf(receiptNumber));
			receiptDataForLogging.put("receiptType",receiptType);
			receiptDataForLogging.put("paymentReference",PortalUtility.getString(request.getParameter("PAYMENT_REFERENCE")));
			receiptDataForLogging.put("downloadMedium","SearchReceiptPage");
			
			Map<String,Object> responseObject = PortalService.doReceipAuditLogging(lid,receiptDataForLogging);
			
			log.info("Printing... loggingResponseObject >>>"+responseObject);
			log.info("Printing... Exiting >>>"+pageName);
		}
		
	}else if(receiptFolderType.equals("FR") || receiptFolderType.equals("FRCT")){
		
		String receiptType = PortalUtility.getString(request.getParameter("receiptType"));
		
		if(receiptType.equals("FR")){
			/* BULK */
			reportFileName = "FIRS_FRCT_FolderPrint_BulkNote.jrxml";
			Map<String, Object> params 	= new HashMap<String, Object>();
			
			String BULK_RECEIPT_NUMBER 	= PortalUtility.getString(request.getParameter("BULK_RECEIPT_NUMBER"));
			String BULK_PAYMENT_REF 	= PortalUtility.getString(request.getParameter("BULK_PAYMENT_REF"));
			String BULK_BANK 			= PortalUtility.getString(request.getParameter("BULK_BANK"));
			String BULK_TIN 			= PortalUtility.getString(request.getParameter("BULK_TIN"));
			String BULK_CURRENCY_TYPE 	= PortalUtility.getString(request.getParameter("BULK_CURRENCY_TYPE"));
			String BULK_TAX_TYPE 		= PortalUtility.getString(request.getParameter("BULK_TAX_TYPE"));
			String BULK_RECV_FROM 		= PortalUtility.getString(request.getParameter("BULK_RECV_FROM"));
			String BULK_TAX_OFFICE 		= PortalUtility.getString(request.getParameter("BULK_TAX_OFFICE"));
			String BULK_ADDRESS 		= PortalUtility.getString(request.getParameter("BULK_ADDRESS"));
			String BULK_PERIOD_FROM 	= PortalUtility.getString(request.getParameter("BULK_PERIOD_FROM"));
			String BULK_PERIOD_TO 		= PortalUtility.getString(request.getParameter("BULK_PERIOD_TO"));
			String BULK_PAYMENT_DATE 	= PortalUtility.getString(request.getParameter("BULK_PAYMENT_DATE"));
			String BULK_AMOUNT 			= PortalUtility.getString(request.getParameter("BULK_AMOUNT"));
			
			BULK_AMOUNT = String.format("%,.2f", Float.parseFloat(BULK_AMOUNT));
			String BULK_AMOUNT_WORD 	= PortalUtility.getString(request.getParameter("BULK_AMOUNT_WORD"));
			
			/* params.put("BULK_PAYMENT_REF",BULK_PAYMENT_REF);
			params.put("BULK_BANK",BULK_BANK);
			params.put("BULK_TIN",BULK_TIN);
			params.put("BULK_TAX_TYPE",BULK_TAX_TYPE);
			params.put("BULK_RECV_FROM",BULK_RECV_FROM);
			params.put("BULK_TAX_OFFICE",BULK_TAX_OFFICE);
			params.put("BULK_ADDRESS",BULK_ADDRESS);
			params.put("BULK_PERIOD_FROM",BULK_PERIOD_FROM);
			params.put("BULK_PERIOD_TO",BULK_PERIOD_TO);
			params.put("BULK_PAYMENT_DATE",BULK_PAYMENT_DATE);
			params.put("BULK_AMOUNT",BULK_AMOUNT); */
			
			params.put("RECEIPT_NUMBER",BULK_RECEIPT_NUMBER);
			params.put("RECV_FROM",BULK_RECV_FROM);
			params.put("ADDRESS",BULK_ADDRESS);
			params.put("TIN",BULK_TIN);
			params.put("CURRENCY_TYPE",BULK_CURRENCY_TYPE);
			params.put("TAX_TYPE",BULK_TAX_TYPE);
			params.put("AMOUNT",BULK_AMOUNT);
			params.put("AMOUNT_WORD",BULK_AMOUNT_WORD);
			params.put("PERIOD_FROM",BULK_PERIOD_FROM);
			params.put("PERIOD_TO",BULK_PERIOD_TO);
			params.put("BANK",BULK_BANK);
			params.put("TAX_OFFICE",BULK_TAX_OFFICE);
			params.put("PAYMENT_REF",BULK_PAYMENT_REF);
			params.put("PAYMENT_DATE",BULK_PAYMENT_DATE);
			params.put("ISSUING_OFFICER","");
			params.put("TAX_OFFICER","");
			params.put("IRNO","");
			
			/* GENERATING REPORTS */
			List<Object> listData = new ArrayList<Object>();
			Map<String, Object> colectionDataSource = new HashMap<String, Object>();
			listData.add(colectionDataSource);
			params.put("dataSource", listData);
			
			//TODO:REPORT NAME HAS TO BE SET
			byte[] reportByteContent = ReportService.getReportContent(params, "dataSource", ReportService.PDF_REPORT_TYPE, reportFileName);
			
			response.setContentType("application/pdf");			
			response.setHeader("Content-Length", String.valueOf((reportByteContent.length)));
			response.setHeader("Content-Disposition", "attachment;filename="+"Receipt"+".pdf"+"");
			BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
			
			try{
				//System.out.println("Coming after download try");
				bos.write(reportByteContent);
				//System.out.println("Coming after download try end");
			}catch(Exception e){
				throw e;
			}finally{
				if(bos!=null){
					bos.flush();
					bos.close();
				}
			}	
			
		}else if(receiptType.equals("FRCT")){
			/* CREDIT */
			reportFileName = "FIRS_FRCT_FolderPrint_CreditNote.jrxml";
			Map<String, Object> params 	= new HashMap<String, Object>();
			
			String CREDIT_RECEIPT_NUMBER 		= PortalUtility.getString(request.getParameter("CREDIT_RECEIPT_NUMBER"));
			String CREDIT_DATE 					= PortalUtility.getString(request.getParameter("CREDIT_DATE"));
			String CREDIT_RECV_FROM 			= PortalUtility.getString(request.getParameter("CREDIT_RECV_FROM"));
			String CREDIT_BENEFICIARY_TIN 		= PortalUtility.getString(request.getParameter("CREDIT_BENEFICIARY_TIN"));
			String CREDIT_ADDRESS 				= PortalUtility.getString(request.getParameter("CREDIT_ADDRESS"));
			String CREDIT_PAYER 				= PortalUtility.getString(request.getParameter("CREDIT_PAYER"));
			String CREDIT_PAYER_TIN 			= PortalUtility.getString(request.getParameter("CREDIT_PAYER_TIN"));
			String CREDIT_TAX_TYPE 				= PortalUtility.getString(request.getParameter("CREDIT_TAX_TYPE"));
			String CREDIT_AMOUNT 				= PortalUtility.getString(request.getParameter("CREDIT_AMOUNT"));
			
			CREDIT_AMOUNT = String.format("%,.2f", Float.parseFloat(CREDIT_AMOUNT));
			String CREDIT_AMOUNT_WORD 			= PortalUtility.getString(request.getParameter("CREDIT_AMOUNT_WORD"));
			String CREDIT_CONTRACT_AMOUNT 		= PortalUtility.getString(request.getParameter("CREDIT_CONTRACT_AMOUNT"));
			String CREDIT_WHT_RATE 				= PortalUtility.getString(request.getParameter("CREDIT_WHT_RATE"));
			String CREDIT_CONTRACT_DESC 		= PortalUtility.getString(request.getParameter("CREDIT_CONTRACT_DESC"));
			String CREDIT_PERIOD_FROM 			= PortalUtility.getString(request.getParameter("CREDIT_PERIOD_FROM"));
			String CREDIT_PERIOD_TO 			= PortalUtility.getString(request.getParameter("CREDIT_PERIOD_TO"));
			String CREDIT_BANK 					= PortalUtility.getString(request.getParameter("CREDIT_BANK"));
			String CREDIT_TAX_OFFICE 			= PortalUtility.getString(request.getParameter("CREDIT_TAX_OFFICE"));
			String CREDIT_PAYMENT_REF 			= PortalUtility.getString(request.getParameter("CREDIT_PAYMENT_REF"));
			String CREDIT_PAYMENT_DATE 			= PortalUtility.getString(request.getParameter("CREDIT_PAYMENT_DATE"));
			String CREDIT_ISSUING_OFFICER 		= PortalUtility.getString(request.getParameter("CREDIT_ISSUING_OFFICER"));
			String CREDIT_BULK_IRNO 			= PortalUtility.getString(request.getParameter("CREDIT_BULK_IRNO"));
			String CREDIT_SUPERVISING_OFFICER 	= PortalUtility.getString(request.getParameter("CREDIT_SUPERVISING_OFFICER"));
			String CREDIT_CURRENCY_TYPE 		= PortalUtility.getString(request.getParameter("CREDIT_CURRENCY_TYPE"));
			
			params.put("RECEIPT_NUMBER",CREDIT_RECEIPT_NUMBER);
			params.put("DATE",CREDIT_DATE);
			params.put("RECV_FROM",CREDIT_RECV_FROM);
			params.put("BENEFICIARY_TIN",CREDIT_BENEFICIARY_TIN);
			params.put("ADDRESS",CREDIT_ADDRESS);
			params.put("PAYER",CREDIT_PAYER);
			params.put("PAYER_TIN",CREDIT_PAYER_TIN);
			params.put("TAX_TYPE",CREDIT_TAX_TYPE);
			params.put("AMOUNT_WORD",CREDIT_AMOUNT_WORD);
			params.put("AMOUNT",CREDIT_AMOUNT);
			params.put("CONTRACT_AMOUNT",CREDIT_CONTRACT_AMOUNT);
			params.put("WHT_RATE",CREDIT_WHT_RATE);
			params.put("CONTRACT_DESC",CREDIT_CONTRACT_DESC);
			params.put("PERIOD_FROM",CREDIT_PERIOD_FROM);
			params.put("PERIOD_TO",CREDIT_PERIOD_TO);
			params.put("BANK",CREDIT_BANK);
			params.put("TAX_OFFICE",CREDIT_TAX_OFFICE);
			params.put("PAYMENT_REF",CREDIT_PAYMENT_REF);
			params.put("PAYMENT_DATE",CREDIT_PAYMENT_DATE);
			params.put("ISSUING_OFFICER","");
			params.put("IRNO",CREDIT_BULK_IRNO);
			params.put("SUPERVISING_OFFICER",CREDIT_SUPERVISING_OFFICER);
			params.put("CURRENCY_TYPE",CREDIT_CURRENCY_TYPE);
			
			/* GENERATING REPORTS */
			List<Object> listData = new ArrayList<Object>();
			Map<String, Object> colectionDataSource = new HashMap<String, Object>();
			listData.add(colectionDataSource);
			params.put("dataSource", listData);
			
			//TODO:REPORT NAME HAS TO BE SET
			byte[] reportByteContent = ReportService.getReportContent(params, "dataSource", ReportService.PDF_REPORT_TYPE, reportFileName);
			
			response.setContentType("application/pdf");			
			response.setHeader("Content-Length", String.valueOf((reportByteContent.length)));
			response.setHeader("Content-Disposition", "attachment;filename="+"Receipt"+".pdf"+"");
			BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
			
			try{
				//System.out.println("Coming after download try");
				bos.write(reportByteContent);
				//System.out.println("Coming after download try end");
			}catch(Exception e){
				throw e;
			}finally{
				if(bos!=null){
					bos.flush();
					bos.close();
				}
			}	
		}
		
	}
%>
