<%@page import="org.apache.axis.encoding.Base64"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsDisplayInfoOptionFlag"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.client.AmandaClient"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@include file="include/includeconstant.jsp"%>
<%@ page language="java" contentType="application/x-www-form-urlencoded;charset=utf-8" pageEncoding="ISO-8859-1"%>

<%response.setHeader("Content-Type", "application/json; charset=UTF-8");%>
<%!
	WsPeople people;
	WsPeople contactPerson;
	WsFolder[] folderwithEmail;
	WsPeople[] peopleList;
	WsFolder[] contactFolder;
	JSONObject validationDetails;
	private String setUserDataInSession(AmandaClient client,String lid, WsPeople people, WsFolder folder,WsFolderInfo[] folderInfo, WsPeopleInfo[] peopleInfo, HttpSession session){
		PortalService.setPeopleDataInSession(session,people,folder,folderInfo,peopleInfo);
		if(people.getPeopleCode()==4){
			return ("IndividualProfile.jsp");
		}else if(people.getPeopleCode()==3){			
			return ("CorporateProfile.jsp");	
		}else{
			return ("ForexProfile.jsp");
		}
	}
	private String loginUser(AmandaClient client,String lid, WsPeople people, HttpSession session, String password){
		String result 				= "";
		String currentPassword 		= "";
		WsFolderInfo[] folderInfo 	= null;
		WsPeopleInfo[] peopleInfo 	= null;
		WsFolder folder = null;
		/*folder=PortalService.getFolderREGDByPeople(client, lid, PortalUtility.getInt(people.getPeopleRSN()));*/
		
		try{
			/*folderInfo = client.getFolderInfo(lid,PortalUtility.getInt(folder.getFolderRSN()),new WsDisplayInfoOptionFlag[] {});*/
			peopleInfo = client.getPeopleInfo(lid,PortalUtility.getInt(people.getPeopleRSN()));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String storedPassword = "";
		
		try{
			storedPassword = PortalUtility.decrypt((PortalUtility.getString(people.getInternetQuestion())));
		}catch(Exception e){e.printStackTrace();}
		
		if(storedPassword.equals(password)){
			return setUserDataInSession(client,lid,people,folder,folderInfo, peopleInfo, session);
		}
		return "";
	}
	private boolean activateUser(AmandaClient client,String lid, WsPeople people, HttpSession session, HttpServletRequest request, String password){
		String firsPassword 		= "";
		String result 				= "";
		String currentPassword 		= "";
		WsFolderInfo[] folderInfo 	= null;
		WsPeopleInfo[] peopleInfo 	= null;
		WsFolder folder=null;
		/*folder=PortalService.getFolderREGDByPeople(client, lid, PortalUtility.getInt(people.getPeopleRSN()));*/
		
		try{
			/*folderInfo = client.getFolderInfo(lid,PortalUtility.getInt(folder.getFolderRSN()),new WsDisplayInfoOptionFlag[] {});*/
			peopleInfo = client.getPeopleInfo(lid,PortalUtility.getInt(people.getPeopleRSN()));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		firsPassword = PortalUtility.getString(people.getInternetAnswer());
		
		
		if(firsPassword.equals(password)){
			PortalService.setPeopleDataInSession(session,people,folder,folderInfo,peopleInfo);
			return true;
		}
		return false;
	}
%>

<%
	String result = "";
	int peopleRSN = 0;
	int folderRSN;
	int contactFolderRSN;
	String mobileNumber;
	String defalutErrorMessage = "Please enter correct TIN/Password";
	
	boolean peopleExistsWithTIN 	= false;
	boolean peopleExistsWithJTBTIN 	= false;
	
	people 				= new WsPeople();
	contactPerson 		= new WsPeople();
	folderwithEmail 	= new WsFolder[1];
	peopleList 			= new WsPeople[1];
	contactFolder 		= new WsFolder[5];
	validationDetails 	= new JSONObject();
	
	final int CORPORATE_PEOPLE_CODE    =  3;
	final int INDIVIDUAL_PEOPLE_CODE   =  4;
	final int FOREX_PEOPLE_CODE        =  5;
	
	log.info("File Name:" + pageName + " Begins");
	log.info("login_ajax Begins...");
	
	String command 			= request.getParameter("command");
	String userName 		= PortalUtility.getString(request.getParameter("userName")).trim();
	String password			= PortalUtility.getString(request.getParameter("password")).trim();
	String currentPassword	= "";
	
	if(command.equalsIgnoreCase("loginUser")){
		result="2";																	
		
		peopleExistsWithTIN = PortalService.checkPeople(PortalService.getWSClient(),lid,"LicenceNumber",userName);
		if(!peopleExistsWithTIN){
			peopleExistsWithJTBTIN = PortalService.checkPeople(PortalService.getWSClient(),lid,"SocialSecurityNumber",userName);
		}
		
		if(peopleExistsWithTIN){
			people       = PortalService.getPeople(PortalService.getWSClient(),lid,"LicenceNumber",userName);
		}else if(peopleExistsWithJTBTIN){
			people       = PortalService.getPeople(PortalService.getWSClient(),lid,"SocialSecurityNumber",userName);
		}
		
		if((peopleExistsWithTIN || peopleExistsWithJTBTIN) && !PortalUtility.isNull(people)){
			if(PortalUtility.getString(people.getSmsFlag()).equalsIgnoreCase("Y")){
				String internetAccessFlag = PortalUtility.getString(people.getInternetAccess());
				//internetAccessFlag = "";
				if(internetAccessFlag.equalsIgnoreCase("A")){ 					
					String nextPage = loginUser(PortalService.getWSClient(),lid,people,session,password);
					if(!nextPage.equalsIgnoreCase("")){
						validationDetails.put("page",nextPage);
						result = "1";
					}else{
						validationDetails.put("errorMessage",defalutErrorMessage);
					}
				}else{
					if(activateUser(PortalService.getWSClient(),lid,people,session,request, password)){
						session.setAttribute("passwordChangeAccess","y"); 				
						validationDetails.put("page",request.getContextPath()+"/portal?cmd=changepassword");
						result = "1";
					}else{
						validationDetails.put("errorMessage",defalutErrorMessage);
					}
				}
			}else{
				validationDetails.put("isAccountActive",false);
				validationDetails.put("errorMessage","Your account is not active.Please check your E Mail for activation URL");
			}
		}else{
			validationDetails.put("errorMessage",defalutErrorMessage);
		}
	}else if(command.equalsIgnoreCase("changePassword")){
		result="2";
		people=(WsPeople)session.getAttribute("people");
		String redirectTo = "";
		validationDetails.put("page",request.getContextPath());
		
		if(people!=null){
			if(PortalUtility.getInt(people.getPeopleCode()) == INDIVIDUAL_PEOPLE_CODE){
				redirectTo = "IndividualProfile.jsp";
			}else if(PortalUtility.getInt(people.getPeopleCode()) == CORPORATE_PEOPLE_CODE){			
				redirectTo = "CorporateProfile.jsp";	
			}else if(PortalUtility.getInt(people.getPeopleCode()) == FOREX_PEOPLE_CODE){
				redirectTo = "ForexProfile.jsp";
			}
			String newPassword1=PortalUtility.getString(request.getParameter("newPassword")).trim();
			
			people.setInternetAccess("A");
			
			people.setInternetQuestion(PortalUtility.encrypt(newPassword1));
			PortalService.getWSClient().updatePeople(lid, people);
			validationDetails.put("page",redirectTo);
			result = "1";
			
		}
		
	}else if(command.equalsIgnoreCase("forgotPassword")){
		
	}
	log.info("File Name:" + pageName + " Ends");
	validationDetails.put("result", result);
	out.print(validationDetails.toString());
	log.info("login_ajax ..validationDetails::"+ validationDetails.toString());
%>
