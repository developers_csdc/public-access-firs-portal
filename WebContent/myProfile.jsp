<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>


<style type="text/css">
.gapMedium {
	padding-bottom: 25px;
}

label {
	font-size: 16px;
}

form .form-control {
	height: 35px;
}

body {
	font-family: "Calibri";
}
.glyphicon-ok {
    color: #41be47;
}

.glyphicon-remove {
    color: red;
}
</style>

<body>
	<div class="section-heading scrollpoint sp-effect3">
		<h1></h1>
		<div class="divider"></div>
		<!--p>For more info and support, contact us!</p-->
	</div>
	<!-- Alert for the user added by admin -->
	<div class="row">
		<div class="col-md-2 col-sm-2"></div>
    	<div class="col-md-8 col-sm-8 scrollpoint sp-effect3">
			
				<div class="alert alert-warning">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<i class="fa fa-exclamation-triangle"></i>
					<strong></strong> 
					
				</div>
				<div class="alert alert-info">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<i class="fa fa-exclamation-circle"></i>
					<strong></strong> 
					
				</div>
		
		</div>
	</div>
	<input type="" name="lid" id="lid" value="">
	<div class="row">
		<div class="col-md-1 col-sm-1"></div>
		<div class="col-md-10 col-sm-10">
			<div class="row">			
				<div class="col-lg-4">
					<label for="EmailAddress" class="mandatory"></label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span> 
						<input id="EmailAddress" name="EmailAddress" readonly="readonly" type="text" class="form-control" value="" />
					</div>
				</div>
				<div class="col-lg-2">
					<label for="Title"></label>
					<select id="Title" name="Title" tabindex="3" class="form-control ">
						<option value=""></option>
						
					</select>
				</div>
				
				<div class="col-lg-6">
					<div class="row">			
						<div class="col-lg-6">
							<label for="Name" class="mandatory"><span style="color: red;">*</span> </label>
							<input id="Name" name="Name" type="text" class="form-control" value="" />
						</div>
			
						<div class="col-lg-6">
							<label for="NameLast" class="mandatory"><span style="color: red;">*</span></label>
							<input id="NameLast" name="NameLast" type="text" class="form-control" value="" />
						</div>
					</div>
				</div>
			</div>
			<div class="gapMedium"></div>
			<div class="row">							
				<div class="col-lg-4">
					<label for="Address" class="mandatory"><span style="color: red;">*</span></label>
					<input id="Address" name="Address" type="text" tabindex="9" class="form-control" value="" />
				</div>
	
				<div class="col-lg-4">
					<label for="City"><span style="color: red;">*</span></label>
					<input type="text" id="City" name="City" class="form-control " value="" />
					<div id="suggestions-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
				</div>
			
				<div class="col-lg-4">
					<label for="Country"></label>
					<input type="text" id="Country" name="Country" class="form-control " value="" />
					<div id="suggestions-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
				</div>
			</div>
			<div class="gapMedium"></div>
			<div class="row">				
				<div class="col-lg-4">
					<label for="Phone" class="mandatory"><span style="color: red;">*</span></label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span> 
						<input id="Phone" name="Phone" type="text" class="form-control" onkeyup="phoneMask(this.id);" value="" />
					</div>
				</div>
				
				<div class="col-lg-4">
					<label for="Fax" class="mandatory"></label>
					<div class="input-group">
						<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span> 
						<input id="Fax" name="Fax" type="text" class="form-control" onkeyup="phoneMask(this.id);" value=""/>
					</div>
				</div>		
			
				<div class="col-lg-4">
					<label for="PostalCode"></label>
					<input id="PostalCode" name="PostalCode" type="text" class="form-control " value="" />
				</div>		
			</div>
			<div class="gapMedium"></div>
			
			<div class="row">
				<div class="col-lg-6">
					<label for="Question" class="mandatory"><span style="color: red;">*</span></label>
					<select id="Question" name="Question" tabindex="" class="form-control ">
						<option value=""></option>
						
					</select>
				</div>
				<div class="col-lg-6">
					<label for="QuestionAnswer" class="mandatory"><span style="color: red;">*</span></label>
					<input id="QuestionAnswer" name="QuestionAnswer" type="text" tabindex="" class="form-control" value="" />
				</div>
			</div>
		
			<div class="gapMedium"></div>
			<div class="row">
				<div class="col-lg-6">
					<div class="checkbox">
						<label> <input type="checkbox" id="ShowPassword" onclick="showPasswordDiv()"></label>
					</div>
				</div>
			</div>
		
			<div class="gapMedium"></div>
		
			<div class="row" id="passwordMainDiv" style="display: none">
				
				<div class="col-lg-4">
					<div class="form-group" id="tempPassword">
						<label for="TempPassword"></label>
						<div class="input-group">
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-exclamation-sign"></span></span> <input
								id="TempPassword" name="TempPassword" type="password"
								class="form-control">
						</div>
					</div>
				</div>
				
				<div class="col-lg-4">
					<div class="form-group" id="password1Div">
						<label for="newPassword"></label>
						<div class="input-group">
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-lock"></span></span> <input id="newPassword"
								name="newPassword" type="password" class="form-control">
							<span id="passstrength"></span>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group" id="passwordDiv">
						<label for="reTypePassword"></label>
						<div class="input-group">
							<span class="input-group-addon"><span
								class="glyphicon glyphicon-pencil"></span></span> <input
								id="reTypePassword" name="reTypePassword" type="password"
								class="form-control">
						</div>
					</div>
				</div>
			</div>
			<div class="gapMedium"></div>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-3"></div>
				<div class="col-lg-3"></div>
				<div class="col-lg-3">
					<div class="btn-toolbar">
						<button id="btnSend" type="button" class="btn btn-success pull-right" onclick="f_submitMyProfile()">
							<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;
						</button>
					</div>
				</div>
			</div>
			<div class="gapMedium"></div>
		</div>
		<div class="col-md-1 col-sm-1"></div>
		
	</div>
	
</body>