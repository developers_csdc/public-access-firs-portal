<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderProcessAttempt"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.*"%>
<%@page import="java.io.BufferedOutputStream"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfo"%>
<%@page import="com.csdcsystems.amanda.client.service.ReportService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderFreeform"%>

<%--ZXING & Graphics IMPORTS --%>
<%!
	String errorResponseMessage="This is not a valid TCC number or this TCC is not issued to you.";
	final int GET_USERDETAILS_TRANSACTION_CODE=100;
	private static int CORPORATE_FOLDER_SUBCODE=1,INDIVIDUAL_FOLDER_SUBCODE=2, FOREX_FOLDER_SUBCODE=3;
	private static int CORPORATE_FREEFORM_CODE=1,INDIVIDUAL_FREEFORM_CODE=2, FOREX_FREEFORM_CODE=3;
	
	private static String CORPORATE_FOLDER_TYPE="TCC",INDIVIDUAL_FOLDER_TYPE="ITCC", FOREX_FOLDER_TYPE="FTCC";
	
	private static int TCC_PROCESS_CODE = 1020,TCC_ATTEMPT_CODE=5;
	private static String QR_CODE_IMAGE_FOLDER = "tempQRImage";
	private static String CUSTOM_DATE_FORMAT = "dd-MMM-yyyy";
	private final int TAX_OFFICE_INFO_CODE = 5090;
	
	final int TCC_INSURANCE_PROCESS_CODE               =  1000;//Edited by ess.abhimanyu process code changed from 1020 to 1000
	final int SOURCE_OF_INCOME_INFO_CODE               =  5095;
	final int OTHER_COMMENTS_INFO_CODE                 =  5099;
	final int SOURCE_OF_INCOME_TRANSACTION_CODE        =   105;
	final int GET_USER_SIGNATURE_TRANSACTION_CODE      =   119;
	
	String SOURCE_OF_INCOME ="";
	String OTHER_COMMENTS ="";
	
	String FIRS_QRCODE;
	File qrFile=null;
	String filePath;
	
	public Map<String,String> natureOfAssessmentMap = new HashMap<String,String>();
	Map<Integer, String> peopleInfoMap = new HashMap<Integer, String>();
	FileInputStream fis=null;
	String encodedTCC;
	int folderSubCode=0;
	String tccFolderType="";
	String reportFileName;
	int floatRoundOffCount = 2; //Variable to control rounding off of floating params.
	public void prepareNatureOfAssessmentMap(){
		natureOfAssessmentMap.put("Best of Judgement","i");
		natureOfAssessmentMap.put("Self Assessment","ii");
		natureOfAssessmentMap.put("Minimum Tax","iii");
		natureOfAssessmentMap.put("Pre-Operation Levy(POL)","iv");
		natureOfAssessmentMap.put("Government(Additional,Audit,Investigation)","v");
		natureOfAssessmentMap.put("Others(Please Specify)","vi");
	}
	public String getAssessmentNatureCode(String assessmentDesc){
		String result;
		for(Map.Entry<String,String> natureOfAssessmentItem: natureOfAssessmentMap.entrySet()){
			if((natureOfAssessmentItem.getKey().replace(" ","")).equalsIgnoreCase(assessmentDesc.replace(" ",""))){
				return natureOfAssessmentItem.getValue();
			}
		}
		return "";
	}
%>

<%
	log.info("Entering"+pageName);
	prepareNatureOfAssessmentMap();
	String REPORT_NAME =null;
	WsFolderFreeform[] freeformList=null;
	Map<Integer,String> folderInfoMap = null;
	WsFolder tccFolder = null;
	String TAX_OFFICE = null;
	String TAX_OFFICE_DESC = null;
	/*Added by samarjit*/
	WsPeople people = null;
	
	
	WsPeopleInfo[] peopleInfos = (WsPeopleInfo[])session.getAttribute("peopleInfo");
	peopleInfoMap = PortalService.getPeopleInfoInMap(peopleInfos);
	
	WsValidInfoValue[] infoValues=null;
	infoValues = PortalService.getWSClient().getValidInfoValues(lid,TAX_OFFICE_INFO_CODE);
	/*Retrieving infoValue Map and getting info value desc*/
	if(peopleInfos != null){
		if(peopleInfoMap.containsKey(TAX_OFFICE_INFO_CODE)){
			TAX_OFFICE_DESC = peopleInfoMap.get(TAX_OFFICE_INFO_CODE);
			if(infoValues != null && infoValues.length>0){
				for(WsValidInfoValue infoValue:infoValues){
					if(infoValue.getInfoValue().equals(TAX_OFFICE_DESC)){
						TAX_OFFICE = infoValue.getInfoDesc();
					}
				}
			}
		}
	/* for(WsPeopleInfo peopleInfo : peopleInfos){
		if(peopleInfo.getInfoCode().equals(5090)){
			TAX_OFFICE = peopleInfo.getInfoValue();
		}
	 } */
	
	}
	people = (WsPeople)session.getAttribute("people");
	
	String orgName =PortalUtility.getString(people.getOrganizationName());
	String peopleAddress = PortalUtility.getString(people.getAddressLine1());
	
	String RC_No = PortalUtility.getString(people.getReferenceFile());
	String DATE_OF_INCOR = PortalUtility.getString(PortalUtility.getDate(people.getBirthDate()));
	String TIN = PortalUtility.getString(people.getLicenceNumber());
	/*Code Ends*/
	
	
	String EXPORT_FILE = "myExportedReport.pdf";
	/*String tccNumber = PortalUtility.getString(request.getParameter("TCCNumber"));
	String tinNumber = PortalUtility.getString(request.getParameter("TINNumber"));
	String folderNumber = PortalUtility.getString(request.getParameter("ApplicationNumber"));*/
	
	
	String tccNumber = PortalUtility.getString(request.getParameter("TCCNumber"));	
	String tinNumber = PortalUtility.getString(people.getLicenceNumber());
	//tccFolder=PortalService.getFolderByTIN(PortalUtility.getString(tinNumber));	
	//String folderNumber = String.valueOf(tccFolder.getFolderRSN());
	
	
	try {
		tccFolder = PortalService.getFolderByTCCTIN(tccNumber,tinNumber);		
		//53691 - Check for status code to ensure only folders with "Issued" status generate a TCC report.
		if(tccFolder!=null && (tccFolder.getStatusCode()==3 || tccFolder.getStatusCode()==4)){
			
			folderSubCode = PortalUtility.getInt(tccFolder.getSubCode());
			tccFolderType = PortalUtility.getString(tccFolder.getFolderType());
			
			Map<String, Object> params = new HashMap<String, Object>();
			
			/* Code for getting Sign of User from DB in BASE 64 format - STARTS */
			String signInByteArray = "";
			WsTransactionRequest[] userSignRequest = new WsTransactionRequest[1];
			userSignRequest[0] = new WsTransactionRequest("argFolderRSN",PortalUtility.getString(String.valueOf(tccFolder.getFolderRSN())));
			WsTransactionResponse[] userSignResponse = PortalService.getWSClient().executeCustomTransaction(lid,GET_USER_SIGNATURE_TRANSACTION_CODE,userSignRequest);
			
			if(userSignResponse!=null && userSignResponse.length>0){
				if(userSignResponse[0].getColumnValues()[0].length()>0){
					signInByteArray = PortalUtility.getString(userSignResponse[0].getColumnValues()[0]);
				}
			}
			
			params.put("USER_SIGN",signInByteArray);
			/* Code for getting Sign of User from DB - ENDS */
			
	if(tccFolderType.equalsIgnoreCase(FOREX_FOLDER_TYPE)){
		//TODO: Mapping for Forex report
		
		reportFileName ="FIRS_TCC_Forex.jrxml";	
	
		freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(tccFolder.getFolderRSN(),3,new int[]{FOREX_FREEFORM_CODE});
		String userID = PortalService.getUserIDByFolderRSNProcessCode(tccFolder.getFolderRSN(),TCC_PROCESS_CODE,TCC_ATTEMPT_CODE);
		
		String rank = "";
		String F03_WORD = "";
		String TCC_ISSUANCE_DATE = "";
		String ATTEMPT_USER = "";
		
		if(userID!=null && !userID.equalsIgnoreCase("")){
			WsTransactionRequest[] requests = new WsTransactionRequest[1];
			requests[0] = new WsTransactionRequest("argUserID",String.valueOf(userID));
			WsTransactionResponse[] transactionResponse = PortalService.getWSClient().executeCustomTransaction(lid,GET_USERDETAILS_TRANSACTION_CODE,requests);
			
			if(transactionResponse!=null && transactionResponse.length>0 ){
				ATTEMPT_USER=PortalUtility.getString(transactionResponse[0].getColumnValues()[0]);
				rank=PortalUtility.getString(transactionResponse[1].getColumnValues()[0]);
			}	
		}else{
			params.put("ATTEMPT_USER","");
			params.put("RANK","");
		}
		
		folderInfoMap =  PortalService.getFolderInfoByFolderRSNInMap(tccFolder.getFolderRSN());
		
		List<Object> listData = new ArrayList<Object>();
		Map<String, Object> colectionDataSource = new HashMap<String, Object>();
		listData.add(colectionDataSource);
		params.put("dataSource", listData);
		
		String 	s_F01 = PortalUtility.getString(freeformList[0].getC05()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[0].getC05()).trim(),
				s_F02 = PortalUtility.getString(freeformList[0].getC06()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[0].getC06()).trim(),
				s_F03 = PortalUtility.getString(freeformList[0].getC07()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[0].getC07()).trim(),
				s_F04 = PortalUtility.getString(freeformList[0].getC08()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[0].getC08()).trim();
		
		s_F01 = s_F01.contains(".") ? s_F01 : s_F01+".00";
		s_F02 = s_F02.contains(".") ? s_F02 : s_F02+".00";
		s_F03 = s_F03.contains(".") ? s_F03 : s_F03+".00";
		s_F04 = s_F04.contains(".") ? s_F04 : s_F04+".00";
		
		DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
		
		String s_formatted_F01 = decimalformatter.format(new BigDecimal(s_F01));
		String s_formatted_F02 = decimalformatter.format(new BigDecimal(s_F02));
		String s_formatted_F03 = decimalformatter.format(new BigDecimal(s_F03));
		String s_formatted_F04 = decimalformatter.format(new BigDecimal(s_F04));
		
		Double d_F03 = 0.0;
		d_F03 = new BigDecimal(s_F03).doubleValue();
		F03_WORD = d_F03 != 0.00 ? PortalService.getConvertCurrencyToWordInDollar(d_F03) : "NIL";
		
		params.put("TCC_R", String.valueOf(tccFolder.getReferenceFile()));
		params.put("FOLDER_RSN", String.valueOf(tccFolder.getFolderRSN()));
		params.put("PEOPLE_NAME", people.getOrganizationName());
		params.put("ORGANIZATION_NAME", people.getAddressLine1());
		params.put("D01", PortalUtility.getDate(freeformList[0].getD01()) );
		params.put("C01", PortalUtility.getString(freeformList[0].getC01()));
		params.put("C02", PortalUtility.getString(freeformList[0].getC02()));
		
		params.put("F01",(!s_formatted_F01.equals("0") && !s_formatted_F01.equals("0.00")) ? s_formatted_F01 : "NIL");
		params.put("N01", String.valueOf(freeformList[0].getN01()));
		params.put("F02", (!s_formatted_F02.equals("0") && !s_formatted_F02.equals("0.00")) ? s_formatted_F02 : "NILL");
		params.put("F03", (!s_formatted_F03.equals("0") && !s_formatted_F03.equals("0.00")) ? s_formatted_F03 : "NILL");
		params.put("F03_WORD", F03_WORD);
		params.put("F04", (!s_formatted_F04.equals("0") && !s_formatted_F04.equals("0.00")) ? s_formatted_F04 : "NILL");
		params.put("RANK", rank);
		
		Calendar issueDate = tccFolder.getIssueDate()!=null?tccFolder.getIssueDate():tccFolder.getIndate();
		//params.put("BACKGROUND",DatatypeConverter.printBase64Binary(byteArrayBG));
		params.put("TCC_ISSUANCE_DATE",PortalUtility.getString(PortalUtility.getDateByPattern(issueDate,CUSTOM_DATE_FORMAT)));
		params.put("ATTEMPT_USER", ATTEMPT_USER);
		if(folderInfoMap!=null){		
			params.put("BENEFICIARY_NAME", folderInfoMap.get(6050));
			params.put("BENEFICIARY_ADDRESS", folderInfoMap.get(6060));
		}else{
			params.put("BENEFICIARY_NAME", "");
			params.put("BENEFICIARY_ADDRESS", "");
		}
		
		/*System.out.println("-----------------Started Printing Varaibles-----------------");
		System.out.println("ReportName : "+REPORT_NAME);
		System.out.println("FolderSubCode : "+folderSubCode);
		System.out.println("FolderRSN : "+String.valueOf(tccFolder.getFolderRSN()));
		System.out.println("OrganizationName : "+people.getOrganizationName());
		System.out.println("Address : "+people.getAddressLine1());
		System.out.println("D01 : "+PortalUtility.getDate(freeformList[0].getD01()));
		System.out.println("C01 : "+freeformList[0].getC01());
		System.out.println("F01 : "+PortalUtility.getFormattedDouble(freeformList[0].getF02(),floatRoundOffCount));
		System.out.println("N01 : "+String.valueOf(freeformList[0].getN01()));
		System.out.println("F02 : "+PortalUtility.getFormattedDouble(freeformList[0].getF02(),floatRoundOffCount));
		System.out.println("F03 : "+PortalUtility.getFormattedDouble(freeformList[0].getF03(),floatRoundOffCount));
		System.out.println("F03 Word : "+F03_WORD);
		System.out.println("F04 : "+PortalUtility.getFormattedDouble(freeformList[0].getF04(),floatRoundOffCount));
		System.out.println("Rank : "+rank);
		System.out.println("TCC_ISSUANCE_DATE : "+PortalUtility.getString(PortalUtility.getDateByPattern(issueDate,CUSTOM_DATE_FORMAT)));
		System.out.println("ATTEMPT_USER : "+ATTEMPT_USER);
		System.out.println("BENEFICIARY_NAME : "+folderInfoMap.get(6050));
		System.out.println("BENEFICIARY_ADDRESS : "+folderInfoMap.get(6060));
		System.out.println("-----------------Ended Printing Varaibles-----------------");*/
			
	}else{
			/*Adding QR code to Receipt report*/
			StringBuffer requestURL = request.getRequestURL();
			
			String t_tempURLWithGetURL = requestURL.toString();
			String t_contextURL = t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));
		
			//53691 - Adding QR code to TCC report
			/*QR CODE TEXT GENERATION*/
			StringBuilder QRCodeLinkBuilder = new StringBuilder();
			QRCodeLinkBuilder.append(t_contextURL);
			QRCodeLinkBuilder.append("/?cmd=searchtcc&tccNumber=");
			//TODO: Encode URL incase special characters are used in TCC
			try{
				encodedTCC = URLEncoder.encode(tccNumber,"UTF-8")
								.replaceAll("\\+", "%20")
			                    .replaceAll("\\%21", "!")
			                    .replaceAll("\\%27", "'")
			                    .replaceAll("\\%28", "(")
			                    .replaceAll("\\%29", ")")
			                    .replaceAll("\\%7E", "~");
			}catch(UnsupportedEncodingException e){
				encodedTCC=tccNumber;
			}
			QRCodeLinkBuilder.append(encodedTCC);
			String qrCodeText=QRCodeLinkBuilder.toString();
			/*QR CODE TEXT GENERATION - END*/
			/*Generating QR CODE*/
			int size = 125;
			String fileType = "png";
			StringBuilder filePathBuilder = new StringBuilder();
			filePathBuilder
				.append(request.getRealPath(QR_CODE_IMAGE_FOLDER))
				//.append(pathName)
				.append("\\")
				.append(tccNumber)
				.append("QRImage");
			filePath = filePathBuilder.toString();
			//qrFile = new File(filePath);
			/*for Testing:*/
			qrFile = new File("img");
			ServletContext context = request.getServletContext();
			String realPath = context.getRealPath("/");
			File bgFile = new File(reportImagePath+"TCC_BACKGROUND.png");
			byte[] byteArrayBG =null;
			
			if(bgFile.exists()){
				byteArrayBG =  IOUtils.toByteArray(new FileInputStream(bgFile));
			}
			
			if(!qrFile.exists()){
				PortalService.createQRImage(qrFile, qrCodeText, size, fileType);
			}
			fis = new FileInputStream(qrFile);
			byte[] byteArrayQR =  IOUtils.toByteArray(fis);
	        FIRS_QRCODE=DatatypeConverter.printBase64Binary(byteArrayQR);
			/*Generating QR CODE - END*/
			
			folderInfoMap =  PortalService.getFolderInfoByFolderRSNInMap(tccFolder.getFolderRSN());
			
			if(PortalUtility.getString(tccFolder.getFolderType()).equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
				freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(tccFolder.getFolderRSN(),3,new int[]{INDIVIDUAL_FREEFORM_CODE});
			}else if(PortalUtility.getString(tccFolder.getFolderType()).equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
				freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(tccFolder.getFolderRSN(),3,new int[]{CORPORATE_FREEFORM_CODE});
			}
			
			//Setting reportFileName to use based on folder subcode
			if(tccFolderType.equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
				reportFileName = "TCC_Corporate_NG.jrxml";
			}else if(tccFolderType.equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
				reportFileName = "TCC_Individual_NG.jrxml";
			}
			//Map<String, Object> params = new HashMap<String, Object>();
			List<Object> listData = new ArrayList<Object>();
			Map<String, Object> colectionDataSource = new HashMap<String, Object>();
			listData.add(colectionDataSource);
			params.put("dataSource", listData);
			params.put("TCC_NO",tccNumber.toString());
			//params.put("SOURCE_OF_INCOME",folderInfoMap.get(5095));
			
			/*Custom Transaction for SOURCE_OF_INCOME and OTHER_COMMENTS - ProcessInfoValue*/
			WsTransactionRequest[] transactionRequests = new WsTransactionRequest[4];
			transactionRequests[0] = new WsTransactionRequest("argFolderRSN",PortalUtility.getString(String.valueOf(tccFolder.getFolderRSN())));
			transactionRequests[1] = new WsTransactionRequest("argInfoCode_SOURCE_OF_INCOME",String.valueOf(SOURCE_OF_INCOME_INFO_CODE));			
			transactionRequests[2] = new WsTransactionRequest("argProcessCode",String.valueOf(TCC_INSURANCE_PROCESS_CODE));
			transactionRequests[3] = new WsTransactionRequest("argInfoCode_OTHER_COMMENTS",String.valueOf(OTHER_COMMENTS_INFO_CODE));
			WsTransactionResponse[] transactionResponse1 = PortalService.getWSClient().executeCustomTransaction(lid,SOURCE_OF_INCOME_TRANSACTION_CODE,transactionRequests);		
			
			Map<Integer,String> mapTransactionResponse1 = new HashMap<Integer,String>();
			if(!PortalUtility.isNull(transactionResponse1)){
				for(int i=0;i<transactionResponse1[0].getColumnValues().length;i++){
					String string = transactionResponse1[0].getColumnValues(i);
					String[] parts = string.split("-");
					int infoCode = 0;
					String infoValue = "";
					if(parts.length>1){
						infoCode = Integer.parseInt(parts[0]);
						infoValue = parts[1];
					}
					mapTransactionResponse1.put(infoCode,infoValue);
					
				}	
			}
			params.put("SOURCE_OF_INCOME",mapTransactionResponse1.get(SOURCE_OF_INCOME_INFO_CODE));
			
			Calendar issueDate = tccFolder.getIssueDate()!=null?tccFolder.getIssueDate():tccFolder.getIndate();
			//params.put("BACKGROUND",DatatypeConverter.printBase64Binary(byteArrayBG));
			params.put("DATE",PortalUtility.getString(PortalUtility.getDateByPattern(issueDate,CUSTOM_DATE_FORMAT)));
			if(folderInfoMap!=null){
				if(tccFolderType.equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
					params.put("NAME_OF_COMPANY",orgName);
					params.put("RC_NO",RC_No);
					params.put("DATE_OF_INCORPORATION",DATE_OF_INCOR);
					params.put("TIN",TIN);
					params.put("BUNESS_ADDRESS",peopleAddress);
					params.put("OTHER_COMMENTS",mapTransactionResponse1.get(OTHER_COMMENTS_INFO_CODE));
					//params.put("OTHER_COMMENTS",folderInfoMap.get(5080));
				}else if(tccFolderType.equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
					//TODO: Mapping for individual report
					params.put("PAYER_FILE_NO",(String.valueOf(tccFolder.getFolderRSN())));
					params.put("NAME_OF_INDIVIDUAL",orgName);
					params.put("INDIVIDUAL_ADDRESS",peopleAddress);
				}
				
				/*Retrieving tax office name*/
				WsValidInfo taxOfficeDetails=null;
				String infoDesc="";
				taxOfficeDetails=PortalService.getWSClient().getValidInfoByInfoCode(lid, TAX_OFFICE_INFO_CODE);
				if(taxOfficeDetails!=null){
					for(WsValidInfoValue info: taxOfficeDetails.getValidInfoValues()){
						if(PortalUtility.getString(info.getInfoValue()).equalsIgnoreCase(PortalUtility.getString(folderInfoMap.get(TAX_OFFICE_INFO_CODE)).trim())){
							infoDesc = PortalUtility.getString(info.getInfoDesc());
							TAX_OFFICE=infoDesc;
							System.out.print(TAX_OFFICE);
						}
					}
				}
				params.put("TAX_OFFICE",TAX_OFFICE);
				System.out.println(PortalUtility.getString(folderInfoMap.get(TAX_OFFICE_INFO_CODE)));
				//params.put("TAX_OFFICE",PortalUtility.getString(folderInfoMap.get(TAX_OFFICE_INFO_CODE)));
			}
			
			//params.put("USERNAME",PortalService.getUserIDByFolderRSNProcessCode(tccFolder.getFolderRSN(),TCC_PROCESS_CODE,TCC_ATTEMPT_CODE));
			params.put("USERGROUP",""); //TEMP
			params.put("FIRS_QRCODE",FIRS_QRCODE);
			
			String userID = PortalService.getUserIDByFolderRSNProcessCode(tccFolder.getFolderRSN(),TCC_PROCESS_CODE,TCC_ATTEMPT_CODE);
			
			if(userID!=null && !userID.equalsIgnoreCase("")){
				WsTransactionRequest[] requests = new WsTransactionRequest[1];
				requests[0] = new WsTransactionRequest("argUserID",String.valueOf(userID));
				WsTransactionResponse[] transactionResponse = PortalService.getWSClient().executeCustomTransaction(lid,GET_USERDETAILS_TRANSACTION_CODE,requests);
				
				if(transactionResponse!=null && transactionResponse.length>0 ){
					params.put("USERNAME",PortalUtility.getString(transactionResponse[0].getColumnValues()[0]));
					params.put("USERDEPARTMENT",PortalUtility.getString(transactionResponse[1].getColumnValues()[0]));
				}	
			}else{
				params.put("USERNAME","");
				params.put("USERDEPARTMENT","");
			}
			
			params.put("EXPIRIES_ON",PortalUtility.getDateInPlainFormat(tccFolder.getExpiryDate()));
			if(freeformList!=null && freeformList.length>0){
				for(int j=freeformList.length-1,i=1;j>=0;j--,i++){
					if(tccFolderType.equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
						params.put("N01"+"_" + i, PortalUtility.getStringFromObject(freeformList[j].getN01()).substring(2,4));
						
						String 	s_F01 = PortalUtility.getString(freeformList[j].getC10()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC10()).trim(),
								s_F02 = PortalUtility.getString(freeformList[j].getC11()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC11()).trim(),
								s_F03 = PortalUtility.getString(freeformList[j].getC12()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC12()).trim(),
								s_F04 = PortalUtility.getString(freeformList[j].getC13()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC13()).trim(),
								s_F05 = PortalUtility.getString(freeformList[j].getC14()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC14()).trim();
						
						s_F01 = s_F01.contains(".") ? s_F01 : s_F01+".00";
						s_F02 = s_F02.contains(".") ? s_F02 : s_F02+".00";
						s_F03 = s_F03.contains(".") ? s_F03 : s_F03+".00";
						s_F04 = s_F04.contains(".") ? s_F04 : s_F04+".00";
						s_F05 = s_F05.contains(".") ? s_F05 : s_F05+".00";
						
						DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
						
						String s_formatted_F01 = decimalformatter.format(new BigDecimal(s_F01));
						String s_formatted_F02 = decimalformatter.format(new BigDecimal(s_F02));
						String s_formatted_F03 = decimalformatter.format(new BigDecimal(s_F03));
						String s_formatted_F04 = decimalformatter.format(new BigDecimal(s_F04));
						String s_formatted_F05 = decimalformatter.format(new BigDecimal(s_F05));
							
						/*Double F01 = s_F01.equals("") ? 0.0 : Double.parseDouble(freeformList[j].getC10()),
								F02 = s_F02.equals("") ? 0.0 : Double.parseDouble(freeformList[j].getC11()),
								F03 = s_F03.equals("") ? 0.0 : Double.parseDouble(freeformList[j].getC12()),
								F04 = s_F04.equals("") ? 0.0 : Double.parseDouble(freeformList[j].getC13()),
								F05 = s_F05.equals("") ? 0.0 : Double.parseDouble(freeformList[j].getC14());*/
						
						//it is changed due to if assessment is negetive it show in bracket in report by puja at 10-11-2017
						//params.put("F01"+"_" + i, PortalUtility.getDouble(freeformList[j].getF01())>0.00?PortalUtility.getFormattedDouble(freeformList[j].getF01(),floatRoundOffCount):"NIL");
						params.put("F01"+"_" + i,(!s_formatted_F01.equals("0")&&!s_formatted_F01.equals("0.00")) ? s_formatted_F01.indexOf("-")!=-1 ? "("+s_formatted_F01.replace("-", "")+")" : s_formatted_F01 : "NIL");
						//it is changed due to if total profit is negetive it show in bracket in report by puja at 10-11-2017
						params.put("F02"+"_" + i,(!s_formatted_F02.equals("0")&&!s_formatted_F02.equals("0.00")) ? s_formatted_F02.indexOf("-")!=-1 ? "("+s_formatted_F02.replace("-", "")+")" : s_formatted_F02 : "NIL");
						params.put("F03"+"_" + i,(!s_formatted_F03.equals("0")&&!s_formatted_F03.equals("0.00")) ? s_formatted_F03.indexOf("-")!=-1 ? "("+s_formatted_F03.replace("-", "")+")" : s_formatted_F03 : "NIL");
						params.put("F04"+"_" + i,(!s_formatted_F04.equals("0")&&!s_formatted_F04.equals("0.00")) ? s_formatted_F04.indexOf("-")!=-1 ? "("+s_formatted_F04.replace("-", "")+")" : s_formatted_F04 : "NIL");
						params.put("F05"+"_" + i,(!s_formatted_F05.equals("0")&&!s_formatted_F05.equals("0.00")) ? s_formatted_F05.indexOf("-")!=-1 ? "("+s_formatted_F05.replace("-", "")+")" : s_formatted_F05 : "NIL");
						//String temp = PortalUtility.getDouble(freeformList[j].getF03())>0.00?PortalUtility.getFormattedDouble(freeformList[j].getF03(),floatRoundOffCount):"NIL";
						//System.out.print(temp);
						params.put("C05"+"_" + i, PortalUtility.getStringFromObject(freeformList[j].getC05()));
						params.put("D01"+"_" + i, freeformList[j].getD01()!=null?PortalUtility.getDateByPattern(freeformList[j].getD01(),CUSTOM_DATE_FORMAT):"");
						params.put("C07"+"_" + i, PortalUtility.getStringFromObject(freeformList[j].getC07()));
						params.put("D02"+"_" + i, freeformList[j].getD02()!=null?PortalUtility.getDateByPattern(freeformList[j].getD02(),CUSTOM_DATE_FORMAT):"");
						//params.put("NATURE"+"_"+i,getAssessmentNatureCode(PortalUtility.getString(freeformList[j].getC08())));
						params.put("NATURE"+"_"+i,PortalUtility.getString(freeformList[j].getC08()));
						System.out.println(getAssessmentNatureCode(PortalUtility.getString(freeformList[j].getC08())));
					}else if(tccFolderType.equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
						//TODO:  Individual mapping for report
						String 	s_F01 = PortalUtility.getString(freeformList[j].getC02()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC02()).trim(),
								s_F02 = PortalUtility.getString(freeformList[j].getC03()).trim().equals("") ? "0" : PortalUtility.getString(freeformList[j].getC03()).trim();
						
						s_F01 = s_F01.contains(".") ? s_F01 : s_F01+".00";
						s_F02 = s_F02.contains(".") ? s_F02 : s_F02+".00";
						
						DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
						String s_formatted_F01 = decimalformatter.format(new BigDecimal(s_F01));
						String s_formatted_F02 = decimalformatter.format(new BigDecimal(s_F02));
					
						params.put("N01"+"_" + i, PortalUtility.getStringFromObject(freeformList[j].getN01()));
						params.put("F01"+"_" + i, (!s_formatted_F01.equals("0")&&!s_formatted_F01.equals("0.00")) ? s_formatted_F01 : "NILL");
						params.put("F02"+"_" + i, (!s_formatted_F02.equals("0")&&!s_formatted_F02.equals("0.00")) ? s_formatted_F02 : "NILL");		
						params.put("C01"+"_" + i, PortalUtility.getStringFromObject(freeformList[j].getC01()));
						params.put("C02"+"_" + i, PortalUtility.getStringFromObject(freeformList[j].getC02()));
						params.put("D01"+"_" + i, freeformList[j].getD01()!=null?PortalUtility.getDateByPattern(freeformList[j].getD01(),CUSTOM_DATE_FORMAT):"");
					}
				}
			}
			
	}
			
	//System.out.println("Coming before download "+reportFileName+" "+ReportService.PDF_REPORT_TYPE+" "+params);
			byte[] reportByteContent = ReportService.getReportContent(params, "dataSource", ReportService.PDF_REPORT_TYPE, reportFileName);
			//System.out.println("after BYTE CONTENT download try"+ reportByteContent);
			response.setContentType("application/pdf");			
			response.setHeader("Content-Length", String.valueOf((reportByteContent.length)));
			response.setHeader("Content-Disposition", "attachment;filename=\"TCC_Report.pdf\"");
			BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
			//System.out.println("Coming after download");
			try{
				//System.out.println("Coming after download try");
				bos.write(reportByteContent);
				//System.out.println("Coming after download try end");
			}catch(Exception e){
				throw e;
			}finally{
				if(bos!=null){
					bos.flush();
					bos.close();
				}
				if(fis!=null){
					fis.close();
					log.info("File has been deleted at "+pageName);
				}
				if(qrFile!=null){
					log.info("File delete status at: "+pageName+" : "+qrFile.delete());
				}
				response.getOutputStream().flush();
				response.getOutputStream().close();
			}
		}else{ 
			/*Scenario where folder was not found or status code did not match*/
			if(tccFolder!=null && (tccFolder.getStatusCode()==3 || tccFolder.getStatusCode()==4)){
				errorResponseMessage="The TCC has not been issued yet. Kindly contact FIRS officials";
			}
			out.println("<h4 style='padding:30px; text-align:center;'>"+errorResponseMessage+"</h4>");
		}
	}catch(Exception e){
		e.printStackTrace();
		out.println("<h4 style='padding:30px; text-align:center;'>An error has occurred.<br/>The provided search criteria did not fetch any results from Amanda.<br/>Kindly contact an administrator.</h4>");
	}
%>