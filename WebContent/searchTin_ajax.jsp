<%@page import="com.softworks.tinvalidation.ws.HttpRestfulTinValidationService"%>
<%@page import="com.softworks.tinvalidation.ws.RestfulTinValidationByApache"%>
<%@page import="com.softworks.tinvalidation.ws.RestfulTinValidationService"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.softworks.tinvalidation.ws.ResponseTinValidation"%>
<%@page import="com.csdcsystems.amanda.client.TINWebServiceClient"%>
<%@page import="com.softworks.tinvalidation.ws.Taxpayer"%>
<%@include file="include/includeconstant.jsp"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<% 
	JSONObject tinData = new JSONObject();
	JSONObject existingPeople = new JSONObject();
	
	String TinNumber = PortalUtility.getString(request.getParameter("tinNumber"));
	String searchMethod = PortalUtility.getString(request.getParameter("searchMethod"));
	Taxpayer tp=null;
	boolean exceptionOccured = false;
	
	TINWebServiceClient client=null;
	ResponseTinValidation TINRes=null;

	/*
	WsPeople people =null;
	WsPeopleInfo[] peopleInfo = null;
	people = PortalService.getPeopleByTIN(TinNumber);
	peopleInfo = PortalService.getWSClient().getPeopleInfo(lid,PortalUtility.getInt(people.getPeopleRSN()));
	
	if(!PortalUtility.isNull(people)){
		existingPeople.put("name-of-people","");
		existingPeople.put("email-of-people","");
		existingPeople.put("phone-of-people","");
	}
	*/
	/*WsFolder existingFolder=PortalService.getREGDFolderByTIN(TinNumber);
		if(existingFolder!=null){
			tinData.put("Success","Retrieved");
			if(existingFolder.getStatusCode()==1){
				tinData.put("Message","You Application Registration With File No."+existingFolder.getFolderRSN()+" Is Under Review.");
			}else if(existingFolder.getStatusCode()==3){
				tinData.put("Message","You Application Is Already Registered With File No."+existingFolder.getFolderRSN()+".");
			}else if(existingFolder.getStatusCode()==6){
				tinData.put("Message","You Registration Application Is Rejected.Kindly Contact Your Tax Office.");
			}else if(existingFolder.getStatusCode()==8){
				tinData.put("Message","You Registration Application Is Already Registered And Is In-Progress.Kindly Contact Your Tax Office.");
			}
		}else{
		*/
		if(searchMethod.equalsIgnoreCase("searchBySOAP")){
			try{
				client=new TINWebServiceClient("http://196.6.103.56:8080/FirsGeneralTinValidation/TinValidationService/TinValidationService?wsdl");
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			
			try{
				TINRes=client.validateTIN(TinNumber);
				tp=TINRes.getTaxPayer();
				if(tp!=null){
					tinData.put("Success", true);
					tinData.put("CompanyName", tp.getTaxpayerName());
					tinData.put("TaxOfficeName", tp.getTaxOfficeName());
					tinData.put("TaxOfficeId", tp.getTaxOfficeId());
					tinData.put("PhoneNumber", tp.getContactNumber());
					tinData.put("EmailId", tp.getEmail());
					tinData.put("RCNo",tp.getRCNumber());
					tinData.put("JTBTIN",tp.getJTBTIN());
					tinData.put("Address",tp.getTaxPayerAddress());
					tinData.put("TIN",tp.getFIRSTIN());
					tinData.put("TaxpayerType",tp.getTaxpayerType());
				}else{
					tinData.put("Success",false);
				}
			}catch(Exception e){
				System.out.println(e.getMessage());
				tinData.put("Success",false);
				tinData.put("exceptionDetails",e.getMessage());
			}
		}else if(searchMethod.equalsIgnoreCase("searchByREST")){
			try{
				/* RestfulTinValidationByApache.doTrustToCertificates(); */
				/* TIN validation by APACHE HTTP Client */
				//tp = RestfulTinValidationByApache.getTaxPayerFromTINDB(TinNumber);
				/* TIN validation by http simple validation */
				tp = HttpRestfulTinValidationService.getTaxPayerFromTINDB(TinNumber);
				
				if(!PortalUtility.isNull(tp) && !PortalUtility.getString(tp.getFIRSTIN()).isEmpty()){
					tinData.put("Success", true);
					tinData.put("isTINFound",true);
					tinData.put("CompanyName", tp.getTaxpayerName());
					tinData.put("TaxOfficeName", tp.getTaxOfficeName());
					tinData.put("TaxOfficeId", tp.getTaxOfficeId());
					tinData.put("PhoneNumber", tp.getContactNumber());
					tinData.put("EmailId", tp.getEmail());
					tinData.put("RCNo",tp.getRCNumber());
					tinData.put("JTBTIN",tp.getJTBTIN());
					tinData.put("Address",tp.getTaxPayerAddress());
					tinData.put("TIN",tp.getFIRSTIN());
					tinData.put("TaxpayerType",tp.getTaxpayerType());
				}else{
					tinData.put("Success",false);
					tinData.put("isTINFound",false);
				}
			}catch(Exception e){
				exceptionOccured = true;
				e.printStackTrace();
				tinData.put("Success",false);
				tinData.put("exceptionDetails",e.getMessage());
			}
		}
		
	/*}*/
	tinData.put("exceptionOccured",exceptionOccured);
	out.println(tinData.toString());
%>
