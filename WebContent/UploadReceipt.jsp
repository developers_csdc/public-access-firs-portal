<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include/includeconstant.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<title>WELCOME TO FIRS - WHT DATA UPLOAD PORTAL</title>
	<link rel="stylesheet" href="<%=CSS_PORTAL%>back-office-login.css">
	<%@include file="include/common/includeCss.jsp"%>
	<script>
		function startTime() {
		    var today = new Date();
		    var h = today.getHours();
		    var m = today.getMinutes();
		    var s = today.getSeconds();
		    m = checkTime(m);
		    s = checkTime(s);
		    document.getElementById('timer').innerHTML =
		    h + ":" + m + ":" + s;
		    var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
		    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		    return i;
		}
	</script>
</head>
<body onload="startTime()">
	<div class="container-fluid">
		<div class="row" style="background-color:rgba(85, 85, 85, 0.14);">
			<div class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 text-center">
				<a class="header-logo" href="#"></a>
			</div>
			<div class="col-lg-4 col-md-4 pull-left" style="margin-top:10px;">
				<p style="margin-top: 25px;">
					<span class="label label-default" style="font-size:16px;border-radius:0px;">WELCOME TO FIRS - WHT DATA UPLOAD PORTAL</span>
				</p>	
			</div>
			<div class="col-lg-4 col-md-4 pull-right" style="margin-top:37px;">
				<p>
					<span style="font-family: sans-serif;font-size: 12px;font-weight: 600;color: #777;"><i class="fa fa-clock-o" aria-hidden="true"></i>  </span>
					<span id="timer" style="font-family: monospace;font-size: 14px;font-weight: 600;"></span>
				</p>
			</div>
		</div>
	</div>
	
	<div class="module form-module container-fluid" style="margin-top: 50px;max-width:350px;width:312px;">
		<div class="form">
				<p>Login to your account</p>
				<p style="margin:5px 5px 10px 5px">Please enter your ID and Password</p>
				<form>
	  				<input type="text" placeholder="Username" id="username"/>
	  				<input type="password" placeholder="Password" id="password"/>
	  				<button style="width: 100%;" id="do-login">Login <i class="fa fa-key" aria-hidden="true"></i></button>
				</form>
		</div>
	</div>
	
	<footer>
	    <div class="container-fluid">
	      <p class="text-muted text-center" style="font-size:12px;">� 2017 Federal Inland Revenue Service.All Right Reserved</p>
	    </div>
	</footer>
	<!-- SCRIPTS -->
	<script src="<%=JS_API%>jquery-3.1.1.min.js"></script>
	<script src="<%=JS_API%>bootstrap.min.js"></script>
	<script src="<%=JS_API%>jquery.growl.js"></script>
	<script src="<%=JS_PORTAL%>notification.js"></script>
	<script src="<%=JS_PORTAL%>uploadReceipt.js"></script>
</body>
</html>
