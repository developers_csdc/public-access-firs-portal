<%@page import="com.org.firs.*"%>
<%@page import="org.apache.commons.io.FilenameUtils"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="org.apache.commons.fileupload.disk.*"%>
<%@ page import="org.apache.commons.fileupload.servlet.*"%>
<%@ page import="org.apache.commons.io.output.*"%>
<%@ page import="org.apache.commons.io.FilenameUtils.*"%>

<%
	System.out.println("Start tracking file here...");
	String UPLOAD_DIRECTORY = "C:/uploads";
	boolean isFileAttachedFlag = false;

	int parentFolderRsn = 0;
	String peopleTaxOffice = "";
	String peopleTaxOfficeCode = "";

	final int TCC_SUPPORTING_DOCUMENT_CODE = 10;
	final int TAX_OFFICE_INFOCODE = 5090;
	final int CORPORATE_PEOPLECODE = 3;
	final int INDIVIDUAL_PEOPLECODE = 4;

	int folderRSN = 0;
	String tin = request.getParameter("tin");

	System.out.println("Ajax tin " + tin);

	String userId = "";
	String peopleRSN = "";
	int attSEQ = 0;
	String filename = "";
	String filename_02 = "";
	String contentType = "";
	long fileSize = 0;
	String fileFormat = "";
	byte[] blob = null;
	String nob = request.getParameter("trade_nature");
	String amt_16 = request.getParameter("turnover_year_1");
	String amt_17 = request.getParameter("turnover_year_2");
	String amt_18 = request.getParameter("turnover_year_3");
	String tcn = request.getParameter("turnover_company");
	String dpt = request.getParameter("tcc_date");
	String assessment = request.getParameter("amount_check");
	String out_01 = request.getParameter("year");
	String out_02 = request.getParameter("amount");
	String liquidate = request.getParameter("amount_check");
	String relevantInfo = request.getParameter("other_doc");
	String taxOffice = "";
	String mailCheck = request.getParameter("amount_check");
	String address = request.getParameter("tccUpdateAddress");
	String mail = request.getParameter("email");
	String phone = request.getParameter("phone_no");
	String file = request.getParameter("support_copy");
	String name = "";
	String sysDate = "";
	String applicationType = "", customFolderNumber = "";
	String assignUSER = "";

	System.out.println(nob);
	System.out.println(amt_16);
	System.out.println(amt_17);
	System.out.println(amt_18);

	PreparedStatement pst = null;
	PreparedStatement pst_04 = null;
	PreparedStatement pst_03 = null;
	PreparedStatement pst_05 = null;
	PreparedStatement pst_06 = null;
	PreparedStatement pst_07 = null;
	PreparedStatement pst_08 = null;
	PreparedStatement pst_09 = null;
	PreparedStatement pst_10 = null;
	PreparedStatement pst_11 = null;
	PreparedStatement pst_12 = null;
	PreparedStatement pst_13 = null;
	PreparedStatement pst_14 = null;
	PreparedStatement pst_15 = null;
	PreparedStatement pst_16 = null;
	PreparedStatement pst_17 = null;
	PreparedStatement pst_18 = null;
	PreparedStatement psql_003 = null;
	JSONObject submissionData = new JSONObject();

	DBConnection connection = new DBConnection();
	Connection conn;
	Statement createStatement;

	if (ServletFileUpload.isMultipartContent(request)) {

		List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
		for (FileItem item : multiparts) {
			isFileAttachedFlag = true;
			if (!item.isFormField()) {
				Map<String, Object> attachmentItem = new HashMap<String, Object>();
				filename = new File(item.getName()).getName();
				filename_02 = FilenameUtils.removeExtension(filename);
				fileFormat = FilenameUtils.getExtension(filename);
				contentType = item.getContentType();
				fileSize = item.getSize();
				blob = item.get();
				System.out.println(blob);
				item.write(new File(UPLOAD_DIRECTORY + File.separator + filename));
				System.out.println(filename_02);
				System.out.println(fileFormat);
			} else {
				if ("trade_nature".equals(item.getFieldName())) {
					nob = new String(item.get());
					System.out.println(nob);
				} else if ("turnover_year_1".equals(item.getFieldName())) {
					amt_16 = new String(item.get());
					System.out.println(amt_16);
				} else if ("turnover_year_2".equals(item.getFieldName())) {
					amt_17 = new String(item.get());
					System.out.println(amt_17);
				} else if ("turnover_year_3".equals(item.getFieldName())) {
					amt_18 = new String(item.get());
					System.out.println(amt_18);
				} else if ("pre_year_check".equals(item.getFieldName())) {
					assessment = new String(item.get());
					System.out.println(assessment);
				} else if ("year".equals(item.getFieldName())) {
					out_01 = new String(item.get());
					System.out.println(out_01);
				} else if ("amount".equals(item.getFieldName())) {
					out_02 = new String(item.get());
					System.out.println(out_02);
				} else if ("amount_check".equals(item.getFieldName())) {
					liquidate = new String(item.get());
					System.out.println(liquidate);
				} else if ("other_doc".equals(item.getFieldName())) {
					relevantInfo = new String(item.get());
					System.out.println(relevantInfo);
				} else if ("phone_no".equals(item.getFieldName())) {
					phone = new String(item.get());
					System.out.println(phone);
				} else if ("email".equals(item.getFieldName())) {
					mail = new String(item.get());
					System.out.println(mail);
				}
			}
		}
	}

	try {

		conn = connection.getConnection();

		String counter = "select * from folder where referencefile2 = '" + tin
				+ "' and statuscode in (1,2,3,8) and folderYear=19 and folderType='TCC'";
		Statement counter_st = conn.createStatement();
		ResultSet counter_01 = counter_st.executeQuery(counter);

		if (counter_01.next() != false) {
			System.out.println("User already have an active application");

			submissionData.put("Error", "Sorry You Still have an active application");
			submissionData.put("Success", false);
			//submissionData.put("applicationNumber", folderRSN);

		} else {

			//date
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			sysDate = dateFormat.format(date);
			System.out.println(sysDate); //2016/11/16 12:08:4

			String psql = "SELECT NEXT VALUE FOR FOLDERSEQ";
			createStatement = conn.createStatement();
			ResultSet prs = createStatement.executeQuery(psql);

			while (prs.next()) {
				folderRSN = prs.getInt(1);
				System.out.println("Folder RSN " + folderRSN);
			}

			String att = "SELECT NEXT VALUE FOR ATTACHMENTSEQ";
			Statement att_01 = conn.createStatement();
			ResultSet atSeq = att_01.executeQuery(att);

			while (atSeq.next()) {
				attSEQ = atSeq.getInt(1);
				System.out.println("Attachment SEQ " + attSEQ);
			}
			// System.out.println("This is PeopleRsn " + peopleRSN);

			String ppRsn = "select * from people where  licencenumber='" + tin + "'";
			Statement createStatement_pp = conn.createStatement();
			ResultSet pprs_01 = createStatement_pp.executeQuery(ppRsn);

			while (pprs_01.next()) {
				peopleRSN = pprs_01.getString("peoplersn");
				System.out.println("PeopleRSN " + peopleRSN);

				tin = pprs_01.getString("licencenumber");
				int peopleCode = pprs_01.getInt("peopleCode");
				System.out.println("TIN " + tin);
				System.out.println("People Code " + peopleCode);

				if (peopleCode == 4) {
					applicationType = "ITCC";
				}

				if (peopleCode == 3) {
					applicationType = "TCC";
				} else {
					applicationType = "TCC";
				}
			}

			System.out.println("Application Type " + applicationType);
			customFolderNumber = "2019 " + folderRSN + " 000 00 " + applicationType;
			System.out.println("Customer Folder NO " + customFolderNumber);

			String taxOff = "select * from people where peoplersn='" + peopleRSN + "'";
			Statement createStatement_tx = conn.createStatement();
			ResultSet tx_01 = createStatement_tx.executeQuery(taxOff);

			String orgName = "";

			while (tx_01.next()) {
				taxOffice = tx_01.getString("community");

				if (taxOffice == null) {
					taxOffice = ApiFactory.getTaxOfficeID(tin);
					System.out.println("This is tax office Id   === " + taxOffice);
				}

				orgName = tx_01.getString("organizationName");
				System.out.println("Tax Office " + taxOffice);
			}

			String assign = "select assignuser from defaultareaassignment where areacode='" + taxOffice + "'";
			Statement createStatement_asign = conn.createStatement();
			ResultSet as = createStatement_asign.executeQuery(assign);

			while (as.next()) {
				assignUSER = as.getString("assignuser");
				System.out.println("Tax Office from default Area assignment  " + assignUSER);
			}

			/*String taxOff = "select community from people where peoplersn='" + peopleRSN + "'";
			Statement createStatement_tx = conn.createStatement();
			ResultSet tx_01 = createStatement_tx.executeQuery(taxOff);
			
			while (tx_01.next()) {
				taxOffice = tx_01.getString("community");
				System.out.println("Tax Office " + taxOffice);
			} */

			/*String psql_02 = "SELECT  TOP 5 VTU.UserId FROM ValidTeam VT,VALIDTEAMUSER VTU,ValidUSER VU \r\n"
					+ "WHERE VT.TeamCode = VTU.TeamCode AND VT.TEAMCODE IN\r\n"
					+ "(SELECT TEAMCODE FROM VALIDTEAM WHERE TEAMDESC='" + taxOffice + "') AND \r\n"
					+ "VTU.USERID=VU.USERID AND VU.DepartmentCode=5 ORDER BY NEWID();";
			Statement createStatement_02 = conn.createStatement();
			ResultSet prs_02 = createStatement_02.executeQuery(psql_02);
			
			while (prs_02.next()) {
				userId = prs_02.getString("UserId");
				System.out.println("USERID " + userId);
			} */

			// one
			String ww = "insert into Folder(FolderRSN,FolderYear,FolderSequence, FolderSection,FolderRevision,FolderType,PropertyRSN, InDate,CopyFlag, StampDate,StampUser,StatusCode, FolderName, FolderCentury,SecurityCode, ReferenceFile2,CustomFolderNumber,PeopleRSN) values('"
					+ folderRSN + "','19','" + folderRSN + "','000','00','TCC','0','" + sysDate + "','DDDDD','"
					+ sysDate + "','FIRS','1','" + orgName + "', '20','0','" + tin + "','" + customFolderNumber
					+ "','" + peopleRSN + "')";
			pst = conn.prepareStatement(ww);
			pst.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 1 Worked......!");

			String ww_03 = "UPDATE folderinfo set infovalue = '" + nob + "', infovalueupper='" + nob
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5035'";
			pst_03 = conn.prepareStatement(ww_03);
			pst_03.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 2 Worked......!");

			String ww_04 = "UPDATE folderinfo set infovalue = '" + amt_16 + "', infovalueupper='" + amt_16
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5040'";
			pst_04 = conn.prepareStatement(ww_04);
			pst_04.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 3 Worked......!");

			String ww_05 = "UPDATE folderinfo set infovalue = '" + amt_17 + "', infovalueupper='" + amt_17
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5041'";
			pst_05 = conn.prepareStatement(ww_05);
			pst_05.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 4 Worked.....!");

			String ww_06 = "UPDATE folderinfo set infovalue = '" + amt_18 + "', infovalueupper='" + amt_18
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5042'";
			pst_06 = conn.prepareStatement(ww_06);
			pst_06.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 5 Worked......!");

			String ww_07 = "UPDATE folderinfo set infovalue = '" + tcn + "', infovalueupper='" + tcn
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5045'";
			pst_07 = conn.prepareStatement(ww_07);
			pst_07.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 6 Worked......!");

			String ww_08 = "UPDATE folderinfo set infovalue = '" + dpt + "', infovalueupper='" + dpt
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5050'";
			pst_08 = conn.prepareStatement(ww_08);
			pst_08.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 7 Worked.....!");

			String ww_09 = "UPDATE folderinfo set infovalue = '" + assessment + "', infovalueupper='"
					+ assessment + "' where folderrsn = '" + folderRSN + "' and infocode = '5055'";
			pst_09 = conn.prepareStatement(ww_09);
			pst_09.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 8 Worked......!");

			String ww_10 = "UPDATE folderinfo set infovalue = '" + out_01 + "', infovalueupper='" + out_01
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5065'";
			pst_10 = conn.prepareStatement(ww_10);
			pst_10.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 9 Worked......!");

			String ww_11 = "UPDATE folderinfo set infovalue = '" + out_02 + "', infovalueupper='" + out_02
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5060'";
			pst_11 = conn.prepareStatement(ww_11);
			pst_11.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 10 Worked......!");

			String ww_12 = "UPDATE folderinfo set infovalue = '" + liquidate + "', infovalueupper='" + liquidate
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5070'";
			pst_12 = conn.prepareStatement(ww_12);
			pst_12.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 11 Worked......!");

			String ww_13 = "UPDATE folderinfo set infovalue = '" + relevantInfo + "', infovalueupper='"
					+ relevantInfo + "' where folderrsn = '" + folderRSN + "' and infocode = '5080'";
			pst_13 = conn.prepareStatement(ww_13);
			pst_13.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 12 Worked......!");

			String ww_14 = "UPDATE folderinfo set infovalue = '" + taxOffice + "', infovalueupper='" + taxOffice
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5090'";
			pst_14 = conn.prepareStatement(ww_14);
			pst_14.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 13 Worked......!");

			String ww_15 = "UPDATE folderinfo set infovalue = '" + mailCheck + "', infovalueupper='" + mailCheck
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '1020'";
			pst_15 = conn.prepareStatement(ww_15);
			pst_15.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 14 Worked......!");

			String ww_16 = "UPDATE folderinfo set infovalue = '" + address + "', infovalueupper='" + address
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5005'";
			pst_16 = conn.prepareStatement(ww_16);
			pst_16.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 15 Worked.....!");

			String ww_17 = "UPDATE folderinfo set infovalue = '" + mail + "', infovalueupper='" + mail
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5025'";
			pst_17 = conn.prepareStatement(ww_17);
			pst_17.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 16 Worked......!");

			String ww_18 = "UPDATE folderinfo set infovalue = '" + phone + "', infovalueupper='" + phone
					+ "' where folderrsn = '" + folderRSN + "' and infocode = '5030'";
			pst_18 = conn.prepareStatement(ww_18);
			pst_18.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 17 Worked......!");

			System.out.println("This is User Id ====" + userId);

			String updateFolderProcess = "update folderprocess set assigneduser = '" + assignUSER
					+ "' where folderrsn='" + folderRSN + "'";
			PreparedStatement ps = conn.prepareStatement(updateFolderProcess);
			ps.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment amanda upload Worked......!");

			String psql_03 = "insert into attachment (attachmentrsn,tablename,tablersn,attachmentfilesuffix,attachmentfilealias,stampdate,stampuser,dospath,currentversionflag,olddospath,attachmentcode,attachmentcontenttype,checkoutflag,confidentialflag)values('"
					+ attSEQ + "','Folder','" + folderRSN + "','" + fileFormat + "','" + filename_02 + "','"
					+ sysDate + "','" + peopleRSN + "','E:\\FIRS\\attachments\\" + filename
					+ "','Y','\\opt\\tomcat\\ng_attachments\\" + filename + "','10','" + contentType
					+ "','N','N')";
			psql_003 = conn.prepareStatement(psql_03);
			psql_003.executeUpdate();
			conn.setAutoCommit(true);
			System.out.println("Statment 00 Worked......!");
			submissionData.put("Success", true);
			submissionData.put("applicationNumber", folderRSN);

		}

	} catch (Exception e) {
		e.printStackTrace();
	}

	out.print(submissionData.toString());
%>