<%@ page language="java" buffer="8kb" import="java.util.*,com.csdcsystems.amanda.common.*" autoFlush="true" isThreadSafe="true" isErrorPage="true" session="false"  %>
<% 
/*
 * This page handles one kind of errors
 * 1) the new CsdcValidator sendError style (new = Jan 2012)
 *
 * errorMessageHuman and errorTitleHuman are always safe to display
 *
 */
 
Integer status_code      = null; //set by the new CsdcValidator style response.sendError()
String errorTitleHuman   = null;
String errorMessageHuman = null; //set by the new CsdcValidator style response.sendError()
String jspPageName       = null; //set by the new CsdcValidator style response.sendError()
Exception ex             = null; //set by the new web.xml <error-code> style 
String IpAddress         = null;

// load the various objects
if (request.getAttribute("javax.servlet.error.message")     instanceof String) errorMessageHuman = (String)request.getAttribute("javax.servlet.error.message");
if (request.getAttribute("javax.servlet.error.request_uri") instanceof String) jspPageName       = (String)request.getAttribute("javax.servlet.error.request_uri");
if (request.getAttribute("javax.servlet.error.status_code") instanceof Integer) status_code = (Integer)request.getAttribute("javax.servlet.error.status_code");
if (request.getAttribute("javax.servlet.error.exception") instanceof Exception) ex = (Exception)request.getAttribute("javax.servlet.error.exception");
if (request.getRemoteAddr() != null && request.getRemoteAddr().trim().length() > 0) IpAddress = request.getRemoteAddr();

/*
javax.servlet.error.status_code = 400
javax.servlet.error.exception_type = 400
javax.servlet.error.message = There was a problem processing your request. (EUID=15jzoiz)
javax.servlet.error.exception = null
javax.servlet.error.request_uri = /eNtraprise/Ohio/test/elevator_result.jsp
javax.servlet.error.servlet_name = jsp

javax.servlet.error.status_code     = < %= request.getAttribute("javax.servlet.error.status_code") % ><br>
javax.servlet.error.exception_type  = < %= request.getAttribute("javax.servlet.error.status_code") % ><br>
javax.servlet.error.message         = < %= request.getAttribute("javax.servlet.error.message") % ><br>
javax.servlet.error.exception       = < %= request.getAttribute("javax.servlet.error.exception") % ><br>
javax.servlet.error.request_uri     = < %= request.getAttribute("javax.servlet.error.request_uri") % ><br>
javax.servlet.error.servlet_name    = < %= request.getAttribute("javax.servlet.error.servlet_name") % ><br>
*/

if (errorTitleHuman   == null || errorTitleHuman.length()   < 1) errorTitleHuman   = "Error";
if (errorMessageHuman == null || errorMessageHuman.length() < 1) errorMessageHuman = "Sorry for the inconvenience, please contact us.";

String ErrorStatusCode = "";
if (status_code != null) ErrorStatusCode = "<b>HTTP Status Code:</b>&nbsp;&nbsp;&nbsp;" + status_code;

/*
 * Establish jspPageName and log any error message not previously logged
 */

if (jspPageName == null || jspPageName.length() < 2) {
    jspPageName = CsdcValidator.getJspPageNameWithPath(request);
}
if (ex != null) {
    CsdcValidatorData data = new CsdcValidatorData();
    data.setIpAddress(IpAddress);
    data.setJspPageName(jspPageName);
    data.setJspPageNameWithPath(jspPageName);
    CsdcValidatorData_row row = data.addRow("errorpage", "", false);
    data.setError("There was a problem processing your request", "errorpage", ex.toString());
    errorMessageHuman = data.getErrorMessageHuman("errorpage"); //adds the EUID
    errorTitleHuman = "Error";
    ex = null;
}

if (status_code != null && status_code > 0) {
    if (errorMessageHuman == null || errorMessageHuman.length() < 1) {
        errorTitleHuman = "Error";
        errorMessageHuman = "There was a problem processing your request";
    }
}


  //end of html
  // A few things you can try
  // 
  //   Pressing the OK button will take you back to the prior page to try again.
  /*
  <H3>A few things you can try:</H3>
  <UL>
    <LI>Use your browser's back button to go the previous page you were 
    viewing</LI>
    <LI>                        If you accessed this page using a bookmark/favorite, we may have 
    moved the page.                                                 Try to access the page by                                                  
                                searching                                                            <EM>Fool.com</EM>                                                         using the 
    search box at the top-right.                                                                         If the page has moved, reset 
    your bookmark/favorite.                    </LI>
 */
  
%>
<html>
  <head>
  <title>Online Services</title>
  <style type="text/css"><!--
   body {background-image: url(/error/bg_repeat.jpg);background-position: top;background-attachment: scroll;background-repeat: repeat-x;font-family:Tahoma,Arial,sans-serif;color:black;text-align: center;}
   H1 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:22px;}
   B {font-family:Tahoma,Arial,sans-serif;color:white;}
   P {font-family:Tahoma,Arial,sans-serif;color:white;font-size:14px; text-align: right;}
   HR {color : #525D76;}
   #MasterContainer {width: 952px;height: auto;position: relative;margin: 0 auto 0 auto;padding: 0px;border: 0px;float: none; clear: both;}
   H3 {font-family:Tahoma,Arial,sans-serif;color:black;font-size:20px;text-align: left;}
   H2.error {font-weight:bold;margin-top:1.2ex;margin-bottom:11px;margin-left:0;color: #aa0c00;text-align: left;}
  --></style>
 </head>
 <body>
  <DIV id="MasterContainer">
   <h1>Unfortunately, you have just experienced what we refer to as a "problem." </h1>
   <HR size="1" noshade="noshade" /><br>
   <H2 class="error"><%= errorTitleHuman %></H2>
   <H3><%= errorMessageHuman %></H3>
   <p><b><%= ErrorStatusCode %></p>
   <HR size="1" noshade="noshade" />
  </DIV>
  <INPUT  class="button" type="button" value="BACK" alt="BACK" onClick="history.back();">
 </body>
</html>