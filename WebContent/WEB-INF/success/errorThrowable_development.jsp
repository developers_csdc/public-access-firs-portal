<%@ page isErrorPage="true" import="java.io.*" contentType="text/plain"%><% 

/*
 * This is used while in development to return compile errors.
 * In production, set web.xml to errorpage.jsp instead
 */

if (exception == null) { %>
  No Error
<% return ; } %>

Message:
<%=exception.getMessage()%>

StackTrace:
<%
	StringWriter stringWriter = new StringWriter();
	PrintWriter printWriter = new PrintWriter(stringWriter);
	exception.printStackTrace(printWriter);
	out.println(stringWriter);
	printWriter.close();
	stringWriter.close();
%>

