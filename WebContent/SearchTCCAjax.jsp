<%@page import="java.math.BigDecimal"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderFreeform"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@include file="include/includeconstant.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%
	log.info("Entering"+ pageName);
	boolean dataFetched=true;
	int tccNumber=-1;
	WsFolder tccfolder=null;
	String tccstatus=null;
	String tccIssueDate=null;
	String tccExpiry=null;
	String tccApplicantName=null;
	String tccApplicantTIN=null;
	int floatRoundOffTo =2;
	final String INDIVIDUAL_FOLDER_TYPE = "ITCC";
	final String CORPORATE_FOLDER_TYPE 	= "TCC";
	final String FOREX_FOLDER_TYPE 		= "FTCC";
	
	JSONObject tccDetails = new JSONObject();
	tccDetails.put("DataFetched", dataFetched);
	String tccnumber=request.getParameter("tcc_number");
	SimpleDateFormat formatter=new SimpleDateFormat("DD-MMM-yyyy"); 
	StringBuilder tccApplicantNameBuilder = new StringBuilder();
	WsPeople associatedPeople = null;
	WsFolderFreeform[] freeformList = null;
	
	JSONArray freeformInfo = new JSONArray();
	
	try{
		tccfolder=PortalService.getFolderByTCC(String.valueOf(tccnumber));
			if(!PortalUtility.isNull(tccfolder)){
				String tcccheck=tccfolder.getReferenceFile();
				int folderrsn = tccfolder.getFolderRSN();
				tccstatus=tccfolder.getStatusDesc();
				//tccIssueDate=PortalUtility.getDate(tccfolder.getIndate());
				tccIssueDate=PortalUtility.getDate(tccfolder.getIssueDate());
				tccExpiry=PortalUtility.getDate(tccfolder.getExpiryDate());
				//tccApplicantName = PortalService.getFolderInfoByFolderRSNInMap(folderrsn).get(5000);
				tccApplicantName=PortalService.getPeopleByFolder(folderrsn).getOrganizationName();
				tccApplicantTIN=PortalService.getPeopleByFolder(folderrsn).getLicenceNumber();
				if(!PortalUtility.isNull(tccfolder)){
					if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
						freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(folderrsn,3,new int[]{2});
					}else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
						freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(folderrsn,3,new int[]{1});
					}else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(FOREX_FOLDER_TYPE)){
						freeformList = PortalService.getFolderFreeFormByFolderRSNCodeLimit(folderrsn,1,new int[]{3});
					}
				}
				
				if(freeformList !=null && freeformList.length>0){
					if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(CORPORATE_FOLDER_TYPE)){
						tccDetails.put("applicantType", "corporate");
						for(WsFolderFreeform freeform : freeformList){
							String 	s_turnover 			= PortalUtility.getString(freeform.getC14()).trim().equals("") ? "0" : PortalUtility.getString(freeform.getC14()).trim(),
									s_assessableProfit 	= PortalUtility.getString(freeform.getC10()).trim().equals("") ? "0" : PortalUtility.getString(freeform.getC10()).trim(),
									s_taxPaid 			= PortalUtility.getString(freeform.getC12()).trim().equals("") ? "0" : PortalUtility.getString(freeform.getC12()).trim();
							
							/* Double 	turnover 			= s_turnover.equals("") ? 0.0 : Double.parseDouble(freeform.getC14()),
										assessableProfit 	= s_assessableProfit.equals("") ? 0.0 : Double.parseDouble(freeform.getC10()),
										taxPaid 			= s_taxPaid.equals("") ? 0.0 : Double.parseDouble(freeform.getC12()); */
							
							s_turnover 			= s_turnover.contains(".") ? s_turnover : s_turnover+".00";
							s_assessableProfit 	= s_assessableProfit.contains(".") ? s_assessableProfit : s_assessableProfit+".00";
							s_taxPaid 			= s_taxPaid.contains(".") ? s_taxPaid : s_taxPaid+".00";
							
							DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
							
							JSONObject freeformObj = new JSONObject();
							freeformObj.put("year",freeform.getN01());
							freeformObj.put("turnover",decimalformatter.format(new BigDecimal(s_turnover)));
							freeformObj.put("assessableProfit",decimalformatter.format(new BigDecimal(s_assessableProfit)));
							freeformObj.put("taxPaid",decimalformatter.format(new BigDecimal(s_taxPaid)));
							freeformInfo.put(freeformObj);
						}	
					 }else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(INDIVIDUAL_FOLDER_TYPE)){
						 tccDetails.put("applicantType", "individual");
						for(WsFolderFreeform freeform : freeformList){
							String s_totalIncome 	= PortalUtility.getString(freeform.getC02()).trim();
							String s_taxPaid 		= PortalUtility.getString(freeform.getC03()).trim();
							
							s_totalIncome 	= s_totalIncome.contains(".") ? s_totalIncome : s_totalIncome+".00";
							s_taxPaid 		= s_taxPaid.contains(".") ? s_taxPaid : s_taxPaid+".00";
							
							DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
							
							JSONObject freeformObj = new JSONObject();
							freeformObj.put("year",freeform.getN01());
							freeformObj.put("totalIncome",decimalformatter.format(new BigDecimal(s_totalIncome)));
							freeformObj.put("taxPaid",decimalformatter.format(new BigDecimal(s_taxPaid)));
							freeformInfo.put(freeformObj);
						}
					}else if(PortalUtility.getString(tccfolder.getFolderType()).equalsIgnoreCase(FOREX_FOLDER_TYPE)){
						 tccDetails.put("applicantType", "forex");
						for(WsFolderFreeform freeform : freeformList){
							String s_F04 = PortalUtility.getString(freeform.getC08()).trim().equals("") ? "0" : PortalUtility.getString(freeform.getC08()).trim();
							s_F04 = s_F04.contains(".") ? s_F04 : s_F04+".00";
							
							DecimalFormat decimalformatter = new DecimalFormat("#,###.00");
							String s_formatted_F04 = decimalformatter.format(new BigDecimal(s_F04));
							
							JSONObject freeformObj = new JSONObject();
							freeformObj.put("remmitanceAmountApproved",(!s_formatted_F04.equals("0") && !s_formatted_F04.equals("0.00")) ? s_formatted_F04 : "NILL");
							freeformInfo.put(freeformObj);
						}
					}
					
				} 
				tccDetails.put("tccnumber", tccnumber);
				tccDetails.put("tccstatus", tccstatus);
				tccDetails.put("tccIssueDate", tccIssueDate);
				tccDetails.put("tccExpiry", tccExpiry);
				tccDetails.put("tccApplicantName", tccApplicantName);
				tccDetails.put("tccApplicantTIN",tccApplicantTIN);
				tccDetails.put("freeformInfo",freeformInfo);
				/* associatedPeople = PortalService.getPeopleByFolder(tccfolder.getFolderRSN());
				System.out.println(associatedPeople);
				if(associatedPeople!=null){
					tccApplicantNameBuilder.append(associatedPeople.getNameFirst());
					tccApplicantNameBuilder.append(" ");
					tccApplicantNameBuilder.append(associatedPeople.getNameMiddle()!=null?associatedPeople.getNameMiddle()+" ":"");
					tccApplicantNameBuilder.append(associatedPeople.getNameLast());
					tccApplicantName =  tccApplicantNameBuilder.toString();
				}  */
			}else{
				dataFetched=false;
				tccDetails.put("err-msg", "tccfolder not Found");
			}
		
	}catch(Exception ex){
		ex.printStackTrace();
		dataFetched=false;
	}
	tccDetails.put("DataFetched", dataFetched);
	out.print(tccDetails.toString());
%>