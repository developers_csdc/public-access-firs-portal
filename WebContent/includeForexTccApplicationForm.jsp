<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@include file="include/includeconstant.jsp"%>
<%!
	private final int TAX_OFFICE_INFOCODE = 5090;
	List<String> newAddressDropdown = new ArrayList<String>();%>

<% 
	int PeopleRSN = 0;
	
	WsPeople people = null;
	
	people 			= (WsPeople) session.getAttribute("people");
	
	if(!PortalUtility.isNull(people)){
		PeopleRSN = PortalUtility.getInt(people.getPeopleRSN());
	}
	
%>

<div class="col-xs-12 col-sm-9 content hide showHideDiv"
	id="ApplicationFormDiv">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a>Apply for TCC
			</h3>
		</div>
		<div class="panel-body">
			<div class="content-row">
				<div class="container-fluid">

					<div>
						<div class="container-fluid">
							<h4 align="center">Application for Tax Clearance Certificate</h4>
							<!-- <ol class="breadcrumb hidden-xs">
							    <li id="crumb1" class="active">Enter Registration Details</li>
							    <li id="crumb2">Complete Registration</a></li>
							  </ol> -->
						</div>

						<div class="container-fluid" id="tccapp_form">

								<div id="ForexProfileFormDiv">
								<div class="row hide" id="applyFormValidationAlert">
									<div class="col-xs-12">
										<br />
										<p class="alert alert-warning">
											<i class="glyphicon glyphicon-alert"></i>&nbsp;&nbsp;<span
												id="formPageAlert">Please fill the mandatory fields!</span>
										</p>
										<p class="well" id="missingMandatoryItemsOuter"
											style="padding: 5px 10px;">
											<span style="margin-right: 10px;">Missing Mandatory
												Items: </span><span id="missingInputsContainer"></span>
										</p>
									</div>
								</div>
								
								<div class="panel panel-info">
									<div class="panel-heading">A.Payer Details</div>
									<div class="panel-body">
										<div style="font-size: 17px">
										<input id="isPayerDifferent" type="checkbox"></input>
										&nbsp;Payer is different than applicant
									</div>
									<br/>
									
									<!-- Payer Name -->
									<div class="row custom_row isPayerDifferentDiv" style="display: none;">
										<div class="form-group">
											<label class="col-md-3 col-lg-3 custom_label"
												for="trade_nature">1. Payer Name</label>
											<div class="col-md-9 col-lg-9">
												<input type="text" class="form-control input-sm custom_input"
													aria-role="" id="PayerName"
													maxlength="150"  name="PayerName"
													class="biginput" pattern="alphabetsWithSpaces" data-required="required">
												<div class="error" id=""></div>
											</div>
										</div>
									</div>
	
									<!-- Payer Address -->
									<div class="row custom_row isPayerDifferentDiv" style="display: none;">
										<div class="form-group">
											<label class="col-md-3 col-lg-3 custom_label" for="turnover">2. Payer Address
											</label>
											<div class="col-md-9 col-lg-9">
												<input type="text" class="form-control input-sm custom_input"
													aria-role="" id="PayerAddress"
													pattern="text"
													onkeypress="" name="PayerAddress">
												<div class="error" id=""></div>
											</div>
										</div>
									</div>
									
									<!-- Business Relationship between PAYER and BENEFICIARY/RECIPIENT -->
									<div class="row custom_row">
										<div class="form-group">
											<label class="col-md-3 col-lg-3 custom_label" for="turnover">3. Business Relationship between PAYER and BENEFICIARY/RECIPIENT<sup class="text-danger">*</sup>
											</label>
											<div class="col-md-9 col-lg-9">
												<input type="text" class="form-control input-sm custom_input required"
													aria-role="" id="BusinessRelationship"
													pattern="alphabetsWithSpaces" data-required="required"
													onkeypress="" name="BusinessRelationship">
												<div class="error" id=""></div>
											</div>
										</div>
									</div>
									</div>
								</div>
								
								<div class="panel panel-info">
									<div class="panel-heading">B. 1.Particulars of Remittance</div>
									<div class="panel-body">
										<!-- Nature of Remittance -->
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label"
													for="trade_nature">4. Nature of Remittance<sup
													class="text-danger">*</sup></label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input required"
														aria-role="" id="NatureOfRemittance" 
														pattern="alphabetsWithSpaces" data-required="required" 
														name="NatureOfRemittance"
														class="biginput">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
		
										<!-- Details of Remittance -->
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover">5. Details of Remittance<sup class="text-danger">*</sup>
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input required"
														aria-role="" id="DetailsOfRemittance"  id="DetailsOfRemittance"
														pattern="text" data-required="required"
														onkeypress="">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<!-- Nature of service provided -->
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover">
													6. Nature of service provided (if payment is in respect to services)
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input"
														aria-role="Turnover of the company.." id="NatureOfService"
														pattern="alphabetsWithSpaces" name="NatureOfService"
														onkeypress="">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<!-- Provide more details -->
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover"> 
													7. Provide more details
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="FILE" class="form-control input-sm custom_input"
														aria-role="" id="MoreDetails" name="MoreDetails"
														pattern="file">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<!-- Amount (with respect to this application) -->
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover">
													8. Amount (with respect to this application)<sup class="text-danger">*</sup>
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input required" 
														id="Amount"
														pattern="numbersLg" data-required="required" name="Amount"
														onkeypress="return onlyNosAndDecimal(event,this)">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="panel panel-info">
									<div class="panel-heading">B. 2.Previous Forex Approval (if any)</div>
									<div class="panel-body">
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover">
													9. Previous TCC No.
												</label>
												<div class="col-md-4 col-lg-4">
													<input type="text" class="form-control input-sm custom_input"
														id="PrevTCCNo" name="PrevTCCNo" pattern="text" onkeypress="return onlyNos(event,this)">
													<div class="error" id=""></div>
												</div>
												<div class="col-md-1 col-lg-1">
													<button id="verifyTCCButton" type="button">Verify</button>
												</div>
												<div id="verifyTCCLoader"></div>
												<div class="col-md-4 col-lg-4">
													<label style="width: 15%" class="pull-left">
														Date
													</label>
													<input type="text" class="form-control input-sm custom_input pull-right" style="width: 85%"
														id="TCCDate" name="TCCDate" pattern="date" disabled="disabled">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="PrevForexDetail"> 
													10. Previous Forex Approval details
												</label>
												<div class="col-md-9 col-lg-9">
													<textarea id="PrevForexDetail" name="PrevForexDetail" rows="" cols="" pattern="textarea" class="form-control input-sm custom_input"></textarea>
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label"
													for="registration_type">11. Has this amount suffered Withholding Tax?<sup
													class="text-danger">*</sup></label>
												<div class="col-md-9 col-lg-9">
													<label class="col-md-3 col-lg-3 custom_label"
														for="registration_type_company"> 
														<input type="radio" value="Yes" class="required"
														aria-role="Registration Type"
														id="WithholdingTax" name="WithholdingTax">
														&nbsp &nbsp&nbsp Yes
													</label> <label class="col-md-3 col-lg-3 custom_label"
														for="registration_type_individual"> 
														<input type="radio" value="No" class="required" checked
														aria-role="Registration Type"
														id="WithholdingTax" name="WithholdingTax">
														&nbsp &nbsp&nbsp No
													</label>
													<div class="error" id=""></div>
													<div class="isWithholdingTax" style="display: none;">
														<br><br>
														<label style="width: 40%" class="custom_label">Tax Payment Evidence </label>
														<input type="file" id="PaymentEvidence" name="PaymentEvidence" pattern="file" class="form-control input-sm custom_input" style="width: 60%">
														<div class="error" id=""></div>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="panel panel-info">
									<div class="panel-heading">C. Particulars of Recipient or Beneficiary</div>
									<div class="panel-body">
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label"
													for="RecipientName">12. Recipient Name<sup
													class="text-danger">*</sup></label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input required"
														aria-role="Nature of Trade/Business" id="RecipientName"
														maxlength="150" data-required="required" name="RecipientName"
														class="biginput" pattern="alphabetsWithSpaces">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
		
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="RecipientAddress">13. Recipient's Overseas Address
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input"
														id="RecipientAddress"
														pattern="text" name="RecipientAddress">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="RecipientNigAddress">
													14. Recipient's Nigerian Address
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input"
														id="RecipientNigAddress" name="RecipientNigAddress"
														pattern="text">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover">
													15. If recipient is non-resident of Nigeria and visited Nigeria more than once in last 2 years, provide the tour details and dates of visits
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input"
														id="TourDetails" name="TourDetails"
														pattern="text">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="RecipientNonNigAddress">
													16. If recipient is non-resident of Nigeria but has offices or businesses in Nigeria, please state the address
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input"
														id="RecipientNonNigAddress" name="RecipientNonNigAddress"
														pattern="text">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label"
													for="registration_type">
													17. If the beneficiary is a company, does it have any representative, office branch subsidiary or associated company in Nigeria? 
													</label>
												<div class="col-md-9 col-lg-9">
													<label class="col-md-3 col-lg-3 custom_label"
														for="IsAssociate"> <input
														type="radio" selected value="Yes"
														aria-role="Registration Type"
														id="IsAssociate" name="IsAssociate">
														&nbsp &nbsp&nbsp Yes
													</label> <label class="col-md-3 col-lg-3 custom_label"
														for="IsAssociate"> <input
														type="radio" value="No" checked
														id="IsAssociate" name="IsAssociate">
														&nbsp &nbsp&nbsp No
													</label>
													<div class="error" id=""></div>
													
												</div>
											</div>
										</div>
										
										<div class="row custom_row">
											<div class="form-group">
												<label class="col-md-3 col-lg-3 custom_label" for="turnover">18. Give details of each associated company with Name and addresses
												</label>
												<div class="col-md-9 col-lg-9">
													<input type="text" class="form-control input-sm custom_input"
														id="AssociateCompanyDetails" name="AssociateCompanyDetails"
														pattern="text">
													<div class="error" id=""></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="panel panel-default">
									<div class="panel-body">
										<div class="text-center" style="margin-bottom: 10px;">
											<span id="formButtonContainer">
												<button class="btn btn-default custom_btn" id="preview_form">Preview</button>
											</span>
										</div>
									</div>
								</div>
							</div>

								<div class="hide" id="PreviewDiv">
								
									<h3 class="text-center" style="text-decoration: underline;">Preview</h3>
									
									<div class="panel panel-info">
										<div class="panel-heading">A. Payer Details</div>
										<div class="panel-body">
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>1. Payer Name:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="PayerName">Payer Name</label>
												</div>
												<div class="col-lg-3">
													<label>2. Payer Address:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="PayerAddress"></label>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>3. Business Relationship between PAYER and BENEFICIARY/RECIPIENT:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="BusinessRelationship"></label>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panel panel-info">
										<div class="panel-heading">B. 1.Particulars of Remittance</div>
										<div class="panel-body">
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>4. Nature of Remittance:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="NatureOfRemittance"></label>
												</div>
												<div class="col-lg-3">
													<label>5. Details of Remittance</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="DetailsOfRemittance"></label>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>6. Nature of service provided (if payment is in respect to services)</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="NatureOfService">Neature of service provided</label>
												</div>
												<div class="col-lg-3">
													<label>8. Amount (with respect to this application)</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="Amount"></label>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panel panel-info">
										<div class="panel-heading">B. 2.Previous Forex Approval (if any)</div>
										<div class="panel-body">
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>9. Previous TCC No:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="PrevTCCNo"></label>
												</div>
												<div class="col-lg-3">
													<label>10. Previous Forex Approval details</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="PrevForexDetail"></label>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>11. Has this amount suffered Withholding Tax?:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="WithholdingTax"></label>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panel panel-info">
										<div class="panel-heading">C. Particulars of Recipient or Beneficiary</div>
										<div class="panel-body">
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>12. Recipient Name</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="RecipientName"></label>
												</div>
												<div class="col-lg-3">
													<label>13. Recipient's Overseas Address</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="RecipientAddress">Recipient's Overseas</label>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>14. Recipient's Nigerian Address:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="RecipientNigAddress"></label>
												</div>
												<div class="col-lg-3">
													<label>15. If recipient is non-resident of Nigeria and visited Nigeria more than once in last 2 years, provide the tour details and dates of visits</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="TourDetails"></label>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>16. If recipient is non-resident of Nigeria but has offices or businesses in Nigeria, please state the address:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="RecipientNonNigAddress"></label>
												</div>
												<div class="col-lg-3">
													<label>17. If the beneficiary is a company, does it have any representative, office branch subsidiary or associated company in Nigeria? </label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="IsAssociate"></label>
												</div>
											</div>
											
											<div class="col-lg-12">
												<div class="col-lg-3">
													<label>18. Give details of each associated company with Name and addresses:</label>
												</div>
												<div class="col-lg-3 labelText">
													<label class="AssociateCompanyDetails"></label>
												</div>
											</div>
										</div>
									</div>
									
									<div class="panel panel-default">
										<div class="panel-body">
											<input type="hidden" name="PeopleRSN" id="PeopleRSN" value="<%=PeopleRSN%>">
											<div class="row">&nbsp;</div>
											<div class="row">&nbsp;</div>
											<label class="checkbox-inline custom_label" for="confirm_form"><input
												type="checkbox" id="confirm_form"> I certify that the
												information given above is correct in respect and confirm that
												to the best of my knowledge and belief, there are no other
												facts the omission of which would be misleading. </label>
											<div class="text-center" style="margin-bottom: 10px;">
												<span id="formButtonContainer">
													<button class="btn btn-default custom_btn" disabled="disabled" id="submit_forex_form">Submit</button>&nbsp&nbsp
													<button class="btn btn-default custom_btn" id="form_back">Back</button>&nbsp&nbsp&nbsp
												</span>
												<br>
												<div class="loader-container hide" id="loader">
													<p class="alert alert-success">
														Please wait, Submitting form data.. <img
															src="images/loader.gif" alt="" height="20px" width="20px">
													</p>
												</div>
											</div>
											<br>
											<div class="hide text-danger text-center"
												style="margin: 5px 0px; font-style: italic"
												id="applyTCCFormError">
											</div>
										</div>
									</div>
								</div>
						</div>
						<div class="container-fluid">
							<div class="panel panel-default hide" id="ConfirmationResponseStatus">
								<div class="" style="padding: 60px 0 60px 0;">
									<h5 class="text-center">
										<b>THANK YOU!</b>
									</h5>
									<h5 class="text-center">Forex TCC application has been submitted
										successfully.</h5>
									<p class="text-center">
										Your application no. is <strong> <span
											id="successApplicationNumberDisplay"></span></strong>
									</p>
									<div class="text-center">
										<b><a class="text-info"
											href="<%=path%>">Home Page</a></b>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>


		</div>
	</div>
</div>
