<%@page import="java.util.Map"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page
	import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@include file="include/includeconstant.jsp"%>
<%@ page language="java" contentType="application/json; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	
	//Code For fetching Tax Office TCC Count
	WsTransactionRequest[] transactionRequests = new WsTransactionRequest[1];
	transactionRequests[0] = new WsTransactionRequest("argYear", PortalUtility.getString(request.getParameter("argYear")));
	WsTransactionResponse[] transactionResponse = PortalService.getWSClient().executeCustomTransaction(lid, 104, transactionRequests);
	

	//Code For fetching Tax Office TCC Count

	String monthArr[] = new String[12];
	monthArr[0] = "Jan";
	monthArr[1] = "Feb";
	monthArr[2] = "Mar";
	monthArr[3] = "Apr";
	monthArr[4] = "May";
	monthArr[5] = "Jun";
	monthArr[6] = "Jul";
	monthArr[7] = "Aug";
	monthArr[8] = "Sep";
	monthArr[9] = "Oct";
	monthArr[10] = "Nov";
	monthArr[11] = "Dec";

	JSONArray items = new JSONArray();
	//for(int j=0;j<6;j++){
	//for (Map.Entry<String, String> entry : taxOfficeParent.entrySet()) {

	if (!PortalUtility.isNull(transactionResponse)) {
		for (int i = 0; i < transactionResponse[0].getColumnValues().length; i++) {
			JSONObject item1 = new JSONObject();
			item1.put("name", transactionResponse[13].getColumnValues(i));
			item1.put("value", transactionResponse[12].getColumnValues(i));
			item1.put("id", transactionResponse[13].getColumnValues(i));
			//Prepare a JSON array for drilldown data
			JSONArray drilldownItem1 = new JSONArray();
			for (int k = 0; k < monthArr.length; k++) {
				JSONArray tuple = new JSONArray();
				tuple.put(monthArr[k]);
				tuple.put(Integer.parseInt(transactionResponse[k].getColumnValues(i)));
				drilldownItem1.put(tuple);
			}
			item1.put("drilldown", drilldownItem1);
			items.put(item1);
		}
	}
	JSONObject output = new JSONObject();
	output.put("data", items);
	out.print(output);
	out.flush();
%>


