<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@page import="com.csdcsystems.amanda.common.*"%>
<%@include file="include/includeconstant.jsp"%>

<%
request.setAttribute("pageName","forgotPassword"); 
request.setAttribute("pageName","forgotPassword"); 
String UserName = request.getParameter("LogonName");

WsValidInfoValue[] securityQueValidInfoValues = PortalService.getWSClient().getValidInfoValues(lid, 1032);

%>
 <div class="section-heading scrollpoint sp-effect3">
    <h1><%=resourceBundle.getText("LABEL_HEADER_MAIN") %></h1>
    <div class="divider"></div>
    <%=resourceBundle.getText("LABEL_HEADER_SUB") %>
 </div>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="row">
			<div class="col-md-3 col-sm-3"></div>
			<div class="col-md-6 col-sm-6 scrollpoint sp-effect3">
				<div class="form-group" id="userName">
					<label for="UserName"><%=resourceBundle.getText("LABEL_FORGOT_PASSWORD_USERNAME")%></label>
					<div class="input-group">
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-user"></span></span> <input id="UserName"
							name="UserName" type="text" readonly="readonly"
							class="form-control" value="<%=UserName%>">
					</div>
				</div>
				<div class="form-group" id="internetQuestion">
					<label for="InternetQuestion"><%=resourceBundle.getText("LABEL_FORGOT_PASSWORD_SECURITY_QUESTION")%></label>
					<div class="input-group">
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-question-sign"></span></span> <select
							id="InternetQuestion" name="InternetQuestion"
							class="form-control">
							<option value=""></option>
							<%
								for (WsValidInfoValue infoValue : securityQueValidInfoValues) {
							%>
							<option value="<%=infoValue.getInfoValue()%>"><%=infoValue.getInfoDesc()%></option>
							<%
								}
							%>
						</select>
					</div>
				</div>
				<div class="form-group" id="internetAnswer">
					<label for="InternetAnswer"><%=resourceBundle.getText("LABEL_FORGOT_PASSWORD_SECURITY_ANSWER")%></label>
					<div class="input-group">
						<span class="input-group-addon"><span
							class="glyphicon glyphicon-pencil"></span></span> <input
							id="InternetAnswer" name="InternetAnswer" type="text"
							class="form-control">
					</div>
				</div>
				<div class="gapMedium"></div>
				<div class="row" style="margin: 0">
					<div class="btn-toolbar">
						<button id="btnSend" type="button"
							class="btn btn-success form-control"
							onclick="f_checkForgotPassword();">
							<span class="glyphicon glyphicon-repeat"></span>&nbsp;&nbsp;<%=resourceBundle.getText("LABEL_FORGOT_PASSWORD_BUTTON_SUBMIT")%></button>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
    var MESSAGE_ALERT_FOR_SECURITY_QUESTION="<%=resourceBundle.getJsText("MESSAGE_ALERT_FOR_SECURITY_QUESTION")%>";
	var MESSAGE_ALERT_FOR_SECURITY_ANSWER="<%=resourceBundle.getJsText("MESSAGE_ALERT_FOR_SECURITY_ANSWER")%>";
	var MESSAGE_FORGOT_PASSWORD_SECURITY_AUTH_FAIL = "<%=resourceBundle.getJsText("MESSAGE_FORGOT_PASSWORD_SECURITY_AUTH_FAIL")%>";
	var LABEL_FORGOT_PASSWORD_BUTTON_SUBMIT = "<%=resourceBundle.getJsText("LABEL_FORGOT_PASSWORD_BUTTON_SUBMIT")%>";
	var LABEL_CONTINUE_BUTTON_PLEASE_WAIT = "<%=resourceBundle.getJsText("LABEL_CONTINUE_BUTTON_PLEASE_WAIT")%>";
	var MESSAGE_PASSWORD_RESET_CONFIRMATION = "<%=resourceBundle.getJsText("MESSAGE_PASSWORD_RESET_CONFIRMATION")%>";
	var langcode='<%=LanguageCode.getLanguageCode() %>';
</script>
<script type="text/javascript" src="<%=JS_PORTAL%>forgotPassword.js"></script>