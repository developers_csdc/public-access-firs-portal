<%@include file= "header.jsp" %>
<div class="container-fluid">
	
	<div class="row" id="tcc_search">
		<div class="col-lg-3 col-md-3"></div>
		<div class="col-lg-6 col-md-6">
			<div class="panel custom_panel panel-default">
		  		<div class="panel-heading">
		  		Verify TCC
		  		</div>
		  		<div class="panel-body">
			  		<p style="margin-bottom: 40px;">Verify your Tax Clearance Certificate by providing the details below</p>
			  		<div class="row">
				  		<div class="form-group">
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">TCC Number: </label>
				    		<div class="col-md-6 col-lg-6 col-sm-6">
					    		<input type="text" class="form-control input-sm custom_input" id="tcc_number" placeholder="Enter TCC Number" maxlength="20" data-required="required" >
					    		<div class="error" id="error_Search"></div>
					    		<br/>
				    		</div>
			 			</div>
		 			</div>
		 			<div class="row" style="margin-bottom:10px;">
                    	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pull-left text-left" style="padding: 0px 0px 0px 0px;line-height: 28px;">
							<span id="miniCaptcha_Search" style="margin-left:17px;"></span>&nbsp&nbsp
                    	</div>
                    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left text-left">
							<input type="text" class="form-control input-sm custom_input"
								id="expression-result" placeholder="Enter Result here"
								maxlength="2" onkeypress="return onlyNos(event,this)">
						</div>
                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 pull-left text-left">
							<button onclick="resetBotBoot();" class="btn btn-primary custom_btn" style="height:30px;width:40px;">
								<span class="glyphicon glyphicon-repeat"></span>
							</button>
                    	</div>
                    </div>
		 			<div class="text-center" style="margin-bottom: 50px;">
		 				<div id="formButtonContainer">
		 					<button class="btn btn-default custom_btn" onclick="searchTCC_validation();" id="submit_button">Submit</button>
		 					<button class="btn btn-default custom_btn" onclick="window.location.reload();">Reset</button>
		 				</div>
		 				<div class="loader-container hide" id="tcc_loader">
							<p class="alert alert-success">Please wait, Retrieving data.. <img src="images/loader.gif" alt="" height="20px" width="20px"></p>
						</div>
		 			</div>
		  		</div>
			</div>
		</div>
	</div>
		
	<div class="row hide" id="tcc_search_result">
		<div class="col-lg-3 col-md-3"></div>
		<div class="col-lg-6 col-md-6">
			<div class="panel custom_panel panel-default">
		  		<div class="panel-heading">
		  		TCC Details
		  		</div>
		  		<div class="panel-body">
		  			<div class="row custom_row">
				  		<div class="form-group">
				    		<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="tccApplicantName">Name of the Company/Individual</label>
				    		<div class="col-md-6 col-lg-6">
				    			<label class="custom_label" id="tccApplicantName"></label>
				    		</div>
				 		</div>
			 		</div>
			 		<div class="row custom_row">
				  		<div class="form-group">
				    		<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="tccApplicantTIN">TIN</label>
				    		<div class="col-md-6 col-lg-6"><input class="search_result" type="text" id="tccApplicantTIN" readonly></div>
			 			</div>
			 		</div>
				  	<div class="row custom_row">
				  		<div class="form-group">
				    		<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="tccnumber">TCC Number</label>
				    		<div class="col-md-6 col-lg-6"><input class="search_result" type="text" id="tccnumber" readonly></div>
			 			</div>
			 		</div>
		 			<div class="row custom_row">
				  		<div class="form-group">
				  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">TCC Status:</label>
				    		<div class="col-md-6 col-lg-6"><input class="search_result" type="text" id="tccstatus" readonly></div>
			 			</div>
		 			</div>
		 			<div class="row custom_row hide" id="remittanceAmountContainer">
				  		<div class="form-group">
				  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="remittanceAmount">Remittance Amount:</label>
				    		<div class="col-md-6 col-lg-6">
				    			<textarea class="search_result" id="remittanceAmount" rows="3" readonly></textarea>
				    		</div>
			 			</div>
		 			</div>
		 			<div class="row custom_row">
				  		<div class="form-group">
				  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Date of Issue:</label>
				    		<div class="col-md-6 col-lg-6"><input class="search_result" type="text" id="tccissue" readonly></div>
			 			</div>
		 			</div>
		 			<div class="row custom_row">
				  		<div class="form-group">
				  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
				    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Date of Expiry:</label>
				    		<div class="col-md-6 col-lg-6"><input class="search_result" type="text" id="tccexpiry" readonly></div>
			 			</div>
		 			</div>
		 			<div class="text-center">
		 				<button class="btn btn-default custom_btn" id="btnRedirectSearchTCC">Back</button>
					</div>			  		
			  	</div>
			</div>
		</div>
	</div>

	<div class="row hide" id="tcc-result-corporate">
		<div class="col-lg-2 col-md-2"></div>
		<div class="col-lg-8 col-md-8">
			<h4>For Corporate: <span style="margin-left:5px; font-size:12px;" class="text-danger"><i>*All fig. in Naira</i></span></h4>
			<div class="table-responsive">
				<div class="container-fluid">
					<table class="table table-bordered table-striped table-custom-tcc"
						id="tcc-search-result-corporate" style="font-family: sans-serif;font-size: 12px;">
						<thead style="background-color: #2c3e50;color: white;">
							<tr>
								<th></th>
								<th><input type="text" class="search_result" id="corporate_year_1" readonly></th>
								<th><input type="text" class="search_result" id="corporate_year_2" readonly></th>
								<th><input type="text" class="search_result" id="corporate_year_3" readonly></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Turnover (in Naira)</td>
								<td><textarea class="search_result" id="corporate_turnover_1" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="corporate_turnover_2" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="corporate_turnover_3" rows="2" readonly style="width: 100%"></textarea></td>
							</tr>
							<tr>
								<td>Assessable Profit (in Naira)</td>
								<td><textarea class="search_result" id="corporate_assessableProfit_1" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="corporate_assessableProfit_2" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="corporate_assessableProfit_3" rows="2" readonly style="width: 100%"></textarea></td>
							</tr>
							<tr>
								<td>Tax Paid</td>
								<td><textarea class="search_result" id="corporate_taxPaid_1" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="corporate_taxPaid_2" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="corporate_taxPaid_3" rows="2" readonly style="width: 100%"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="row hide" id="tcc-result-individual">
		<div class="col-lg-2 col-md-2"></div>
		<div class="col-lg-8 col-md-8">
			<h4>For Individual: <span style="margin-left:5px; font-size:12px;" class="text-danger"><i>*All fig. in Naira</i></span></h4>
			<div class="table-responsive">
				<div class="container-fluid">
					<table class="table table-bordered table-striped table-custom-tcc"
						id="tcc-search-result-individual" style="font-family: sans-serif;font-size: 12px;">
						<thead style="background-color: #2c3e50;color: white;">
							<tr>
								<th></th>
								<th><input type="text" class="search_result" id="ind_year_1" readonly></th>
								<th><input type="text" class="search_result" id="ind_year_2" readonly></th>
								<th><input type="text" class="search_result" id="ind_year_3" readonly></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Total Income</td>
								<td><textarea class="search_result" id="ind_totalIncome_1" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="ind_totalIncome_2" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="ind_totalIncome_3" rows="2" readonly style="width: 100%"></textarea></td>
							</tr>
							<tr>
								<td>Tax Paid</td>
								<td><textarea class="search_result" id="ind_taxPaid_1" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="ind_taxPaid_2" rows="2" readonly style="width: 100%"></textarea></td>
								<td><textarea class="search_result" id="ind_taxPaid_3" rows="2" readonly style="width: 100%"></textarea></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
<script>
	$('#tcc-search-result-corporate,#tcc-search-result-individual').DataTable({
		"bPaginate": false,
        "bFilter": false,
        "bInfo": false
	});
</script>