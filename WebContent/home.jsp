
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<script>
 var contextPath = "/FIRSDEMO/";
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>FIRS | TCC Application Portal</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		

		<!-- Common -->
<link rel="stylesheet" href="css/api/bootstrap.min.css" async>
<link rel="stylesheet" href="css/api/template.css" async>
<link rel="stylesheet" href="css/api/font-awesome.min.css" async>
<link rel="stylesheet" href="css/api/bootstrap-datetimepicker.min.css"  async/>
<link rel="stylesheet" href="css/api/pikaday.css"  async/>
<link rel="stylesheet" href="css/api/jquery-ui.1.11.4.css"  async/>
<link rel="stylesheet" href="css/api/bootstrap-dialog.min.css" async>
<link rel="stylesheet" href="css/api/dataTables.bootstrap.min.css" async>
<link href='//fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css' async>
<link rel="stylesheet" href="css/api/features-quick-link.css" async>
<link rel="stylesheet" href="css/api/font-awesome-animation.min.css" async>
<link rel="stylesheet" href="css/api/remodal.css" async>
<link rel="stylesheet" href="css/api/jquery.growl.css" async>
<link rel="stylesheet" href="css/api/remodal-default-theme.css" async>
<link rel="stylesheet" href="css/portal/Custom.css" async>
<link rel="stylesheet" href="css/api/jquery-ui.css" async>
		
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js" ></script>
		
		
		<script type="text/javascript" src="js/bootstrap-dialog.min.js" ></script>
		<script type="text/javascript" src="js/jquery.growl.js" ></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
		<script type="text/javascript" src="js/portal/notification.js"></script>
		
		<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
		
		<script type="text/javascript" src="js/moment.min.js"></script>
		<script type="text/javascript" src="js/remodal.min.js" ></script>
		
		<script type="text/javascript" src="js/portal/forgotPassword.js" ></script>
		<script type="text/javascript" src="js/portal/portal.js" ></script>
 		<script type="text/javascript" src="js/portal/logon.js"></script>
 		
		
		<script type="text/javascript" src="js/portal/captcha.js"></script>
		<script type="text/javascript" src="js/portal/miniCaptcha.js"></script>
		<script type="text/javascript" src="js/portal/template.js"></script>
		<script type="text/javascript" src="js/portal/validator.js" ></script>
		<script type="text/javascript" src="js/portal/downloadTCC.js" ></script>
		<script type="text/javascript" src="js/portal/TccFormValidation.js"></script>
		<script type="text/javascript" src="js/portal/TccFormSubmission.js"></script>
		<script type="text/javascript" src="js/portal/SearchTCC.js"></script>
		<script type="text/javascript" src="js/portal/SearchReceipt.js"></script>		
		<script type="text/javascript" src="js/portal/registration.js"></script>
		<script type="text/javascript" src="js/portal/corporateProfile.js"></script>
		<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
		
	
		<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
		
		<script type="text/javascript" src="js/portal/tradenature-autocomplete.js"></script>
		<script type="text/javascript" src="js/portal/TINRevalidation.js"></script>

		
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<!-- Modal for Registration -->
	<div class="row">
		<div class="col-lg-offset-3 col-lg-6">
			<div class="modal fade" id="myModal" role="dialog">
   				<div class="modal-dialog">
					<div class="modal-content" style="border-radius:0px;">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-12">
				          			<img alt="" class="img-responsive" src="images/FIRS-Logo.png" height="50px" width="280px">
				          		</div>
				          	</div>
				          </div>
				        </div>
				        <div class="modal-body">
				          <p class="text-info-custom"><b>Please select the type of Registration</b></p>
				         <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Individual">
				          			<img class="custom_icon" alt="" src="images/individual.png">
				          			<h5 class="text-info">Individual</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Corporate">
				          			<img class="custom_icon" alt="" src="images/corporate.png">
				          			<h5 class="text-info">Corporate</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Forex">
				          			<img class="custom_icon" alt="" src="images/forex.png">
				          			<h5 class="text-info">Forex</h5>
				          			</a>
				          		</div>
				          	</div>
				          </div> 
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default custom_btn" data-dismiss="modal">Close</button>
				        </div>
		      		</div>
		      	</div>
		      </div>
		</div>
	</div>
</body>
</html>
	</head>
	<body>
	
			
		</div>		

		<div>
			<div id="container-fluid">
						<div class="container-fluid">
		<div class="row" style="margin:">
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
				<h3 style="color:#ab0000;">TCC Application Portal</h3>
				<h5>Welcome to FIRS TCC Application Portal</h5>
				<div class="row">
					<!-- <div class="col-lg-4 col-md-4 col-sm-4  text-center">
						<a href="portal?cmd=tccapplicationform"><img class="custom_icon" alt="" src="images/app.png">
						<p><h6 class="custom_icontext">Apply for TCC</h6></p>
						</a>
					</div> -->
					
					<div class="col-lg-4 col-md-4 col-sm-4  text-center">
						<a href="" data-toggle="modal" data-target="#myModal" style="text-decoration:none;border:0;outline:none;">
						<img class="custom_icon" alt="" src="images/app.png">
						<p><h6 class="custom_icontext"><b>Register to get your TCC account</b></h6></p>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4  text-center">
						<a href="portal?cmd=searchreceipt"><img class="custom_icon" alt="" src="images/download1.jpg">
						<p><h6 class="custom_icontext"><b>Verify Receipt</b></h6></p>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 text-center">
						<a href="portal?cmd=searchtcc"><img class="custom_icon" alt="" src="images/verify.png">
						<p><h6 class="custom_icontext"><b>Verify TCC</b></h6></p>
						</a>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12  text-center">
						<!-- <a href="portal?cmd=searchreceipt"><img class="custom_icon" alt="" src="images/download1.png">
						<p><h6 class="custom_icontext">Verify Receipt</h6></p>
						</a> -->
						<!--  Added by samarjit 
						<div class="text-center alert alert-info" style="border-radius:0px;">
							<span>Account Login</span>	
						</div>-->
						<div class="loader-section hide" id="logon-loader-section">
							<p>Please wait</p>
							<p>Logging you in..<i class="custom-loader"></i></p>
						</div>
						<div id="logon-form-section" class="">
				            <div class="container-fluid">
					            <div>
								 	<h3 style="width :100px;text-decoration: underline;" class="pull-left">Login</h3>	
					            </div>	
				            	<div class="row">
				            		<div class="col-xs-7 col-sm-8 col-xs-offset-5 col-sm-offset-4">
				            			<div id="registration-message" class="pull-left">
						                	<span class="custom-question"><b>Haven't Registered?
						                	<a href="" data-toggle="modal" data-target="#myModal" class="custom-link" id="signUpBtnTrigger" >Register Here</a></b></span>
						               	</div>
						               	<br/>
				            		</div>
				            	</div>
				            </div>
							<div class="container-fluid">
				                <div class="form-horizontal" id="login-section">
				                	<div id="login_section">
					                    <div class="form-group row" >
					                    	<div class="col-xs-12 hide" id="info-text-forgot-password">
				                    			<span class="info-text text-center"></span>
				                    		</div>
					                        <div class="col-xs-5 col-sm-4 ">
					                            <label class="text-large" id="label_FP_Login">TIN</label>
					                        </div>
					                        <div class="col-xs-7 col-sm-8">
					                        	<input
					                                type="text"
					                                class="form-control input-sm custom_input"
					                                placeholder="Enter your TIN" id="username_FP_Login"
					                                name="username_FP_Login"
					                                maxlength="20"
					                                pattern="TIN"
					                                errorMessage="Enter a valid TIN"
					                                earia-describedby="basic-addon1"/>
					                            <div  style="left:20px;" class="error"></div>
					                        </div>
					                    </div>
					                    <div class="form-group row">
					                    	<div class="col-xs-5 col-sm-4">
					                    		<label class="text-large" id="label_Password_Login">Password</label>
					                    	</div>
					                    	<div class="col-xs-7 col-sm-8">
					                    		<input type="password" class="form-control input-sm custom_input" placeholder="Enter Password" id="password_FP_Login"
					                    			name="password_FP_Login" maxlength="20" errorMessage="Enter a valid password"/>
					                    		<div id="login_FP_error" style="left:20px;" class="error"></div>
					                    	</div>
					                    </div>
					                    <div class="row" style="margin-bottom:10px;">
					                    	<div class="col-xs-5 col-sm-4 col-md-4 col-lg-4 pull-left text-left" style="padding: 0px 0px 0px 0px;line-height: 28px;font-weight:600;">
												<span id="miniCaptcha_Search"></span>&nbsp&nbsp
					                    	</div>
					                    	<div class="col-xs-5 col-sm-6 col-md-6 col-lg-6 pull-left text-left">
											<input type="text" class="form-control input-sm custom_input"
												id="expression-result" placeholder="Enter Result here"
												maxlength="2" onkeypress="return onlyNos(event,this)">
										</div>
					                    	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-left text-left">
												<button onclick="resetBotBoot();" class="btn btn-primary custom_btn" style="height:30px;width:40px;">
													<span class="glyphicon glyphicon-repeat"></span>
												</button>
					                    	</div>
					                    </div>
					                   <!--  <div class="form-group row">
					                    	<div class="col-xs-5 col-sm-4">
					                    		<label class="text-large" id="label_Password_Login"></label>
					                    	</div>
					                    	<div class="col-xs-7 col-sm-8">
					                    		<div class="pull-left" style="font-weight:600;">Enter the code Shown</div>
					                    		<input type="text" class="form-control input-sm custom_input" placeholder="Enter captcha value here" id="txtInput"
					                    			name="password_FP_Login" maxlength="7" errorMessage=""/>
					                    		<div id="login_FP_error" style="left:20px;" class="error"></div>
					                    	</div>
					                    </div> -->
					                    <span class="pull-right">
					                    	<a href="#" class="plain-link"
										id="forgotPasswordSectionTrigger" data-toggle="modal"
										data-target="#forgotPassword-modal" data-backdrop="static" data-keyboard="false"><u>Forgot Password</u></a>
									</span>
				                	</div>
				                    <div class="form-group row hide" id="login_waiting_section">
				                    	<div class="col-sm-12 text-center" id="login_waiting_login">
				                    		<h2 class="custom-question text-center">Please wait, <span id="login_waiting_message">logging in..</span><i class="custom-preloader"></i></h2>
				                    	</div>
				                    </div>
				                    
				                    <div  style="margin-left: 52px;"
				                        class="error-class">
				                    </div>
				                    <div class="form-group row" id="login_buttons_section">
				                        <div class="col-xs-12 col-sm-8 col-lg-8 col-lg-offset-4 col-sm-offset-6" style="margin-top:10px;">
				                            <button id="resetLoginFrontPage" onclick="resetLoginProcess()" class="btn btn-primary custom_btn">Clear</button>                                
				                            <button id="btnLogin" class="btn btn-primary custom_btn">Login</button>
				                            <button id="forgotPasswordLoginFrontPage" class=" hide btn btn-primary custom_btn">Submit</button>
				                        </div>
				                        <!-- <div class="col-xs-12  col-lg-10 col-lg-offset-2 col-sm-offset-2 col-sm-10">
				                            <div  class="info-text" id="">
				                   				<span class="text-info-custom">**First time login users should use the password provided by their Tax Office</span>
											</div> 
				                        </div> -->
				                    </div>
				                </div>
				            </div>
			            </div>
						
					</div>
				</div>
			</div>
		</div>
		<!-- added for test Purpose by Samarjit 03Mar -->
		<!-- Modal for Forgot Password -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<div class="modal fade" id="forgotPassword-modal" role="dialog">
    				<div class="modal-dialog">
						<div class="modal-content container-fluid" style="border-radius:0px;width: 85%;margin-left: 6%;">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
					          <h4 class="modal-title">Forgot Password</h4>
					        </div>
					        <div class="modal-body">
								<div class="row">
									<div class="form-group">
		    							<label class="col-md-3 col-lg-3 col-sm-3" for="TIN">TIN:</label>
		    							<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="text" class="form-control input-sm custom_input"
											aria-role="" id="TIN"
											maxlength="150" data-required="" name=""
											class="" placeholder="Enter your FIRS TIN">
										<div class="error" id="TIN-Error"></div>
		    							</div>
	 								</div>
	 							</div>
					        </div>
					        <div class="modal-footer">
								<button type="button" class="btn btn-default custom_btn"
									data-toggle="" data-dismiss=""
									data-target="" id="forgotPassword-submit">Submit</button>
								<button type="button" class="btn btn-default custom_btn" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
					        </div>
			      		</div>
			      	</div>
			      </div>
			</div>
		</div>
		<!-- Modal for Forgot Password - OTP Section -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<div class="modal fade" id="forgotPasswordOTP-modal" role="dialog">
    				<div class="modal-dialog">
						<div class="modal-content" style="border-radius:0px;width: 85%;margin-left: 6%;">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
					          <h4 class="modal-title">Forgot Password</h4>
					        </div>
					        <div class="modal-body">
							  <!-- <div class="row"">
							      <label class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-2 col-xs-2" for="otp">OTP:</label> 
							      <input type="text"
									class="form-control input-sm custom_input col-lg-4 col-md-4 col-sm-6 col-xs-6"
									id="otp" placeholder="Enter OTP" style="width: 45%">
									&nbsp&nbsp<span id=""></span>
								  <div class="error">2223232323</div>
							  </div> -->
							  <div class="row">
								<div class="form-group">
		    					<label class="col-md-3 col-lg-3 col-sm-3" for="TIN-Preview">TIN:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="text" class="form-control input-sm custom_input"
											aria-role="" id="TIN-Preview"
											maxlength="150" data-required="required" name=""
											class="biginput" disabled="disabled">
										<div class="error" id=""></div>
		    						</div>
	 							</div>
	 						</div>
	 						<div class="row" style="margin-top:15px;">
								<div class="form-group">
		    					<label class="col-md-3 col-lg-3 col-sm-3" for="trade_nature">OTP:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="text" class="form-control input-sm custom_input"
											aria-role="" id="OTP"
											maxlength="4" data-required="" name=""
											class="" placeholder="Enter Your OTP here" onkeypress="return onlyNosAndDecimal(event,this)">
										<div class="error" id=""></div>
		    						</div>
									<div class="col-lg-3 col-md-3 col-sm-3" id=""
										style="font-size: 15px; font-family: monospace; font-weight: 600; color: rgba(142, 17, 17, 0.9);">
										<span id="counter"></span><span id="msg"></span>
									</div>
								</div>
	 						</div>
							 
							  <div class="row" style="margin-top:5px;">
							      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								      <span class="text-info-custom">**Enter the OTP sent to your email.</span>
							      </div>
							  </div>
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default custom_btn" id="resend-OTP">Re-Send OTP</button>	
					          <button type="button" class="btn btn-default custom_btn" id="OTP-Submit">Submit</button>
					        </div>
			      		</div>
			      	</div>
			      </div>
			</div>
		</div>
		<!-- Modal for Change of Password -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<div class="modal fade" id="changePassword-modal" role="dialog">
    				<div class="modal-dialog">
						<div class="modal-content" style="border-radius:0px;width: 85%;margin-left: 6%;">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
					          <h4 class="modal-title">Change Password</h4>
					        </div>
					        <div class="modal-body">
							  <div class="row">
								<div class="form-group">
		    					<label class="col-md-5 col-lg-5 col-sm-5" for="new-password">Enter New password:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="password" class="form-control input-sm custom_input"
											aria-role="" id="new-password"
											maxlength="150" data-required="" name=""
											class="" placeholder="Enter your password here">
										<div class="error" id="">
										<input type="hidden" id="TINPreview">
										</div>
		    						</div>
	 							</div>
	 						</div>
	 						<div class="row" style="margin-top:15px;">
								<div class="form-group">
		    					<label class="col-md-5 col-lg-5 col-sm-5" for="confirm-password">Re-enter New Password:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="password" class="form-control input-sm custom_input"
											aria-role="" id="confirm-password"
											maxlength="" data-required="required" name=""
											class="" placeholder="Re-enter your password here">
										<div class="error" id=""></div>
		    						</div>
	 							</div>
	 						</div>
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default custom_btn" id="updatePassword-submit">Submit</button>
					        </div>
			      		</div>
			      	</div>
			      </div>
			</div>
		</div>
		
		<div class="text-center" style="margin:20px 10px 10px 10px">
		Thank you for Visiting FIRS TCC Application Portal!!
		</div>
	</div>	
	
	<br class="clearfix" />
	
			</div>
			
		</div>
		
		<div class="container-fluid">
			<footer class="footer">
     <div class="container-fluid" style="font-family: sans-serif;font-size: 12px;">
       <p class="text-muted">
        <div class="text-center">
			&copy;&nbsp;Copyright 2017 <a href="#" target="_blank">Federal Inland Revenue Service. All Right Reserved </a>.
			<!-- <span class="text-right small"><b>Need help?&nbsp</b><a href="#" class="text-info"><u>Click here for LIVE CHAT</u></a></span> -->
		</div>
		</p>
     </div>
</footer>
	

		</div>
		
		
	</body>
</html>