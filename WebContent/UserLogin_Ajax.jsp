<%@page import="java.util.Arrays"%>
<%@ page language="java"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="java.rmi.RemoteException"%>
<%@page import="org.apache.axis.AxisFault"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%!
	JSONObject validationDetails;
%>
<%
 	response.setHeader("Content-Type", "application/json; charset=UTF-8");	
	String result = "";
	validationDetails = new JSONObject();

	String command = request.getParameter("command");
	String userName = PortalUtility.getString(request.getParameter("userID"));
	String password=PortalUtility.getString(request.getParameter("password"));

	if(command.equalsIgnoreCase("loginUser")){
		try{
			lid = SecurityPortalService.doPortalLoginByInput(false, userName,password,"FIRS");
		}catch(Exception ex){
			ex.printStackTrace();
		};
		if(!PortalUtility.isNull(lid) && lid!=""){
			validationDetails.put("success",true);
			validationDetails.put("page",request.getContextPath()+"/"+"UploadReceiptHome.jsp");
			validationDetails.put("user",userName);
			session.setAttribute("user",userName);
		}else{
			validationDetails.put("success",false);
		}
	}
	validationDetails.put("result", result);
	out.print(validationDetails.toString());
	
%>