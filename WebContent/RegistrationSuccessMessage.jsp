<%@include file= "header.jsp" %>
	<div>
		<div id="container-fluid">
			<!-- User Registration Page Starts -->
    <div class="hid" id="registration-sucess-info">
		<div class="row panel panel-default" style="margin: 0px 0px 0px 0px;">
			<div class="col-lg-offset-4 col-lg-4 text-center"
				style="padding: 30px 10px 30px 10px">
				<strong>Please check your mail to complete your
					registration. Registration Successful.</strong>
					<p></p>
				<!--  <p>
					Your application number is : <%//request.getAttribute("peopleRSN") %><strong><span
						id="application-number"></span></strong>
				</p>-->
				<p>
					<a href="index.jsp">Home Page</a><span></span>
				</p>
			</div>
		</div>
	</div>
		</div>

	</div>

	<div class="container-fluid">
		<footer class="footer">
		<div class="container-fluid"
			style="font-family: sans-serif; font-size: 12px;">
			<p class="text-muted">
				<div class="text-center">
					&copy;&nbsp;Copyright 2017 <a href="#" target="_blank">Federal
						Inland Revenue Service. All Right Reserved </a>.
					<!-- <span class="text-right small"><b>Need help?&nbsp</b><a href="#" class="text-info"><u>Click here for LIVE CHAT</u></a></span> -->
				</div>
			</p>
		</div>
		</footer>


	</div>


</body>
</html>