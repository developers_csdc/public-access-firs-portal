<%@page import="org.apache.commons.lang.ArrayUtils"%>
<%@page import="java.util.Map"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionResponse"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsTransactionRequest"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@ page language="java" contentType="charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%!
	private static JSONArray getReceiptObj(String lid,String receiptTypeKey,String yearMonthKey,String p_Date,String n_month,String year,int code,String TIN)throws Exception{
		JSONArray tempReceiptObj = new JSONArray();
		
		/* Custom Trasaction for getting Array of Receipt Object */ 
		WsTransactionRequest[] transactionRequests = new WsTransactionRequest[6];
		
		//transactionRequests[0] = new WsTransactionRequest("argTinNumber",PortalUtility.getString("01126466-0001"));
		transactionRequests[0] = new WsTransactionRequest("argTinNumber",PortalUtility.getString(TIN));
		transactionRequests[1] = new WsTransactionRequest("argContentType",PortalUtility.getString(receiptTypeKey));
		transactionRequests[2] = new WsTransactionRequest("argSearchType",PortalUtility.getString(String.valueOf(yearMonthKey)));
		transactionRequests[3] = new WsTransactionRequest("argDate",PortalUtility.getString(yearMonthKey).equalsIgnoreCase("Daily") ? p_Date : "");
		transactionRequests[4] = new WsTransactionRequest("argMonth",PortalUtility.getString(yearMonthKey).equalsIgnoreCase("Monthly") ? n_month : String.valueOf(0));
		transactionRequests[5] = new WsTransactionRequest("argYear",PortalUtility.getString(yearMonthKey).equalsIgnoreCase("Monthly") ? year : String.valueOf(0));
		
		WsTransactionResponse[] transactionResponse = PortalService.getWSClient().executeCustomTransaction(lid, code, transactionRequests);
		
		if(!PortalUtility.isNull(transactionResponse) && !transactionResponse[0].getColumnValues()[0].equals("null")){
			if(transactionResponse[0].getColumnValues()[0].length() > 0){
				if(transactionResponse[0].getColumnValues()[0].startsWith("[")){
					tempReceiptObj = new JSONArray(transactionResponse[0].getColumnValues()[0]);
				}else{
					tempReceiptObj = new JSONArray("["+transactionResponse[0].getColumnValues()[0]+"]");
				}
			}
		}
		
		return tempReceiptObj;
	}
%>
<%
	response.setContentType("application/json");

	JSONObject responseObj = new JSONObject();
	JSONArray arrayOfReceiptObj = new JSONArray();
	
	String 	year 			= "",
			month 			= "",
			day 			= "",
			cmd 			= "",
			receiptTypeKey 	= "",
			yearMonthKey 	= "",
			TIN 			= "";
	
	boolean isExceptionOccured = false;
	
	final int CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY = 125;
	Map<String, String> monthMap = new HashMap<String, String>();
	String[] monthArray = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	
	cmd = PortalUtility.getString(request.getParameter("cmd"));
	
	try{
		
		if(cmd.equalsIgnoreCase("searchCorporateReceipt")){
			year 			= PortalUtility.getString(request.getParameter("year"));
			month 			= PortalUtility.getString(request.getParameter("month"));
			String n_month 	= String.valueOf(ArrayUtils.indexOf(monthArray, month)+1);
			day 			= PortalUtility.getString(request.getParameter("day"));
			receiptTypeKey 	= PortalUtility.getString(request.getParameter("corporateReceiptTypeKey"));
			yearMonthKey 	= PortalUtility.getString(request.getParameter("corporateMonthYearKey").toUpperCase());
			TIN 			= PortalUtility.getString(request.getParameter("TIN"));
			
			String p_Date = PortalUtility.getString(year+"-"+n_month+"-"+day);
			System.out.println(year+month+day+receiptTypeKey+yearMonthKey+TIN+p_Date);
			
			arrayOfReceiptObj = getReceiptObj(lid, receiptTypeKey, yearMonthKey, p_Date, n_month, year, CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY, TIN);
			
		}else if(cmd.equalsIgnoreCase("searchIndividualReceipt")){
			year 			= PortalUtility.getString(request.getParameter("year"));
			month 			= PortalUtility.getString(request.getParameter("month"));
			String n_month 	= String.valueOf(ArrayUtils.indexOf(monthArray, month)+1);
			day 			= PortalUtility.getString(request.getParameter("day"));
			receiptTypeKey 	= PortalUtility.getString(request.getParameter("individualReceiptTypeKey"));
			yearMonthKey 	= PortalUtility.getString(request.getParameter("individualMonthYearKey").toUpperCase());
			TIN 			= PortalUtility.getString(request.getParameter("TIN"));
			
			String p_Date = PortalUtility.getString(year+"-"+n_month+"-"+day);
			System.out.println(year+month+day+receiptTypeKey+yearMonthKey+TIN+p_Date);
			
			arrayOfReceiptObj = getReceiptObj(lid, receiptTypeKey, yearMonthKey, p_Date, n_month, year, CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY, TIN);
		}else if(cmd.equalsIgnoreCase("searchForexReceipt")){
			year 			= PortalUtility.getString(request.getParameter("year"));
			month 			= PortalUtility.getString(request.getParameter("month"));
			String n_month 	= String.valueOf(ArrayUtils.indexOf(monthArray, month)+1);
			day 			= PortalUtility.getString(request.getParameter("day"));
			receiptTypeKey 	= PortalUtility.getString(request.getParameter("forexReceiptTypeKey"));
			yearMonthKey 	= PortalUtility.getString(request.getParameter("forexMonthYearKey").toUpperCase());
			TIN 			= PortalUtility.getString(request.getParameter("TIN"));
			
			String p_Date = PortalUtility.getString(year+"-"+n_month+"-"+day);
			System.out.println(year+month+day+receiptTypeKey+yearMonthKey+TIN+p_Date);
			
			arrayOfReceiptObj = getReceiptObj(lid, receiptTypeKey, yearMonthKey, p_Date, n_month, year, CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY, TIN);
		}
	
	}catch(Exception e){
		e.printStackTrace();
		isExceptionOccured = true;
		responseObj.put("errorMsg", e.getMessage());
	}
	
	responseObj.put("isExceptionOccured", isExceptionOccured);
	responseObj.put("success", true);
	responseObj.put("arrayOfReceiptObj", arrayOfReceiptObj);
	responseObj.put("data", arrayOfReceiptObj);
	//responseObj.put("recordsTotal", 10);
	//responseObj.put("recordsFiltered", 10);
	response.getWriter().write(responseObj.toString());
	//out.println(responseObj.toString());
	System.out.print(responseObj.toString());
%>
