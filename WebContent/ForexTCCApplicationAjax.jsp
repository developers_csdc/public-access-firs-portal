<%@page import="com.csdcsystems.amanda.client.stub.WsAttachmentContent"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsAttachment"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>

<%@include file="include/includeconstant.jsp"%>

<%
	response.setHeader("Content-Type", "application/json; charset=UTF-8");

	final int INFOCODE_PAYER_NAME 				= 6000;
	final int INFOCODE_PAYER_ADDRESS 			= 6005;
	final int INFOCODE_BUSINESS_RELATIONSHIP 	= 6010;
	final int INFOCODE_NATURE_OF_REMITTANCE 	= 6015;
	final int INFOCODE_DETAILS_OF_REMITTANCE 	= 6020;
	final int INFOCODE_NATURE_OF_SERVICE 		= 6025;
	final int INFOCODE_MORE_DETAILS_REMITTANCE 	= 6030;
	final int INFOCODE_AMOUNT 					= 6035;
	final int INFOCODE_PREV_TAX_CLEARANCE 		= 5045;
	final int INFOCODE_PREV_TAX_CLEARANCE_DATE 	= 5050;
	
	final int INFOCODE_WITHHOLDING_TAX 			= 6045;
	final int INFOCODE_RECIPIENT_NAME			= 6050;
	final int INFOCODE_RECIPIENT_ADDRESS 		= 6055;
	final int INFOCODE_RECIPIENT_NIG_ADDRESS 	= 6060;
	final int INFOCODE_TOUR_DETAILS 			= 6065;
	final int INFOCODE_BUSINESS_ADDRESS 		= 6070;
	
	final int INFOCODE_IS_COMPANY_ASSOCIATED 	= 6075;
	final int INFOCODE_ASSOCIATION_ADDRESS 		= 6080;
	final int TAX_OFFICE_INFO_CODE 				= 5090;
	
	final int ATTACHMENT_CODE 					= 40;
	final int ATTACHMENT_CODE1 					= 50;
	
	String FormData 		= "";
	int peopleRSN 			= 0;
	JSONObject FormDataJson = new JSONObject();
	WsFolder folder 		= null;
	WsFolderInfo folderInfo = null;
 	JSONObject responseJson = new JSONObject();
 	String peopleTaxOffice = "";
 	String peopleTaxOfficeCode = "";
 	String result = "";
	
	ArrayList<Map<String, Object>> fileItems 	= new ArrayList<Map<String, Object>>();
	Map<Integer, String> peopleInfoMap = new HashMap<Integer, String>();
	
	WsPeopleInfo[] peopleInfos;
	
	/*Retrieving folder from session*/
	WsFolder regdFolder=new WsFolder();
	regdFolder = (WsFolder)session.getAttribute("folder");
	WsPeople people = (WsPeople)session.getAttribute("people");
	
	//Checking any alive TCC Application folder by Search Criteria
	Map<String, String> responseMap = new HashMap<String, String>();
	responseMap = PortalService.getTCCApplicationDetailMap(PortalUtility.getString(people.getLicenceNumber()),PortalUtility.getInt(people.getPeopleCode()));
	
	if(Integer.valueOf(responseMap.get("tccActiveFolderCount")) == 0 && !Boolean.getBoolean(responseMap.get("isExceptionOccured"))){
		System.out.println("TCC not exsist");
		peopleInfos = (WsPeopleInfo[])session.getAttribute("peopleInfo");
		peopleInfoMap = PortalService.getPeopleInfoInMap(peopleInfos);
		
		if(peopleInfoMap.containsKey(TAX_OFFICE_INFO_CODE)){
			peopleTaxOfficeCode = PortalUtility.getString(peopleInfoMap.get(TAX_OFFICE_INFO_CODE));
		}
		
		//Extracting data from request
		if(ServletFileUpload.isMultipartContent(request)){
			List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
			for (FileItem item : multiparts) {
				if (!item.isFormField()) {
					Map<String, Object> attachmentItem =new HashMap<String, Object>();
					attachmentItem.put("fileData", item.get());
					attachmentItem.put("fileName", item.getFieldName()+"@@@@@@"+item.getName());
					fileItems.add(attachmentItem);
				}else{
					if("inputData".equals(item.getFieldName()))		FormData = PortalUtility.getString(new String(item.get()));
				}
			}
		}
		
		if(!"".equals(FormData)){
			FormDataJson = new JSONObject(FormData);
		}
		
		int PeopleRSN = PortalUtility.getInt(FormDataJson.getInt("PeopleRSN"));
		
		//System.out.println("JSON data : "+FormDataJson);
		
		//Create Folder
		folder = new WsFolder();
		folder.setFolderType("FTCC");
		folder.setStatusCode(1);
		//folder.setSubCode(3);
		folder.setIssueDate(Calendar.getInstance());
		folder.setReferenceFile2(PortalUtility.getString(people.getLicenceNumber()));
		folder.setReferenceFile5(PortalUtility.getString(people.getSocialSecurityNumber()));
		
		//FolderPeople
		WsFolderPeople[] folderPeopleList = new WsFolderPeople[1];
		WsFolderPeople folderPeople = new WsFolderPeople();
		folderPeople.setPeopleRSN(PeopleRSN);
		folderPeople.setPeopleCode(5);
		folderPeopleList[0] = folderPeople;
		
		int folderRSN = PortalService.getWSClient().addNewFolder(lid, folder, peopleRSN, null, folderPeopleList, null, null);
		
		List<WsFolderInfo> folderInfoList = new ArrayList<WsFolderInfo>();
		
		//Amount
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_AMOUNT);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("Amount"));
		folderInfoList.add(folderInfo);
		
		//Associate Company details
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_ASSOCIATION_ADDRESS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("AssociateCompanyDetails"));
		folderInfoList.add(folderInfo);
		
		//Business Relationship
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_BUSINESS_RELATIONSHIP);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("BusinessRelationship"));
		folderInfoList.add(folderInfo);
		
		//Details Of Remittance
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_DETAILS_OF_REMITTANCE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("DetailsOfRemittance"));
		folderInfoList.add(folderInfo);
		
		//Is company associated
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_IS_COMPANY_ASSOCIATED);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("IsAssociate"));
		folderInfoList.add(folderInfo);
		
		/* //More details of remittance
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_MORE_DETAILS_REMITTANCE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("MoreDetails"));
		folderInfoList.add(folderInfo); */
		
		//Nature of remittance
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_NATURE_OF_REMITTANCE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("NatureOfRemittance"));
		folderInfoList.add(folderInfo);
		
		//Nature of service
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_NATURE_OF_SERVICE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("NatureOfService"));
		folderInfoList.add(folderInfo);
		
		//Payer Address
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_PAYER_ADDRESS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("PayerAddress"));
		folderInfoList.add(folderInfo);
		
		//Payer Name
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_PAYER_NAME);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("PayerName"));
		folderInfoList.add(folderInfo);
		
		//Previous forex details
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_TOUR_DETAILS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("PrevForexDetail"));
		folderInfoList.add(folderInfo);
		
		//Previous TCC Number
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_PREV_TAX_CLEARANCE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("PrevTCCNo"));
		folderInfoList.add(folderInfo);
		
		//Recipient Address
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_RECIPIENT_ADDRESS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("RecipientAddress"));
		folderInfoList.add(folderInfo);
		
		//Recipient Name
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_RECIPIENT_NAME);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("RecipientName"));
		folderInfoList.add(folderInfo);
		
		//recipient Nigerian Address
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_RECIPIENT_NIG_ADDRESS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("RecipientNigAddress"));
		folderInfoList.add(folderInfo);
		
		//Reciepient Non Nigerian Address
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_TOUR_DETAILS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("RecipientNonNigAddress"));
		folderInfoList.add(folderInfo);
		
		//TCC date
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_PREV_TAX_CLEARANCE_DATE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(PortalUtility.getDateFormat2(PortalUtility.toDate(FormDataJson.getString("TCCDate"))));
		folderInfoList.add(folderInfo);
		
		//Withholding Tax
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_WITHHOLDING_TAX);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("WithholdingTax"));
		folderInfoList.add(folderInfo);
		
		//Tour Details
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(INFOCODE_TOUR_DETAILS);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(FormDataJson.getString("TourDetails"));
		folderInfoList.add(folderInfo);
		//Tax Office Code
		folderInfo = new WsFolderInfo();
		folderInfo.setInfoCode(TAX_OFFICE_INFO_CODE);
		folderInfo.setFolderType("FTCC");
		folderInfo.setInfoValue(peopleTaxOfficeCode);
		folderInfoList.add(folderInfo);
		
		boolean flag = PortalService.getWSClient().updateFolderInfo(lid, folderRSN, folderInfoList.toArray(new WsFolderInfo[folderInfoList.size()]));
		
		if(flag && fileItems.size() > 0)
			for(Map<String, Object> attachment:fileItems){
				int attachmentRSN = 0;
				WsAttachment attachmentFile = new WsAttachment();
				
				if(attachment.get("fileName").toString().split("@@@@@@")[0].equals("MoreDetails")){
					attachmentFile.setAttachmentTypeDesc("More Details");
					attachmentFile.setAttachmentCode(ATTACHMENT_CODE);
				}else if(attachment.get("fileName").toString().split("@@@@@@")[0].equals("PaymentEvidence")){
					attachmentFile.setAttachmentTypeDesc("Tax Payment Evidence");
					attachmentFile.setAttachmentCode(ATTACHMENT_CODE1);
				}
				
				WsAttachmentContent attachmentContent = new WsAttachmentContent();
				attachmentContent.setContent((byte[])attachment.get("fileData"));
				attachmentContent.setFileName((String)attachment.get("fileName").toString().split("@@@@@@")[1]);
				attachmentContent.setContentType("image");
				
				PortalService.getWSClient().addFolderAttachment(lid, folderRSN, attachmentFile, attachmentContent);
			}
		
		folder = PortalService.getWSClient().getFolder(lid, folderRSN);
	}else{
		result = "failed";
		System.out.println("TCC already exsist");
	}
	
	result = "success";
	responseJson.put("result", result);
	responseJson.put("folderRefNo", folder.getFolderRSN());
	
	out.print(responseJson);
%>