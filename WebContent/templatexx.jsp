<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>

<%@include file="include/includeconstant.jsp"%>
<script>
 var contextPath = "<%=path%>";
</script>
<%
	String cmd = request.getParameter("cmd");
	

	Map<String, String> portalPageRouter = new HashMap();	
	portalPageRouter.put("home", "home.jsp");
	portalPageRouter.put("logon", "logon.jsp");
	portalPageRouter.put("forgot_password", "forgotPassword.jsp");
	//portalPageRouter.put("newregistration", "userRegistration.jsp");
	portalPageRouter.put("myProfile", "myProfile.jsp");
	portalPageRouter.put("tccapplicationform", "TccApplicationForm.jsp");
	portalPageRouter.put("searchtcc", "SearchTCC.jsp");
	portalPageRouter.put("searchreceipt", "SearchReceipt.jsp");
	portalPageRouter.put("searchforeignreceipt", "SearchForeignReceipt.jsp");
	portalPageRouter.put("downloadtcc", "DownloadTCC.jsp");
	portalPageRouter.put("registration", "Registration.jsp");
	portalPageRouter.put("registrationPreview", "RegistrationPreview.jsp");
	portalPageRouter.put("changepassword", "ChangePassword.jsp");
	
	
	

	List<String> loginRequiredPages = new ArrayList<String>();	
	loginRequiredPages.add("embassy_local");

	
	pageName = "home.jsp";
	
	if(cmd!=null && cmd.equalsIgnoreCase("changePassword") && request.getSession(false).getAttribute("people")==null){
		cmd = "home";
	}
	
	if(portalPageRouter.containsKey(cmd)){
			pageName = portalPageRouter.get(cmd);
			if(loginRequiredPages.contains(cmd)){
				pageName = portalPageRouter.get("logon");
				pageName +="?showMsgInLogonPage=y" ;
			}
	}
	

	log.info("=========>Loading page::::"+pageName);	
	request.setAttribute("pageName", "common");
	

%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>FIRS | TCC Application Portal</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		

		<%@include file="include/common/includeCss.jsp"%>
		<%@include file="include/common/includeJs.jsp"%>
		<%@include file="include/common/includeRegistration.jsp"%>
		
	
		<script type="text/javascript">	
		    var language = "<%=(LanguageCode.getLanguageCode() == 1) ? "en" : "fr" %>";
		    var isFrLanguage = "<%=LanguageCode.getLanguageCode()==2 ? "2" : "" %>";
			var cmd = "<%=cmd%>";
		    var httpStatusCodes = [500, 502, 503, 504, 505, 520, 522, 524];
		    /*
		    	500 : Internal Server Error
		    	502 : Bad Gateway
		        503 : Service Unavailable
		        504 : Gateway Timeout
		        505 : HTTP Version Not Supported 
		        520 : Unknown Error 
		        522 : Connection Timed Out
		        524 : A Timeout Occurred
		    */

		    /*Phone Masking */
		    var phoneMaskFormat = "<%=PHONE_MASK_FORMAT%>";
		    var phoneMaskFormatChar = "<%=PHONE_MASK_FORMAT_CHAR%>";
		  	var phoneMaskFormatLiteralPattern = /["<%=PHONE_MASK_FORMAT_CHAR%>"\*]/; 
		  	
			<%if(isAdmin){out.println("var currentPageName = '"+pageName+"';");}%>
			var isAdmin = <%=isAdmin%>;
		</script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row" style="height:80px;">
				<div class="col-md-6 col-lg-6 col-sm-6">
				<a href="portal" ><img class="img-responsive" src="images/FIRS-Logo.png"/ style="padding:5px 5px 5px 5px;margin-top:5px;"></a>
				</div>
				<div class="col-md-6">
					<h1></h1>
				</div>
			</div>
		</div>
		
		<div class="container-fluid" style="height:60px;margin-top:10px;">
			<% if(!(cmd!=null && cmd.equalsIgnoreCase("changePassword"))){ %>
				<%@include file="include/common/includeHeaderMenu.jsp"%>
			<%} %>
		</div>		

		<div>
			<div id="container-fluid">
					<jsp:include page="<%=pageName %>" />
			</div>
			
		</div>
		
		<div class="container-fluid">
			<%@include file="include/common/includeFooter.jsp"%>
		</div>
		<div data-remodal-id="website-icon">
		  <h3>
		    <i><i class="glyphicon glyphicon-question" style="margin-right:5px;"></i>Are you sure you want to navigate away from this page?</i>
		  </h3>
		  <p class="text-warning"><i>All form changes will be lost</i></p>
		  <br/>
		 	<button data-remodal-action="cancel" class="remodal-cancel">No, stay on this page</button>
  			<button data-remodal-action="confirm" class="remodal-confirm">Yes, navigate away from this page</button>
		</div>
		<script type="text/javascript">
		
		var MESSAGE_ALERT_WHEN_LOGGED_ON="<%=resourceBundle.getJsText("MESSAGE_ALERT_WHEN_LOGGED_ON") %>";
		var MESSAGE_ALERT_WHEN_LANGUAGE_CHANGE_WITHOUT_SAVE="<%=resourceBundle.getJsText("MESSAGE_ALERT_WHEN_LANGUAGE_CHANGE_WITHOUT_SAVE") %>";
		var MESSAGE_ALERT_WHEN_AUTO_INSERT_NOT_DONE="<%=resourceBundle.getJsText("MESSAGE_ALERT_WHEN_AUTO_INSERT_NOT_DONE") %>";
		var MESSAGE_SEARCH_NO_RESULT_FOUND = "<%=resourceBundle.getJsText("MESSAGE_SEARCH_NO_RESULT_FOUND") %>";
		
	
		var lid = "<%=lid%>";
		var availableTags = <%//=keywords%>"";
		var langCode = "<%=LanguageCode.getLanguageCode()%>";
		var peopleRSN = "<%=request.getParameter("peopleRSN")%>"
		var folderRSN = "<%=request.getParameter("folderRSN")%>"
		var isRequireBack = "<%=request.getParameter("isRequireBack")%>"
		var from = "<%=request.getParameter("from") %>";
		</script>
		
	</body>
</html>