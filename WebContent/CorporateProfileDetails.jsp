
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@include file="include/includeconstant.jsp"%>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>

<% 

	//Infocodes
	final int INFO_CODE_NEW_TAX_OFFICE 		= 5091;
	final int INFO_CODE_NEW_EMAIL 			= 5026;
	final int INFO_CODE_NEW_PHONE_NUMBER 	= 5031;
	final int INFO_CODE_NEW_ADDRESS 		= 5006;
	final int INFO_CODE_TAX_OFFICE 			= 5090;
	//Infocodes end
	
	String Name 		= "";
	String JTBTIN 		= "";
	String TIN 		    = "";
	String RCNumber 	= "";
	String DateOfIncorporation 		= "";
	String TaxOffice 	= "";
	String Email 		= "";
	String NewEmail 	= "";
	String Mobile 		= "";
	String Address 		= "";
	String NewTaxOffice = "";
	String NewMobile 	= "";
	String NewAddress 	= "";
	
	WsPeopleInfo[] peopleInfoList 	= null;
	WsFolderInfo[] folderInfoList	= null;
	WsPeople people 				= null;
	
	people 			= (WsPeople) session.getAttribute("people");
	peopleInfoList 	= (WsPeopleInfo[]) session.getAttribute("peopleInfo");
	folderInfoList 	= (WsFolderInfo[]) session.getAttribute("folderInfo");
	
	final Map<String, String> TAX_OFFICE = PortalService.getInfoValidValues(5090);
	
	if(!PortalUtility.isNull(people)){
		Name 	= PortalUtility.getString(people.getOrganizationName());
		TIN 	= PortalUtility.getString(people.getLicenceNumber());
		JTBTIN 	= PortalUtility.getString(people.getSocialSecurityNumber());
		Email 	= PortalUtility.getString(people.getEmailAddress());
		Mobile 	= PortalUtility.getString(people.getPhone1());
		Address = PortalUtility.getString(people.getAddressLine1());
		DateOfIncorporation = PortalUtility.getString(PortalUtility.getDate(people.getBirthDate()));
		RCNumber = PortalUtility.getString(people.getReferenceFile());
	}
	
	
	if(!PortalUtility.isNull(folderInfoList))
		for(WsFolderInfo folderInfo : folderInfoList){
			if(PortalUtility.getInt(folderInfo.getInfoCode()) == INFO_CODE_TAX_OFFICE)				
				//TaxOffice = PortalUtility.getString(folderInfo.getInfoValue());
				TaxOffice = PortalUtility.getString(TAX_OFFICE.get(folderInfo.getInfoValue()));
			
			if(PortalUtility.getInt(folderInfo.getInfoCode()) == INFO_CODE_NEW_TAX_OFFICE)
				//NewTaxOffice = PortalUtility.getString(folderInfo.getInfoValue());
				 NewTaxOffice = PortalUtility.getString(TAX_OFFICE.get(folderInfo.getInfoValue()));
			
			if(PortalUtility.getInt(folderInfo.getInfoCode()) == INFO_CODE_NEW_EMAIL)
				NewEmail = PortalUtility.getString(folderInfo.getInfoValue());
			
			if(PortalUtility.getInt(folderInfo.getInfoCode()) == INFO_CODE_NEW_PHONE_NUMBER)
				NewMobile = PortalUtility.getString(folderInfo.getInfoValue());
			
			if(PortalUtility.getInt(folderInfo.getInfoCode()) == INFO_CODE_NEW_ADDRESS)
				NewAddress = PortalUtility.getString(folderInfo.getInfoValue());
		}
	
	if(!PortalUtility.isNull(peopleInfoList)){
		for(WsPeopleInfo peopleInfo : peopleInfoList){
			if(PortalUtility.getInt(peopleInfo.getInfoCode()) == INFO_CODE_TAX_OFFICE){
				TaxOffice = PortalUtility.getString(TAX_OFFICE.get(peopleInfo.getInfoValue()));
			}
		}
	}
%>


<div class="col-xs-12 col-sm-9 content showHideDiv" id="tcc_profile_details">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a> Corporate Profile Details
			</h3>
		</div>
		<div class="panel-body">
			<div class="content-row">

				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-lg-10 ">
							<div class="panel panel-profile">
								<div class="panel-heading">
									<b>Basic Details :</b>
								</div>
								<div class="panel-body">
									<div class="col-sm-12">
										<h4 style="color: #00b1b1;"><%=Name %></h4>
										<%-- <span><p><label>JTBTIN:</label><span class="custom-text">&nbsp&nbsp&nbsp<%=JTBTIN %></span></p></span>
										<span><p><label>RC Number:</label><span class="custom-text">&nbsp&nbsp&nbsp<%=RCNumber %></span></p></span>
										<span><p><label>Date of Incorporation:</label><span class="custom-text">&nbsp&nbsp&nbsp<%=DateOfIncorporation %></span></p></span> --%>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">JTBTIN:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=JTBTIN %></span></div>
									</div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">RC Number:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=RCNumber %></span></div>
									</div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Date of Incorporation:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=DateOfIncorporation %></span></div>
									</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							
							<div class="panel panel-profile">
								<div class="panel-heading">
									<b>Existing Details :</b>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Tax Office:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=TaxOffice %></span></div>
									</div>
									<div class="clearfix"></div>
									<div class="bot-border"></div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="text-right pull-right-md" style="font-weight:600px;">E Mail:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=Email %></span></div>
									</div>

									<div class="clearfix"></div>
									<div class="bot-border"></div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Mobile:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=Mobile %></span></div>
									</div>
									<div class="clearfix"></div>
									<div class="bot-border"></div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Address:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=Address %></span></div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="panel panel-profile">
								<div class="panel-heading">
									<b>Re Validate Your TIN here :</b>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<button class="btn btn-primary custom_btn" id ="revalidateTIN" tin="<%= TIN%>" style="font-size: 12px;margin-left:10px;" >
												Click this for Re-validation
											</button>
											<button class="hide" data-toggle="modal" data-target="#tinDatailsModal" data-backdrop="static" data-keyboard="false">
												Demo
											</button>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<p style="font-size: 11px;color: #F44336;font-family: sans-serif;">
												Please Re-validate your TIN information before applying a new TCC
											</p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							
							<%-- <div class="panel panel-profile">
								<div class="panel-heading">
									<b>New Details :</b>
								</div>
								<div class="panel-body">
									<div class="row hide">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">New Tax Office:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=NewTaxOffice %></span></div>
										<div class="clearfix"></div>
										<div class="bot-border"></div>
									</div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">New E Mail:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=NewEmail %></span></div>
										<div class="clearfix"></div>
										<div class="bot-border"></div>
									</div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">New Mobile:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=NewMobile %></span></div>
										<div class="clearfix"></div>
										<div class="bot-border"></div>
									</div>
									<div class="row hide">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">New Address:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"><%=NewAddress %></span></div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div> --%>
						</div>
						<script>
						    $(function() {
							    $('#profile-image1').on('click', function() {
							        $('#profile-image-upload').click();
							    });
						    });       
						    </script>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- Modal for Conforming Informations -->
 <div class="modal fade" id="tinDatailsModal" role="dialog">
   <div class="modal-dialog">
   
     <!-- Modal content-->
     <div class="modal-content" style="border-radius: 0px;">
       <div class="modal-header" style="background-color: #e2e2e2;padding: 20px 20px 20px 20px;">
         <h4 class="modal-title label label-default" style="font-size: 12px;border-radius: 0px;background-color: #303641;color: white;">
         	TAX PAYER | Information
         </h4>
       </div>
       <div class="modal-body">
         <div id="dataPreviewSection">
          <div class="row" style="margin-top:10px;">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">COMPANY NAME:</div>
          			<div class="col-lg-4" id="companyName"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">TAX OFFIC NAME:</div>
          			<div class="col-lg-6" id="taxOfficeName"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">TAX OFFICE ID:</div>
          			<div class="col-lg-4" id="taxOfficeID"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">PHONE NUMBER:</div>
          			<div class="col-lg-4" id="phoneNumber"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">EMAIL ID:</div>
          			<div class="col-lg-4" id="emailID"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">RC NUMBER:</div>
          			<div class="col-lg-4" id="rcNumber"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">JTBTIN:</div>
          			<div class="col-lg-4" id="JTBTIN"></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">ADDRESS:</div>
          			<div class="col-lg-4" id="address"></div>
          		</div>
          		</div>
          	</div>
          <div class="row">
          	<div class="col-lg-12">
          		<div class="row">
          			<div class="col-lg-offset-2 col-lg-4">TIN</div>
          			<div class="col-lg-4" id="TIN"><%=TIN %></div>
          		</div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-lg-12">
          		<p>
          			<label><input type="checkbox" value=""
						id="confirmCheck"></label> Please confirm to save this information 
				</p>
          	</div>
          </div>
         </div>
         <div class="hide" id="successDiv">
         	<div class="col-lg-12 text-center" style="margin-top: 10px;">
         		<p style="border: 1px solid #808080ab;box-shadow: 1px 1px 1px #9E9E9E;padding: 20px 20px 20px 20px;background-color: #008000a8;
   				color: white;font-family: sans-serif;font-size: 15px;">
         			Thank You ! Your Data has been Updated Successfully</p>
         	</div>
         </div>
       </div>
       <div class="modal-footer">
       	<button type="button" class="btn btn-primary custom_btn" id="cancelUpdateTINData" data-dismiss="modal" style="font-size: 12px;">Cancel</button>
         	<button type="button" class="btn btn-primary custom_btn" id="updateTINData" tin="<%= TIN%>" disabled="disabled" style="font-size: 12px;">
         		Update
         	</button>
         	<button type="button" class="btn btn-primary custom_btn hide" id="redirectToHome" tin="<%= TIN%>" style="font-size: 12px;">
         		Home
         	</button>
       </div>
     </div>
     
   </div>
 </div>
 <script>
  	
  	$("#confirmCheck").click(function(){
		if($("#confirmCheck").prop("checked") == true){   
			$("#updateTINData").attr('disabled',false);
		}else{
			$("#updateTINData").attr('disabled',true);
			}
	});
  
  </script>