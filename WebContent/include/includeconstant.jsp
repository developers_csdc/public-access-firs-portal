<%@page import="java.util.HashMap"%>
<%@page import="com.csdcsystems.amanda.security.service.SecurityPortalService"%>
<%@page import="com.csdcsystems.amanda.client.service.PortalService"%>
<%@page import="com.csdcsystems.amanda.util.LidHandler"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.util.Language"%>
<%@page import="com.csdcsystems.amanda.util.ResourceBundle"%>
<%@page import="org.apache.axis.AxisFault" %>
<%@page import="org.apache.log4j.Logger" %>
<%
	String uri = request.getRequestURI();
	String pageName = uri.substring(uri.lastIndexOf("/") + 1);
	
	boolean isAxisOn = true;
	final Logger log = Logger.getLogger("FIRS PORTAL - ");
	//String CONNECTION_CACHE="FIRS_SQL";
	//String CONNECTION_CACHE="TRAINING";
	//String CONNECTION_CACHE="TRAINING";
	
	String loggedonLid = (String)session.getAttribute("logonLid");
	String LogonName = (String) session.getAttribute("LogonName"); 
	
	String CONTEXT_PATH = request.getContextPath();	
	String requestUrlPath = request.getRequestURL().toString();	// return complete Url.	(e.g 'http://localhost:8080/FIRS/portal')
	String contextPath = requestUrlPath.substring(0, requestUrlPath.indexOf(request.getContextPath()));	// substring the above url upto context path('/FIRS') (and return 'http://localhost:8080')
	String completeContextPath = contextPath+request.getContextPath();	// (return 'http://localhost:8080/ABC')
	
	/* if("/".equals( request.getRequestURI()))
		contextPath = requestUrlPath;
		String completeContextPath = contextPath+CONTEXT_PATH; */
	
	LogonName = (PortalUtility.isNull(LogonName)) ? "":LogonName; //to avoid isNull checkings for LogonName everywhere else
	boolean isRegisteredUser = PortalService.isRegisteredUser(request);
	boolean isAdmin = (PortalUtility.isNull(session.getAttribute("isAdmin"))) ? false: ((Boolean)session.getAttribute("isAdmin"));
	
	String lid = "";
	String requestFrom=""; //used to find out dce/dci
	String DCE_CONTEXT="";
	String DCI_CONTEXT="";
	String MAIN_CONTEXT="";
	String path = request.getRequestURI();
	if(path.indexOf("DCE")==-1)
		DCE_CONTEXT = "DCE/";
	if(path.indexOf("DCI")==-1)
		DCI_CONTEXT = "DCI/";
	
	if(path.indexOf("DCE")>-1 || path.indexOf("DCI")>-1)
		MAIN_CONTEXT = "../";
	
	HashMap<String, String> paths = new HashMap();
	HashMap<String, String> pathconstant = new HashMap();
	
	pathconstant.put("METRO_PATH","js/metro/");
	pathconstant.put("CSS_API","css/api/");
	pathconstant.put("CSS_PORTAL","css/portal/");
	pathconstant.put("JS_API","js/");
	pathconstant.put("JS_PORTAL","js/portal/");
	
	pathconstant.put("BO_CONTEXT_PATH","TPS/bo/");
	pathconstant.put("BO_JS_PATH","js/TPS/bo/");
	pathconstant.put("BO_CSS_PATH","css/bo/");
	
	pathconstant.put("BD_CONTEXT_PATH","TPS/bd/");
	pathconstant.put("BD_JS_PATH","js/TPS/bd/");
	pathconstant.put("BD_CSS_PATH","css/bd/");
	
	pathconstant.put("IE_CONTEXT_PATH","ie/");
	pathconstant.put("IE_JS_PATH","js/ie/");
	pathconstant.put("IE_CSS_PATH","css/ie/");
	
	pathconstant.put("TC_CONTEXT_PATH","tc/");
	pathconstant.put("TC_JS_PATH","js/tc/");
	pathconstant.put("TC_CSS_PATH","css/tc/");
	
	
	pathconstant.put("FT_CONTEXT_PATH","TPS/ft/");
	pathconstant.put("FT_JS_PATH","js/TPS/ft/");
	pathconstant.put("FT_CSS_PATH","css/ft/");
	
	pathconstant.put("FTS_CONTEXT_PATH","TPS/fts/");
	pathconstant.put("FTS_JS_PATH","js/TPS/fts/");
	pathconstant.put("FTS_CSS_PATH","css/fts/");
	
	pathconstant.put("SED_CONTEXT_PATH","TPS/sed/");
	pathconstant.put("SED_JS_PATH","js/TPS/sed/");
	
	pathconstant.put("FREE_CONTEXT_PATH","TPS/free/");
	pathconstant.put("FREE_JS_PATH","js/TPS/free/");
	pathconstant.put("IMAGE_PATH","images/");
	
	//final String DCE_CONTEXT = "DCE/";
	
	//String JQUERY_CDN = "//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js";
	//String BOOTSTRAP_CDN = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js";
	//String BOOTSTRAP_CSS_CDN = "//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css";
	
	final String METRO_PATH 		=   pathconstant.get("METRO_PATH");
	final String CSS_API 			= 	pathconstant.get("CSS_API");
	final String CSS_PORTAL 		= 	pathconstant.get("CSS_PORTAL");
	final String JS_API 			= 	pathconstant.get("JS_API");
	final String JS_PORTAL 			= 	pathconstant.get("JS_PORTAL");
	
	final String BO_CONTEXT_PATH 	= 	pathconstant.get("BO_CONTEXT_PATH");
	final String BO_JS_PATH 		= 	pathconstant.get("BO_JS_PATH");
	final String BO_CSS_PATH 		= 	pathconstant.get("BO_CSS_PATH");
	
	final String IE_CONTEXT_PATH 	= 	pathconstant.get("IE_CONTEXT_PATH");
	final String IE_JS_PATH 		= 	pathconstant.get("IE_JS_PATH");
	final String IE_CSS_PATH 		=	pathconstant.get("IE_CSS_PATH");
	
	final String TC_CONTEXT_PATH 	= 	pathconstant.get("TC_CONTEXT_PATH");
	final String TC_JS_PATH 		= 	pathconstant.get("TC_JS_PATH");
	final String TC_CSS_PATH 		= 	pathconstant.get("TC_CSS_PATH");
	
	final String BD_CONTEXT_PATH 	= 	pathconstant.get("BD_CONTEXT_PATH");
	final String BD_JS_PATH 		= 	pathconstant.get("BD_JS_PATH");
	final String BD_CSS_PATH 		= 	pathconstant.get("BD_CSS_PATH");
	
	final String FT_CONTEXT_PATH 	= 	pathconstant.get("FT_CONTEXT_PATH");
	final String FT_JS_PATH 		= 	pathconstant.get("FT_JS_PATH");
	final String FT_CSS_PATH 		= 	pathconstant.get("FT_CSS_PATH");
	
	final String FTS_CONTEXT_PATH 	= 	pathconstant.get("FTS_CONTEXT_PATH");
	final String FTS_JS_PATH 		= 	pathconstant.get("FTS_JS_PATH");
	final String FTS_CSS_PATH 		= 	pathconstant.get("FTS_CSS_PATH");
	
	final String SED_CONTEXT_PATH	=	pathconstant.get("SED_CONTEXT_PATH");
	final String SED_JS_PATH 		= 	pathconstant.get("SED_JS_PATH");
	
	final String FREE_CONTEXT_PATH	=	pathconstant.get("FREE_CONTEXT_PATH");
	final String FREE_JS_PATH		=	pathconstant.get("FREE_JS_PATH");
	final String IMAGE_PATH			=	pathconstant.get("IMAGE_PATH");

	
	
	//System.out.println("=====ic============"+loggedonLid);
	
	if(PortalUtility.isNull(loggedonLid) && PortalUtility.isNull(LogonName)){
		lid = SecurityPortalService.doPortalLogin(false, request);
		LidHandler.setLoggedInLid(lid,request);
		session.setAttribute("LogonName", SecurityPortalService.CONNECTION_CACHE);
	}else{
		lid = loggedonLid;
	}
	
	System.out.println("LID === "+loggedonLid);
	
	if(PortalUtility.isNull(lid)){ //even after authentication system could not generated lid. In that case, the webservices may be down
		isAxisOn = false;
	}
	if(PortalUtility.isNull(session.getAttribute("LanguageCode")))
		session.setAttribute("LanguageCode", Language.DEFAULT);
	
	Language LanguageCode = (Language)session.getAttribute("LanguageCode");
	
	ResourceBundle resourceBundle = new ResourceBundle(request);
	//To set headers ex: some cases we need to pass lid as header
	//BindingProvider headerBindingProvider  = (BindingProvider) PortalService.getWSClient();
	//Add Header for every time you are calling a service method
	//SetHeader.addHeader(headerBindingProvider, lid, LanguageCode.getLanguageCode());
	

	String realPpath = (application.getRealPath("")).replace("\\", "/");
	String reportImagePath = realPpath+"/reportImage/";		

	final String PHONE_MASK_FORMAT = "(+234) 0@@@ @@@ @@@@";
	final String PHONE_MASK_FORMAT_CHAR = "@";
	final String TIN_IGNORE_PATTERN = "-0001";
			
%>

<%//@include file="includesearch.jsp"%>