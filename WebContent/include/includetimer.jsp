<link rel="stylesheet" href="css/portal/timer.css" async>
<script type="text/javascript" src="js/portal/timer.js"></script>
<body onload="countdown(year,month,day,hour,minute)">
<div id="form">
    <div class="numbers" id="count2" style="position: absolute; top: 10px; height: 60px; padding: 15px 0 0 10px; background-color: #000000; z-index: 20;"></div>
    <img src="images/bkgdimage.gif" class="background" style="position: absolute; left: 69px; top: 12px;"/>
    <img src="images/line.jpg" class="line" style="position: absolute; left: 69px; top: 40px;"/> 
    <div class="numbers" id="dday" style="position: absolute; left: 69px; top: 21px;" ></div>
	
    <img src="images/bkgdimage.gif" class="background" style="position: absolute; left: 141px; top: 12px;"/>
    <img src="images/line.jpg" class="line" style="position: absolute; left: 141px; top: 40px;"/>
    <div class="numbers" id="dhour" style="position: absolute; left: 141px; top: 21px;" ></div>
	
    <img src="images/bkgdimage.gif" class="background" style="position: absolute; left: 213px; top: 12px;"/>
    <img src="images/line.jpg" class="line" style="position: absolute; left: 213px; top: 40px;"/>
    <div class="numbers" id="dmin" style="position: absolute; left: 213px; top: 21px;" ></div>
	
    <img src="images/bkgdimage.gif" class="background" style="position: absolute; left: 285px; top: 12px;"/>
    <img src="images/line.jpg" class="line" style="position: absolute; left: 285px; top: 40px;"/>
    <div class="numbers" id="dsec" style="position: absolute; left: 285px; top: 21px;" ></div>
	
    <div class="title" id="days" style="position: absolute; left: 66px; top: 73px;" >Days</div>
    <div class="title" id="hours" style="position: absolute; left: 138px; top: 73px;" >Hours</div>
    <div class="title" id="minutes" style="position: absolute; left: 210px; top: 73px;" >Minutes</div>
    <div class="title" id="seconds" style="position: absolute; left: 282px; top: 73px;" >Seconds</div>
</div>
</body>