<!-- Common -->
<link rel="stylesheet" href="<%=CSS_API%>bootstrap.min.css" async>
<link rel="stylesheet" href="<%=CSS_API%>template.css" async>
<link rel="stylesheet" href="<%=CSS_API%>font-awesome.min.css" async>
<link rel="stylesheet" href="<%=CSS_API%>bootstrap-datetimepicker.min.css"  async/>
<link rel="stylesheet" href="<%=CSS_API%>pikaday.css"  async/>
<link rel="stylesheet" href="<%=CSS_API%>jquery-ui.1.11.4.css"  async/>
<link rel="stylesheet" href="<%=CSS_API%>bootstrap-dialog.min.css" async>
<link rel="stylesheet" href="<%=CSS_API%>dataTables.bootstrap.min.css" async>
<link href='//fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css' async>
<link rel="stylesheet" href="<%=CSS_API%>features-quick-link.css" async>
<link rel="stylesheet" href="<%=CSS_API%>font-awesome-animation.min.css" async>
<link rel="stylesheet" href="<%=CSS_API%>remodal.css" async>
<link rel="stylesheet" href="<%=CSS_API%>jquery.growl.css" async>
<link rel="stylesheet" href="<%=CSS_API%>remodal-default-theme.css" async>
<link rel="stylesheet" href="<%=CSS_PORTAL%>Custom.css" async>
<link rel="stylesheet" href="<%=CSS_API%>jquery-ui.css" async>