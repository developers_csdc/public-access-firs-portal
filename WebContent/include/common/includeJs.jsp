
		<script type="text/javascript" src="<%=JS_API%>jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="<%=JS_API%>bootstrap.min.js" ></script>
		
		<%--<script type="text/javascript" src="<%=JS_API%>jquery-ui.1.11.4.js" ></script> --%>
		<script type="text/javascript" src="<%=JS_API%>bootstrap-dialog.min.js" ></script>
		<script type="text/javascript" src="<%=JS_API%>jquery.growl.js" ></script>
		<script type="text/javascript" src="<%=JS_API%>jquery-ui.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>notification.js"></script>
		
		<script type="text/javascript" src="<%=JS_API%>jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<%=JS_API%>dataTables.bootstrap.min.js"></script>
		<%-- <script type="text/javascript" src="<%=JS_API%>bootstrap-filestyle.js"> </script> --%>
		<script type="text/javascript" src="<%=JS_API%>moment.min.js"></script>
		<script type="text/javascript" src="<%=JS_API%>remodal.min.js" ></script>
		
		<script type="text/javascript" src="<%=JS_PORTAL%>forgotPassword.js" ></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>portal.js" ></script>
 		<script type="text/javascript" src="<%=JS_PORTAL%>logon.js"></script>
 		<%-- <script type="text/javascript" src="<%=JS_PORTAL%>myProfile.js"></script>
 		<script type="text/javascript" src="<%=JS_PORTAL%>userRegistration.js"></script> --%>
		
		<script type="text/javascript" src="<%=JS_PORTAL%>captcha.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>miniCaptcha.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>template.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>validator.js" ></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>downloadTCC.js" ></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>TccFormValidation.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>TccFormSubmission.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>SearchTCC.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>SearchReceipt.js"></script>		
		<script type="text/javascript" src="<%=JS_PORTAL%>registration.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>corporateProfile.js"></script>
		<script type="text/javascript" src="<%=JS_API%>jquery.autocomplete.min.js"></script>
		
	
		<script type="text/javascript" src="<%=JS_API%>bootstrap-datetimepicker.min.js"></script>
		
		<script type="text/javascript" src="<%=JS_PORTAL%>tradenature-autocomplete.js"></script>
		<script type="text/javascript" src="<%=JS_PORTAL%>TINRevalidation.js"></script>
