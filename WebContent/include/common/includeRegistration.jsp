<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
	<!-- Modal for Registration -->
	<div class="row">
		<div class="col-lg-offset-3 col-lg-6">
			<div class="modal fade" id="myModal" role="dialog">
   				<div class="modal-dialog">
					<div class="modal-content" style="border-radius:0px;">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-12">
				          			<img alt="" class="img-responsive" src="images/FIRS-Logo.png" height="50px" width="280px">
				          		</div>
				          	</div>
				          </div>
				        </div>
				        <div class="modal-body">
				          <p class="text-info-custom"><b>Please select the type of Registration</b></p>
				         <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Individual">
				          			<img class="custom_icon" alt="" src="images/individual.png">
				          			<h5 class="text-info">Individual</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Corporate">
				          			<img class="custom_icon" alt="" src="images/corporate.png">
				          			<h5 class="text-info">Corporate</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Forex">
				          			<img class="custom_icon" alt="" src="images/forex.png">
				          			<h5 class="text-info">Forex</h5>
				          			</a>
				          		</div>
				          	</div>
				          </div> 
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default custom_btn" data-dismiss="modal">Close</button>
				        </div>
		      		</div>
		      	</div>
		      </div>
		</div>
	</div>
</body>
</html>