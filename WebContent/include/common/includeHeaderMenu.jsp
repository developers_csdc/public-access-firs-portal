<%
	String currentPageName = request.getParameter("cmd")!=null?request.getParameter("cmd"):"portal";
%>
	
	<nav class="navbar navbar-default custom_navbar" style="z-index:1;">
		
		 	<div class="navbar-header">
		      <button type="button" style="background-color: transparent;" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>
		      
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		      <!-- Class returned by the server -->
		        <li  class='<%=currentPageName.equals("portal")?"custom_active":""%>'
		        ><a href="portal">Home</a></li>
		        <%-- <li class="dropdown">
		          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Products<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">Page 1-1</a></li>
		            <li><a href="#">Page 1-2</a></li>
		            <li><a href="#">Page 1-3</a></li>
		          </ul>
		        </li>
		        <li><a href="#">Page 3</a></li>
				<li><a href="#">Contact</a></li>
				<%if(session.getAttribute("PeopleRSN")!=null && ((Integer)(session.getAttribute("PeopleRSN")))!=-1) {%>
					<li data-ma5-order="ma5-li-8"><a href="portal?cmd=myProfile"><i class="fa fa-user"></i> &nbsp; <%=resourceBundle.getText("LABEL_WELCOME_PAGE_MENU_MY_PROFILE") %></a></li>
				<%} %>
				<li>
					<%if (!(isRegisteredUser || isAdmin)) {%> <a href="portal?cmd=logon"
					class="linkText"><%=resourceBundle.getText("LABEL_MENU_LOGIN") %></a>
					<%}else{ %> <a href="logoff.jsp" class="linkText"><%=resourceBundle.getText("LABEL_MENU_LOGOFF") %></a>
					<%} %>
				</li> --%>
				
				<%--   <li class='<%=currentPageName.equals("tccapplicationform")?"custom_active":""%>'
				 ><a href="portal?cmd=tccapplicationform">Apply TCC</a></li>--%>
				
				<li class='<%=currentPageName.equals("registration")?"custom_active":""%>'
				><a href="" data-toggle="modal" data-target="#myModal" style="color: white;text-decoration:none;border:0;outline:none;">Registration</a></li>
				
				<%--<li class='<%=currentPageName.equals("downloadtcc")?"custom_active":""%>'
				><a href="portal?cmd=downloadtcc">Download Receipt</a></li> --%>
				
				<li class='<%=currentPageName.equals("searchreceipt")?"custom_active":""%>'
				><a href="portal?cmd=searchreceipt">Verify Local Receipt</a></li>
				
				<li class='<%=currentPageName.equals("searchforeignreceipt")?"custom_active":""%>'
				><a href="portal?cmd=searchforeignreceipt">Verify Foreign Receipt</a></li>
				
				<li  class='<%=currentPageName.equals("searchtcc")?"custom_active":""%>'
				><a href="portal?cmd=searchtcc">Verify TCC</a></li>
				
				<li  class=''><a target="_blank" href="https://www.scan.me/download/">QR Reader</a></li>
				
				
		      </ul>
		   	<%--   <ul class="nav navbar-nav navbar-right">
		   	  	<li style="line-height:3;">
					<%=resourceBundle.getText("LABEL_MENU_LANGUAGE") %>
					<select onchange="toggleLanguageCode(this.value);">
						<option <%if(LanguageCode.getLanguageCode() == 1) {%> selected="selected" <%}%> value="1">English</option>
						<option <%if(LanguageCode.getLanguageCode() == 2) {%> selected="selected" <%}%> value="2">Fran�ais</option>
					</select>
				</li>
		        
		        <li><a href="#"><span class=""></span></a></li>
		        <li><a href="#"><span class=""></span></a></li>
		      </ul> --%>
		    </div>
		
	</nav>

	<br class="clearfix" />
	
	
	