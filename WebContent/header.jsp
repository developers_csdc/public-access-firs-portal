<%@ page import="java.util.*, com.org.firs.*" language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<script>
 var contextPath = "/FIRSDEMO/";
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>FIRS | TCC Application Portal</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		

		<!-- Common -->
<link rel="stylesheet" href="css/api/bootstrap.min.css" async>
<link rel="stylesheet" href="css/api/template.css" async>
<link rel="stylesheet" href="css/api/font-awesome.min.css" async>
<link rel="stylesheet" href="css/api/bootstrap-datetimepicker.min.css"  async/>
<link rel="stylesheet" href="css/api/pikaday.css"  async/>
<link rel="stylesheet" href="css/api/jquery-ui.1.11.4.css"  async/>
<link rel="stylesheet" href="css/api/bootstrap-dialog.min.css" async>
<link rel="stylesheet" href="css/api/dataTables.bootstrap.min.css" async>
<link href='//fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css' async>
<link rel="stylesheet" href="css/api/features-quick-link.css" async>
<link rel="stylesheet" href="css/api/font-awesome-animation.min.css" async>
<link rel="stylesheet" href="css/api/remodal.css" async>
<link rel="stylesheet" href="css/api/jquery.growl.css" async>
<link rel="stylesheet" href="css/api/remodal-default-theme.css" async>
<link rel="stylesheet" href="css/portal/Custom.css" async>
<link rel="stylesheet" href="css/api/jquery-ui.css" async>
		
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js" ></script>
		
		
		<script type="text/javascript" src="js/bootstrap-dialog.min.js" ></script>
		<script type="text/javascript" src="js/jquery.growl.js" ></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
		<script type="text/javascript" src="js/portal/notification.js"></script>
		
		<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
		
		<script type="text/javascript" src="js/moment.min.js"></script>
		<script type="text/javascript" src="js/remodal.min.js" ></script>
		
		<script type="text/javascript" src="js/portal/forgotPassword.js" ></script>
		<script type="text/javascript" src="js/portal/portal.js" ></script>
 		<script type="text/javascript" src="js/portal/logon.js"></script>
 		
		
		<script type="text/javascript" src="js/portal/captcha.js"></script>
		<script type="text/javascript" src="js/portal/miniCaptcha.js"></script>
		<script type="text/javascript" src="js/portal/template.js"></script>
		<script type="text/javascript" src="js/portal/validator.js" ></script>
		<script type="text/javascript" src="js/portal/downloadTCC.js" ></script>
		<script type="text/javascript" src="js/portal/TccFormValidation.js"></script>
		<script type="text/javascript" src="js/portal/TccFormSubmission.js"></script>
		<script type="text/javascript" src="js/portal/SearchTCC.js"></script>
		<script type="text/javascript" src="js/portal/SearchReceipt.js"></script>		
		<script type="text/javascript" src="js/portal/registration.js"></script>
		<script type="text/javascript" src="js/portal/corporateProfile.js"></script>
		<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
		
	
		<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
		
		<script type="text/javascript" src="js/portal/tradenature-autocomplete.js"></script>
		<script type="text/javascript" src="js/portal/TINRevalidation.js"></script>

		

</head>
<body>
	<!-- Modal for Registration -->
	<div class="row">
		<div class="col-lg-offset-3 col-lg-6">
			<div class="modal fade" id="myModal" role="dialog">
   				<div class="modal-dialog">
					<div class="modal-content" style="border-radius:0px;">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-12">
				          			<img alt="" class="img-responsive" src="images/FIRS-Logo.png" height="50px" width="280px">
				          		</div>
				          	</div>
				          </div>
				        </div>
				        <div class="modal-body">
				          <p class="text-info-custom"><b>Please select the type of Registration</b></p>
				         <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Individual">
				          			<img class="custom_icon" alt="" src="images/individual.png">
				          			<h5 class="text-info">Individual</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Corporate">
				          			<img class="custom_icon" alt="" src="images/corporate.png">
				          			<h5 class="text-info">Corporate</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Forex">
				          			<img class="custom_icon" alt="" src="images/forex.png">
				          			<h5 class="text-info">Forex</h5>
				          			</a>
				          		</div>
				          	</div>
				          </div> 
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default custom_btn" data-dismiss="modal">Close</button>
				        </div>
		      		</div>
		      	</div>
		      </div>
		</div>
	</div>
</body>
</html>
</head>
	<body>
		<div class="container-fluid">
			<div class="row" style="height:80px;">
				<div class="col-md-6 col-lg-6 col-sm-6">
				<a href="index.jsp" ><img class="img-responsive" src="images/FIRS-Logo.png"/ style="padding:5px 5px 5px 5px;margin-top:5px;"></a>
				</div>
				<div class="col-md-6">
					<h1></h1>
				</div>
			</div>
		</div>
		
		<div class="container-fluid" style="height:60px;margin-top:10px;">
			
				
	
	<nav class="navbar navbar-default custom_navbar" style="z-index:1;">
		
		 	<div class="navbar-header">
		      <button type="button" style="background-color: transparent;" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>                        
		      </button>
		      
		    </div>
		    <div class="collapse navbar-collapse" id="myNavbar">
		      <ul class="nav navbar-nav">
		      <!-- Class returned by the server -->
		        <li  class='custom_active'
		        ><a href="index.jsp">Home</a></li>
		        		
				<li class=''
				><a href="" data-toggle="modal" data-target="#myModal" style="color: white;text-decoration:none;border:0;outline:none;">Registration</a></li>
				
				<li class=''
				><a href="SearchReceipt.jsp">Verify Receipt</a></li>
				
				
				<li  class=''
				><a href="SearchTCC.jsp">Verify TCC</a></li>
				
				<li  class=''><a target="_blank" href="https://www.scan.me/download/">QR Reader</a></li>
				
				
		      </ul>
		   	
		    </div>
		
	</nav>

	<br class="clearfix" />
	
		</div>		

		<div>