<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="com.csdcsystems.amanda.util.SetJAXBElementColumn"%>
<%@page import="org.apache.commons.lang.RandomStringUtils"%>
<%@page import="org.apache.commons.lang.time.DateUtils"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidOperator"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsSearchCriteria"%>
<%@page import="com.csdcsystems.amanda.util.SetHeader"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@ page language="java" buffer="8kb" import="java.util.*,com.csdcsystems.amanda.common.*,java.sql.SQLException" 
	autoFlush="true" isThreadSafe="true" isErrorPage="false" session="true" contentType="text/html; charset=UTF-8"%>
<%@ include file="include/includeconstant.jsp"%>
<%! 
	/* ESS:Samarjit This method will Flush OTP and TIME STAMP from session Obj */
	private void flushOTPData(HttpSession session ){
		boolean isOTPDataFlused = false;
		session.removeAttribute("OTP");
		session.removeAttribute("OTPTIMESTAMP");
		System.out.println("OTP Data has been removed.");
	}
%>
<%
	log.info("Entering "+pageName);
	
	//But it takes the email address configured during the SMTP configuration.
	String fromEmailAddress = "eservices@firs.gov.ng"; 
	String CCemailAddress = "";
	
	String command 		= PortalUtility.getString(request.getParameter("command"));
	String randomPassword = PortalUtility.getString(request.getParameter("OTP"));
	String newPassword 	= PortalUtility.getString(request.getParameter("confirmPassword"));
	String peopleName = "";
	
	boolean peopleExists = false;
	boolean flag = false;
	boolean isTINValid = false;
	boolean isAccountActive = false;
	boolean isTempPasswordUsedByUser = false;
	
	Map<Integer, String> PeopleInfoInMap = new HashMap<Integer, String>();
	JSONObject result 	= new JSONObject();
	
	final int NEW_EMAIL_INFO_CODE = 5026;
	final int EMAIL_INFO_CODE = 5025;
	
	String TIN = PortalUtility.getString(request.getParameter("TIN"));
	
	WsPeople people = new WsPeople();
	WsPeopleInfo[] peopleInfos = {};
	
	if(TIN != null && TIN != ""){
		people = PortalService.getPeople(PortalService.getWSClient(),lid,"LicenceNumber",TIN);
		if(!PortalUtility.isNull(people)){
			isAccountActive = PortalUtility.getString(people.getSmsFlag()).equals("Y") ? true : false;
			isTempPasswordUsedByUser = PortalUtility.getString(people.getInternetAccess()).equals("A") ? true : false;
			peopleName = PortalUtility.getString(people.getOrganizationName());
			peopleInfos = PortalService.getWSClient().getPeopleInfo(lid, PortalUtility.getInt(people.getPeopleRSN()));	
		}
	}
	
	if(!PortalUtility.isNull(peopleInfos) && !PortalUtility.isNull(people)){
		PeopleInfoInMap = PortalService.getPeopleInfoInMap(peopleInfos);
	}
	
	if(command.equals("peopleChecK")){
		peopleExists = PortalService.checkPeople(PortalService.getWSClient(),lid,"LicenceNumber",TIN);
		if(!PortalUtility.isNull(peopleExists)){
			if(peopleExists){
				if(isAccountActive){
					boolean mailExist = false;
					boolean isMailSentToPeople = false;
					
					String toEmailAddress = "";
					String randomOTP = RandomStringUtils.random(4, false, true);
					isTINValid = true;
					
					System.out.println(randomOTP);
					
					/* Flusing OTP Data */
					flushOTPData(session);
					session.setAttribute("OTP", randomOTP);
					session.setAttribute("OTPTIMESTAMP", new Date());
					System.out.print(randomOTP);
					
					if(!PortalUtility.isNull(people)){
						String peopleEmailInInfo = PortalUtility.getString(PeopleInfoInMap.get(EMAIL_INFO_CODE));
						//String email = "samarjit@esspl.com";
						
						if(peopleEmailInInfo !=""){
							toEmailAddress = peopleEmailInInfo;
						}else{
							toEmailAddress = PortalUtility.getString(people.getEmailAddress());
						}
						
						if(toEmailAddress!=null && toEmailAddress!=""){
							mailExist=true;
						}
							
						if(mailExist){
							//toEmailAddress = "samarjit@esspl.com";
							isMailSentToPeople = PortalService.getWSClient().sendHtmlEmail(lid, toEmailAddress, fromEmailAddress, CCemailAddress, "OTP for password reset.", "Dear "+peopleName+",<br/><br/>OTP for resetting your password is given below:<br/><br/>OTP : <b style='color:red;'>"+randomOTP+"</b><br><br>"+
									"<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."+
									"<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."+
									"The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."+
									"The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."+
									"Before opening and accessing the attachment, if any, please check and scan for virus.</div>");
						}
						
						System.out.println(isMailSentToPeople);
						result.put("isMailSentToPeople",isMailSentToPeople);
						result.put("mailExist",mailExist);
					}
					result.put("flag",true);
				}else{
					result.put("errorMessage","Your Account is Not active. Please activate your account");
				}
				
			}else{
				result.put("errorMessage","No records found for this TIN! Please try Again");
			}
			
			result.put("isTINValid",isTINValid);
		}
		
	}else if(command.equals("checkEmailAndSendOTP")){
		
		
	}else if(command.equals("checkOTP")){
			boolean isOTPValidFlag = false;
		
		   	if(!PortalUtility.isNull(session.getAttribute("OTP")) && !PortalUtility.isNull(session.getAttribute("OTPTIMESTAMP"))){
			
				String OTP = PortalUtility.getString(String.valueOf(session.getAttribute("OTP")));
				Date OTPTimeStamp = (Date)session.getAttribute("OTPTIMESTAMP");
				Date currentTimeStamp = new Date();
				
				long duration  = currentTimeStamp.getTime() - OTPTimeStamp.getTime();
				long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
				
				if(OTP.equals(randomPassword) && diffInMinutes<10){
					isOTPValidFlag = true;
			   }else{
				   	result.put("ErrorMsg", "Invalid OTP!");
			   }
			}
		   	result.put("isOTPValidFlag", isOTPValidFlag);
		   	
	}else if(command.equals("resendOTP")){
		
		String randomOTP = RandomStringUtils.random(4, false, true);
		System.out.println(randomOTP);
		
		/* Flusing OTP Data */
		flushOTPData(session);
		session.setAttribute("OTP", randomOTP);
		session.setAttribute("OTPTIMESTAMP", new Date());
		
		String toEmailAddress = "";
		boolean isMailSentToPeople = false;
		
		String peopleEmailInInfo = PortalUtility.getString(PeopleInfoInMap.get(EMAIL_INFO_CODE));
		//String email = "samarjit@esspl.com";
		
		if(peopleEmailInInfo != ""){
			toEmailAddress = peopleEmailInInfo;
		}else{
			toEmailAddress=PortalUtility.getString(people.getEmailAddress());
		}
			
		//toEmailAddress ="samarjit@esspl.com";
		//String email = "samarjit@esspl.com";
		//String OTP = PortalUtility.getString(String.valueOf(session.getAttribute(email)));
		isMailSentToPeople = PortalService.getWSClient().sendHtmlEmail(lid, toEmailAddress, fromEmailAddress, CCemailAddress, "OTP for password reset.", "Dear "+peopleName+",<br/><br/>OTP for resetting your password is given below:<br/><br/>OTP : <b style='color:red;'>"+randomOTP+"</b><br><br>"+
				"<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."+
				"<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."+
				"The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."+
				"The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."+
				"Before opening and accessing the attachment, if any, please check and scan for virus.</div>");
		
		System.out.println(isMailSentToPeople);
		flag = true;
		result.put("result", flag);
		result.put("isMailSentToPeople", isMailSentToPeople);
	}else if(command.equals("resetPassword")){
		/* Flusing OTP Data */
		flushOTPData(session);
		
		String toEmailAddress = "";
		boolean isMailSentToPeople = false;
		boolean isPasswordUpdated = false;
		
		//toEmailAddress = "samarjit@gmail.com";
		String peopleEmailInInfo = PortalUtility.getString(PeopleInfoInMap.get(EMAIL_INFO_CODE));
		//String email = "samarjit@esspl.com";
		if(peopleEmailInInfo != ""){
			toEmailAddress = peopleEmailInInfo;
		}else{
			toEmailAddress = PortalUtility.getString(people.getEmailAddress());
		}
		
		people.setInternetQuestion(PortalUtility.encrypt(newPassword));
		/* Samarjit 27032018:Updating INTERNET ACCESS,This will enable user to login using updated Password(i.e password set through forgotPassword window) */  
		if(!isTempPasswordUsedByUser) people.setInternetAccess("A");
		isPasswordUpdated = PortalService.getWSClient().updatePeople(lid, people);
		 
		//Set people new password
		//boolean flag = client.updatePeopleInternetPassword(lid, peopleList[0].getPeopleRSN(), newPassword);
		/*Sending mail for successful password change*/
		//toEmailAddress = "samarjit@esspl.com";
		isMailSentToPeople = PortalService.getWSClient().sendHtmlEmail(lid,toEmailAddress, fromEmailAddress, CCemailAddress, "Password Changed Successfully.", "Dear "+peopleName+",<br/><br/>"+
				    "You have successfully changed your password. Please use this password  to login from now on wards.Report the situation immediately in case of any suspicion.<br/><br/>"+
					"<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."+
					"<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."+
					"The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."+
					"The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."+
					"Before opening and accessing the attachment, if any, please check and scan for virus.</div>");
		flag = true;
		result.put("result", flag);
		result.put("isMailSentToPeople", isMailSentToPeople);
		result.put("isPasswordUpdated", isPasswordUpdated);
	}
	
	out.print(result.toString());
	log.info("File Name:" + pageName + " Ends");
	log.info("forgotPassword_ajax ..response Details::"+ result.toString());
%>
