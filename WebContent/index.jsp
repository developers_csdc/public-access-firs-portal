
<%@include file= "header.jsp" %>
<body>

	<!-- Modal for Registration -->
	<div class="row">
		<div class="col-lg-offset-3 col-lg-6">
			<div class="modal fade" id="myModal" role="dialog">
   				<div class="modal-dialog">
					<div class="modal-content" style="border-radius:0px;">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-12">
				          			<img alt="" class="img-responsive" src="images/FIRS-Logo.png" height="50px" width="280px">
				          		</div>
				          	</div>
				          </div>
				        </div>
				        <div class="modal-body">
				          <p class="text-info-custom"><b>Please select the type of Registration</b></p>
				         <div class="conatainer-fluid">
				          	<div class="row">
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Individual">
				          			<img class="custom_icon" alt="" src="images/individual.png">
				          			<h5 class="text-info">Individual</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Corporate">
				          			<img class="custom_icon" alt="" src="images/corporate.png">
				          			<h5 class="text-info">Corporate</h5>
				          			</a>
				          		</div>
				          		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
				          			<a href="FirsRegistration.jsp?applicationType=Forex">
				          			<img class="custom_icon" alt="" src="images/forex.png">
				          			<h5 class="text-info">Forex</h5>
				          			</a>
				          		</div>
				          	</div>
				          </div> 
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default custom_btn" data-dismiss="modal">Close</button>
				        </div>
		      		</div>
		      	</div>
		      </div>
		</div>
	</div>
</body>
</html>
	</head>
	<body>
	
			
		</div>		

		<div>
			<div id="container-fluid">
						<div class="container-fluid">
		<div class="row" style="margin:">
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
				<h3 style="color:#ab0000;">TCC Application Portal</h3>
				<h5>Welcome to FIRS TCC Application Portal</h5>
				<div class="row">
					<!-- <div class="col-lg-4 col-md-4 col-sm-4  text-center">
						<a href="portal?cmd=tccapplicationform"><img class="custom_icon" alt="" src="images/app.png">
						<p><h6 class="custom_icontext">Apply for TCC</h6></p>
						</a>
					</div> -->
					
					<div class="col-lg-4 col-md-4 col-sm-4  text-center">
						<a href="" data-toggle="modal" data-target="#myModal" style="text-decoration:none;border:0;outline:none;">
						<img class="custom_icon" alt="" src="images/app.png">
						<p><h6 class="custom_icontext"><b>Register to get your TCC account</b></h6></p>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4  text-center">
						<a href="SearchReceipt.jsp"><img class="custom_icon" alt="" src="images/download1.jpg">
						<p><h6 class="custom_icontext"><b>Verify Receipt</b></h6></p>
						</a>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 text-center">
						<a href="SearchTCC.jsp"><img class="custom_icon" alt="" src="images/verify.png">
						<p><h6 class="custom_icontext"><b>Verify TCC</b></h6></p>
						</a>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12  text-center">
						<!-- <a href="portal?cmd=searchreceipt"><img class="custom_icon" alt="" src="images/download1.png">
						<p><h6 class="custom_icontext">Verify Receipt</h6></p>
						</a> -->
						<!--  Added by samarjit 
						<div class="text-center alert alert-info" style="border-radius:0px;">
							<span>Account Login</span>	
						</div>-->
						<!-- <div class="loader-section hide" id="logon-loader-section">
							<p>Please wait</p>
							<p>Logging you in..<i class="custom-loader"></i></p>
						</div>-->
						<div id="logon-form-section" class="">
				            <div class="container-fluid">
					            <div>
								 	<h3 style="width :100px;text-decoration: underline;" class="pull-left">Login</h3>	
					            </div>	
				            	<div class="row">
				            		<div class="col-xs-7 col-sm-8 col-xs-offset-5 col-sm-offset-4">
				            		    <%
				            		    String message = (String)request.getAttribute("message");
				            		    if(message!=null){
				            		    	System.out.println("Error " + message);
				            		    %>
				            		    <div class="alert alert-danger" style="border-radius:0; font-weight:bold"><%=request.getAttribute("message") %>. <!--Please Enter Valid Credentials and Try Again!--></div>
				            		    <%} %>
				            		    
				            			<!-- <div id="registration-message" class="pull-left">
						                	<span class="custom-question"><b>Haven't Registered?
						                	<a href="" data-toggle="modal" data-target="#myModal" class="custom-link" id="signUpBtnTrigger" >Register Here</a></b></span>
						               	</div> -->
						               	<br/>
				            		</div>
				            	</div>
				            </div>
							<div class="container-fluid">
							<form name="LoginForm" action="authenticationController" method="post">
				                <div class="form-horizontal" id="login-section">
				                	<div id="login_section">
					                    <div class="form-group row" >
					                    	<div class="col-xs-12 hide" id="info-text-forgot-password">
				                    			<span class="info-text text-center"></span>
				                    		</div>
					                        <div class="col-xs-5 col-sm-4 ">
					                            <label class="text-large" id="">TIN</label>
					                        </div>
					                        <div class="col-xs-7 col-sm-8">
					                        	<input
					                                type="text"
					                                class="form-control input-sm custom_input"
					                                placeholder="Enter your TIN" name="username"
					           
					                                errorMessage="Enter a valid TIN"
					                                earia-describedby="basic-addon1"/>
					                            <div  style="left:20px;" class="error"></div>
					                        </div>
					                    </div>
					                    <div class="form-group row">
					                    	<div class="col-xs-5 col-sm-4">
					                    		<label class="text-large" id="">Password</label>
					                    	</div>
					                    	<div class="col-xs-7 col-sm-8">
					                    		<input type="password" class="form-control input-sm custom_input" placeholder="Enter Password" id=""
					                    			name="password" maxlength="20" errorMessage="Enter a valid password"/>
					                    		<div id="login_FP_error" style="left:20px;" class="error"></div>
					                    	</div>
					                    </div>
					                    <!-- <div class="row" style="margin-bottom:10px;">
					                    	<div class="col-xs-5 col-sm-4 col-md-4 col-lg-4 pull-left text-left" style="padding: 0px 0px 0px 0px;line-height: 28px;font-weight:600;">
												<span id="miniCaptcha_Search"></span>&nbsp&nbsp
					                    	</div>
					                    	<div class="col-xs-5 col-sm-6 col-md-6 col-lg-6 pull-left text-left">
											<input type="text" class="form-control input-sm custom_input"
												id="expression-result" placeholder="Enter Result here"
												maxlength="2" onkeypress="return onlyNos(event,this)">
										</div>
					                    	<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 pull-left text-left">
												<button onclick="resetBotBoot();" class="btn btn-primary custom_btn" style="height:30px;width:40px;">
													<span class="glyphicon glyphicon-repeat"></span>
												</button>
					                    	</div>
					                    </div> -->
					                  
					                    <span class="pull-right">
					                    	<a href="#" class="plain-link"
										id="forgotPasswordSectionTrigger" data-toggle="modal"
										data-target="#forgotPassword-modal" data-backdrop="static" data-keyboard="false"><u>Forgot Password</u></a>
									</span>
				                	</div>
				                    
				                    <div class="form-group row" id="login_buttons_section">
				                        <div class="col-xs-12 col-sm-8 col-lg-8 col-lg-offset-4 col-sm-offset-6" style="margin-top:10px;">
				                            <button type="reset" id=""  class="btn btn-primary custom_btn">Clear</button>                                
				                            <button id="" class="btn btn-primary custom_btn" name="command" value="login">Login</button>
				                            <button id="forgotPasswordLoginFrontPage" class=" hide btn btn-primary custom_btn">Submit</button>
				                        </div>
				                        <!-- <div class="col-xs-12  col-lg-10 col-lg-offset-2 col-sm-offset-2 col-sm-10">
				                            <div  class="info-text" id="">
				                   				<span class="text-info-custom">**First time login users should use the password provided by their Tax Office</span>
											</div> 
				                        </div> -->
				                    </div>
				                </div>
				                
				                </form>
				                
				            </div>
			            </div>
						
					</div>
				</div>
			</div>
		</div>
		<!-- added for test Purpose by Samarjit 03Mar -->
		<!-- Modal for Forgot Password -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<div class="modal fade" id="forgotPassword-modal" role="dialog">
    				<div class="modal-dialog">
						<div class="modal-content container-fluid" style="border-radius:0px;width: 85%;margin-left: 6%;">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
					          <h4 class="modal-title">Forgot Password</h4>
					        </div>
					        <div class="modal-body">
					        <form action="registrationControllerService" method="post">
								<div class="row">
									<div class="form-group">
		    							<label class="col-md-3 col-lg-3 col-sm-3" for="TIN">TIN:</label>
		    							<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="text" class="form-control input-sm custom_input"
											aria-role="" id="TIN"
											maxlength="150" name="ftin"
											placeholder="Enter your FIRS TIN" required>
		    							</div>
	 								</div>
	 								<div class="clearfix"></div><br>
	 								<div class="form-group">
		    							<label class="col-md-3 col-lg-3 col-sm-3" for="TIN">EMAIL:</label>
		    							<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="email" class="form-control input-sm custom_input"
											aria-role=""
											name="email"
											placeholder="Enter a Valid Email" required>
		    							</div>
	 								</div>
	 								
	 							</div>
	 							
					        </div>
					        <div class="modal-footer">
								<button type="submit" name="command" value="SendPassword" class="btn btn-default custom_btn">Submit</button>
								<button type="button" class="btn btn-default custom_btn" data-dismiss="modal" onclick="javascript:window.location.reload()">Close</button>
					        </div>
					        </form>
			      		</div>
			      	</div>
			      </div>
			</div>
		</div>
		<!-- Modal for Forgot Password - OTP Section -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<div class="modal fade" id="forgotPasswordOTP-modal" role="dialog">
    				<div class="modal-dialog">
						<div class="modal-content" style="border-radius:0px;width: 85%;margin-left: 6%;">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
					          <h4 class="modal-title">Forgot Password</h4>
					        </div>
					        <div class="modal-body">
							  <!-- <div class="row"">
							      <label class="col-lg-offset-1 col-lg-2 col-md-offset-1 col-md-2 col-sm-2 col-xs-2" for="otp">OTP:</label> 
							      <input type="text"
									class="form-control input-sm custom_input col-lg-4 col-md-4 col-sm-6 col-xs-6"
									id="otp" placeholder="Enter OTP" style="width: 45%">
									&nbsp&nbsp<span id=""></span>
								  <div class="error">2223232323</div>
							  </div> -->
							  <div class="row">
								<div class="form-group">
		    					<label class="col-md-3 col-lg-3 col-sm-3" for="TIN-Preview">TIN:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="text" class="form-control input-sm custom_input"
											aria-role="" id="TIN-Preview"
											maxlength="150" data-required="required" name=""
											class="biginput" disabled="disabled">
										<div class="error" id=""></div>
		    						</div>
	 							</div>
	 						</div>
	 						<div class="row" style="margin-top:15px;">
								<div class="form-group">
		    					<label class="col-md-3 col-lg-3 col-sm-3" for="trade_nature">OTP:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="text" class="form-control input-sm custom_input"
											aria-role="" id="OTP"
											maxlength="4" data-required="" name=""
											class="" placeholder="Enter Your OTP here" onkeypress="return onlyNosAndDecimal(event,this)">
										<div class="error" id=""></div>
		    						</div>
									<div class="col-lg-3 col-md-3 col-sm-3" id=""
										style="font-size: 15px; font-family: monospace; font-weight: 600; color: rgba(142, 17, 17, 0.9);">
										<span id="counter"></span><span id="msg"></span>
									</div>
								</div>
	 						</div>
							 
							  <div class="row" style="margin-top:5px;">
							      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								      <span class="text-info-custom">**Enter the OTP sent to your email.</span>
							      </div>
							  </div>
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default custom_btn" id="resend-OTP">Re-Send OTP</button>	
					          <button type="button" class="btn btn-default custom_btn" id="OTP-Submit">Submit</button>
					        </div>
			      		</div>
			      	</div>
			      </div>
			</div>
		</div>
		<!-- Modal for Change of Password -->
		<div class="row">
			<div class="col-lg-offset-3 col-lg-6">
				<div class="modal fade" id="changePassword-modal" role="dialog">
    				<div class="modal-dialog">
						<div class="modal-content" style="border-radius:0px;width: 85%;margin-left: 6%;">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal" onclick="javascript:window.location.reload()">&times;</button>
					          <h4 class="modal-title">Change Password</h4>
					        </div>
					        <div class="modal-body">
							  <div class="row">
								<div class="form-group">
		    					<label class="col-md-5 col-lg-5 col-sm-5" for="new-password">Enter New password:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="password" class="form-control input-sm custom_input"
											aria-role="" id="new-password"
											maxlength="150" data-required="" name=""
											class="" placeholder="Enter your password here">
										<div class="error" id="">
										<input type="hidden" id="TINPreview">
										</div>
		    						</div>
	 							</div>
	 						</div>
	 						<div class="row" style="margin-top:15px;">
								<div class="form-group">
		    					<label class="col-md-5 col-lg-5 col-sm-5" for="confirm-password">Re-enter New Password:</label>
		    						<div class="col-md-6 col-lg-6 col-sm-6">
										<input type="password" class="form-control input-sm custom_input"
											aria-role="" id="confirm-password"
											maxlength="" data-required="required" name=""
											class="" placeholder="Re-enter your password here">
										<div class="error" id=""></div>
		    						</div>
	 							</div>
	 						</div>
					        </div>
					        <div class="modal-footer">
					          <button type="button" class="btn btn-default custom_btn" id="updatePassword-submit">Submit</button>
					        </div>
			      		</div>
			      	</div>
			      </div>
			</div>
		</div>
		
		<div class="text-center" style="margin:20px 10px 10px 10px">
		Thank you for Visiting FIRS TCC Application Portal!!
		</div>
	</div>	
	
	<br class="clearfix" />
	
			</div>
			
		</div>
		
		<div class="container-fluid">
			<footer class="footer">
     <div class="container-fluid" style="font-family: sans-serif;font-size: 12px;">
       <p class="text-muted">
        <div class="text-center">
			&copy;&nbsp;Copyright 2017 <a href="#" target="_blank">Federal Inland Revenue Service. All Right Reserved </a>.
			<!-- <span class="text-right small"><b>Need help?&nbsp</b><a href="#" class="text-info"><u>Click here for LIVE CHAT</u></a></span> -->
		</div>
		</p>
     </div>
</footer>
	

		</div>
		
		
	</body>
</html>