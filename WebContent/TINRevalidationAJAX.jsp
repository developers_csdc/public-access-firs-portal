<%@page
	import="com.softworks.tinvalidation.ws.HttpRestfulTinValidationService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.org.firs.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="org.json.simple.parser.ParseException"%>

<%
	boolean isTINValid = false;
	System.out.println("TINRevalidationAJAX.jsp is running");
	JSONObject tinData = new JSONObject();
	JSONParser parser = new JSONParser();

	String tin = request.getParameter("TIN");
	String command = request.getParameter("command");
	System.out.println("TIN..." + tin);

	if (command.equalsIgnoreCase("fetchTaxPayerData")) {
		System.out.println("Fetching TaxPayerData...");
		ApiFactory apiFactory = new ApiFactory();
		String tinValidationData = apiFactory.tinValidation(tin);
		// isTINValid = true;

		JSONObject json = new JSONObject();
		try {
			json = (JSONObject) parser.parse(tinValidationData);
			String tinTop = (String) json.get("TIN");
			String address = (String) json.get("Address");
			String email = (String) json.get("Email");
			String taxPayerName = (String) json.get("TaxPayerName");
			String taxOfficeID = (String) json.get("TaxOfficeID");
			String taxOfficeName = (String) json.get("TaxOfficeName");
			String RCNumber = (String) json.get("RCNumber");
			String phone = (String) json.get("Phone");
			String JTBTIN = (String) json.get("JTBTIN");

			//System.out.println(RCNumber);

			tinData.put("Success", true);
			tinData.put("companyName", taxPayerName);
			tinData.put("taxOfficeName", taxOfficeName);
			tinData.put("taxOfficeID", taxOfficeID);
			tinData.put("phoneNumber", phone);
			tinData.put("emailID", email);
			tinData.put("rcNumber", RCNumber);
			tinData.put("JTBTIN", JTBTIN);
			tinData.put("address", address);
			tinData.put("TIN", tinTop);

			System.out.println(tinData);
			out.println(tinData.toString());
		} catch (Exception e) {
			System.out.println("Could not fetchTaxPayerData: " + e.getMessage());
		}
	} else if (command.equalsIgnoreCase("updateData")) {
		System.out.println("Updating Amanda..." + tin);
		ApiFactory apiFactory = new ApiFactory();
		String tinValidationData = apiFactory.tinValidation(tin);
		// isTINValid = true;
		System.out.println("Received Data: "+tinValidationData);

		try{
			JSONObject json = (JSONObject) parser.parse(tinValidationData);

			String tinTop = (String) json.get("TIN");
			String address = (String) json.get("Address");
			String email = (String) json.get("Email");
			String taxPayerName = (String) json.get("TaxPayerName");
			String taxOfficeID = (String) json.get("TaxOfficeID");
			String taxOfficeName = (String) json.get("TaxOfficeName");
			String RCNumber = (String) json.get("RCNumber");
			String phone = (String) json.get("Phone");
			String JTBTIN = (String) json.get("JTBTIN");

			People people = new People(tinTop, JTBTIN, taxPayerName, address, taxOfficeID, taxOfficeName, RCNumber,
					email, phone);
			PortalUtil portalUtil = new PortalUtil();

			boolean status = portalUtil.updatePeopleByTin(people);

			System.out.println("This is update Status " + status);

			tinData.put("Success", true);
			System.out.println(tinData);
			out.println(tinData.toString());
		} catch (Exception e) {
			System.out.println("Could not updateData: " + e.getMessage());
		}
	}

	/*Taxpayer taxPayerBean = new Taxpayer();
	
	String TIN = PortalUtility.getString(request.getParameter("TIN"));
	String command = PortalUtility.getString(request.getParameter("command"));
	boolean peopleUpdateFlag = false;
	boolean updatePeopleInfoFlag = false;
	boolean isTINValid = false;
	
	final int INFO_CODE_TAX_OFFICE = 5090;
	
	WsPeople peopleInSession = (WsPeople)session.getAttribute("people");
	
	if(command.equalsIgnoreCase("fetchTaxPayerData")){
		if(!PortalUtility.isNull(peopleInSession)){
			if(PortalUtility.getString(peopleInSession.getLicenceNumber()).trim().equals(TIN)){
				isTINValid = true;
				try{
					//TODO:Please select the service between REST and SOAP 
					/* Retrieving TIN data using SOAP service */
	//taxPayerBean = PortalService.getTaxPayerBean(TIN);

	/* Retrieving TIN data using REST service */
	/*taxPayerBean = HttpRestfulTinValidationService.getTaxPayerFromTINDB(TIN);
	
	if(taxPayerBean!=null){
		tinData.put("Success", true);
		tinData.put("companyName", taxPayerBean.getTaxpayerName());
		tinData.put("taxOfficeName", taxPayerBean.getTaxOfficeName());
		tinData.put("taxOfficeID", taxPayerBean.getTaxOfficeId());
		tinData.put("phoneNumber", taxPayerBean.getContactNumber());
		tinData.put("emailID", taxPayerBean.getEmail());
		tinData.put("rcNumber",taxPayerBean.getRCNumber());
		tinData.put("JTBTIN",taxPayerBean.getJTBTIN());
		tinData.put("address",taxPayerBean.getTaxPayerAddress());
		tinData.put("TIN",taxPayerBean.getFIRSTIN());
	}else{
		tinData.put("Success",false);
	}
	}catch(Exception e){
	System.out.println(e.getMessage());
	tinData.put("Success",false);
	}
	}else{
	isTINValid = false;
	}
	}
	
	}else if(command.equalsIgnoreCase("updateDataInAmanda")){
	WsPeople existingPeople = PortalService.getPeopleByTIN(TIN);
	WsPeople people = new WsPeople();
	
	if(!PortalUtility.isNull(peopleInSession)){
	if(PortalUtility.getString(peopleInSession.getLicenceNumber()).trim().equals(TIN)){
	isTINValid = true;
	int peopleRSN = PortalUtility.getInt(peopleInSession.getPeopleRSN());
	try{
	//TODO:Please select the service between REST and SOAP 
	/* Retrieving TIN data using SOAP service */
	//taxPayerBean = PortalService.getTaxPayerBean(TIN);

	/* Retrieving TIN data using REST service */
	//taxPayerBean = HttpRestfulTinValidationService.getTaxPayerFromTINDB(TIN);

	/*if(!PortalUtility.isNull(taxPayerBean) && taxPayerBean != null){
		people.setPeopleRSN(PortalUtility.getInt(existingPeople.getPeopleRSN()));
		people.setOrganizationName(taxPayerBean.getTaxpayerName());
		people.setAddressLine1(taxPayerBean.getTaxPayerAddress());
		people.setReferenceFile(taxPayerBean.getRCNumber());
		people.setLicenceNumber(taxPayerBean.getFIRSTIN());
		people.setEmailAddress(taxPayerBean.getEmail());
		people.setPhone1(taxPayerBean.getContactNumber());
		people.setSocialSecurityNumber(taxPayerBean.getJTBTIN());
		
		WsPeopleInfo peopleinfoTaxOffice = new WsPeopleInfo();
		peopleinfoTaxOffice.setInfoCode(INFO_CODE_TAX_OFFICE);
		peopleinfoTaxOffice.setInfoValue(PortalUtility.getString(taxPayerBean.getTaxOfficeId().trim()));
		
		List<WsPeopleInfo> infoList = new ArrayList<WsPeopleInfo>();
		infoList.add(peopleinfoTaxOffice);
		
		WsPeopleInfo[] peopleinfosObject = infoList.toArray(new WsPeopleInfo[infoList.size()]);
		
		peopleUpdateFlag = PortalService.getWSClient().updatePeople(lid, people);
		updatePeopleInfoFlag = PortalService.getWSClient().updatePeopleInfo(lid, peopleRSN,peopleinfosObject);
		
		if(peopleUpdateFlag && updatePeopleInfoFlag){
			session.setAttribute("isProfileRevalided", true);
		}
	}
	}catch(Exception e){
	e.printStackTrace();
	}
	}else{
	isTINValid = false;
	}
	}
	
	}
	
	tinData.put("peopleUpdateFlag", peopleUpdateFlag);
	tinData.put("isTINValid", isTINValid);
	tinData.put("updatePeopleInfoFlag",updatePeopleInfoFlag);
	out.println(tinData.toString()); */
%>
