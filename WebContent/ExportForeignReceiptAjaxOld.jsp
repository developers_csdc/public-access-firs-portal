<%@page import="com.csdc.nigeria.firs.foreing.Receipt"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="javax.xml.bind.DatatypeConverter"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="org.apache.commons.io.IOUtils"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.io.BufferedOutputStream"%>
<%
	String encodedTCC;
	String encodedTIN;
	String receiptnumber = PortalUtility.getString(request.getParameter("ReceiptNumber"));
	String tinnumber = PortalUtility.getString(request.getParameter("TIN"));
	String tccNumber = PortalUtility.getString(request.getParameter("tccNumber"));
	StringBuffer requestURL = request.getRequestURL();

	String t_tempURLWithGetURL = requestURL.toString();
	String t_contextURL = t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));
	
	
	/*QR CODE TEXT GENERATION*/
	StringBuilder QRCodeLinkBuilder = new StringBuilder();
	QRCodeLinkBuilder.append(t_contextURL);
	if (receiptnumber != null) {
		QRCodeLinkBuilder.append("/?cmd=searchreceipt&receiptNumber=");
		//TODO: Encode URL incase special characters are used in TCC
		try {
			encodedTCC = URLEncoder.encode(receiptnumber, "UTF-8").replaceAll("\\+", "%20")
					.replaceAll("\\%21", "!").replaceAll("\\%27", "'").replaceAll("\\%28", "(")
					.replaceAll("\\%29", ")").replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			encodedTCC = receiptnumber;
		}

		QRCodeLinkBuilder.append(encodedTCC);
		QRCodeLinkBuilder.append("&tinNumber=");

		try {
			encodedTIN = URLEncoder.encode(tinnumber, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!")
					.replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")")
					.replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			encodedTIN = tinnumber;
		}
		QRCodeLinkBuilder.append(encodedTIN);
	} else {
		QRCodeLinkBuilder.append(t_contextURL);
		QRCodeLinkBuilder.append("/?cmd=searchtcc&tccNumber=");
		//TODO: Encode URL incase special characters are used in TCC
		try {
			encodedTCC = URLEncoder.encode(tccNumber, "UTF-8").replaceAll("\\+", "%20").replaceAll("\\%21", "!")
					.replaceAll("\\%27", "'").replaceAll("\\%28", "(").replaceAll("\\%29", ")")
					.replaceAll("\\%7E", "~");
		} catch (UnsupportedEncodingException e) {
			encodedTCC = tccNumber;
		}
		QRCodeLinkBuilder.append(encodedTCC);
	}
	String qrCodeText = QRCodeLinkBuilder.toString();
	/*QR CODE TEXT GENERATION - END*/

	System.out.println("QRText: " + qrCodeText);
	String receiptType = "";
	if (request.getParameter("tccNumber") != null) {
		receiptType = "TCC";
	} else {
		receiptType = Receipt.getFolderType(request.getParameter("ReceiptNumber"));
	}

	byte[] reportByteContent = null;
	BufferedOutputStream bos = null;
	
	try{
		if (receiptType.equals("FR") || receiptType.equals("RCPT")) {
			System.out.println("Found Receipt: " + receiptType);
			reportByteContent = Receipt.generateReceipt(request.getParameter("ReceiptNumber"), qrCodeText);
			System.out.println("Receipt Size: "+reportByteContent.length);
		} else if (receiptType.equals("TCC") || receiptType.equals("FTCC")) {
			System.out.println("Found Receipt: " + receiptType);
			reportByteContent = Receipt.generateTCC(request.getParameter("tccNumber"), qrCodeText);
			System.out.println("Receipt Size: "+reportByteContent.length);
		} else if (receiptType.equals("RCTI") || receiptType.equals("FRCT")) {
			System.out.println("Found Receipt: " + receiptType);
			reportByteContent = Receipt.generateReceiptWHT(request.getParameter("ReceiptNumber"), qrCodeText);
			System.out.println("Receipt Size: "+reportByteContent.length);
		}

		response.setContentType("application/pdf");
		response.setHeader("Content-Length", String.valueOf((reportByteContent.length)));
		response.setHeader("Content-Disposition",
				"attachment;filename=" + receiptType + " Receipt" + " Result.pdf" + "");
		bos = new BufferedOutputStream(response.getOutputStream());
	}catch(Exception e){
		System.out.println("Error Found: "+e.getMessage());
		e.printStackTrace();
	}

	try {
		bos.write(reportByteContent);
	} catch (Exception e) {
		throw e;
	} finally {
		if (bos != null) {
			bos.flush();
			bos.close();
		}
	}
%>
