<%@page import="com.csdcsystems.amanda.common.*"%>
<%@include file="include/includeconstant.jsp"%>

<%

	request.setAttribute("pageName", "logon"); 
	String clickedOn = "";
	String logon_event = request.getParameter("logon")==null?"":request.getParameter("logon");
	
	if(!"logon".equals(request.getParameter("cmd"))) 
		clickedOn = request.getParameter("cmd");

	//These two variable for searching feature throw logon
	String sf = request.getParameter("sf") == null ? "" : request.getParameter("sf");	//this is searchFlag
	String sk = request.getParameter("sk") == null ? "" : request.getParameter("sk");	//this is searchKeyword
	requestFrom=PortalUtility.getString(request.getParameter("from"));
	System.out.println("=========LOGON==============="+requestFrom);

%>

    
                <div class="section-heading scrollpoint sp-effect3">
                    <h1><%=resourceBundle.getText("LABEL_PAGE_HEADER_CONNECTION") %></h1>
                    <div class="divider"></div>
                </div>



                <div class="row">
                    <div class="col-md-12 col-sm-12">  
                        <div class="row">
                        	<div class="col-md-3 col-sm-3"></div>
                            <div class="col-md-6 col-sm-6 scrollpoint sp-effect3">                  
	                    		<div class="clear"></div>
								<br class="brh5">
								<%if(logon_event.equals("failed") && logon_event!=""){ %>
									<div class="alert alert-warning place_middle" role="alert">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										<span class="sr-only"><%=resourceBundle.getText("LABEL_MESSAGE")%></span>
										<%=resourceBundle.getText("LABEL_MESSAGE_DATA")+"  "+resourceBundle.getText("MESSAGE_ALERT_ON_USER_EXISTS_NOT_ACTIVE")%>
									</div>
								<%}else if(logon_event.equals("passwordExpired") && logon_event!=""){ %>
									<div class="alert alert-warning place_middle" role="alert">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
										<span class="sr-only"><%=resourceBundle.getText("LABEL_MESSAGE")%></span>
										<%=resourceBundle.getText("MESSAGE_ALERT_ON_USER_PASSWORD_EXPIRED")%>
									</div>
								<%}%>
								<br class="brh5">
							</div>
                        	<div class="col-md-3 col-sm-3"></div>
						</div>
						<div class="row">
							<div class="col-md-3 col-sm-3"></div>
							<div class="col-md-6 col-sm-6">
								<div class="alert alert-warning" id="showMessageNeedToLogin" style="display: blank;">
					  				<strong>Warning!</strong>  <%= resourceBundle.getText("MESSAGE_ALERT_LOGIN_REQUIRED") %>
								</div>
							</div>
							<div class="col-md-3 col-sm-3"></div>
						</div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                        	<div class="col-md-3 col-sm-3"></div>
                            <div class="col-md-6 col-sm-6 scrollpoint sp-effect5"> 
		                    		<div class="form-group" id="userName">
										<label for="UserName"><%=resourceBundle.getText("LABEL_USER_NAME") %></label>
										<div class="input-group">
											<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
											<input id="UserName" name="UserName" type="text" class="form-control" />
		                       			</div>
									</div>
		                    		<div class="form-group" id="passWord">
										<label for="PassWord"><%=resourceBundle.getText("LABEL_PASSWORD") %></label>
										<div class="input-group">
											<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
		                                    <input id="PassWord"  name="PassWord" type="password" class="form-control">
		                                </div>
									</div>									
		                    		<div class="form-group" align="center">
		                    		<!-- 12-1-16: Santosh commented out as functionality not working -->
		                    			<div id="forgotPassword" class="aligncx" style="padding: 10px 0px 10px 0px;">
											<a href="javaScript:" onclick="f_checkForgotPassword()" style="font-size: 14px;margin-right: 10px"><%=resourceBundle.getText("LABEL_MENU_FORGOT_PASSWORD") %></a>
										</div>
										<div class="gapMedium"></div>
										<button id="btnSend" type="button" class="btn btn-success " onclick="f_login()"><span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;<%=resourceBundle.getText("LABEL_BTN_CONNECT") %></button>&nbsp;&nbsp;
										<button id="btnReset" type="reset" class="btn btn-warning " ><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;<%=resourceBundle.getText("LABEL_BTN_CANCEL") %></button>
										<div class="gapMedium"></div>
										<div class="aligncx" style="padding-top: 10px;">
											<% if(requestFrom.equalsIgnoreCase("dce")){ %>
											<a href="portal?cmd=registration&from=dce" style="font-size: 14px;"><%=resourceBundle.getText("LABEL_MENU_NEW_REGISTRATION") %></a>
											<%} else if(requestFrom.equalsIgnoreCase("dci")){ %>
											<a href="portal?cmd=registration&from=dci" style="font-size: 14px;"><%=resourceBundle.getText("LABEL_MENU_NEW_REGISTRATION") %></a>
											<%}else{ %>
											<a href="portal?cmd=registration" style="font-size: 14px;"><%=resourceBundle.getText("LABEL_MENU_NEW_REGISTRATION") %></a>
											<%} %>
										</div>
										<input type="hidden" id="CurrentPage" name="CurrentPage" value="logon"/>
										<input type="hidden" id="clickedOn" name="clickedOn" value="<%=clickedOn %>"/>
										<input type="hidden" id="sf" name="sf" value="<%=sf %>"/>
										<input type="hidden" id="sk" name="sk" value="<%=sk %>"/>
		                    		</div> 
							</div>
                        	<div class="col-md-3 col-sm-3" ></div>
						</div>
					</div>
				</div>

    <script type="text/javascript">

	    var MESSAGE_ALERT_ON_USER_EXISTS_NOT_REGISTERED="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_USER_EXISTS_NOT_REGISTERED") %>";
	    var MESSAGE_ALERT_ON_USER_REGISTERED="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_USER_REGISTERED") %>";
	    var MESSAGE_ALERT_ON_USER_EXISTS_CANNOT_LOGIN="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_USER_EXISTS_CANNOT_LOGIN") %>";
	    var MESSAGE_ALERT_ON_USER_EXISTS_NOT_ACTIVE="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_USER_EXISTS_NOT_ACTIVE") %>";
	    var MESSAGE_ALERT_ON_MAIL_SENT="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_MAIL_SENT") %>";
		var MESSAGE_ALERT_ON_INCORRECT_EMAIL="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_INCORRECT_EMAIL") %>";
	    var MESSAGE_ALERT_ON_EMPTY_USERNAME= "<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_EMPTY_USERNAME") %>";
	    var MESSAGE_ALERT_ON_EMPTY_PASSWORD="<%=resourceBundle.getJsText("MESSAGE_ALERT_ON_EMPTY_PASSWORD") %>";
		var MESSAGE_ALERT_FOR_FORGOT_PASSWORD="<%=resourceBundle.getJsText("MESSAGE_ALERT_FOR_FORGOT_PASSWORD") %>";
		var LABEL_MENU_FORGOT_PASSWORD="<%=resourceBundle.getJsText("LABEL_MENU_FORGOT_PASSWORD") %>";
		var LABEL_BTN_PLEASE_WAIT="<%=resourceBundle.getJsText("LABEL_BTN_PLEASE_WAIT") %>";
		var showMsgInLogonPage = '<%=request.getParameter("showMsgInLogonPage")%>';
		var reqFrom = '<%=requestFrom%>';
	
    </script> 
    <script src="<%=JS_PORTAL%>logon.js"></script>
    <script src="<%=JS_PORTAL%>enterKeyFormSubmit.js"></script>