<%@page import="org.apache.commons.lang.ArrayUtils"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.org.firs.*"%>
<%@ page language="java" contentType="charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%!private static JSONArray getReceiptObj(String receiptTypeKey, String yearMonthKey, String p_Date, String n_month,
			String year, int code, String tin) throws Exception {
		JSONArray tempReceiptObj = new JSONArray();
		String receivedData = SearchProfileReceipt.getReceipts(tin, p_Date, receiptTypeKey, yearMonthKey);

		if (!receivedData.equals("null") || receivedData == null) {
			if (receivedData.startsWith("[")) {
				tempReceiptObj = new JSONArray(receivedData);
			} else {
				tempReceiptObj = new JSONArray("[" + receivedData + "]");
			}
		}
		return tempReceiptObj;
	}%>
<%
	response.setContentType("application/json");

	JSONObject responseObj = new JSONObject();
	JSONArray arrayOfReceiptObj = new JSONArray();

	String year = "", month = "", day = "", cmd = "", receiptTypeKey = "", yearMonthKey = "", TIN = "";

	boolean isExceptionOccured = false;

	final int CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY = 125;
	Map<String, String> monthMap = new HashMap<String, String>();
	String[] monthArray = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
			"Dec" };

	cmd = request.getParameter("cmd");
	String monthAppend = "";
	month = request.getParameter("month");
	if ((ArrayUtils.indexOf(monthArray, month) + 1) < 10) {
		monthAppend = "0";
	}
	try {

		if (cmd.equalsIgnoreCase("searchCorporateReceipt")) {
			year = request.getParameter("year");
			month = request.getParameter("month");
			String n_month = monthAppend + String.valueOf(ArrayUtils.indexOf(monthArray, month) + 1);
			day = request.getParameter("day").equals("") ? "01" : request.getParameter("day");
			receiptTypeKey = request.getParameter("corporateReceiptTypeKey");
			yearMonthKey = request.getParameter("corporateMonthYearKey").toUpperCase();
			TIN = request.getParameter("TIN");

			String p_Date = n_month + "-" + day + "-" + year;

			arrayOfReceiptObj = getReceiptObj(receiptTypeKey, yearMonthKey, p_Date, n_month, year,
					CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY, TIN);

		} else if (cmd.equalsIgnoreCase("searchIndividualReceipt")) {
			year = request.getParameter("year");
			month = request.getParameter("month");
			String n_month = monthAppend + String.valueOf(ArrayUtils.indexOf(monthArray, month) + 1);
			day = request.getParameter("day").equals("") ? "01" : request.getParameter("day");
			receiptTypeKey = request.getParameter("individualReceiptTypeKey");
			yearMonthKey = request.getParameter("individualMonthYearKey").toUpperCase();
			TIN = request.getParameter("TIN");

			String p_Date = n_month + "-" + day + "-" + year;

			arrayOfReceiptObj = getReceiptObj(receiptTypeKey, yearMonthKey, p_Date, n_month, year,
					CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY, TIN);
		} else if (cmd.equalsIgnoreCase("searchForexReceipt")) {
			year = request.getParameter("year");
			month = request.getParameter("month");
			String n_month = monthAppend + String.valueOf(ArrayUtils.indexOf(monthArray, month) + 1);
			day = request.getParameter("day").equals("") ? "01" : request.getParameter("day");
			receiptTypeKey = request.getParameter("forexReceiptTypeKey");
			yearMonthKey = request.getParameter("forexMonthYearKey").toUpperCase();
			TIN = request.getParameter("TIN");

			String p_Date = n_month + "-" + day + "-" + year;

			arrayOfReceiptObj = getReceiptObj(receiptTypeKey, yearMonthKey, p_Date, n_month, year,
					CUSTOM_TRANSACTION_CODE_FOR_RECEIPT_JSON_ARRAY, TIN);
		}

	} catch (Exception e) {
		e.printStackTrace();
		isExceptionOccured = true;
		responseObj.put("errorMsg", e.getMessage());
	}

	responseObj.put("isExceptionOccured", isExceptionOccured);
	responseObj.put("success", true);
	responseObj.put("arrayOfReceiptObj", arrayOfReceiptObj);
	responseObj.put("data", arrayOfReceiptObj);
	//responseObj.put("recordsTotal", 10);
	//responseObj.put("recordsFiltered", 10);
	response.getWriter().write(responseObj.toString());
	//out.println(responseObj.toString());
	System.out.print(responseObj.toString());
%>
