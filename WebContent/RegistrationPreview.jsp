<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@include file="include/includeconstant.jsp"%>

<%
	String peopleType=PortalUtility.getString(request.getParameter("peopleType-preview-hidden"));
	String tinNumber = PortalUtility.getString(request.getParameter("tin"));
	String namePreview = PortalUtility.getString(request.getParameter("name-preview-hidden"));
	String taxOfficePreview = PortalUtility.getString(request.getParameter("tax-office-preview-hidden"));
	String emailPreview = PortalUtility.getString(request.getParameter("email-perview-hidden"));
	String phonePreview = PortalUtility.getString(request.getParameter("phone-preview-hidden"));
	String newMobile = PortalUtility.getString(request.getParameter("mobile-new"));
	String newTaxOffice = PortalUtility.getString(request.getParameter("tax-office-new"));
	String newEmail = PortalUtility.getString(request.getParameter("email-new"));
	String newAddress = PortalUtility.getString(request.getParameter("address-new"));
%>

<style>
	.preloaderReg{
		border: 2px solid #bbb5b5;
	    width: 125px;
	    position: absolute;
	    top: 50%;
	    background-color: #eceaea;
	    border-radius: 5px;
	    left: 45%;
	    padding: 10px 5px 5px;
	}
</style>


<div class="container-fluid" id="preview-section">
<p class="alert alert-success text-center">Preview Page</p>
	<div class="panel panel-default" id="preview-background" style="padding:30px 10px 55px 10px">
		<div class="row custom-row-margin">
			<div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
				<span class="preview-lable">Application Type:<sup class="text-danger">*</sup></span>
				<span id="save_peopleType"><%=peopleType%></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<span class="preview-lable">Tax Identification Number:<sup class="text-danger">*</sup></span>
				<span id="save_tin"><%=tinNumber %></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<span class="preview-lable">Name:</span>
				<span id="save_name"><%=namePreview %></span>
			</div>
		</div>
		
		
		<p class="text-center alert"><u>Existing Details:</u></p>
		<div class="row custom-row-margin">
			<div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
				<span class="preview-lable">Tax Office</span>
				<span id="save_taxOffice"><%=taxOfficePreview%></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<span class="preview-lable">E mail:</span>
				<span id="save_email"><%=emailPreview %></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<span class="preview-lable">Mobile:</span>
				<span id="save_phone"><%=phonePreview %></span>
			</div>
		</div>
		
		<p class="text-center alert"><u>New Details:</u></p>
		<div class="row custom-row-margin">
			<div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
				<span class="preview-lable">New Tax Office:</span>
				<span  id="save_newTaxOffice"><%=newTaxOffice %></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<span class="preview-lable">New E Mail:</span>
				<span  id="save_newEmail"><%=newEmail %></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3">
				<span class="preview-lable">New Mobile:</span>
				<span id="save_newMobile"><%=newMobile %></span>
			</div>
		</div>
		<div class="row custom-row-margin">
			<div class="col-lg-offset-2 col-lg-3 col-md-offset-2 col-md-3 col-sm-offset-2 col-sm-3">
				<span class="preview-lable">New Address:</span>
				<span  id="save_newAddress"><%=newAddress %></span>
			</div>
		</div>
	</div>
	<input type="hidden" id="TaxOfficeId" value="<%=PortalUtility.getString(request.getParameter("TaxOfficeId"))%>">
	<input type="hidden" id="RCNo" value="<%=PortalUtility.getString(request.getParameter("RCNo"))%>">
	<input type="hidden" id="JTBTIN" value="<%=PortalUtility.getString(request.getParameter("JTBTIN"))%>">
	<input type="hidden" id="Address" value="<%=PortalUtility.getString(request.getParameter("Address"))%>">
	<div class="row">
		<div class="col-lg-12 text-center">
			<p>
			<label><input type="checkbox" value=""  id="confirm-registration"></label>
			I certify that the information given above is correct in
			respect and confirm that to the best of my knowledge and belief,
			there are no other facts the omission of which would be misleading
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center">
			<button type="button" class="btn btn-default custom_btn" onclick="goToBackPage();">Back</button>
			<button type="button" class="btn btn-default custom_btn" id="submit-registration-form" disabled="disabled">Submit</button>
		</div>
	</div>
	<div class="loader-section preloaderReg hide" id="wait">
		<p>Loading...<i class="custom-loader"></i></p>
	</div>
</div>
<div class="container-fluid hide" id="registration-sucess-info">
	<div class="row panel panel-default" style="margin:0px 0px 0px 0px;">
		<div class="col-lg-offset-4 col-lg-4 text-center" style="padding:30px 10px 30px 10px">
			<strong>Registration Successful.</strong>
			<p>Your application number is :<strong><span id="application-number"></span></strong></p>
			<p><a href="<%=request.getContextPath()%>">Home Page</a><span></span></p>
		</div>
	</div>
</div>