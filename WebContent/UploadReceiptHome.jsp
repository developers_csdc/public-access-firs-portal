<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="../include/includeconstant.jsp" %>
<%
	String rootURL = PortalService.getROOTURL(request);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<title>WELCOME | User</title>
	<link rel="stylesheet" href="<%=CSS_PORTAL%>back-office-login.css">
	<%@include file="include/common/includeCss.jsp"%>
	<script>
		function startTime() {
		    var today = new Date();
		    var h = today.getHours();
		    var m = today.getMinutes();
		    var s = today.getSeconds();
		    m = checkTime(m);
		    s = checkTime(s);
		    document.getElementById('timer').innerHTML =
		    h + ":" + m + ":" + s;
		    var t = setTimeout(startTime, 500);
		}
		function checkTime(i) {
		    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
		    return i;
		}
	</script>
</head>
<body onload="startTime()">
	<div class="container-fluid">
		<div class="row" style="background-color:rgba(85, 85, 85, 0.14);">
			<div class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 text-center">
				<a class="header-logo" href="#"></a>
			</div>
			<div class="col-lg-4 col-md-4 pull-left" style="margin-top:10px;">
				<p style="margin-top: 25px;">
					<span class="label label-default" style="font-size:16px;border-radius:0px;">WELCOME TO FIRS - WHT DATA UPLOAD PORTAL</span>
				</p>	
			</div>
			<div class="col-lg-4 col-md-4 pull-right" style="margin-top:37px;">
				<p>
					<span style="font-family: sans-serif;font-size: 12px;font-weight: 600;color: #777;"><i class="fa fa-clock-o" aria-hidden="true"></i>  </span>
					<span id="timer" style="font-family: monospace;font-size: 14px;font-weight: 600;"></span>
				</p>
			</div>
		</div>
		<div class="row" style="background-color:rgba(85, 85, 85, 0.14);">
			<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-3" style="padding-left: 0px;padding-right:0px;">
				<nav class="navbar navbar-default custom_navbar" style="min-height: 39px;height: 38px;box-shadow: 9px 8px 10px #888888;">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>                        
				      </button>
				      <a class="navbar-brand hide" href="#">WebSiteName</a>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				      <ul class="nav navbar-nav">
				        <li class=""><a href="#" style="height: 43px;line-height: 9px;">Welcome User</a></li>
				      </ul>
				     <ul class="nav navbar-nav navbar-right">
        				<li>
        					<a href="UploadReceipt.jsp" style="height: 43px;line-height: 9px;">
        						<span class="glyphicon glyphicon-user" style="line-height:6px;"></span> Log Out
        					</a>
        				</li>
      				</ul>
				    </div>
				  </div>
				</nav>
			</div>
		</div>
	</div>
	<div class="row" id="chooseSectionContainer">
		<div class="col-lg-offset-1 col-lg-10">
			<div class="module form-module container-fluid" style="margin-top: 50px;">
				<div id="">
					<p style="margin-top:10px;">
						<span class="label label-default custom-label">Download Template & Choose upload Type:</span>
					</p>
					<div class="row">
						<div class="col-lg-3 text-center">
							<a href="Template_files/Excel/Credit_Note_Template.xls">
								<span><i class="fa fa-download custom-tile" aria-hidden="true"></i></span>
								<p>Download WHT Data Template</p>
							</a>
						</div>
						<div class="col-lg-3 text-center">
							<a href="Template_files/Excel/Credit_Note_Template.xls">
								<span><i class="fa fa-download custom-tile" aria-hidden="true"></i></span>
								<p>Download CREDIT Note Data Template</p>
							</a>
						</div>
						<div class="col-lg-3 text-center">
							<div class="" id="showUploadWHT" key="WHT">
								<span><i class="fa fa-upload custom-tile" aria-hidden="true" style="color: #31708f;"></i></span>
								<p>Upload WHT Data</p>
							</div>
						</div>
						<div class="col-lg-3 text-center">
							<div class="" id="showUploadCREDIT" key="CREDIT">
								<span><i class="fa fa-upload custom-tile" aria-hidden="true" style="color: #31708f;"></i></span>
								<p>Upload CREDIT Note Data</p>
							</div>
						</div>
					</div>
				</div>
				<div id="statusTableContainer" class="">
					<p style="margin-top:30px;">
					<span class="label label-default custom-label">Status Table:</span>
					</p>
					<div class="col-lg-offset-1 col-lg-10">
						<div class="">          
						  <table class="table table-bordered table-hover" id="userStatusTable">
						    <thead style="background-color: #2c3e50;color: #ddd;font-family: sans-serif;font-size: 11px;font-weight: 100!important;">
						      <tr>
						        <th>TimeStamp</th>
						        <th>LoggedIn User</th>
						        <th>Receipt Type</th>
						        <th>Number of Receipt</th>
						        <th>Status</th>
						        <th>Download</th>
						      </tr>
						    </thead>
						    <tbody>
						      <tr>
						        <td>2009-09-22 16:47:08</td>
						        <td>User</td>
						        <td>CREDIT</td>
						        <td>50</td>
						        <td><i class="fa fa-check" aria-hidden="true"></i></td>
						        <td>
						        	<a href="<%=rootURL%>/DownloadZIPAJAX.jsp?key=FIRS118018121241">
							        	<span id="downloadZIP" key="001" style="cursor: pointer;"><i class="fa fa-download" aria-hidden="true"></i></span>
						        	</a>
						        </td>
						      </tr>
						    </tbody>
						  </table>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row hide" id="uploadSectionContainer">
		<div class="col-lg-offset-1 col-lg-10">
			<div class="module form-module container-fluid" style="margin-top: 50px;">
				<p style="margin-top:10px;">
					<span class="label label-default custom-label">UPLOAD Excel Section:</span>
				</p>
				<div class="row" style="margin-top:20px;">
					<div class="col-lg-3 text-center">
						<p>Upload your Excel Here</p>
					</div>
					<div class="col-lg-3 text-center">
						<input type="file" id="uploadExcelInput" class="" accept=".csv,.xls,.xlsx">
						<div class="error"></div>	
					</div>
					<div class="col-lg-1 hide" id="loadingTextContainer">
						<span id="loadingText" style="line-height: 50px;font-size:13px;font-style:italic;color:#3c763d;"></span>
					</div>
					<div class="col-lg-3 hide" id="loaderContainer">
						<img class="img-responsive" src="images/loader4.gif" style="margin-top: -3px;height: 44px;">
					</div>
				</div>
				<div class="row" style="margin-top:40px;padding: 10px 10px 10px 10px;">
					<div class="col-lg-12 text-center">
						<button class="btn btn-default custom_btn" id="backToProfile">Back</button>
						<button class="btn btn-default custom_btn" id="validateBtn">Validate Excel</button>
						<button class="btn btn-default custom_btn" id="uploadBtn" disabled="disabled">Upload</button>
						<button class="btn btn-default custom_btn" id="resetInputSection">Reset</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<footer>
	    <div class="container-fluid">
	      <p class="text-muted text-center" style="font-size:12px;">� 2017 Federal Inland Revenue Service.All Right Reserved</p>
	    </div>
	</footer>
	<!-- SCRIPTS -->
	<script src="<%=JS_API%>jquery-3.1.1.min.js"></script>
	<script src="<%=JS_API%>bootstrap.min.js"></script>
	<script src="<%=JS_API%>jquery.growl.js"></script>
	<script type="text/javascript" src="<%=JS_API%>jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<%=JS_API%>dataTables.bootstrap.min.js"></script>
	<script src="<%=JS_PORTAL%>notification.js"></script>
	<script src="<%=JS_PORTAL%>uploadReceiptHome.js"></script>
	<script>
		$(document).ready(function(){
			/* $("#downloadZIP").click(function(){
				$downloadZIP = $("#downloadZIP");
				
				var key = "key123";
				var url = "DownloadZIPAJAX.jsp";
				var cmd = "downloadZIP";
				
				var formData = new Object();
				
				$.ajax({
					url: url,
					type: "POST",
					async:true,
					data:formData,
					success: function(res){
						alert("Success",'success');
					},
					error: function(res){
						alert("Some error occured!",'error');
					}
				});
			}) */
			$("#userStatusTable").DataTable();
			
		})
	</script>
</body>
</html>
