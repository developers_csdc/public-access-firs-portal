<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.csdcsystems.amanda.client.service.ReportService"%>
<%@page import="org.apache.commons.lang.RandomStringUtils"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsAttachmentDetail"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidFolderType"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderPayment"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderPeople"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeopleInfo"%>
<%@page import="java.util.Arrays"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@ page language="java" buffer="8kb"
	import="java.util.*,java.text.*,com.csdcsystems.amanda.common.*,java.util.ArrayList,java.util.List"
	autoFlush="true" isThreadSafe="true" isErrorPage="false"
	session="true" contentType="text/html; charset=UTF-8"%>
	
<%@page import="java.sql.Timestamp"%>
<%@ include file="include/includeconstant.jsp"%>
	
<%

	String fromEmailAddress = "no_reply@gouvlive.org"; // But it takes the email address configured during the SMTP configuration.
	String CCemailAddress = "";
	final int PEOPLE_STATUS_INACTIVE = 1601;
	final int FOLDER_STATUS_INACTIVE = 1601;

	request.setAttribute("pageName","updatePeople");
	String pagename = request.getParameter("pagename");
	
	String operationResponse="The operation is Successfull";
	
	//Gathering request data
	
	String EmailAddress = request.getParameter("EmailAddress");
	String Title = request.getParameter("Title");
	String Name = request.getParameter("Name");
	String NameLast = request.getParameter("NameLast");

	String Address = request.getParameter("Address");	
	String AddrCity = request.getParameter("City");
	String AddrCountry = request.getParameter("Country");
	String AddrPostal = request.getParameter("PostalCode");

	String Phone1 = PortalService.getUnmask(request.getParameter("Phone1"));
	String Phone2 = PortalService.getUnmask(request.getParameter("Phone2"));
	
	String SecurityQuestion = request.getParameter("SecurityQuestion");
	String SecurityQuestionAnswer = request.getParameter("SecurityQuestionAnswer");

		
	
	List<String> folderList = null;

	boolean UpdatePasswordFlag = Boolean.valueOf(request.getParameter("updatePasswordFlag"));
	
	//Setting up data to WSPeople
	WsPeople people = new WsPeople();
	people.setEmailAddress(EmailAddress);
	people.setNameTitle(Title);
	people.setNameFirst(Name);
	people.setNameLast(NameLast);

	people.setAddressLine1(Address);
	people.setAddrCity(AddrCity);
	people.setAddrCountry(AddrCountry);
	people.setAddrPostal(AddrPostal);
	
	people.setPhone1(Phone1);
	people.setPhone2(Phone2);

	people.setInternetQuestion(SecurityQuestion);
	people.setInternetAnswer(SecurityQuestionAnswer);
	people.setPeopleCode(7);//People type 'registred'
	
	
	
	//Adding People Info
	List<WsPeopleInfo> peopleInfoList = new ArrayList<WsPeopleInfo>();
	/*WsPeopleInfo webAddressInfo = new WsPeopleInfo();
	webAddressInfo.setInfoCode(7003);
	webAddressInfo.setInfoValue("");
	
	peopleInfoList.add(webAddressInfo);*/
	
	
	
		
	if(UpdatePasswordFlag){
		String InternetPassword = request.getParameter("PassWord");
		people.setInternetPassword(InternetPassword);
		people.setEmailFlag("N");
	}
	
	int peopleRSN =0;
	boolean updatePeopleCheck = false;
	try{
		
		if("myProfile".equalsIgnoreCase(pagename)){
			people.setPeopleRSN(Integer.valueOf(session.getAttribute("PeopleRSN").toString()));
			updatePeopleCheck = PortalService.getWSClient().updatePeople(lid, people);
			
			//Updating People Info
			//PortalService.getWSClient().updatePeopleInfo(lid, Integer.valueOf(session.getAttribute("PeopleRSN").toString()), peopleInfoList.toArray(new WsPeopleInfo[peopleInfoList.size()] ));
			
		}else{
			
			String tempPassword = RandomStringUtils.random(8, true, true);
			people.setInternetAccess("A");
			people.setInternetPassword(tempPassword);
			people.setEmailFlag("Y");
				
			//Inserting People record.
			peopleRSN = PortalService.getWSClient().addPeople(lid, people);

			//inserting People Info
			//PortalService.getWSClient().updatePeopleInfo(lid, peopleRSN, peopleInfoList.toArray(new WsPeopleInfo[peopleInfoList.size()]));
			
			
			StringBuilder mailBody = new StringBuilder();
			mailBody.append("<p>"+resourceBundle.getText("MAIL_BODY_01")+" <b>"+PortalUtility.getString(people.getNameFirst()) + " " + PortalUtility.getString(people.getNameLast()) +"</b>,</p>");
			
			
			mailBody.append("<p>"+resourceBundle.getText("MAIL_BODY_07")+" <a href='"+completeContextPath+"'>"+resourceBundle.getText("MAIL_BODY_08")+"</a> "+resourceBundle.getText("MAIL_BODY_09")+"</p>");
			mailBody.append("<p>"+resourceBundle.getText("MAIL_BODY_10")+"</p>");
			mailBody.append("<p>"+resourceBundle.getText("MAIL_BODY_11")+"<b style='color: red;'>"+tempPassword+"</b></p>");
			mailBody.append("<p>"+resourceBundle.getText("MAIL_BODY_12")+"</p>");
			mailBody.append("<p>"+resourceBundle.getText("MAIL_BODY_05")+"<br>"+resourceBundle.getText("MAIL_BODY_06")+"</p>");
			PortalService.getWSClient().sendHtmlEmail(lid, EmailAddress, fromEmailAddress, CCemailAddress, resourceBundle.getJsText("MAIL_SUBJECT_02"), mailBody.toString());
		
		}
	}catch(org.apache.axis.AxisFault af){
		operationResponse = "failed: "+af.getFaultString();
	}catch(Exception ex){
		operationResponse = "failed";
		ex.printStackTrace();
	}
	if(peopleRSN >0 || updatePeopleCheck){
		session.setAttribute("LogonName", Name);
		operationResponse = "success";
	}/* else{
		operationResponse = "failed";
	} */
	out.print(operationResponse.toString());

%>