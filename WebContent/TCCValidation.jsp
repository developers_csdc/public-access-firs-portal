<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.axis.encoding.Base64"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="com.org.firs.TccValidation"%>
<%@ page language="java" contentType="charset=UTF-8"
	pageEncoding="UTF-8"%>
<%!private static String getRequestBodyJSONString(HttpServletRequest request) throws Exception {
		BufferedReader bufferReader = request.getReader();
		StringBuilder stringBuilder = new StringBuilder();
		String line;

		while ((line = bufferReader.readLine()) != null) {
			stringBuilder.append(line.trim());
		}

		return stringBuilder.toString();
	}

	private Map<String, String> authenticateRequest(HttpServletRequest request) throws Exception {
		String requestUserName = "";
		String requestPassword = "";
		String storedPassword = "";
		String activeFlag = "";
		String errorCode = "";
		String errorMessage = "";

		boolean isRequestAuthentic = false;
		final String authorization = request.getHeader("Authorization");
		Map<String, String> authRes = new HashMap<String, String>();

		if (authorization != null && authorization.startsWith("Basic")) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			System.out.println(base64Credentials);

			String authCredentials = new String(Base64.decode(base64Credentials));
			System.out.println(authCredentials);

			final String[] authCredentialsValues = authCredentials.split(":", 2);

			requestUserName = authCredentialsValues[0];
			requestPassword = authCredentialsValues[1];
		}

		authRes.put("username", requestUserName);
		authRes.put("password", requestPassword);
		return authRes;
	}%>
<%
	System.out.println(">TCCValidation API");

	response.setContentType("application/json");
	response.setHeader("Content-Disposition", "inline");
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setHeader("Access-Control-Allow-Headers",
			"Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With ,X-ConsumerKey, X-ConsumerSecretKey");
	response.setHeader("Access-Control-Request-Headers",
			"Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, X-ConsumerKey, X-ConsumerSecretKey");

	boolean isRequestAuthentic = false;
	boolean isRequestBodyEmpty = true;
	boolean isRequestBodyDataValid = false;

	String requestBodyJSONString = "";
	String statusCode = "";
	String description = "";
	String stampUser = "";

	JSONObject tccDetailsJSONObj = new JSONObject();
	JSONObject responseJSONObj = new JSONObject();
	JSONObject requestBodyJSONObj = new JSONObject(getRequestBodyJSONString(request));

	Map<String, String> requestHeaderMap = new HashMap<String, String>();
	Map<String, String> authRespMap = new HashMap<String, String>();

	authRespMap = authenticateRequest(request);
	
	String tin = requestBodyJSONObj.get("tin").toString();
	String tcc = requestBodyJSONObj.get("tccNumber").toString();
	
	System.out.println("Username: "+authRespMap.get("username")+" Password: "+authRespMap.get("password")+" "+requestBodyJSONObj.get("tccNumber")+" "+requestBodyJSONObj.get("tin"));
	
	
	String response2 = TccValidation.validateTCC(tin, tcc, authRespMap.get("username"), authRespMap.get("password"));
	response.getWriter().write(response2);
%>
