<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.*"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidCity"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidLookup"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolderInfo"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsPeople"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="org.codehaus.jettison.json.JSONArray"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsFolder"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidOperator"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsSearchCriteria"%>
<%@include file="include/includeconstant.jsp"%>
<%
WsValidOperator validOperator = PortalService.getClientValidOperators();
ArrayList<WsSearchCriteria> wsCriteria = new ArrayList<WsSearchCriteria>();
WsFolder[] folders = null;
WsFolderInfo[] Folderinfo = null;
final int CODE_PRODUCTCODE = 7100;
final int CODE_PEOPLE_BD 	= 9; 
final int CODE_SED_SUB_IMPORT	= 3100;
final int CODE_SED_SUB_EXPORT	= 3101;
final int CODE_SED_COUNTRY		= 7105;
final int FTS_LOOKUPCODE		= 8050;
final int COUNTRY_LOOKUPCODE	= 8989;
final int CITY_LOOKUPCODE	= 8999;
JSONObject responseObject = new JSONObject();
JSONArray array = new JSONArray();

String inputName = PortalUtility.getString(request.getParameter("inputName"));
String searchVal = PortalUtility.getString(request.getParameter("query"));
String cmd = PortalUtility.getString(request.getParameter("cmd"));

//Bo: Code starts here.....
if(cmd.equalsIgnoreCase("licensesearch")){
	//if(inputName.equalsIgnoreCase("folderRSN")){
		WsSearchCriteria searchCriteria = new WsSearchCriteria();
		searchCriteria.setFieldName("Folder.FolderType");
		searchCriteria.setTableName("Folder");
		searchCriteria.setOperator(validOperator.getEQUAL());
		searchCriteria.setValue(new String[]{"BO"});
		searchCriteria.setConjuctiveOperator(validOperator.getAND());
		
		WsSearchCriteria searchCriteria2 = new WsSearchCriteria();
		searchCriteria2.setFieldName("Folder.SubCode");
		searchCriteria2.setTableName("Folder");
		searchCriteria2.setOperator(validOperator.getNOT_EQUAL());
		searchCriteria2.setValue(new String[]{"128"});
		searchCriteria2.setConjuctiveOperator(validOperator.getAND());
	//}

	if(inputName.equalsIgnoreCase("folderName")){
		WsSearchCriteria searchCriteria3 = new WsSearchCriteria();
		searchCriteria3.setFieldName("Folder.FolderName");
		searchCriteria3.setTableName("Folder");
		searchCriteria3.setOperator(validOperator.getLIKE());
		searchCriteria3.setValue(new String[]{"%"+searchVal+"%"});
		searchCriteria3.setConjuctiveOperator(validOperator.getAND());
		wsCriteria.add(searchCriteria3);
	}
	
	else if("Folder.FolderName".equalsIgnoreCase(inputName)){
		
		WsSearchCriteria searchCriteria3 = new WsSearchCriteria();
		searchCriteria3.setFieldName("Folder.FolderName");
		searchCriteria3.setTableName("Folder");
		searchCriteria3.setConjuctiveOperator(validOperator.getAND());
		searchCriteria3.setOperator(validOperator.getLIKE());
		searchCriteria3.setValue(new String[]{"%"+searchVal+"%"});
		wsCriteria.add(searchCriteria3);
		
		String OpportunityType = request.getParameter("OpportunityType");
		String OpportunityNo =request.getParameter("OpportunityNo");
		String InDate = request.getParameter("InDate");
		String ExpiryDate = request.getParameter("ExpiryDate");
		String Country = request.getParameter("Markets");
		
		if(!PortalUtility.isNull(OpportunityType)){
			searchCriteria2.setFieldName("Folder.SubCode");
			searchCriteria2.setTableName("Folder");
			searchCriteria2.setConjuctiveOperator(validOperator.getAND());
			searchCriteria2.setOperator(validOperator.getEQUAL());
			searchCriteria2.setValue(new String[]{OpportunityType});
		}

		if(!PortalUtility.isNull(OpportunityNo)){
			WsSearchCriteria searchCriteria4 = new WsSearchCriteria();
			searchCriteria4.setFieldName("Folder.FolderRSN");
			searchCriteria4.setTableName("Folder");
			searchCriteria4.setConjuctiveOperator(validOperator.getAND());
			searchCriteria4.setOperator(validOperator.getEQUAL());
			searchCriteria4.setValue(new String[]{OpportunityNo});
			wsCriteria.add(searchCriteria4);
		}

		if(!PortalUtility.isNull(InDate)){
			WsSearchCriteria searchCriteria5 = new WsSearchCriteria();
			searchCriteria5.setFieldName("Folder.ExpiryDate");
			searchCriteria5.setTableName("Folder");
			searchCriteria5.setConjuctiveOperator(validOperator.getAND());
			searchCriteria5.setOperator(validOperator.getGREATER_THAN_EQUAL());
			searchCriteria5.setValue(new String[]{InDate});
			wsCriteria.add(searchCriteria5);
		}

		if(!PortalUtility.isNull(ExpiryDate)){
			WsSearchCriteria searchCriteria6 = new WsSearchCriteria();
			searchCriteria6.setFieldName("Folder.ExpiryDate");
			searchCriteria6.setTableName("Folder");
			searchCriteria6.setConjuctiveOperator(validOperator.getAND());
			searchCriteria6.setOperator(validOperator.getLESS_THAN_EQUAL());
			searchCriteria6.setValue(new String[]{ExpiryDate});
			wsCriteria.add(searchCriteria6);
		}
		//Country Name
		if (!PortalUtility.isNull(Country)) {
			WsSearchCriteria searchCriteria7 = new WsSearchCriteria();
			searchCriteria7.setFieldName("FolderInfo.InfoValue");
			searchCriteria7.setTableName("FolderInfo");
			searchCriteria7.setConjuctiveOperator(validOperator.getAND());
			searchCriteria7.setOperator(validOperator.getIN());
			searchCriteria7.setValue(Country.split(", "));
			wsCriteria.add(searchCriteria7);
		}
	}

	wsCriteria.add(searchCriteria);
	wsCriteria.add(searchCriteria2);

	folders = PortalService.getWSClient().searchFolder(lid, wsCriteria.toArray(new WsSearchCriteria[wsCriteria.size()]), 0, 100, null);
	if(folders != null){
		for(WsFolder folder : folders){
			if(inputName.equalsIgnoreCase("folderRSN"))
			if(String.valueOf(folder.getFolderRSN()).contains(searchVal)){
				if(folder.getFolderDescription() == null || folder.getFolderDescription().trim().equals(""))
					array.put(String.valueOf(folder.getFolderRSN()));
				else
					array.put(String.valueOf(folder.getFolderRSN())+"-"+folder.getFolderDescription());
			}
			
			if(inputName.equalsIgnoreCase("folderName"))
				if(String.valueOf(folder.getFolderName()).contains(searchVal))
					array.put(String.valueOf(folder.getFolderName()));
			
			if(inputName.equalsIgnoreCase("Folder.FolderName"))
				array.put(String.valueOf(folder.getFolderName()));
		}
	}else{
		array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));	
	}
}//Bo: Code Ends here.....

//BD: Code Starts Here
if(cmd.equalsIgnoreCase("bdlicensesearch")){
	
		//Folder Type
		WsSearchCriteria FolderTypeCriteria = new WsSearchCriteria(); 
		FolderTypeCriteria.setTableName("Folder");
		FolderTypeCriteria.setFieldName("Folder.FolderType");
		FolderTypeCriteria.setConjuctiveOperator(validOperator.getAND());
		FolderTypeCriteria.setOperator(validOperator.getEQUAL());
		FolderTypeCriteria.setValue(new String[]{"BD"});			
		wsCriteria.add(FolderTypeCriteria);
		
		//Set people type code as 9
		WsSearchCriteria PeopleTypeCriteria = new WsSearchCriteria();
		PeopleTypeCriteria.setTableName("People");
		PeopleTypeCriteria.setFieldName("People.PeopleCode");
		PeopleTypeCriteria.setConjuctiveOperator(validOperator.getAND());
		PeopleTypeCriteria.setOperator(validOperator.getEQUAL());
		PeopleTypeCriteria.setValue(new String[]{String.valueOf(CODE_PEOPLE_BD)});
		wsCriteria.add(PeopleTypeCriteria);

	if("People.OrganizationName".equalsIgnoreCase(inputName)){
		wsCriteria = new ArrayList<WsSearchCriteria>();
		WsSearchCriteria searchCriteria = new WsSearchCriteria();
		searchCriteria.setFieldName("People.organizationname");
		searchCriteria.setTableName("People");
		searchCriteria.setConjuctiveOperator(validOperator.getAND());
		searchCriteria.setOperator(validOperator.getLIKE());
		searchCriteria.setValue(new String[]{"%"+searchVal+"%"});
		wsCriteria.add(searchCriteria);
		
		String City 		= request.getParameter("City");
		String Sigile 		= request.getParameter("Sigile");
		String LegalStatus 	= request.getParameter("LegalStatus");
		String Activity 	= request.getParameter("Activity");
		
		//City
		if (!PortalUtility.isNull(City)) {
			WsSearchCriteria CityCriteria = new WsSearchCriteria();
			CityCriteria.setTableName("People");
			CityCriteria.setFieldName("People.AddrCity");
			CityCriteria.setConjuctiveOperator(validOperator.getAND());
			CityCriteria.setOperator(validOperator.getEQUAL());
			CityCriteria.setValue(new String[]{City});
			wsCriteria.add(CityCriteria);
		}
		
		//Sigile
		if (!PortalUtility.isNull(Sigile)) {
			WsSearchCriteria SigileCriteria = new WsSearchCriteria();
			SigileCriteria.setTableName("People");
			SigileCriteria.setFieldName("People.NameLast");
			SigileCriteria.setConjuctiveOperator(validOperator.getAND());
			SigileCriteria.setOperator(validOperator.getEQUAL());
			SigileCriteria.setValue(new String[]{Sigile});
			
			wsCriteria.add(SigileCriteria);
		}
		
		//LegalStatus
		if (!PortalUtility.isNull(LegalStatus)) {
			WsSearchCriteria LegalStatusCriteria = new WsSearchCriteria();
			LegalStatusCriteria.setTableName("PeopleInfo");
			LegalStatusCriteria.setFieldName("PeopleInfo.InfoValue");
			LegalStatusCriteria.setConjuctiveOperator(validOperator.getAND());
			LegalStatusCriteria.setOperator(validOperator.getEQUAL());
			LegalStatusCriteria.setValue(new String[]{LegalStatus});
			wsCriteria.add(LegalStatusCriteria);
		}

		//Activity
		else if (!PortalUtility.isNull(Activity)) {
			WsSearchCriteria ActivityCriteria = new WsSearchCriteria();
			ActivityCriteria.setTableName("PeopleInfo");
			ActivityCriteria.setFieldName("PeopleInfo.InfoValue");
			ActivityCriteria.setConjuctiveOperator(validOperator.getAND());
			ActivityCriteria.setOperator(validOperator.getEQUAL());
			ActivityCriteria.setValue(new String[]{Activity});
			wsCriteria.add(ActivityCriteria);
		}
		
	}else if(inputName.equalsIgnoreCase("OrgName")){
		wsCriteria = new ArrayList<WsSearchCriteria>();
		WsSearchCriteria searchCriteria = new WsSearchCriteria();
		searchCriteria.setFieldName("People.organizationname");
		searchCriteria.setTableName("People");
		searchCriteria.setOperator(validOperator.getLIKE());
		searchCriteria.setValue(new String[]{"%"+searchVal+"%"});
		searchCriteria.setConjuctiveOperator(validOperator.getAND());
		wsCriteria.add(searchCriteria);
	}else if(inputName.equalsIgnoreCase("NameLast")){
		wsCriteria = new ArrayList<WsSearchCriteria>();
		WsSearchCriteria searchCriteria = new WsSearchCriteria();
		searchCriteria.setFieldName("People.NameLast");
		searchCriteria.setTableName("People");
		searchCriteria.setOperator(validOperator.getLIKE());
		searchCriteria.setValue(new String[]{"%"+searchVal+"%"});
		searchCriteria.setConjuctiveOperator(validOperator.getAND());
		wsCriteria.add(searchCriteria);
	}
	
	WsPeople[] wsPeoples= PortalService.getWSClient().searchPeople(lid, wsCriteria.toArray(new WsSearchCriteria[wsCriteria.size()]), 0, 100, null);
	if(wsPeoples!=null){
		for(WsPeople people : wsPeoples){
			if(inputName.equalsIgnoreCase("OrgName"))
				if(String.valueOf(people.getOrganizationName()).contains(searchVal))
					array.put(String.valueOf(people.getOrganizationName()));
				
				if(inputName.equalsIgnoreCase("NameLast"))
					if(String.valueOf(people.getNameLast()).contains(searchVal))
						array.put(String.valueOf(people.getNameLast()));

				if("People.OrganizationName".equalsIgnoreCase(inputName))
					array.put(String.valueOf(people.getOrganizationName()));
		}
	}else{
		array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));	
	}
}//BD: Code Ends Here


//SED:Code Starts Here
if(cmd.equalsIgnoreCase("sedsearch")){
	String yearRange 		= request.getParameter("yearRange");
	wsCriteria = new ArrayList<WsSearchCriteria>();
	
	WsSearchCriteria searchCriteria = new WsSearchCriteria();
	searchCriteria.setFieldName("Folder.PropertyLocation");
	searchCriteria.setTableName("Folder");
	searchCriteria.setConjuctiveOperator(validOperator.getAND());
	searchCriteria.setOperator(validOperator.getLIKE());
	searchCriteria.setValue(new String[]{"%"+searchVal+"%"});
	wsCriteria.add(searchCriteria);
	
	WsSearchCriteria FolderTypeCriteria = new WsSearchCriteria(); 
	FolderTypeCriteria.setTableName("Folder");
	FolderTypeCriteria.setFieldName("Folder.FolderType");
	FolderTypeCriteria.setConjuctiveOperator(validOperator.getAND());
	FolderTypeCriteria.setOperator(validOperator.getEQUAL());
	FolderTypeCriteria.setValue(new String[]{"SED"});		
	wsCriteria.add(FolderTypeCriteria);	
	
	if(!PortalUtility.isNull(yearRange)){
		WsSearchCriteria refCriteria = new WsSearchCriteria(); 
		refCriteria.setTableName("Folder");
		refCriteria.setFieldName("Folder.ReferenceFile");
		refCriteria.setConjuctiveOperator(validOperator.getAND());
		refCriteria.setOperator(validOperator.getEQUAL());
		refCriteria.setValue(new String[]{yearRange});		
		wsCriteria.add(refCriteria);	
	}
	folders = PortalService.getWSClient().searchFolder(lid, wsCriteria.toArray(new WsSearchCriteria[wsCriteria.size()]), 0, 100, null);
	TreeSet<String> treeSet = new TreeSet<String>();
	if(folders != null){
		for(WsFolder folder : folders){
			treeSet.add(String.valueOf(folder.getPropertyLocation()));
		}
		Iterator<String> iterator = treeSet.iterator();
		while(iterator.hasNext()){
			array.put(iterator.next());
		}
	}else{
		array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));	
	}
}//SED: Code Ends Here



//FT:Code Starts Here
if(cmd.equalsIgnoreCase("ftsearch")){
	array = new JSONArray();
	
	if(inputName.equalsIgnoreCase("FTCountry")){
		//WsValidLookup[] lookList 			= null;
		
		//lookList = PortalService.getWSClient().getValidLookup(lid, FTS_LOOKUPCODE);
		//if(lookList!=null){
			//for(WsValidLookup lokup: lookList){
				Map<String,String> countries = new HashMap();
				countries = PortalService.getFTCountries(lid, FTS_LOOKUPCODE);
				for (Map.Entry<String,String> entry : countries.entrySet()) {
					  String key = entry.getKey();
					  String value = entry.getValue();
					  if(value.contains(searchVal)){
						  JSONObject suggestionObj = new JSONObject();
						  suggestionObj.put("value", key);
						  suggestionObj.put("data",  value);
						  array.put(suggestionObj);
					  }
					  // do stuff
				}
				//if(countries.containsValue(searchVal)){
				//if(lokup.getLookUp1()==2 && lokup.getLookUpString().contains(searchVal)){
					//JSONObject suggestionObj = new JSONObject();
					//suggestionObj.put("value", lokup.getLookUpString());
					//suggestionObj.put("data",  lokup.getLookUpString2());
					//suggestionObj.put("value", countries.get(i));
					//array.put(suggestionObj);
				//}
				//}
				//}
			//}
		//}
		
	}
	
	if(inputName.equalsIgnoreCase("productDesc") || inputName.equalsIgnoreCase("product")){
		Map<String,String> products = new HashMap();
		TreeSet<String> productSet = new TreeSet<String>();
		Map<String,String> productsMap = new HashMap<String, String>();
		products = PortalService.getFTProducts(lid, FTS_LOOKUPCODE);
		for (Map.Entry<String,String> entry : products.entrySet()) {
			  String pcode = entry.getKey();
			  String pdesc = entry.getValue();
			  productsMap.put(pcode, pdesc);
			  if(inputName.equalsIgnoreCase("product")){
					if(String.valueOf(pcode).contains(searchVal))
						productSet.add(pcode);
				}
			  // do stuff
		}
		if(inputName.equalsIgnoreCase("product")){
			Iterator<String> iterator = productSet.iterator();
			while(iterator.hasNext()){
				JSONObject suggestionObj = new JSONObject();
				String productCode = iterator.next();
				suggestionObj.put("value", productCode);
				suggestionObj.put("data", productsMap.get(productCode));
				array.put(suggestionObj);
			}
		}else if(inputName.equalsIgnoreCase("productDesc")){
			productsMap = PortalUtility.sortByValue(productsMap);
			for(Entry<String,String> product : productsMap.entrySet()){
				if(String.valueOf(product.getValue()).contains(searchVal)){
					JSONObject suggestionObj = new JSONObject();
					suggestionObj.put("value", product.getValue());
					suggestionObj.put("data", product.getKey());
					array.put(suggestionObj);
				}
			}
		}
		/*
		WsValidLookup[] lookList = PortalService.getWSClient().getValidLookup(lid, FTS_LOOKUPCODE);
		TreeSet<String> productSet = new TreeSet<String>();
		Map<String,String> productsMap = new HashMap<String, String>();
		if(lookList!=null){
			for(WsValidLookup lokup: lookList){
				if(lokup.getLookUp1()==1){
					String productCode = lokup.getLookUpString();
					String productDesc = lokup.getLookUpString2();
					productsMap.put(productCode, productDesc);
					if(inputName.equalsIgnoreCase("product")){
						if(String.valueOf(productCode).contains(searchVal))
							productSet.add(productCode);
					}
				}
			}
		}

		if(inputName.equalsIgnoreCase("product")){
			Iterator<String> iterator = productSet.iterator();
			while(iterator.hasNext()){
				JSONObject suggestionObj = new JSONObject();
				String productCode = iterator.next();
				suggestionObj.put("value", productCode);
				suggestionObj.put("data", productsMap.get(productCode));
				array.put(suggestionObj);
			}
		}else if(inputName.equalsIgnoreCase("productDesc")){
			productsMap = PortalUtility.sortByValue(productsMap);
			for(Entry<String,String> product : productsMap.entrySet()){
				if(String.valueOf(product.getValue()).contains(searchVal)){
					JSONObject suggestionObj = new JSONObject();
					suggestionObj.put("value", product.getValue());
					suggestionObj.put("data", product.getKey());
					array.put(suggestionObj);
				}
			}
		}
		*/
		
		if(array.length()==0)
			array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));
		
		
	}else{
		wsCriteria = new ArrayList<WsSearchCriteria>();
		WsSearchCriteria searchCriteria = new WsSearchCriteria();
		searchCriteria.setFieldName("Folder.FolderType");
		searchCriteria.setTableName("Folder");
		searchCriteria.setOperator(validOperator.getEQUAL());
		searchCriteria.setValue(new String[]{"FT"});
		searchCriteria.setConjuctiveOperator(validOperator.getAND());
		wsCriteria.add(searchCriteria);
		
		if(inputName.equalsIgnoreCase("FolderCountry")){
			String tradeBalance = request.getParameter("tradeBalance");
			
			WsSearchCriteria CountryCriteria = new WsSearchCriteria(); 
			CountryCriteria.setTableName("FolderInfo");
			CountryCriteria.setFieldName("FolderInfo.InfoValue");
			CountryCriteria.setConjuctiveOperator(validOperator.getAND());;
			CountryCriteria.setOperator(validOperator.getLIKE());
			CountryCriteria.setValue(new String[]{"%"+searchVal+"%"});	
			wsCriteria.add(CountryCriteria);
			
			if(!(!PortalUtility.isNull(tradeBalance) && Boolean.valueOf(tradeBalance))){
				String Subcode 		= request.getParameter("SubType");
				String Year 		= request.getParameter("Year");
				
				//Subcode
				WsSearchCriteria SubcodeCriteria = new WsSearchCriteria(); 
				SubcodeCriteria.setTableName("Folder");
				SubcodeCriteria.setFieldName("Folder.Subcode");
				SubcodeCriteria.setConjuctiveOperator(validOperator.getAND());
				SubcodeCriteria.setOperator(validOperator.getEQUAL());
				SubcodeCriteria.setValue(new String[]{Subcode});			
				wsCriteria.add(SubcodeCriteria);
				
				//Year
				if (!PortalUtility.isNull(Year)) {
					String stDate = Year+"-01-01";
					String enDate = Year+"-12-31";
					if(Year.contains(" - ")){
						String years[] = Year.split(" - ");
						stDate = years[0]+"-01-01";
						enDate = years[1]+"-12-31";
					}
					WsSearchCriteria YearCriteria = new WsSearchCriteria();
					YearCriteria.setTableName("Folder");
					YearCriteria.setFieldName("Folder.InDate");
					YearCriteria.setConjuctiveOperator(validOperator.getAND());
					YearCriteria.setOperator(validOperator.getBETWEEN());
					YearCriteria.setValue(new String[]{stDate,enDate});
					wsCriteria.add(YearCriteria);
				}
			}
		}
		
		WsFolder[] folWsFolders = PortalService.getWSClient().searchFolder(lid, wsCriteria.toArray(new WsSearchCriteria[wsCriteria.size()]), 0, 100, null);
		TreeSet<String> treeSet = new TreeSet<String>();
		String prodname = "";
		String prodcode="";
		if(folWsFolders!=null){
			for(WsFolder fldr: folWsFolders){
				if(inputName.equalsIgnoreCase("product"))
					if(String.valueOf(fldr.getFolderRSN()).contains(searchVal))
						treeSet.add(String.valueOf(fldr.getFolderRSN()));
				if(inputName.equalsIgnoreCase("FolderCountry")){
					Map<Integer, String> mapFolderInfo = PortalService.getFolderInfoInMap(PortalService.getWSClient().getFolderInfo(lid, fldr.getFolderRSN(), null));
					treeSet.add(String.valueOf(mapFolderInfo.get(CODE_SED_COUNTRY)));
				}
			}
			Iterator<String> iterator = treeSet.iterator();
			while(iterator.hasNext()){
				array.put(iterator.next());
			}
		}else{
			array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));
		}
	}
}//FT:Code Ends Here



//ViewSubscribers:Code Starts Here
if(cmd.equalsIgnoreCase("ViewSubscribers")){
	array = new JSONArray();
	final int SUBSCRIBED_PEOPLECODE = 3;
	String adminViewSubscribers = request.getParameter("adminViewSubscribers")==null ? "1, 2, 3, 4" : request.getParameter("adminViewSubscribers");
	String adminViewSubscribersArr[] =  adminViewSubscribers.split(", ");
	String[] folderTypes = new String[adminViewSubscribersArr.length];
	for(int i=0; i<adminViewSubscribersArr.length; i++){
		if(adminViewSubscribersArr[i].contains("1"))		folderTypes[i] = "BO";
		else if(adminViewSubscribersArr[i].contains("2"))	folderTypes[i] = "BO";
		else if(adminViewSubscribersArr[i].contains("3"))	folderTypes[i] = "FT";
		else if(adminViewSubscribersArr[i].contains("4"))	folderTypes[i] = "SED";
	}

	List<WsSearchCriteria> criteriaList = new ArrayList<WsSearchCriteria>(); 

	WsSearchCriteria criteriaFolderType = new WsSearchCriteria();
	criteriaFolderType.setFieldName("Folder.FolderType");
	criteriaFolderType.setTableName("Folder");
	criteriaFolderType.setConjuctiveOperator(validOperator.getAND());
	criteriaFolderType.setOperator(validOperator.getIN());
	criteriaFolderType.setValue(folderTypes);
	criteriaList.add(criteriaFolderType);

	WsSearchCriteria criteriaPeopleCode = new WsSearchCriteria();
	criteriaPeopleCode.setFieldName("FolderPeople.PeopleCode");
	criteriaPeopleCode.setTableName("FolderPeople");
	criteriaPeopleCode.setConjuctiveOperator(validOperator.getAND());
	criteriaPeopleCode.setOperator(PortalService.getClientValidOperators().getEQUAL());
	criteriaPeopleCode.setValue(new String[]{String.valueOf(SUBSCRIBED_PEOPLECODE)});
	criteriaList.add(criteriaPeopleCode);

	WsSearchCriteria[] scs = criteriaList.toArray(new WsSearchCriteria[criteriaList.size()]);  
	WsPeople[] wsPeopleList = PortalService.getWSClient().searchPeople(lid, scs, 1, 100, new String[]{"People.EmailAddress"}); 

	if(wsPeopleList!=null && wsPeopleList.length>0)
    	for(WsPeople people:wsPeopleList){
			array.put(PortalUtility.getString(people.getEmailAddress()));
    	}
	else array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e")); 
}//ViewSubscribers:Code Ends Here


//Auto complete country field in user_registration page:Code Starts Here
if(cmd.equalsIgnoreCase("userRegistration") || cmd.equalsIgnoreCase("myprofile")){
	array = new JSONArray();
	if(inputName.equalsIgnoreCase("country")){
	Map<String,String> countries = new HashMap();
	countries = PortalService.getCountriesByLookupCode(lid, COUNTRY_LOOKUPCODE);
	for (Map.Entry<String,String> entry : countries.entrySet()) {
		  String key = entry.getKey();
		  String value = entry.getValue();
		  if(value.contains(searchVal.toUpperCase())){
			  JSONObject suggestionObj = new JSONObject();
			  suggestionObj.put("value", key);
			  suggestionObj.put("data",  value);
			  array.put(suggestionObj);
		  }
		  
	}
	}else if(inputName.equalsIgnoreCase("city")){
		
		//WsValidCity[] cities = PortalService.getValidCities();
		WsValidLookup[] cities = PortalService.getWSClient().getValidLookup(lid, CITY_LOOKUPCODE);
		for (WsValidLookup city : cities) {
			  String key = city.getLookUpString();
			  String value = city.getLookUpString();
			  if(value.contains(searchVal.toUpperCase())){
				  JSONObject suggestionObj = new JSONObject();
				  suggestionObj.put("value", key);
				  suggestionObj.put("data",  value);
				  array.put(suggestionObj);
			  }
			  
		}
		
	}
	if(array.length()==0){
		array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));
	}
	 
}//Code Ends Here

//Auto complete country field in License_Add.jsp and EditOpportunity.jsp(BO) page:Code Starts Here
if(cmd.equalsIgnoreCase("BOAdd") || cmd.equalsIgnoreCase("BOEdit")){
	array = new JSONArray();
	Map<String,String> countries = new HashMap();
	countries = PortalService.getCountriesByLookupCode(lid, COUNTRY_LOOKUPCODE);
	for (Map.Entry<String,String> entry : countries.entrySet()) {
		  String key = entry.getKey();
		  String value = entry.getValue();
		  if(value.contains(searchVal.toUpperCase())){
			  JSONObject suggestionObj = new JSONObject();
			  suggestionObj.put("value", key);
			  suggestionObj.put("data",  value);
			  array.put(suggestionObj);
		  }
		  
	}
	if(array.length()==0){
		array.put(String.valueOf("No Data Found / Aucune donn�e trouv�e"));
	}
	 
}//Code Ends Here

responseObject.put("suggestions", array);
 
out.print(responseObject);
%>