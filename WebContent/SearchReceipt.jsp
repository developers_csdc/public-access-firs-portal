	<%@include file= "header.jsp" %>
	<div class="container-fluid">
		<div class="row" id="Receipt_Search">
			<div class="col-lg-3 col-md-3"></div>
			<div class="col-lg-6 col-md-6">
				<div class="panel custom_panel panel-default">
			  		<div class="panel-heading">
			  		Verify Receipt
			  		</div>
			  		<div class="panel-body">
				  		<p style="margin-bottom: 30px;">Verify your Receipt by providing the details below</p>
				  		<div class="row custom_row" style="margin-bottom: 20px;">
					  		<div class="form-group hide">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="">Select a Receipt type</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<!-- <label class="radio-inline"><input type="radio" name="receiptTypeName" value="NONFOREIGN" checked>Other than FOREIGN</label>
									<label class="radio-inline"><input type="radio" name="receiptTypeName" value="FOREIGN">FOREIGN</label> -->
						    		<div class="error" id="error_Search"></div>
					    		</div>
				 			</div>
			 			</div>
				  		<div class="row custom_row">
					  		<div class="form-group">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Receipt Number</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<input type="text" class="form-control input-sm custom_input" id="receipt_no" placeholder="Enter Receipt Number">
						    		<div class="error" id="error_Search"></div>
					    		</div>
				 			</div>
			 			</div>
			 			<div class="row custom_row" id="tinInputContainer">
					  		<div class="form-group">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">TIN</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<input type="text" class="form-control input-sm custom_input" id="TIN" placeholder="Enter TIN Here">
						    		<div class="error" id="error_Search"></div>
					    		</div>
				 			</div>
			 			</div>
			 			<div class="row" style="margin-bottom:10px;">
	                    	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 pull-left text-left" style="padding: 0px 0px 0px 0px;line-height: 28px;">
								<span id="miniCaptcha_Search" style="margin-left:17px;"></span>&nbsp&nbsp
	                    	</div>
	                    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 pull-left text-left">
							<input type="text" class="form-control input-sm custom_input"
								id="expression-result" placeholder="Enter Result here"
								maxlength="2" onkeypress="return onlyNos(event,this)">
						</div>
	                    	<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 pull-left text-left">
								<button onclick="resetBotBoot();" class="btn btn-primary custom_btn" style="height:30px;width:40px;">
									<span class="glyphicon glyphicon-repeat"></span>
								</button>
	                    	</div>
	                    </div>
			 			<div class="row">
				 			<div class="col-lg-12">
							<span class="text-danger" style="color: #a94442;font-size: 11px;font-weight: 700;font-style: italic;">
								NOTE:<sup class="text-danger">*</sup><sup
								class="text-danger">*</sup>Receipt Numbers can be verified after 24 hours of the payment date.
							</span>
						</div>
			 			</div>
			 			<div class="text-center" style="margin-top:5px;margin-bottom: 5px;">
			 				<button class="btn btn-default custom_btn" onclick="localReceiptValidation();" id="submit_button">Submit</button>
			 				<button class="btn btn-default custom_btn" id="btnRedirectSearchRceceipt">Reset</button>
			 			</div>
			 			<div class="loader-container hide" id="receipt_loader">
							<p class="alert alert-success">Please wait, Retrieving data.. <img src="images/loader.gif" alt="" height="20px" width="20px"></p>
						</div>
			  		</div>
				</div>
			</div>
		</div>
		
		<div class="row hide" id="Receipt_Details">
			
			<div class="col-lg-3 col-md-3"></div>
			<div class="col-lg-6 col-md-6">
				<div class="panel custom_panel panel-default">
			  		<div class="panel-heading">
			  		Receipt Details
			  		</div>
			  		<div class="panel-body">
				  		<div class="row custom_row">
					  		<div class="form-group">
					    		<div class="col-md-2 col-lg-2 col-sm-2"></div>
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Receipt Number</label>
					    		<div class="col-md-6 col-lg-6" id="Receipt_No"></div>
				 			</div>
			 			</div>
			 			<div class="row custom_row">
					  		<div class="form-group">
					  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Name:</label>
					    		<div class="col-md-6 col-lg-6" id="Name"></div>
				 			</div>
			 			</div>
			 			<div class="row custom_row">
					  		<div class="form-group">
					  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Amount:</label>
					    		<div class="col-md-6 col-lg-6" id="Amount"></div>
				 			</div>
			 			</div>
			 			<div class="row custom_row">
					  		<div class="form-group">
					  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Date of Payment</label>
					    		<div class="col-md-6 col-lg-6" id="Payment_date"></div>
				 			</div>
			 			</div>
			 			<div class="row custom_row">
					  		<div class="form-group">
					  			<div class="col-md-2 col-lg-2 col-sm-2"></div>
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="reg_addr">Payment Status</label>
					    		<div class="col-md-6 col-lg-6">Full</div>
				 			</div>
			 			</div>
			 			<div class="text-center" style="margin-bottom: 50px;">
							<a id="export_Receipt" type="button" class="btn btn-default custom_btn">Download</a>
							<button type="button" id="RedirectSearchRceceipt" class="btn btn-default custom_btn ">Reset</button>
						</div>
			  		</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$("input[name='receiptTypeName']").click(function () {
	    	var value = $(this).val();
	    	
	    	if(value != null && value != ""){
	    		switch (value) {
			        case "NONFOREIGN":
			            $("#TIN").val("");
			            $("#tinInputContainer").show();
			            break; 
			        case "FOREIGN":
			        	$("#TIN").val("");
			        	$("#tinInputContainer").hide();
			            break; 
			        default: 
			            console.log("Value not found in CASE");
	    		}
	    	}
	    	
		});
	</script>