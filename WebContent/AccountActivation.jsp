

</head>
<body>
	<div class="container-fluid">
		<!--  <div class="row" style="margin-top:30px;" id="activation-process-section">
			<div class="panel panel-default col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-sm-10">
      			<div class="panel-body">
        		<center>
	          		<img class="img-responsive" src="images/loading.gif" style="padding:5px 5px 5px 5px;margin-top:5px;width:75px; height:75px;"><br>
	          		<p class="desc">
	          		<font color="darkgreen" style="font-weight:bold">
	          			Getting ready for Account activation...Please wait...
	          		</font> 
	          		</p>
        		</center>        
      			</div>
			</div>
		</div> -->
		<div class="row hide" style="margin-top:30px;" id="activation-success">
			<div class="panel panel-default col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-sm-10">
      			<div class="panel-body">
        		<center>
	          		<img src="https://upload.wikimedia.org/wikipedia/commons/c/cb/B-check.svg" style="width:50px; height: 40px;">
	          		<p class="desc"><font color="darkgreen" style="font-weight:bold"><%=request.getAttribute("message") %></font>
	          		<br>You can login using your credentials. 
	          		 <br><br><a href="index.jsp"><button class="btn btn-default" style="border-radius:0px;">Home</button></a> 
	          		</p>
        		</center>        
      			</div>
			</div>
		</div>
		<!--<div class="row hide" hide style="margin-top:30px;" id="already-activated">
			<div class="panel panel-default col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-sm-10">
      			<div class="panel-body">
        		<center>
	          		<img src="https://upload.wikimedia.org/wikipedia/commons/c/cb/B-check.svg" style="width:50px; height: 40px;">
	          		<p class="desc"><font color="darkgreen" style="font-weight:bold">Your Account is already active!</font>
	          		<br>Please login using your credentials. 
	          		<br><br><a href=""><button class="btn btn-default" style="border-radius:0px;">Home</button></a> 
	          		</p>
        		</center>        
      			</div>
			</div>
		</div> -->
		<!--  <div class="row hide" style="margin-top:30px;" id="invalid-url">
			<div class="panel panel-default col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-sm-10">
      			<div class="panel-body">
        		<center>
	          		<img class="img-responsive" src="images/Red-Cancel.jpg" style="padding:5px 5px 5px 5px;margin-top:5px;width:75px; height:75px;"><br>
	          		<p class="desc"><font color="red" style="font-weight:bold">Invalid URL/ Error Occurred</font></p>
	          		<br><br><a href=""><button class="btn btn-default" style="border-radius:0px;">Home</button></a> 
        		</center>        
      			</div>
			</div>
		</div> -->
	</div>
		
</body>
</html>