<%@page import="java.util.List"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidInfoValue"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidStreetType" %>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidProvinces" %>
<%@page import="com.csdcsystems.amanda.client.stub.WsValidStreet" %>
<%@page import="com.csdcsystems.amanda.common.*"%>
<%@page import="com.csdcsystems.amanda.util.Language"%>
<%@include file="include/includeconstant.jsp"%>
<%@page buffer="8kb" %>

<%
	request.setAttribute("pageName", "userRegistration");
	WsValidInfoValue[] securityQueValidInfoValues = PortalService.getWSClient().getValidInfoValues(lid, 1032);  //1032 : infovalue code for security Question
	List<JSONObject> titleList = PortalService.getNameTitleList(LanguageCode.getLanguageCode());
	resourceBundle.getText("LABEL_HEADER_MAIN");
%>
<link rel="stylesheet" href="<%=CSS_PORTAL%>userRegistration.css">

<div class="">
	<h1 style="font-size: 28px;"><%=resourceBundle.getText("LABEL_HEADER_MAIN")%></h1>
	<div class=""></div>
	<!--p>For more info and support, contact us!</p-->
</div>

<div class="row" style="margin: 0 9%;">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<span class="FormtableData" style="font-size: 12px"> <%=resourceBundle.getText("MESSAGE_PROPERTY_INSERT_MANDATORY1")%>
			<B><U><%=resourceBundle.getText("MESSAGE_PROPERTY_INSERT_NOTE")%></U>:</B>
			<%=resourceBundle.getText("MESSAGE_PROPERTY_INSERT_MANDATORY2")%> <span
			style="color: red;">*</span> <%=resourceBundle.getText("MESSAGE_PROPERTY_INSERT_MANDATORY3")%>
		</span>
		<hr />
	</div>
</div>

<input type="HIDDEN" name="lid" id="lid" value="<%=lid%>">
<div id="userRegistrationDiv" style="margin-left: 10%; margin-right: 10%;">



	<div class="row">
		<div class="col-lg-4" id="emailAddressDiv">
			<label for="EmailAddress" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_USERNAME")%><span style="color: red;">*</span></label>
			<div class="input-group">
				<span class="input-group-addon"><span class="">@</span></span> 
				<input id="EmailAddress" name="EmailAddress" type="text" class="form-control req" value="" onblur="checkValidate();">
			</div>
		</div>
		<div class="col-lg-2">
			<label for="Title" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_TITLE")%></label>
			<select id="Title" name="Title" class="form-control" onblur="checkValidate();">
				<option value=""></option>
				<%for(JSONObject  titleJsonObj : titleList){%>
					<option value="<%=titleJsonObj.get("TitleValue")%>"><%=titleJsonObj.get("TitleLable")%></option>
				<%} %>
			</select>
		</div>
		<div class="col-lg-6">
			<div class="row">
				<div class="col-lg-6" id="nameDiv">
					<label for="Name"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_NAME")%>
					<span style="color: red;">*</span></label> 
					<input id="Name" name="Name" type="text" class="form-control req" value="" onblur="checkValidate();">
				</div>
				<div class="col-lg-6" id="nameLastDiv">
					<label for="NameLast"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_NAME_LAST")%>
					<span style="color: red;">*</span></label> 
					<input id="NameLast" name="NameLast" type="text" class="form-control req" value="" onblur="checkValidate();">
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	<div class="row">
		<div class="col-lg-4" id="addressDiv">
			<label for="Address" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_ADDRESS")%>
			<span style="color: red;">*</span></label> 
			<input id="Address" name="Address" type="text" class="form-control req" onblur="checkValidate();">
		</div>
		<div class="col-lg-4" id="cityDiv">
			<label for="City" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_CITY")%>
			<span style="color: red;">*</span></label>
			<input id="City" name="City" type="text" class="form-control req" onblur="checkValidate();"> 
		</div>
		<div class="col-lg-4" id="countryDiv">
			<label for="Country"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_COUNTRY")%></label>				
			<input type="text" name="Country" id="Country" class="form-control" value="" />
			<div id="suggestions-container" style="position: relative; float: left; width: 400px; margin: 10px;"></div>
		</div>
	</div>
	
	
	<div class="row">
		<div class="col-lg-4" id="phone1Div">
			<label for="Phone1" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_PHONE")%>
			<span style="color: red;">*</span></label>
			<div class="input-group">
				<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span> 
				<input id="Phone1" name="Phone1" type="text" class="form-control req mandatory" onkeyup="phoneMask(this.id);" value="" onblur="validPhoneMask(this.id);checkValidate();" >
			</div>
		</div>
		<div class="col-lg-4" id="phone2Div">
			<label for="Phone2" class=""><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_FAX")%></label>
			<div class="input-group">
				<span class="input-group-addon"><i class="fa fa-fax"></i></span> 
				<input id="Phone2" name="Phone2" type="text" class="form-control" onkeyup="phoneMask(this.id);" value="" onblur="validPhoneMask(this.id);checkValidate();" >
			</div>
		</div>
		<div class="col-lg-4" id="postalCodeDiv">
			<label for="PostalCode"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_ZIP_CODE")%></label>
			<input id="PostalCode" name="PostalCode" type="text" class="form-control " value="" onkeypress="return numbersonly(event);">
		</div>
	</div>
	
	
	<div class="row">
		<div class="col-lg-6" id="questionDiv">
			<label for="Question" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_SECURITY_QUESTION")%>
			<span style="color: red;">*</span></label>
			<select id="Question" name="Question" class="form-control req" onblur="checkValidate();">
				<option value=""></option>
				<%
					 for(WsValidInfoValue infoValue : securityQueValidInfoValues){
				%>
					<option value="<%=infoValue.getInfoValue() %>" ><%=infoValue.getInfoDesc() %></option>						
				<%} %>
			</select>
		</div>
		<div class="col-lg-6" id="questionAnswerDiv">
			<label for="QuestionAnswer" class="mandatory"><%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_SECURITY_QUESTION_ANSWER")%>
			<span style="color: red;">*</span></label>
			<input id="QuestionAnswer" name="QuestionAnswer" type="text" class="form-control req" onblur="checkValidate();">
		</div>
	</div>
	


	
	<div class="row">
		<div class="panel panel-success">
			<div class="panel-body">
				<div class="col-md-9 col-lg-9" style="padding-top: 8px;">
					<input type="checkbox" id="checkTermsAndConditions" style="margin-top: -1px;"/> &nbsp; 
					<b>
						<%=resourceBundle.getText("LABEL_CHECK_TERMS_AND_CONDITIONS") %>
						<a href="javascript:" onclick="$('#termsAndConditionsModal').modal('show');"><%=resourceBundle.getText("LINK_CHECK_TERMS_AND_CONDITIONS") %></a>.
					</b>
				</div>
				<div class="pull-right">
					<button id="btnSend" type="button" class="btn btn btn-success"
						onclick="f_submitMyRegistration()" disabled="disabled">
						<span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;
						<%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_BUTTON_SEND")%>
					</button>
					 &nbsp; 
					<button id="btnReset" type="button" class="btn  btn-warning"
						onclick="f_gotoHome();">
						<span class="glyphicon glyphicon-remove"></span>&nbsp;
						<%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_BUTTON_CANCEL")%>
					</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="termsAndConditionsModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><%=resourceBundle.getText("LINK_CHECK_TERMS_AND_CONDITIONS") %></h4>
				</div>
				<div class="modal-body">
					<%=resourceBundle.getText("LABEL_REGISTRATION_POLICY") %><br>
					<%-- <%=resourceBundle.getText("LABEL_REGISTRATION_POLICY1") %><br>
					<%=resourceBundle.getText("LABEL_REGISTRATION_POLICY2") %><br>
					<ul>
						<li type="round"><%=resourceBundle.getText("LABEL_REGISTRATION_POLICY3") %></li>
						<li type="round"><%=resourceBundle.getText("LABEL_REGISTRATION_POLICY4") %></li>
						<li type="round"><%=resourceBundle.getText("LABEL_REGISTRATION_POLICY5") %></li>
					</ul> --%>
				</div>
			</div>
		</div>
	</div>
	
	<div id="userCheckModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title"><%=resourceBundle.getText("USER_CHECK_MESSAGE_WARNING") %></h4>
				</div>
				<div class="modal-body" id="userCheckModalBody" style="font-size: large; min-height: 135px">
				</div>
			</div>
		</div>
	</div>
	
	<div id="ajaxLoaderDiv" style="display: none;">
		<div class="spin-loader">
			<%=resourceBundle.getText("LABEL_SUBSCRIBER_REGISTRATION_BUTTON_PLEASE_WAIT") %>
			<span><i class="fa fa-spinner fa-spin fa-2x"></i></span>
		</div>
	</div>

	<div style="height: 30px"></div>
</div>
<input type="hidden" name="query" id="query" value="">
<input type="hidden" name="pageName" id="pageName"
	value="user_registration">
<input type="hidden" name="commad" id="command" value="">
<input type="hidden" name="peopleRSN" id="peopleRSN" value="">

<script type="text/javascript">

var JS_ALERT_ERROR_FIRST_NAME = "<%=resourceBundle.getJsText("JS_ALERT_ERROR_FIRST_NAME")%>";
var JS_ALERT_ERROR_LAST_NAME="<%=resourceBundle.getJsText("JS_ALERT_ERROR_LAST_NAME")%>";
var JS_ALERT_ERROR_CITY= "<%=resourceBundle.getJsText("JS_ALERT_ERROR_CITY")%>";
var JS_ALERT_ERROR_ADDRESS= "<%=resourceBundle.getJsText("JS_ALERT_ERROR_ADDRESS")%>";
var JS_ALERT_ERROR_COMPANY_NAME ="<%=resourceBundle.getJsText("JS_ALERT_ERROR_COMPANY_NAME")%>";
var JS_ALERT_ERROR_COMMUNITY ="<%=resourceBundle.getJsText("JS_ALERT_ERROR_COMMUNITY")%>";
var JS_ALERT_ERROR_PROVINCE= "<%=resourceBundle.getJsText("JS_ALERT_ERROR_PROVINCE")%>";
var JS_ALERT_ERROR_PHONE1="<%=resourceBundle.getJsText("JS_ALERT_ERROR_PHONE1")%>";
var JS_ALERT_ERROR_EMAIL_ADDRESS="<%=resourceBundle.getJsText("JS_ALERT_ERROR_EMAIL_ADDRESS")%>";
var JS_ALERT_ERROR_BIRTH_DATE="<%=resourceBundle.getJsText("JS_ALERT_ERROR_BIRTH_DATE")%>";
var JS_ALERT_ERROR_IDENTIFICATION_DOCUMENT="<%=resourceBundle.getText("JS_ALERT_ERROR_IDENTIFICATION_DOCUMENT")%>";
var JS_ALERT_ERROR_IDENTIFICATION_DOCUMENT_NUMBER= "<%=resourceBundle.getText("JS_ALERT_ERROR_IDENTIFICATION_DOCUMENT_NUMBER")%>";
var JS_ALERT_ERROR_PASSWORD1= "<%=resourceBundle.getJsText("JS_ALERT_ERROR_PASSWORD1")%>";
var JS_ALERT_ERROR_PASSWORD2="<%=resourceBundle.getJsText("JS_ALERT_ERROR_PASSWORD2")%>";
var JS_ALERT_ERROR_QUESTION="<%=resourceBundle.getJsText("JS_ALERT_ERROR_QUESTION")%>";
var JS_ALERT_ERROR_QUESTION_ANSWER="<%=resourceBundle.getJsText("JS_ALERT_ERROR_QUESTION_ANSWER")%>";
var JS_ALERT_ERROR_MISMATCH = "<%=resourceBundle.getJsText("JS_ALERT_ERROR_MISMATCH")%>";
var JS_ALERT_ERROR_PASSWORD_STRENGTH = "<%=resourceBundle.getJsText("JS_ALERT_ERROR_PASSWORD_STRENGTH")%>";
var JS_ALERT_ERROR_SHORT_PASSWORD = "<%=resourceBundle.getJsText("JS_ALERT_ERROR_SHORT_PASSWORD")%>";
var JS_ALERT_ERROR_IDENTIFICATION_DOCUMENT_NUMBER_STRENGTH= "<%=resourceBundle.getJsText("JS_ALERT_ERROR_IDENTIFICATION_DOCUMENT_NUMBER_STRENGTH")%>";
var CALENDAR_BUTTON_PREV_YEAR="<%=resourceBundle.getJsText("CALENDAR_BUTTON_PREV_YEAR")%>";
var CALENDAR_BUTTON_NEXT_YEAR="<%=resourceBundle.getJsText("CALENDAR_BUTTON_NEXT_YEAR")%>";
var LABEL_SUBSCRIBER_REGISTRATION_BUTTON_SEND="<%=resourceBundle.getJsText("LABEL_SUBSCRIBER_REGISTRATION_BUTTON_SEND")%>";
var LABEL_SUBSCRIBER_REGISTRATION_BUTTON_PLEASE_WAIT="<%=resourceBundle.getJsText("LABEL_SUBSCRIBER_REGISTRATION_BUTTON_PLEASE_WAIT")%>";
var MESSAGE_PROFILE_INSERT_SUCCESS="<%=resourceBundle.getJsText("MESSAGE_PROFILE_INSERT_SUCCESS")%>";
var MESSAGE_PROFILE_INSERT_FAIL="<%=resourceBundle.getJsText("MESSAGE_PROFILE_INSERT_FAIL")%>";
var JS_ALERT_EMAIL_ALREADY_EXIST="<%=resourceBundle.getJsText("JS_ALERT_EMAIL_ALREADY_EXIST")%>";
var JS_ALERT_NINEA_NUMEBR="<%=resourceBundle.getJsText("JS_ALERT_NINEA_NUMEBR")%>";
var JS_ALERT_ERROR_PAYMENT_TYPE="<%=resourceBundle.getJsText("JS_ALERT_ERROR_PAYMENT_TYPE")%>";
var LABEL_PAYMENT_PROCESSING = "<%=resourceBundle.getJsText("LABEL_PAYMENT_PROCESSING")%>";
var MESSAGE_CHECK_TERMS_AND_CONDITIONS="<%=resourceBundle.getJsText("MESSAGE_CHECK_TERMS_AND_CONDITIONS") %>";
var JS_ALERT_INVALID_EMAIL="<%=resourceBundle.getJsText("JS_ALERT_INVALID_EMAIL") %>";
var MESSAGE_USER_EXIST_NEEDS_PASSWORD="<%=resourceBundle.getJsText("MESSAGE_USER_EXIST_NEEDS_PASSWORD") %>";
var MESSAGE_USER_EXIST="<%=resourceBundle.getJsText("MESSAGE_USER_EXIST") %>";
var MESSAGE_PASSWORD_SETUP_SUCCESS="<%=resourceBundle.getJsText("MESSAGE_PASSWORD_SETUP_SUCCESS") %>";
var MESSAGE_PASSWORD_SETUP_FAIL="<%=resourceBundle.getJsText("MESSAGE_PASSWORD_SETUP_FAIL") %>";
var LABEL_BUTTON_CONTINUE="<%=resourceBundle.getJsText("LABEL_BUTTON_CONTINUE") %>";
var USER_CHECK_MESSAGE_WARNING="<%=resourceBundle.getJsText("USER_CHECK_MESSAGE_WARNING") %>";
var LABEL_BUTTON_LOGIN="<%=resourceBundle.getJsText("LABEL_BUTTON_LOGIN") %>";
var JS_ALERT_NINIA_EXISTS="<%=resourceBundle.getJsText("JS_ALERT_NINIA_EXISTS") %>";

var languageSelected = '<%=LanguageCode.getLanguageCode()%>';
</script>
<script src="<%=JS_PORTAL %>userRegistration.js"></script>
<script src="<%=JS_PORTAL %>validator.js"></script>
<script src="<%=JS_PORTAL %>enterKeyFormSubmit.js"></script>
<script src="<%=JS_PORTAL %>checkModifiedFields.js"></script>
