<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@include file="include/includeconstant.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title>FIRS Receipt Print | Home</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="receiptPrint/view.css">
</head>
<body>
<body id="">
	<div id="form_container" style="margin-top:80px;">
		<div id="form_53308" class="" style="padding: 20px 20px 20px 20px;height: 50vh;">
			<div class="form_description" style="background-color:rgba(134, 13, 13, 0.45);">
				<h2 style="padding: 10px 10px 10px 10px;">FIRS Receipt Print | Home </h2>
			</div>
			<ul>
				<li id="li_1" style="list-style-type:none">
					<label class="description" for="element_1">Upload your file here &#8628;</label>
					<div style="margin-top:15px;">
						<input type="file" name="pic" id="excelFileInput" accept=".xlsx, .xls, .csv">
					</div>
				</li>
				<li class="buttons" style="margin-top:30px;">
					<button class="button_text" id="btnGenerateReports">Generate Receipts</button>
				</li>
			</ul>
		</div>
		<div id="footer">
			By <a href="#">FIRS Development Team</a>
		</div>
	</div>
	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/portal/receiptPrint.js"></script>
</body>
</html>