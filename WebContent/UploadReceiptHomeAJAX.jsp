<%@page import="java.util.Date"%>
<%@page import="java.io.BufferedInputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStream"%>
<%@page import="com.esspl.amanda.receiptdownload.JasperPDFDownload"%>
<%@page import="jxl.Cell"%>
<%@page import="jxl.Sheet"%>
<%@page import="jxl.Workbook"%>
<%@page import="com.esspl.amanda.receiptdownload.CreditNotesBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="java.util.Arrays"%>
<%@ page language="java"%>
<%@ include file="../include/includeconstant.jsp"%>
<%@page import="com.csdcsystems.amanda.util.PortalUtility"%>
<%@page import="org.codehaus.jettison.json.JSONObject"%>
<%!
	public static String getFolderName(String user,String reportType){
		String folderName = "";
		Date d = new Date();
		long timeMilli = d.getTime();
		System.out.print(d.toString());
		
		folderName = user+reportType+String.valueOf(timeMilli);
		
		return folderName;
	}

	public static JSONObject validateExcel(Workbook workbook) throws Exception{
		JSONObject validationObj = new JSONObject();
		int emptyRow = 0;
		
		Sheet excelSheet = workbook.getSheet(0);
		
		int totalRows = excelSheet.getRows();
        int totalColumns = excelSheet.getColumns();
        
        if(emptyRow < 51){
        	for(int row = 1; row < totalRows; row++) {
                System.out.println("Starting Row : " + row);
                
                for(int col = 0; col < totalColumns; col++) {
                    /*Obtain reference to the Cell using getCell(int col, int row) method of sheet*/
                    Cell cell = excelSheet.getCell(col, row);
                    /*Read the contents of the Cell using getContents() method, which will return it as a String*/
                    String cellValue = cell.getContents();
                    switch (col) {
                    case 0:
                    	if(cellValue == "" || cellValue == null){
                        	emptyRow++;
                        }
                        break;
                    }
                    
                }
            }
        }
		
        validationObj.put("emptyRow", emptyRow);
		return validationObj;
	}
%>
<%
 	response.setHeader("Content-Type", "application/json; charset=UTF-8");	
	String 	result = "",
			command = "",
			key = "",
			pdfDownloadPath = "D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\GeneratedPdfs\\",
			//pdfDownloadPath = "/opt/tomcat/FIRSRECEIPTDOWNLOAD/GeneratedPdfs/",
			jrxmlContainerPath = "D:\\work-now\\FIRS_CREDIT_NOTES_PDF_GENERATION\\";
			//jrxmlContainerPath = "/opt/tomcat/FIRSRECEIPTDOWNLOAD/";
	
	JSONObject pageResponseObject = new JSONObject();
	Workbook wrk1 = null;
	File file = new File("");

	if(ServletFileUpload.isMultipartContent(request)){
		List<FileItem> multiparts = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
		for (FileItem item : multiparts) {
			if (!item.isFormField()) {
				file = new File(item.getName());
				try{
					wrk1 = Workbook.getWorkbook(item.getInputStream());
				}catch(Exception e){
					e.printStackTrace();
				}
				System.out.println(file.toString());
			}else{
				if("cmd".equals(item.getFieldName())){
					command = new String(item.get());
				}else if("key".equals(item.getFieldName())){
					key = new String(item.get());
				}
			}
		}
		
	}

	if(command.equalsIgnoreCase("ValidateWHT")){
		/* Reading and Validation for the EXCEL File */
		boolean isExcelContainsData = false;
		boolean isExcelDataExceedsLimit = false;
		boolean isExceptionOccured = false;
		boolean isExcelQualified = false;
		final int EXCEL_MAX_ROW_LIMIT = 51;
				
		try{
			Sheet sheet1 = wrk1.getSheet(0);
			
			int totRows = sheet1.getRows();
            int totCols = sheet1.getColumns();
            
            /* Checking EXCEL */
            isExcelContainsData = totRows > 1 ? true : false;
            isExcelDataExceedsLimit = totRows > EXCEL_MAX_ROW_LIMIT ? true : false;
            int errorCount = (int)validateExcel(wrk1).get("emptyRow");
            
            if(isExcelContainsData && !isExcelDataExceedsLimit && errorCount == 0){
            	System.out.println("File is Qualified for Upload");
            	isExcelQualified = true;
            }
            
            System.out.println(totRows);
            System.out.println(totCols);
			
		}catch(Exception e){
			isExceptionOccured = true;
			pageResponseObject.put("exceptionMsg", e.getMessage());
			e.printStackTrace();
		}finally{
			pageResponseObject.put("isExcelQualified", isExcelQualified);
			pageResponseObject.put("isExceptionOccured", isExceptionOccured);
			pageResponseObject.put("isExcelContainsData", isExcelContainsData);
			pageResponseObject.put("isExcelDataExceedsLimit", isExcelDataExceedsLimit);
		}
	}else if(command.equalsIgnoreCase("ValidateCREDIT")){
		/* Reading and Validation for the EXCEL File */
		boolean isExcelContainsData = false;
		boolean isExcelDataExceedsLimit = false;
		boolean isExceptionOccured = false;
		boolean isExcelQualified = false;
		final int EXCEL_MAX_ROW_LIMIT = 51;
				
		try{
			Sheet sheet1 = wrk1.getSheet(0);
			
			int totRows = sheet1.getRows();
            int totCols = sheet1.getColumns();
            
            /* Checking EXCEL */
            int emptyRow = (int)validateExcel(wrk1).get("emptyRow");
            isExcelContainsData = emptyRow == 0 ? true : false;
            isExcelDataExceedsLimit = totRows > EXCEL_MAX_ROW_LIMIT ? true : false;
            
            
            if(isExcelContainsData && !isExcelDataExceedsLimit && emptyRow == 0){
            	System.out.println("File is Qualified for Upload");
            	isExcelQualified = true;
            }
            
            System.out.println(totRows);
            System.out.println(totCols);
			
		}catch(Exception e){
			isExceptionOccured = true;
			pageResponseObject.put("exceptionMsg", e.getMessage());
			e.printStackTrace();
		}finally{
			pageResponseObject.put("isExcelQualified", isExcelQualified);
			pageResponseObject.put("isExceptionOccured", isExceptionOccured);
			pageResponseObject.put("isExcelContainsData", isExcelContainsData);
			pageResponseObject.put("isExcelDataExceedsLimit", isExcelDataExceedsLimit);
		}		
	} else if(command.equalsIgnoreCase("uploadCREDIT")){
		/* Reading and Validation for the EXCEL File */
		ArrayList <CreditNotesBean> totList=new ArrayList <CreditNotesBean>();
		boolean isExcelContainsData = false;
		boolean isExcelDataExceedsLimit = false;
		boolean isExceptionOccured = false;
		boolean isExcelQualified = false;
		
		final int EXCEL_MAX_ROW_LIMIT = 51;
		String creditNoteJRMXLLocation = jrxmlContainerPath+"FIRS_BEN_WHT_NGN_RECEIPTDOWNLOAD1.jrxml";
				
		try{
			Sheet sheet1 = wrk1.getSheet(0);
			
			int totRows = sheet1.getRows();
            int totCols = sheet1.getColumns();
            
            /* Checking EXCEL */
            int emptyRow = (int)validateExcel(wrk1).get("emptyRow");
            isExcelContainsData = emptyRow == 0 ? true : false;
            isExcelDataExceedsLimit = totRows > EXCEL_MAX_ROW_LIMIT ? true : false;
            
            if(isExcelContainsData && !isExcelDataExceedsLimit){
            	isExcelQualified = true;
            }
            
            if(isExcelQualified){
            	/*Loop has been started with 1 because we do not need the header to print*/
	            for (int row = 1; row < totRows; row++) {
	                System.out.println("Starting Row : " + row);
	                CreditNotesBean bean=new CreditNotesBean();
	                
	                for (int col = 0; col < totCols; col++) {
	                    /*Obtain reference to the Cell using getCell(int col, int row) method of sheet*/
	                    Cell cell = sheet1.getCell(col, row);
	                    /*Read the contents of the Cell using getContents() method, which will return it as a String*/
	                    String cellValue = cell.getContents();
	                    /*Printing the contents of the columns*/ 
	                    /*System.out.print(cellValue + "  ");*/
	                    
	                    switch (col) {
	                        case 0:
	                            bean.setPAYMENT_REFERENCE(cellValue);
	                            break;
	                        case 1:
	                            bean.setPAYMENT_DATE(cellValue);
	                            break;
	                        case 2:
	                            bean.setPAYER_TIN(cellValue);
	                            break;
	                        case 3:
	                            bean.setPAYER(cellValue);
	                            break;
	                        case 4:
	                            bean.setBENEFICIARY_TIN(cellValue);
	                            break;
	                        case 5:
	                            bean.setRECEIVED_FORM(cellValue);//BENEFICIARY NAME
	                            break;
	                        case 6:
	                            bean.setTAX_ON(cellValue.replace(",", ""));
	                            break;
	                        case 7:
	                            bean.setWHR_RATE(cellValue);
	                            break;
	                        case 8:
	                            bean.setSUM_OF_AMT(cellValue.replace(",", ""));
	                            break;
	                        case 9:
	                            bean.setTAX_TYPE(cellValue);
	                            break;
	                        case 10:
	                            bean.setCONTRACT_DESC(cellValue);
	                            break;
	                        case 11:
	                            bean.setSCHEDULE_REF(cellValue);
	                            break;
	                        case 12:
	                            bean.setPAYMENT_REF(cellValue);
	                            break;
	                        case 13:
	                            bean.setADDRESS(cellValue);
	                            break;
	                        case 14:
	                            bean.setPERIOD_FROM(cellValue);
	                            break;
	                        case 15:
	                            bean.setPERIOD_TO(cellValue);
	                            break;
	                        case 16:
	                            bean.setBANK(cellValue);
	                            break;
	                        case 17:
	                            bean.setTAX_OFFICE(cellValue);
	                            break;
	                        case 18:
	                            bean.setRECEIPT_NO(cellValue);
	                            break;
	                        case 19:
	                            bean.setDATE(cellValue);
	                            break;
	                        case 20:
	                            bean.setISSUING_OFFICER(cellValue);
	                            break;
	                        case 21:
	                            bean.setIRNO(cellValue);
	                            break;
	                        case 22:
	                            bean.setSUPERVISING_OFFICER(cellValue);
	                            break;
	                        case 23:
	                            bean.setSIGNATURE(cellValue);
	                            break;
	                        default:
	                            break;
	                    }
	                }
	                
	                totList.add(bean);
	                System.out.println("Ending Row : " + row);
	            }
            }
            String folderName = getFolderName("FIRS","CREDIT");
            boolean isPDFGenerated = JasperPDFDownload.processAndGeneratePDFs(totList, creditNoteJRMXLLocation, pdfDownloadPath,folderName);
            
            System.out.println("isPDFGenerated >>>>"+isPDFGenerated);
			
		}catch(Exception e){
			isExceptionOccured = true;
			pageResponseObject.put("exceptionMsg", e.getMessage());
			e.printStackTrace();
		}finally{
			pageResponseObject.put("isExcelDataExceedsLimit", isExcelDataExceedsLimit);
		}
	}else if(command.equalsIgnoreCase("uploadWHT")){
		/* Reading and Validation for the EXCEL File */
		ArrayList <CreditNotesBean> totList=new ArrayList <CreditNotesBean>();
		boolean isExcelContainsData = false;
		boolean isExcelDataExceedsLimit = false;
		boolean isExceptionOccured = false;
		boolean isExcelQualified = false;
		
		final int EXCEL_MAX_ROW_LIMIT = 51;
		String jrxmlLocation = jrxmlContainerPath+"FIRS_BEN_WHT_NGN_RECEIPTDOWNLOAD1.jrxml";
				
		try{
			Sheet sheet1 = wrk1.getSheet(0);
			
			int totRows = sheet1.getRows();
            int totCols = sheet1.getColumns();
            
            /* Checking EXCEL */
            isExcelContainsData = totRows > 1 ? true : false;
            isExcelDataExceedsLimit = totRows > EXCEL_MAX_ROW_LIMIT ? true : false;
            
            if(isExcelContainsData && !isExcelDataExceedsLimit){
            	isExcelQualified = true;
            }
            
            if(isExcelQualified){
            	/*Loop has been started with 1 because we do not need the header to print*/
	            for (int row = 1; row < totRows; row++) {
	                System.out.println("Starting Row : " + row);
	                CreditNotesBean bean=new CreditNotesBean();
	                
	                for (int col = 0; col < totCols; col++) {
	                    /*Obtain reference to the Cell using getCell(int col, int row) method of sheet*/
	                    Cell cell = sheet1.getCell(col, row);
	                    /*Read the contents of the Cell using getContents() method, which will return it as a String*/
	                    String cellValue = cell.getContents();
	                    /*Printing the contents of the columns*/ 
	                    /*System.out.print(cellValue + "  ");*/
	                    
	                    switch (col) {
	                        case 0:
	                            bean.setPAYMENT_REFERENCE(cellValue);
	                            break;
	                        case 1:
	                            bean.setPAYMENT_DATE(cellValue);
	                            break;
	                        case 2:
	                            bean.setPAYER_TIN(cellValue);
	                            break;
	                        case 3:
	                            bean.setPAYER(cellValue);
	                            break;
	                        case 4:
	                            bean.setBENEFICIARY_TIN(cellValue);
	                            break;
	                        case 5:
	                            bean.setRECEIVED_FORM(cellValue);//BENEFICIARY NAME
	                            break;
	                        case 6:
	                            bean.setTAX_ON(cellValue.replace(",", ""));
	                            break;
	                        case 7:
	                            bean.setWHR_RATE(cellValue);
	                            break;
	                        case 8:
	                            bean.setSUM_OF_AMT(cellValue.replace(",", ""));
	                            break;
	                        case 9:
	                            bean.setTAX_TYPE(cellValue);
	                            break;
	                        case 10:
	                            bean.setCONTRACT_DESC(cellValue);
	                            break;
	                        case 11:
	                            bean.setSCHEDULE_REF(cellValue);
	                            break;
	                        case 12:
	                            bean.setPAYMENT_REF(cellValue);
	                            break;
	                        case 13:
	                            bean.setADDRESS(cellValue);
	                            break;
	                        case 14:
	                            bean.setPERIOD_FROM(cellValue);
	                            break;
	                        case 15:
	                            bean.setPERIOD_TO(cellValue);
	                            break;
	                        case 16:
	                            bean.setBANK(cellValue);
	                            break;
	                        case 17:
	                            bean.setTAX_OFFICE(cellValue);
	                            break;
	                        case 18:
	                            bean.setRECEIPT_NO(cellValue);
	                            break;
	                        case 19:
	                            bean.setDATE(cellValue);
	                            break;
	                        case 20:
	                            bean.setISSUING_OFFICER(cellValue);
	                            break;
	                        case 21:
	                            bean.setIRNO(cellValue);
	                            break;
	                        case 22:
	                            bean.setSUPERVISING_OFFICER(cellValue);
	                            break;
	                        case 23:
	                            bean.setSIGNATURE(cellValue);
	                            break;
	                        default:
	                            break;
	                    }
	                }
	                
	                totList.add(bean);
	                /*System.out.println("");*/
	                System.out.println("Ending Row : " + row);
	            }
            }
            String folderName = getFolderName("FIRS","WHT");
            boolean isPDFGenerated = JasperPDFDownload.processAndGeneratePDFs(totList, jrxmlLocation, pdfDownloadPath,folderName);
            
            System.out.println("isPDFGenerated >>>>"+isPDFGenerated);
			
		}catch(Exception e){
			isExceptionOccured = true;
			pageResponseObject.put("exceptionMsg", e.getMessage());
			e.printStackTrace();
		}finally{
			pageResponseObject.put("isExcelDataExceedsLimit", isExcelDataExceedsLimit);
		}
	}
	
	pageResponseObject.put("result", result);
	out.print(pageResponseObject.toString());
%>