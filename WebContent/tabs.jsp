















<!-- Common -->
<link rel="stylesheet" href="css/api/bootstrap.min.css" async>
<link rel="stylesheet" href="css/api/template.css" async>
<link rel="stylesheet" href="css/api/font-awesome.min.css" async>
<link rel="stylesheet" href="css/api/bootstrap-datetimepicker.min.css"  async/>
<link rel="stylesheet" href="css/api/pikaday.css"  async/>
<link rel="stylesheet" href="css/api/jquery-ui.1.11.4.css"  async/>
<link rel="stylesheet" href="css/api/bootstrap-dialog.min.css" async>
<link rel="stylesheet" href="css/api/dataTables.bootstrap.min.css" async>
<link href='//fonts.googleapis.com/css?family=Raleway:400,200' rel='stylesheet' type='text/css' async>
<link rel="stylesheet" href="css/api/features-quick-link.css" async>
<link rel="stylesheet" href="css/api/font-awesome-animation.min.css" async>
<link rel="stylesheet" href="css/api/remodal.css" async>
<link rel="stylesheet" href="css/api/jquery.growl.css" async>
<link rel="stylesheet" href="css/api/remodal-default-theme.css" async>
<link rel="stylesheet" href="css/portal/Custom.css" async>
<link rel="stylesheet" href="css/api/jquery-ui.css" async>



		


<!DOCTYPE html>
<html style="position: relative;min-height: 100%;">
  <head>
    <meta charset="utf-8">
    <title>FIRS | Individual Profile</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="dist/css/site.min.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/portal/Custom.css" async>
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="dist/js/site.min.js"></script>
    
		<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js" ></script>
		
		
		<script type="text/javascript" src="js/bootstrap-dialog.min.js" ></script>
		<script type="text/javascript" src="js/jquery.growl.js" ></script>
		<script type="text/javascript" src="js/jquery-ui.js"></script>
		<script type="text/javascript" src="js/portal/notification.js"></script>
		
		<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
		
		<script type="text/javascript" src="js/moment.min.js"></script>
		<script type="text/javascript" src="js/remodal.min.js" ></script>
		
		<script type="text/javascript" src="js/portal/forgotPassword.js" ></script>
		<script type="text/javascript" src="js/portal/portal.js" ></script>
 		<script type="text/javascript" src="js/portal/logon.js"></script>
 		
		
		<script type="text/javascript" src="js/portal/captcha.js"></script>
		<script type="text/javascript" src="js/portal/miniCaptcha.js"></script>
		<script type="text/javascript" src="js/portal/template.js"></script>
		<script type="text/javascript" src="js/portal/validator.js" ></script>
		<script type="text/javascript" src="js/portal/downloadTCC.js" ></script>
		<script type="text/javascript" src="js/portal/TccFormValidation.js"></script>
		<script type="text/javascript" src="js/portal/TccFormSubmission.js"></script>
		<script type="text/javascript" src="js/portal/SearchTCC.js"></script>
		<script type="text/javascript" src="js/portal/SearchReceipt.js"></script>		
		<script type="text/javascript" src="js/portal/registration.js"></script>
		<script type="text/javascript" src="js/portal/corporateProfile.js"></script>
		<script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
		
	
		<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
		
		<script type="text/javascript" src="js/portal/tradenature-autocomplete.js"></script>
		<script type="text/javascript" src="js/portal/TINRevalidation.js"></script>

    
     <style>
		.list-group-item.active, .list-group-item.active:focus, .list-group-item.active:hover{
			background-color: #E6E9ED;
	    	border-color: #E6E9ED;
		}
	</style>
	
  </head>
  <body style="margin-bottom: 15px!important;">
    
  	<!-- Nav Bar -->
    














<nav role="navigation" class="navbar navbar-custom">
   <div class="container-fluid">
     <div class="navbar-header col-lg-5 col-md-5 col-sm-4">
       <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a href="#" class="navbar-brand" style="font-size:13px;" onclick="return false;"><span style="font-family: cursive;font-size:15px;">Welcome&nbsp&nbsp</span><span style="font-size:16px;"></span></a>
     </div>
     <div class="text-center col-lg-4 col-md-4 col-sm-5">
     	<label style="margin-top: 3%;">&nbsp;</label>
     	<a href="#" style="font-size:13px;color:#bec0c3;font-weight:700">FIRS TIN:<span style="font-size:14px;"></span></a>
     </div>
     <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse pull-right col-lg-3 col-md-3 col-sm-3">
       <ul class="nav navbar-nav navbar-right">
         <li class="active"><a href="#"></a></li>
         
         <li class="active"><a href="javascript:"
			onclick="window.location.href = 'logoff.jsp';">Log Out&nbsp<span class="glyphicon glyphicon-log-out"></span></a></li>
       </ul>

     </div>
   </div>
</nav>
    
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><i class="glyphicon glyphicon-align-justify"></i><b>Individual DashBoard</b></li>
                <li class="list-group-item active"><a href="javascript:" data-target="tcc_profile_details"><i class="glyphicon glyphicon-user"></i>Profile Details</a></li>
                <li class="list-group-item"><a href="javascript:" data-target="apply_tcc"><i class="glyphicon glyphicon-file"></i>Apply for TCC</a></li>
                <li class="list-group-item"><a href="javascript:" data-target="download_tcc"><i class="glyphicon glyphicon-download-alt"></i>Download TCC</a></li>
                <li class="list-group-item"><a href="javascript:" data-target="search_receipt"><i class="fa fa-search" aria-hidden="true"></i>Search Receipt</a></li>
             </ul>
          </div>
          
          <!-- Profile -->
          

























<div class="col-xs-12 col-sm-9 content showHideDiv" id="tcc_profile_details">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a> Individual Profile Details
			</h3>
		</div>
		<div class="panel-body">
			<div class="content-row">

				<div class="container-fluid">
					<div class="row">
						<div class="col-md-10 col-lg-10 ">
							<div class="panel panel-profile">
								<div class="panel-heading">
									<b>Basic Details :</b>
								</div>
								<div class="panel-body">
									<div class="col-sm-12">
										<h4 style="color: #00b1b1;"></h4>
										
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">JTBTIN:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"></span></div>
									</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							
							<div class="panel panel-profile">
								<div class="panel-heading">
									<b>Existing Details :</b>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Tax Office:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"></span></div>
									</div>
									<div class="clearfix"></div>
									<div class="bot-border"></div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="text-right pull-right-md" style="font-weight:600px;">E Mail:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"></span></div>
									</div>

									<div class="clearfix"></div>
									<div class="bot-border"></div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Mobile:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"></span></div>
									</div>
									<div class="clearfix"></div>
									<div class="bot-border"></div>
									<div class="row">
										<div class="col-sm-3 col-xs-12"><label class="pull-right-md text-right">Address:</label></div>
										<div class="col-sm-9 col-xs-12"><span class="custom-text pull-left text-left"></span></div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="panel panel-profile">
								<div class="panel-heading">
									<b>Re Validate Your TIN here :</b>
								</div>
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<button class="btn btn-primary custom_btn" id ="revalidateTIN" tin="" style="font-size: 12px;margin-left:10px;" >
												Click this for Re-validation
											</button>
											<button class="hide" data-toggle="modal" data-target="#tinDatailsModal" data-backdrop="static" data-keyboard="false">
												Demo
											</button>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<p style="font-size: 11px;color: #F44336;font-family: sans-serif;">
												Please Re-validate your TIN information before applying a new TCC
											</p>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							
							
						</div>
						<script>
						    $(function() {
							    $('#profile-image1').on('click', function() {
							        $('#profile-image-upload').click();
							    });
						    });
						    
						</script>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<!-- Modal for Conforming Informations -->
  <div class="modal fade" id="tinDatailsModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="border-radius: 0px;">
        <div class="modal-header" style="background-color: #e2e2e2;padding: 20px 20px 20px 20px;">
          <h4 class="modal-title label label-default" style="font-size: 12px;border-radius: 0px;background-color: #303641;color: white;">
          	TAX PAYER | Information
          </h4>
        </div>
        <div class="modal-body">
          <div id="dataPreviewSection">
	          <div class="row" style="margin-top:10px;">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">COMPANY NAME:</div>
	          			<div class="col-lg-4" id="companyName"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">TAX OFFIC NAME:</div>
	          			<div class="col-lg-6" id="taxOfficeName"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">TAX OFFICE ID:</div>
	          			<div class="col-lg-4" id="taxOfficeID"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">PHONE NUMBER:</div>
	          			<div class="col-lg-4" id="phoneNumber"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">EMAIL ID:</div>
	          			<div class="col-lg-4" id="emailID"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">RC NUMBER:</div>
	          			<div class="col-lg-4" id="rcNumber"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">JTBTIN:</div>
	          			<div class="col-lg-4" id="JTBTIN"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">ADDRESS:</div>
	          			<div class="col-lg-4" id="address"></div>
	          		</div>
	          		</div>
	          	</div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<div class="row">
	          			<div class="col-lg-offset-2 col-lg-4">TIN</div>
	          			<div class="col-lg-4" id="TIN"></div>
	          		</div>
	          	</div>
	          </div>
	          <div class="row">
	          	<div class="col-lg-12">
	          		<p>
	          			<label><input type="checkbox" value=""
							id="confirmCheck"></label> Please confirm to update this information 
					</p>
	          	</div>
	          </div>
          </div>
          <div class="hide" id="successDiv">
          	<div class="col-lg-12 text-center" style="margin-top: 10px;">
          		<p style="border: 1px solid #808080ab;box-shadow: 1px 1px 1px #9E9E9E;padding: 20px 20px 20px 20px;background-color: #008000a8;
    				color: white;font-family: sans-serif;font-size: 15px;">
          			Thank You ! Your Data has been Saved Successfully</p>
          	</div>
          </div>
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-primary custom_btn" id="cancelUpdateTINData" data-dismiss="modal" style="font-size: 12px;">Cancel</button>
          	<button type="button" class="btn btn-primary custom_btn" id="updateTINData" tin="" disabled="disabled" style="font-size: 12px;">
          		Update
          	</button>
          	<button type="button" class="btn btn-primary custom_btn hide" id="redirectToHome" tin="" style="font-size: 12px;">
          		Home
          	</button>
        </div>
      </div>
      
    </div>
  </div>
  <script>
  	
  	$("#confirmCheck").click(function(){
		if($("#confirmCheck").prop("checked") == true){   
			$("#updateTINData").attr('disabled',false);
		}else{
			$("#updateTINData").attr('disabled',true);
			}
	});
  
  </script>
          
           <div class="col-xs-12 col-sm-9 content showHideDiv hide" id="apply_tcc">
            
            
		  	<div class="panel panel-default">
              <div class="panel-heading">
				<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a>Apply for TCC
				</h3>
			 </div>
             <div class="panel-body">
                 <div class="content-row">
                  <div class="container-fluid">
					<div class="container-fluid" id="application-review-exist">
		        		<div class="panel panel-default">
			        		<div class="col-lg-12g text-center panel-body" style="padding:40px 20px 40px 20px;margin-top:20px;">
				        		<p class=""><strong><u>Application has been submitted Sucessfully!</u></strong></p>
				        		<p class="">Application Number:<span id="application-number">&nbsp&nbsp&nbsp&nbsp<strong></strong></span></p>
				        		<p class="">Application Status:<span id="application-number">&nbsp&nbsp<strong></strong></span></p>
			        		</div>
		        		</div>
          			</div>
				  </div>
                </div>
              </div>
             </div>
		  		
			
            <div class="panel panel-default">
              <div class="panel-heading">
				<h3 class="panel-title">
				<a href="javascript:void(0);" class="toggle-sidebar"><span
					class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
					title="Maximize Panel"></span></a>Apply for TCC
				</h3>
			 </div>
             <div class="panel-body">
                 <div class="content-row">
                  <div class="container-fluid">
					

















	

	<div>
	<div class="container-fluid">
	  <h4 align="center">Application for Tax Clearance Certificate</h4>
	  <!-- <ol class="breadcrumb hidden-xs">
	    <li id="crumb1" class="active">Enter Registration Details</li>
	    <li id="crumb2">Complete Registration</a></li>
	  </ol> -->
	</div>
	
	<div class="container-fluid" id="tccapp_form">
		<div style="margin-bottom:30px;">
			<p class="text-danger">
				<sup class="text-danger">*</sup><sup class="text-danger">*</sup>For
				any mandatory field like amount kindly put 0.00 in case there is no
				information available.
			</p>
		</div>
		<div style="padding:0px 20px 0px 20px;" class="panel panel-default">
			<div class="row hide" id="applyFormValidationAlert">
				<div class="col-xs-12">
					<br/>
					<p class="alert alert-warning"><i class="glyphicon glyphicon-alert"></i>&nbsp;&nbsp;<span id="formPageAlert">Please fill the mandatory fields!</span></p>
					<p class="well" id="missingMandatoryItemsOuter" style="padding:5px 10px;" >
						<span style="margin-right:10px;">Missing Mandatory Items: </span><span id="missingInputsContainer"></span>
					</p>
				</div>
			</div>

			<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="trade_nature">1.Nature of Trade/Business<sup class="text-danger">*</sup></label>
		    		<div class="col-md-9 col-lg-9">
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Nature of Trade/Business" id="trade_nature" maxlength="150" data-required="required"
			    		 name="currency" class="biginput">
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		<br>
	 		<span style="font-weight:200;">2.Turnover of the company in last 3 years in N<sup class="text-danger">*</sup></span><br><br>
	 		
	 		<div class="row custom_row">
				<div class="form-group" style="margin-bottom:0px">
		    		<label class="col-md-1 col-lg-1 custom_label" for="turnover-year-1">2018<sup class="text-danger">*</sup></label>
		    		<div class="col-md-3 col-lg-3"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company-1" id="turnover-year-1" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 			<div class="form-group" style="margin-bottom:0px">
		    		<label class="col-md-1 col-lg-1 custom_label" for="turnover-year-2">2017<sup class="text-danger">*</sup></label>
		    		<div class="col-md-3 col-lg-3"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company-2" id="turnover-year-2" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 			<div class="form-group" style="margin-bottom:0px">
		    		<label class="col-md-1 col-lg-1 custom_label" for="turnover-year-3">2016<sup class="text-danger">*</sup></label>
		    		<div class="col-md-3 col-lg-3"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company-3" id="turnover-year-3" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		<div class="row custom_row hide">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="">2.Turnover of the company in the latest tax return file with the Revenue N<sup class="text-danger">*</sup></label>
		    		<div class="col-md-9 col-lg-9"> 
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Turnover of the company.." id="" pattern="numbersLg" data-required="required" onkeypress="return onlyNosAndDecimal(event,this)" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		<br>
			<span style="font-weight:200;">3.Particular of previous Tax Clearance Certificate (if Any):</span><br><br>
			<!-- <div class="alert alert-info" id="verifyTCCPrerequisiteAlert">Please verify your TIN to enable TCC verification.</div> -->
			<div class="row custom_row" id="verifyTCCContainer">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="oldtcc_num">Number:</label>
		    		<div class="col-md-4 col-lg-4">
		    			<input type="text" class="form-control input-sm custom_input input-with-btn" aria-role="Particular of previous Tax clearanace" style="float:left;padding-right:25px;" id="oldtcc_num" maxlength="20" pattern="alphanumeric&specialWithSpacesLG">
						<span id="verifyTCCLoader" class="hide inline-loader"><img src="images/loader.gif" alt="" height="20px" width="20px" style="margin-top:5px;"></span>
						<button class="btn btn-primary custom_btn input-addon-btn" id="verifyTCCBtn"><i style="margin-right:4px; font-size:12px;" class="glyphicon glyphicon-search"></i><span id="verifyTCCText" class="text-content">Verify</span></button>	    			
		    			<div class="error" id=""></div>
		    		</div>
		    		<label class="col-md-2 col-lg-2 custom_label" for="tcc_date">Date:</label>
		    		<div class="col-md-3 col-lg-3">
		    			<input type="text" class="form-control input-sm custom_input" disabled="disabled" aria-role="Date" id="tcc_date" pattern="alphanumeric&specials">
		    			<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		<br>
	 		<span style="font-weight:200;">4.Has the Company Paid all assessments raised up to the immediate previous Year of Assessment.<sup class="text-danger">*</span></sup><br>
			<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="pre_year_check"><input type="radio" aria-role="Previous year/s assessment payment status" name="pre_year_check" id="pre_year_check" value="true" checked="checked">
		    		&nbsp &nbsp&nbsp Yes
		    		</label>
		    		<label class="col-md-3 col-lg-3 custom_label" for="pre_year_uncheck"><input type="radio" aria-role="Previous year/s assessment payment status" name="pre_year_check" id="pre_year_uncheck" value="false">
		    		&nbsp &nbsp&nbsp No
		    		</label>
	 			</div>
	 		</div>
	 		<br>
	 		<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="year">5.If 'No' state the amount outstanding and the Year(s) affected:<sup class="text-danger">*</sup></label>
		    		<div class="col-md-4 col-lg-4">
		    			<input type="text" class="form-control input-sm custom_input" aria-role="Year for amount outstanding" id="year" pattern="alphanumeric&specialWithSpacesLG" disabled="disabled">
		    			<div class="error" id=""></div>
		    		</div>
		    		<label class="col-md-2 col-lg-2 custom_label" for="amount">Amount:</label>
		    		<div class="col-md-3 col-lg-3">
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Amount outstanding" onkeypress="return onlyNosAndDecimal(event,this)" id="amount" pattern="numbers" disabled="disabled">
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		
	 		<span style="font-weight:200;">6.Any Special arrangement made with the revenue to liquidate the amount outstanding?<sup class="text-danger">*</sup></span>
	 		<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="amount_out_check"><input type="radio" value="true" aria-role="Special Arrangements for outstanding amount" id="amount_out_check" name="amount_check">
		    		&nbsp &nbsp&nbsp Yes
		    		</label>
		    		<label class="col-md-3 col-lg-3 custom_label" for="amount_out_uncheck"><input type="radio" value="false" aria-role="Special Arrangements for outstanding amount" id="amount_out_uncheck" name="amount_check" checked="checked">
		    		&nbsp &nbsp&nbsp No
		    		</label>
	 			</div>
	 		</div>
	 		
	 		<div class="hide" id="spc_arg" style="border:1px solid black;margin-top:25px;padding:10px 10px 10px 10px;">
		    	<div class="row custom_row">
					<div class="form-group">
			    		<label class="col-md-3 col-lg-3 custom_label" for="arrange_type">Type of Arrangements<sup class="text-danger">*</sup></label>
			    		<div class="col-md-4 col-lg-4">
				    		<input type="text" class="form-control input-sm custom_input" aria-role="Type of arrangement" id="arrange_type" pattern="alphanumeric&specialWithSpacesLG" >
				    		<div class="error" id=""></div>
			    		</div>
		 			</div>
	 			</div>
		    	
		    	<div class="row custom_row">
					<div class="form-group">
			    		<label class="col-md-3 col-lg-3 custom_label" for="support_copy">Support Copy approval letter<sup class="text-danger">*</sup></label>
			    		<div class="col-md-6 col-lg-6">
				    		<b><span id="support_copy_preview" class="text-info" style="margin-right:5px;"></span></b>
				    		<button type="button" onclick="$('#support_copy').trigger('click');" id="support_copy_trigger" class="btn custom_btn btn-primary">Browse</button>
				    		<input type="file" class="hide" style="position:fixed;top:-1000px;" aria-role="Support Copy of approval" accept="image/jpeg, image/gif,application/pdf" id="support_copy">
				    		<div class="error" id="" style="margin-top:2px;"></div>
			    		</div>
			    		<div class="col-md-4 col-lg-4"></div>
		 			</div>
	 			</div>
	 			
	    	</div>
	    	<br>
	    	
	    	<div class="row custom_row">
				<div class="form-group">
		    		<label class="col-md-3 col-lg-3 custom_label" for="other_doc">7.Any Other Relevant Information</label>
		    		<div class="col-md-9 col-lg-9">
			    		<input type="text" class="form-control input-sm custom_input" aria-role="Other relevant Information" id="other_doc" pattern="AddressLG" >
			    		<div class="error" id=""></div>
		    		</div>
	 			</div>
	 		</div>
	 		
	 		
	 		<div class="">
				<label class="checkbox-inline custom_label" for="confirm_form"><input type="checkbox" id="confirm_form">
				I certify that the information given above is correct in respect and confirm that to the best
				of my knowledge and belief, there are no other facts the omission of which would be
				misleading.

				</label>
				<div class="text-center" style="margin-bottom:10px;">
					<span id="formButtonContainer">
						<button class="btn btn-default custom_btn" id="submit_form" disabled="disabled" TIN=>Submit</button>&nbsp&nbsp
						<button class="btn btn-default custom_btn" id="form_reset" onclick="location.reload();">Cancel</button>&nbsp&nbsp&nbsp
					</span>	
					<div class="loader-container hide" id="loader">
						<p class="alert alert-success">Please wait, Submitting form data.. <img src="images/loader.gif" alt="" height="20px" width="20px"></p>
					</div>
				</div>
				<div class="hide text-danger text-center" style="margin:5px 0px;font-style:italic" id="applyTCCFormError">
				</div>
			</div>
 		</div>
	</div>
	<div class="container-fluid">
		<div class="panel panel-default hide" id="ConfirmationResponseStatus">
			<div class="" style="padding: 60px 0 60px 0;">
					<h5 class="text-center"><b>THANK YOU!</b></h5>
					<h5 class="text-center">TCC application has been submitted successfully.</h5>
					<p class="text-center">Your application no. is <strong> <span id="successApplicationNumberDisplay"></span></strong></p>
					<div class="text-center">
						
					</div>
			</div>
		</div>
	</div>
</div>
				  </div>
                </div>
              </div>
             </div>
             
            </div>
            <div class="col-xs-12 col-sm-9 col-lg-10 content showHideDiv hide" id="download_tcc">
            	<div class="panel panel-default">
             	 <div class="panel-heading">
					 <h3 class="panel-title">
					 <a href="javascript:void(0);" class="toggle-sidebar"><span
						class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
						title="Maximize Panel"></span></a>Download TCC
					</h3>
			    </div>
                <div class="panel-body">
                  <div class="content-row row">
                   <div class="col-lg-12">
					 	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-1 col-md-1"></div>
			<div class="col-lg-10 col-md-10">
				<div class="panel custom_panel panel-profile">
			  		<div class="panel-heading">
			  		Download TCC
			  		</div>
			  		<div class="panel-body">
				  		<p style="margin-bottom: 40px;">Download your Tax Clearance Certificate by providing the details below</p>
				  		<div class="row custom_row">
					  		<div class="form-group">
					    		<label class="col-md-4 col-lg-4 col-sm-4 custom_label" for="TCC_Input">TCC Number</label>
					    		<div class="col-md-6 col-lg-6 col-sm-6">
						    		<input type="text" class="form-control input-sm custom_input" data-required="required" errorMessage="Please provide a valid TCC number" pattern ="TINInput" maxlength="20" id="TCC_Input" placeholder="Enter TCC Number">
						    		<div class="error" id=""></div>
						    		<br/>
					    		</div>
				 			</div>
				 			
			 			</div>
			 			<div class="row custom-row">
			 				<div class="col-xs-12">
			 					<h5 class="text-warning text-center" ><i id="downloadTCCErrorDisplay"></i></h5>
			 				</div>
			 			</div>
			 			<div class="text-center" style="margin-bottom: 50px;">
			 				<button class="btn btn-default custom_btn "  id="export_TCC">Download</button>
			 				<button type="button" id="reset_DownloadTCC"  class="btn btn-default custom_btn ">Reset</button>
			 			</div>
			  		</div>
				</div>
			</div>
		</div>
		
	</div>
					</div>
                  </div>
                </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-lg-10 content showHideDiv hide" id="search_receipt">
            	<div class="panel panel-default">
             	 <div class="panel-heading">
					 <h3 class="panel-title">
					 <a href="javascript:void(0);" class="toggle-sidebar"><span
						class="glyphicon glyphicon-tasks" data-toggle="offcanvas"
						title="Maximize Panel"></span></a>Search Receipt
					</h3>
			    </div>
                <div class="panel-body">
                  <div class="content-row row">
                   	<div class="col-lg-12">
                   		<p style="color: #303641;font-size: 12px;">Search Receipts here by Providing below Inputs</p>
					</div>
                  </div>
                  <div class="row content-row">
                  	<div class="col-lg-12">
                  		<p style="font-size: 12px;">- Please choose type here<sup class="text-danger">*</sup></p>
                  		<label class="radio-inline">
                  			<input type="radio" name="individual-receipt-type-key" checked="checked" value="RCPT">Payment
                  		</label>
						<label class="radio-inline">
							<input type="radio" name="individual-receipt-type-key" value="RCTI">Credit Note
						</label>
                  	</div>
                  </div>
                  <div class="row content-row">
                  	<div class="col-lg-12">
                  		<p style="font-size: 12px;">- Please choose Monthly / Daily here<sup class="text-danger">*</sup></p>
                  		<label class="radio-inline">
                  			<input type="radio" name="individual-month-year-key" checked="checked" onclick="handleClick(this);" value="Monthy">Monthly
                  		</label>
						<label class="radio-inline">
							<input type="radio" name="individual-month-year-key" onclick="handleClick(this);" value="Daily">Daily
						</label>
                  	</div>
                  </div>
                  <div class="row content-row">
                  	<div class="col-lg-7 col-sm-7">
                  		<div class="row" id="receiptInputContainer">
                  			<div class="col-lg-1 col-sm-1">
                  				<p style="line-height: 26px;">Year<sup class="text-danger">*</sup></p>
                  			</div>
                  			<div class="col-lg-3 col-sm-3">
                  				<select class="form-control input-sm custom_input" id="individualYearInput" style="font-size: 11px;height: 28px;">
                  					<option value="" selected>Year</option>
                  					
                  						<option value="2019">2019</option>
                  					
                  						<option value="2018">2018</option>
                  					
                  						<option value="2017">2017</option>
                  					
                  						<option value="2016">2016</option>
                  					
                  						<option value="2015">2015</option>
                  					
                  						<option value="2014">2014</option>
                  					
                  						<option value="2013">2013</option>
                  					
                  						<option value="2012">2012</option>
                  					
                  						<option value="2011">2011</option>
                  					
                  						<option value="2010">2010</option>
                  					
                  				</select>
                  			</div>
                  			<div class="col-lg-1 col-sm-1">
                  				<p style="line-height: 26px;">Month<sup class="text-danger">*</sup></p>
                  			</div>
                  			<div class="col-lg-3 col-sm-3">
                  				<select class="form-control input-sm custom_input" id="individualMonthInput" style="font-size: 11px;height: 28px;">
                  					<option value="" selected>Month</option>
                  					
                  						<option value="Jan">Jan</option>
                  					
                  						<option value="Feb">Feb</option>
                  					
                  						<option value="Mar">Mar</option>
                  					
                  						<option value="Apr">Apr</option>
                  					
                  						<option value="May">May</option>
                  					
                  						<option value="Jun">Jun</option>
                  					
                  						<option value="Jul">Jul</option>
                  					
                  						<option value="Aug">Aug</option>
                  					
                  						<option value="Sep">Sep</option>
                  					
                  						<option value="Oct">Oct</option>
                  					
                  						<option value="Nov">Nov</option>
                  					
                  						<option value="Dec">Dec</option>
                  					
                  				</select>
                  			</div>
                  			<div class="col-lg-1 col-sm-1">
                  				<p style="line-height: 26px;">Day<sup class="text-danger">*</sup></p>
                  			</div>
                  			<div class="col-lg-2 col-sm-2">
                  				<select class="form-control input-sm custom_input" id="individualDayInput" style="font-size: 11px;height: 28px;" disabled="disabled">
                  					<option value="" selected>Day</option>
                  				</select>
                  			</div>
                  		</div>
                  	</div>
                  	<div class="col-lg-5 col-sm-5">
                  		<div class="row">
                  			<div class="col-lg-12 text-center">
                  				<button class="btn btn-default custom_btn"  id="searchIndividualReceipt" style="font-size:12px;">
                  					Search&nbsp;<i class="fa fa-search" aria-hidden="true"></i>
                  				</button>
			 					<button type="button" id="resetReceiptInputs"  class="btn btn-default custom_btn" style="font-size:12px;">
			 						Reset&nbsp;<i class="fa fa-times" aria-hidden="true"></i>
			 					</button>
                  			</div>
                  		</div>
                  	</div>
                  	<div class="row content-row hide" id="receiptSearchLoader">
                  		<div class="col-lg-12 pull-left">
                  			<span class="" style="font-size: 13px;color: #7990ae;font-weight: 600">Please Wait Searching. . .</span>
                  			<img class="img-responsive" src="images/loader-hor.gif" style="height: 50px;width: 200px;display:inline-block;">
                  		</div>
                  	</div>
                  	<div class="col-lg-offset-1 col-lg-10 col-sm-12 hide" id="individualReceiptSearchResultContainer">
                  		<p style="color: #303641;font-size: 12px;display: inline-block;padding: 5px 10px 5px 10px;margin-left: -15px;background-color: #e6e9ed;">
                  			Search Result : 
                  		</p>
                  		<div class="row" style="margin-top:20px;">
                  			<div class="table-responsive">          
							  <table class="table table-hover table-bordered table-striped" id="receiptSearchTable" style="font-size: 11px;font-family: sans-serif;">
							    <thead style="background-color: #2c3e50; color: #ffffff96;">
							      <tr>
							      	<th>S/N</th>
							        <th>Payment Reference</th>
							        <th>Payment Amount</th>
							        <th>Payment Date</th>
							        <th>Download</th>
							      </tr>
							    </thead>
							    <tbody align="center">
							      
							    </tbody>
							  </table>
							</div>
                  		</div>
                  	</div>
                  </div>
                </div>
               </div>
            </div>
         </div><!-- panel body -->
       </div>
       
	   <!--footer-->
	   <div class="container-fluid">
	<div class="site-footer text-center">
		&copy;&nbspFIRS 2017-18<a href="#" target="_blank"
			rel="external nofollow"></a>.
	</div>
</div>
	   
	   
    <script>
		$(document).ready(function(){
			var applicationFlag = localStorage.getItem('applicationFlag');
			if(applicationFlag){
				$.each($(".list-group li a"), function(obj){
					if($(this).attr("data-target") == "apply_tcc"){
						$(".list-group li").removeClass("active");
						$(this).parent("li").addClass("active");
						var curDiv = $(this).attr("data-target");
						$(".showHideDiv").hide();
						$(".showHideDiv").removeClass("hide");
						$("#"+curDiv).show();
					}
				})
				localStorage.removeItem('applicationFlag');
			}
			$(".list-group li a").on("click", function(){
				$(".list-group li").removeClass("active");
				$(this).parent("li").addClass("active");
				var curDiv = $(this).attr("data-target");
				
				if(curDiv == "apply_tcc" && formSubmittedFlag){
					window.location.reload();
					return false;
				}
				
				$(".showHideDiv").hide();
				$(".showHideDiv").removeClass("hide");
				$("#"+curDiv).show();
			})
		})
   </script>
  
  </body>
    <script type="text/javascript" src="js/jquery.autocomplete.min.js"></script>
	<script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="js/portal/template.js"></script>
	<script type="text/javascript" src="js/portal/tradenature-autocomplete.js"></script>
		
	<script type="text/javascript" src="js/jquery.growl.js" ></script>
	<script type="text/javascript" src="js/portal/notification.js"></script>
	<!-- Codes for AUTO Re - validation -->
	<script>
		var tinData = new Object();
		
		var isProfileRevalided = false;
		
		tinData["TIN"] = TIN.trim();
		tinData["command"] = "updateDataInAmanda";
		
				//console.log("temp_isProfileRevalided");
		//console.log(temp_isProfileRevalided);
		
		temp_isProfileRevalided == null ? isProfileRevalided = false : isProfileRevalided = true; 
		//console.log("isProfileRevalided");
		//console.log(isProfileRevalided);
		
		if(!isProfileRevalided){
			autoUpdatePeopleRecord(tinData);
		}
	</script>
</html>
