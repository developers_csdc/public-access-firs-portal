<div class="container-fluid">
	<div class="row">
		<div class="panel panel-default col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-1 col-xs-10" 
			style="padding:10px 10px 10px 10px;">
			<div class="container-fluid">
				<div class="row alert alert-info">
					Change Your Password Here:
				</div>
				<div class="row">
			      <label class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-3 col-xs-6" for="new-password" style="">Enter New password:</label> 
			      <input type="password"
					class="form-control input-sm custom_input col-lg-3 col-md-3 col-sm-6 col-xs-6"
					id="new-password" placeholder="Enter New password" style="width: 40%">
				</div>
			  	<div class="row" style="margin-top:10px;">
			      <label class="col-lg-offset-1 col-lg-3 col-md-offset-1 col-md-3 col-sm-3 col-xs-6" for="confirm-new-password">Re-enter New Password:</label> 
			      <input type="password"
					class="form-control input-sm custom_input col-lg-3 col-md-3 col-sm-6 col-xs-6"
					id="confirm-new-password" placeholder="Re-enter New Password" style="width: 40%">
					<div class="error" id=""></div>
			  	</div>
				<div
					class="alert alert-danger text-center col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 hide"
					style="margin-top: 5px;" id="password-error"></div>
				<div
					class="alert alert-info text-center col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4 hide"
					style="margin-top: 5px;" id="change-password-loader"><span style="font-size:18px;">Please wait.....<i class="custom-loader"></i></span></div>	
				<div class="row" style="margin-top:25px;margin-bottom:15px;" id="change-password-bottons">
			      <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12 text-center">
			      	<button type="button" class="btn btn-default custom_btn" id="reset-password" onclick="resetChangePasswordSection()">Reset</button>
			      	<button type="button" class="btn btn-default custom_btn" id="update-password">Submit</button>
			      </div>
				</div>
			</div>
		</div>
	</div>
</div>