package com.org.firs;

import java.sql.ResultSet;

import com.google.gson.Gson;
import com.org.firs.tccValidation.Assessment;
import com.org.firs.tccValidation.Authentication;
import com.org.firs.tccValidation.CorporateTIN;
import com.org.firs.tccValidation.ErrorResponse;
import com.org.firs.tccValidation.ForexTIN;
import com.org.firs.tccValidation.IndividualTIN;

public class TccValidation {
	static DBConnection dbc = new DBConnection();

	public static String validateTCC(String tin, String tcc, String username,String password) {
		System.out.println("Data in processor");
		Authentication auth = tccAuthentication(username,password);
		if(auth.getStatusCode() < 1) {
			Gson json = new Gson();
			return json.toJson(auth);
		}
		if(tin.trim().length() < 1 || tcc.trim().length() < 1 || username.trim().length() < 1 || password.trim().length() < 1){
			ErrorResponse error = new ErrorResponse();
			error.setStatusCode(5);
			error.setDescription("Invalid Request");
			Gson json = new Gson();
			return json.toJson(error);
		}
		String response = "", query = "", assessment = "";
		int assessmentCount = 0;
		String folderType = getFolderType(tin, tcc);
		System.out.println("FOLDER TYPE: "+folderType);
		String folderRSN = getFolderRSN(tin, tcc);
		System.out.println("FOLDER RSN: "+folderRSN);
		if(folderRSN.trim().equals("")) {
			ErrorResponse error = new ErrorResponse();
			error.setStatusCode(7);
			error.setDescription("TCC not found");
			Gson json = new Gson();
			return json.toJson(error);
		}
		if (folderType.equals("TCC")) {
			CorporateTIN ind = new CorporateTIN();
			query = "SELECT 0 STATUSCODE,'Success' DESCRIPTION,'Corporate' TAXPAYERTYPE, P.ORGANIZATIONNAME TAXPAYERNAME,P.ADDRESSLINE1 TAXPAYERADDRESS,P.REFERENCEFILE RCNO,F.REFERENCEFILE2 TIN, (SELECT STATUSDESC FROM VALIDSTATUS WHERE STATUSCODE=F.STATUSCODE) TCCSTATUS, (SELECT TOP 1 INFODESC FROM VALIDINFOVALUE WHERE INFOVALUE=(SELECT TOP 1 INFOVALUE FROM FOLDERINFO WHERE FOLDERRSN=F.FOLDERRSN AND INFOCODE=5090)) TAXOFFICE, F.ISSUEDATE TCCISSUEDATE,F.EXPIRYDATE TCCEXPIRYDATE FROM FOLDER F,PEOPLE P WHERE P.PEOPLERSN IN(SELECT PEOPLERSN FROM FOLDERPEOPLE WHERE FOLDERRSN=F.FOLDERRSN) AND FOLDERRSN="
					+ folderRSN;
			assessment = "SELECT FF.N01 as Assessmentyear,case when FF.c14 is null then '0.0' else format(TRY_CAST(FF.c14 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Turnover', case when FF.c10 is null then '0.0' else format(TRY_CAST(FF.c10 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Assessable Profit/Loss',case when FF.c11 is null then '0.0' else format(TRY_CAST(FF.c11 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'TotalProfit', case when FF.c12 is null then '0.0' else format(TRY_CAST(FF.c12 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'TaxPayable', ISNULL(FF.C05,'') as 'Receipt No1', FF.D01 as 'Date1', case when FF.c13 is null then '0.0' else format(TRY_CAST(FF.c13 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Tax Outs1', FF.C07 as PetReceipt1, FF.D02 as PetDate1, FF.C09 as SourceofInc1, FF.C10 as Comments1, FF.C08 as Nature1 FROM FOLDERFREEFORM FF,FOLDER F WHERE FF.FREEFORMCODE=1 AND FF.FOLDERRSN=F.FOLDERRSN AND F.FOLDERRSN="
					+ folderRSN + " ORDER BY FF.N01 DESC";
			try {
				ResultSet r1 = dbc.getConnection().createStatement().executeQuery(query);
				while (r1.next()) {
					ind.setDescription(r1.getString("DESCRIPTION"));
					ind.setRcNumber(r1.getString("RCNO"));
					ind.setStatusCode(0);
					ind.setTaxOffice(r1.getString("TAXOFFICE"));
					ind.setTaxPayerName(r1.getString("TAXPAYERNAME"));
					ind.setTaxPayerType(r1.getString("TAXPAYERTYPE"));
					ind.setTccExpiryDate(r1.getString("TCCEXPIRYDATE"));
					ind.setTccIssueDate(r1.getString("TCCISSUEDATE"));
					ind.setTccNumber(tcc);
					ind.setTccStatus(r1.getString("TCCSTATUS"));
					ind.setTin(r1.getString("TIN"));
				}
				ResultSet r2 = dbc.getConnection().createStatement().executeQuery(assessment);
				Assessment[] assessments = new Assessment[3];
				assessments[0] = new Assessment();
				assessments[1] = new Assessment();
				assessments[2] = new Assessment();
				while (r2.next()) {
					assessments[assessmentCount].setYear(r2.getInt("Assessmentyear")+"");
					assessments[assessmentCount].setAssessableProfit(r2.getString("Assessable Profit/Loss"));
					assessments[assessmentCount].setTaxPaid(r2.getString("TaxPayable"));
					assessments[assessmentCount].setTotalProfit(r2.getString("TotalProfit"));
					assessments[assessmentCount].setTurnOver(r2.getString("Turnover"));
					assessmentCount++;
				}
				ind.setAssessment(assessments);
				Gson json = new Gson();
				response = json.toJson(ind);
			} catch (Exception e) {
				System.out.println("Can not process transactions. "+e.getMessage());
				e.printStackTrace();
				ErrorResponse error = new ErrorResponse();
				error.setStatusCode(1);
				error.setDescription("Faliure");
				Gson json = new Gson();
				return json.toJson(error);
			}
		} else if (folderType.equals("ITCC")) {
			IndividualTIN ind = new IndividualTIN();
			query = "SELECT 0 STATUSCODE,'Success' DESCRIPTION,'Individual' TAXPAYERTYPE, P.ORGANIZATIONNAME TAXPAYERNAME,P.ADDRESSLINE1 TAXPAYERADDRESS,P.REFERENCEFILE RCNO,F.REFERENCEFILE2 TIN, (SELECT STATUSDESC FROM VALIDSTATUS WHERE STATUSCODE=F.STATUSCODE) TCCSTATUS, (SELECT TOP 1 INFODESC FROM VALIDINFOVALUE WHERE INFOVALUE=(SELECT TOP 1 INFOVALUE FROM FOLDERINFO WHERE FOLDERRSN=F.FOLDERRSN AND INFOCODE=5090)) TAXOFFICE, F.ISSUEDATE TCCISSUEDATE,F.EXPIRYDATE TCCEXPIRYDATE FROM FOLDER F,PEOPLE P WHERE P.PEOPLERSN IN(SELECT PEOPLERSN FROM FOLDERPEOPLE WHERE FOLDERRSN=F.FOLDERRSN) AND FOLDERRSN="
					+ folderRSN;
			assessment = "SELECT FF.N01 as Assessmentyear,case when FF.c14 is null then '0.0' else format(TRY_CAST(FF.c14 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Turnover', case when FF.c10 is null then '0.0' else format(TRY_CAST(FF.c10 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Assessable Profit/Loss',case when FF.c11 is null then '0.0' else format(TRY_CAST(FF.c11 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'TotalProfit', case when FF.c12 is null then '0.0' else format(TRY_CAST(FF.c12 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'TaxPayable', ISNULL(FF.C05,'') as 'Receipt No1', FF.D01 as 'Date1', case when FF.c13 is null then '0.0' else format(TRY_CAST(FF.c13 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Tax Outs1', FF.C07 as PetReceipt1, FF.D02 as PetDate1, FF.C09 as SourceofInc1, FF.C10 as Comments1, FF.C08 as Nature1 FROM FOLDERFREEFORM FF,FOLDER F WHERE FF.FREEFORMCODE=2 AND FF.FOLDERRSN=F.FOLDERRSN AND F.FOLDERRSN="
					+ folderRSN + " ORDER BY FF.N01 DESC";
			try {
				ResultSet r1 = dbc.getConnection().createStatement().executeQuery(query);
				while (r1.next()) {
					ind.setDescription(r1.getString("DESCRIPTION"));
					ind.setRcNumber(r1.getString("RCNO"));
					ind.setStatusCode(0);
					ind.setTaxOffice(r1.getString("TAXOFFICE"));
					ind.setTaxPayerName(r1.getString("TAXPAYERNAME"));
					ind.setTaxPayerType(r1.getString("TAXPAYERTYPE"));
					ind.setTccExpiryDate(r1.getString("TCCEXPIRYDATE"));
					ind.setTccIssueDate(r1.getString("TCCISSUEDATE"));
					ind.setTccNumber(tcc);
					ind.setTccStatus(r1.getString("TCCSTATUS"));
					ind.setTin(r1.getString("TIN"));
				}
				ResultSet r2 = dbc.getConnection().createStatement().executeQuery(assessment);
				Assessment[] assessments = new Assessment[3];
				assessments[0] = new Assessment();
				assessments[1] = new Assessment();
				assessments[2] = new Assessment();
				while (r2.next()) {
					assessments[assessmentCount].setYear(r2.getInt("Assessmentyear")+"");
					assessments[assessmentCount].setAssessableProfit(r2.getString("Assessable Profit/Loss"));
					assessments[assessmentCount].setTaxPaid(r2.getString("TaxPayable"));
					assessments[assessmentCount].setTotalProfit(r2.getString("TotalProfit"));
					assessments[assessmentCount].setTurnOver(r2.getString("Turnover"));
					assessmentCount++;
				}
				ind.setAssessment(assessments);
				Gson json = new Gson();
				response = json.toJson(ind);
			} catch (Exception e) {
				System.out.println("Can not process transactions. "+e.getMessage());
				e.printStackTrace();
				ErrorResponse error = new ErrorResponse();
				error.setStatusCode(1);
				error.setDescription("Faliure");
				Gson json = new Gson();
				return json.toJson(error);
			}
		} else if (folderType.equals("FTCC")) {
			ForexTIN ind = new ForexTIN();
			query = "SELECT 0 STATUSCODE,'Success' DESCRIPTION,'Forex' TAXPAYERTYPE, P.ORGANIZATIONNAME TAXPAYERNAME,P.ADDRESSLINE1 TAXPAYERADDRESS,P.REFERENCEFILE RCNO,F.REFERENCEFILE2 TIN, (SELECT STATUSDESC FROM VALIDSTATUS WHERE STATUSCODE=F.STATUSCODE) TCCSTATUS, (SELECT TOP 1 INFODESC FROM VALIDINFOVALUE WHERE INFOVALUE=(SELECT TOP 1 INFOVALUE FROM FOLDERINFO WHERE FOLDERRSN=F.FOLDERRSN AND INFOCODE=5090)) TAXOFFICE, F.ISSUEDATE TCCISSUEDATE,F.EXPIRYDATE TCCEXPIRYDATE FROM FOLDER F,PEOPLE P WHERE P.PEOPLERSN IN(SELECT PEOPLERSN FROM FOLDERPEOPLE WHERE FOLDERRSN=F.FOLDERRSN) AND FOLDERRSN="
					+ folderRSN;
			assessment = "SELECT FF.N01 as Assessmentyear,case when FF.c14 is null then '0.0' else format(TRY_CAST(FF.c14 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Turnover', case when FF.c10 is null then '0.0' else format(TRY_CAST(FF.c10 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Assessable Profit/Loss',case when FF.c11 is null then '0.0' else format(TRY_CAST(FF.c11 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'TotalProfit', case when FF.c12 is null then '0.0' else format(TRY_CAST(FF.c12 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'TaxPayable', ISNULL(FF.C05,'') as 'Receipt No1', FF.D01 as 'Date1', case when FF.c13 is null then '0.0' else format(TRY_CAST(FF.c13 as decimal(38,2)),'#,#0.00;(#,#0.00);0.00') end as 'Tax Outs1', FF.C07 as PetReceipt1, FF.D02 as PetDate1, FF.C09 as SourceofInc1, FF.C10 as Comments1, FF.C08 as Nature1 FROM FOLDERFREEFORM FF,FOLDER F WHERE FF.FREEFORMCODE=3 AND FF.FOLDERRSN=F.FOLDERRSN AND F.FOLDERRSN="
					+ folderRSN + " ORDER BY FF.N01 DESC";
			try {
				ResultSet r1 = dbc.getConnection().createStatement().executeQuery(query);
				while (r1.next()) {
					ind.setDescription(r1.getString("DESCRIPTION"));
					ind.setRcNumber(r1.getString("RCNO"));
					ind.setStatusCode(0);
					ind.setTaxOffice(r1.getString("TAXOFFICE"));
					ind.setTaxPayerName(r1.getString("TAXPAYERNAME"));
					ind.setTaxPayerType(r1.getString("TAXPAYERTYPE"));
					ind.setTccExpiryDate(r1.getString("TCCEXPIRYDATE"));
					ind.setTccIssueDate(r1.getString("TCCISSUEDATE"));
					ind.setTccNumber(tcc);
					ind.setTccStatus(r1.getString("TCCSTATUS"));
					ind.setTin(r1.getString("TIN"));
				}
				ResultSet r2 = dbc.getConnection().createStatement().executeQuery(assessment);
				Assessment[] assessments = new Assessment[1];
				assessments[0] = new Assessment();
				while (r2.next()) {
					assessments[assessmentCount].setRemmitanceAmountApproved(r2.getString("PetReceipt1"));;
					assessmentCount++;
				}
				ind.setAssessment(assessments);
				Gson json = new Gson();
				response = json.toJson(ind);
			} catch (Exception e) {
				System.out.println("Can not process transactions. "+e.getMessage());
				e.printStackTrace();
				ErrorResponse error = new ErrorResponse();
				error.setStatusCode(1);
				error.setDescription("Faliure");
				Gson json = new Gson();
				return json.toJson(error);
			}
		} else {
			ErrorResponse error = new ErrorResponse();
			error.setStatusCode(7);
			error.setDescription("TCC not found");
			Gson json = new Gson();
			response = json.toJson(error);
		}
		return response;
	}

	private static String getFolderType(String tin, String tcc) {
		String ft = "";
		String getFolderType = "SELECT DISTINCT FOLDERTYPE FROM FOLDER WHERE REFERENCEFILE='" + tcc
				+ "' AND REFERENCEFILE2='" + tin + "'";
		try {
			ResultSet r = dbc.getConnection().createStatement().executeQuery(getFolderType);
			while (r.next()) {
				ft = r.getString("FOLDERTYPE");
			}
		} catch (Exception e) {
			System.out.println("Exception occured: " + e.getMessage());
			ErrorResponse error = new ErrorResponse();
			error.setStatusCode(1);
			error.setDescription("Faliure");
			Gson json = new Gson();
			return json.toJson(error);
		}
		return ft;
	}

	private static String getFolderRSN(String tin, String tcc) {
		String ft = "";
		String getFolderType = "SELECT DISTINCT FOLDERRSN FROM FOLDER WHERE REFERENCEFILE='" + tcc
				+ "' AND REFERENCEFILE2='" + tin + "'";
		try {
			ResultSet r = dbc.getConnection().createStatement().executeQuery(getFolderType);
			while (r.next()) {
				ft = r.getString("FOLDERRSN");
				System.out.println("folderrsn: "+ft);
			}
		} catch (Exception e) {
			System.out.println("Exception occured: " + e.getMessage());
			ErrorResponse error = new ErrorResponse();
			error.setStatusCode(1);
			error.setDescription("Faliure");
			Gson json = new Gson();
			return json.toJson(error);
		}
		return ft;
	}

	private static Authentication tccAuthentication(String username, String password) {
		String activeFlag = "N";
		Authentication auth = new Authentication();
		String query = "SELECT * FROM API_SECRET_KEY WHERE USERNAME='" + username + "' AND PASSWORD='" + password + "'";
		try {
			ResultSet r = dbc.getConnection().createStatement().executeQuery(query);
			auth.setStatusCode(2);
			auth.setDescription("Unauthorized Request / Invalid UserName or Password");
			while (r.next()) {
				System.out.println("Found User "+r.getString("USERNAME")+" with password: "+r.getString("PASSWORD"));
				if (username == r.getString("USERNAME") && password == r.getString("PASSWORD")) {
					System.out.println("User Authenticated...");
					auth.setStatusCode(0);
					auth.setDescription("Success");
				}else {
					System.out.println("Unauthorized Request / Invalid UserName or Password");
				}
				activeFlag = r.getString("ACTIVE");
				if (activeFlag.equals("") || !activeFlag.equals("1")) {
					System.out.println("InactiveUser. Please contact your System Administrator");
					auth.setStatusCode(3);
					auth.setDescription("InactiveUser. Please contact your System Administrator");
				}
			}

		} catch (Exception e) {
			System.out.println("Error authenticating user");
		}
		return auth;
	}
}
