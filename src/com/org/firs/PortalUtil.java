package com.org.firs;

import java.util.Date;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class PortalUtil {

	DBConnection conn = new DBConnection();
	Connection connection;
	Statement createStatement;
	Statement Statement;
	boolean flag;
	private final String USER_AGENT = "Mozilla/5.0";

	public void activateSendMail(String textMail, String toEmail, String subject) {
		final String fromEmail = "firsreg@csdcconsulting.com"; // requires valid gmail id
		final String password = "CSDC@nigeria123"; // correct password for gmail id
		// final String toEmail = toEmail; // can be any email id

		System.out.println("Email Sending In progress");
		Properties props = new Properties();
		props.put("mail.smtp.host", "webhosting1000.is.cc"); // SMTP Host
		props.put("mail.smtp.port", "587"); // TLS Port
		props.put("mail.smtp.auth", "true"); // enable authentication
		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		Session session = Session.getInstance(props, auth);
		sendEmail(session, toEmail, subject, textMail);
	}

	public void sendEmail(Session session, String toEmail, String subject, String body) {

		MimeMessage msg = new MimeMessage(session);
		// set message headers
		try {
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");

			msg.setFrom(new InternetAddress("firsreg@csdcconsulting.com", "firsreg@csdcconsulting.com"));

			msg.setReplyTo(InternetAddress.parse("no_reply@firsreg.com", false));

			msg.setSubject(subject, "UTF-8");

			msg.setContent(body, "text/html");

			msg.setSentDate(new Date());

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, true));
			System.out.println("Message is ready");

			Transport.send(msg);
			System.out.println("Email With Activation Link Sent Successfully!!");
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	

	public boolean activateUser(HttpSession session, HttpServletRequest request, String password, String username)
			throws ClassNotFoundException, SQLException {

		if (firstTimeLogin(session, request, username, password)) {
			System.out.println("Login successfull");
			session.setAttribute("tin", username);
			return true;
		} else {
			System.out.println("Login successfull failed");
			return false;
		}
	}

	public boolean firstTimeLogin(HttpSession session, HttpServletRequest request, String tin, String internetAnswer)
			throws ClassNotFoundException, SQLException {
		boolean status = false;
		try {
			connection = conn.getConnection();
			String sql = "SELECT * FROM people where LicenceNumber='" + tin + "' AND internetAnswer='" + internetAnswer
					+ "' AND peoplecode in(1,3,4,5)";
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(sql);
			while (rs.next()) {
				status = true;
			}
		} catch (SQLException e) {
			throw e;
		}
		return status;
	}

	public boolean login(HttpSession session, HttpServletRequest request, String tin, String internetQuestion)
			throws ClassNotFoundException, SQLException {
		boolean status = false;
		try {
			connection = conn.getConnection();
			String sql = "SELECT * FROM people where LicenceNumber='" + tin + "' AND internetQuestion='"
					+ internetQuestion + "' AND PEOPLECODE IN(1,3,4,5)";
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(sql);
			while (rs.next()) {
				status = true;
				System.out.println("This user exists");
			}
		} catch (SQLException e) {
			throw e;
		}
		return status;
	}

	public boolean loginUser(HttpSession session, HttpServletRequest request, String username, String password)
			throws ClassNotFoundException, SQLException {
		System.out.println("InSide login User...");
		boolean flag = false;
		try {
			String firsPassword = encrypt(password);
			System.out.println("Incomming password by User " + firsPassword);
			flag = login(session, request, username, firsPassword);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Credential is Inside LoginUser() return " + flag);
		return flag;

	}

	public static String encrypt(String stringValue) throws UnsupportedEncodingException {
		return DatatypeConverter.printBase64Binary(stringValue.getBytes("UTF-8"));
	}

	/**
	 * This method is use for decrypt the Base64 encrypted String to normal String
	 * value.
	 * 
	 * @param encryptedStringValue
	 *            is the encrypted String Value in Base64 format
	 * @return decrypted String value.
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws UnsupportedEncodingException
	 */

	public String getPreviousTaxCert(String tcc) throws ClassNotFoundException, SQLException {
		String indate = "";
		System.out.println(("This is getPreviousTaxCert"));
		try {
			String sql = "SELECT indate FROM folder where referenceFile='" + tcc + "'";
			// String sql = "SELECT indate FROM folder where stampdate='2017-05-15
			// 08:28:19.793'";
			connection = conn.getConnection();
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(sql);
			while (rs.next()) {
				indate = rs.getString("indate");
				// System.out.println("Indate value " + rs.getString("indate"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// indate = null;
		return indate;
	}

	public String getFolderByTCC(String tcc) {
		String existingFolder = "";
		System.out.println(("This is getFolderByTCC"));
		try {
			String sql = "SELECT indate FROM folder where referenceFile='" + tcc + "'";
			// String sql = "SELECT folderrsn FROM folder where stampdate='2017-05-15
			// 08:28:19.793'";
			connection = conn.getConnection();
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(sql);
			while (rs.next()) {
				existingFolder = rs.getString("folderrsn");
				// System.out.println("Indate value " + rs.getString("indate"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// indate = null;
		return existingFolder;
	}

	public static String decrypt(String encryptedStringValue) throws UnsupportedEncodingException {
		return new String(DatatypeConverter.parseBase64Binary(encryptedStringValue), "UTF-8");
	}

	public String getInternetAccess(String tin) {
		String InternetAccess = "";
		try {
			connection = conn.getConnection();
			String psql = "SELECT InternetAccess from People where LicenceNumber='" + tin + "'";
			createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery(psql);
			while (executeQuery.next()) {
				InternetAccess = executeQuery.getString("InternetAccess");
			}
		}

		catch (Exception e) {
			System.out.println("Network Error Check your connection " + e.getMessage());
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				System.out.println("Network Error Check your connection ys " + e.getMessage());
			}

		}
		return InternetAccess;

	}

	public People getAllPeople(String tin) {

		People people = null;
		try {
			connection = conn.getConnection();
			String psql = "SELECT * From People where LicenceNumber='" + tin + "' AND PEOPLECODE IN(1,3,4,5)";
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(psql);
			String jtbTin = "", taxOffice = "", email = "", phone = "", address = "",addressLine1="", addressLine2="", peopleCode = "",
					internetAnswer = "", internetQuestion = "", internetAccess = "", peopleRSN="";
			while (rs.next()) {
				tin = rs.getString("LicenceNumber");
				peopleRSN = rs.getString("peopleRSN");
				jtbTin = rs.getString("LicenceNumber");
				taxOffice = rs.getString("OrganizationName");
				email = rs.getString("EmailAddress");
				phone = rs.getString("phone1");
				addressLine1 = rs.getString("addressLine1");
				addressLine2 = rs.getString("addressLine2");
				peopleCode = rs.getString("PeopleCode");
				internetAnswer = rs.getString("internetAnswer");
				internetQuestion = rs.getString("internetQuestion");
				internetAccess = rs.getString("internetAccess");
				System.out.println("Internet Access from DB " + internetAccess);
				
				System.out.println("peopleRSN" + " " +peopleRSN);
				
				if(addressLine1==null) {
					address = addressLine2;
				}else {
					address = addressLine1;
				}

			}

			// public People(String tIN, String jTBTIN, String taxPayerName, String address,
			// String taxOfficeName,
			// String taxPayerType, String email, String phone)
			//
			people = new People(tin, jtbTin, taxOffice, address, taxOffice, peopleCode, email, phone, internetAnswer,
					internetQuestion, internetAccess, peopleRSN);
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return people;
	}

	public String getInternetAnswer(int peopleRSN) {
		String internetAnswer = "";
		try {
			connection = conn.getConnection();
			String psql = "SELECT internetAnswer From People where peopleRSN='" + peopleRSN + "'";
			createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery(psql);
			while (executeQuery.next()) {
				internetAnswer = executeQuery.getString("internetAnswer");
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return internetAnswer;

	}
	
	
	public boolean getFolderByTin(String tin) {
		boolean flag = false;
		try {
			connection = conn.getConnection();
			String psql = "SELECT * From folder where folderType in ('TCC', 'ITCC') and ReferenceFile2='" + tin + "'";
			createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery(psql);
			if(executeQuery.next() != false) {
				flag = true;
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		System.out.println("Checked Mail" + flag);
		return flag;
	}
	

	public boolean getSmsFlagByTin(String tin) {
		String smsFlag = "";
		boolean flag = false;
		try {
			connection = conn.getConnection();
			//String psql = "SELECT SMSFlag From People where LicenceNumber='" + tin + "' AND PEOPLECODE IN(1,3,4,5)";
			
			String psql = "SELECT SMSFlag From People where LicenceNumber='" + tin + "'";
			createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery(psql);
			while (executeQuery.next()) {
				smsFlag = executeQuery.getString("SMSFlag");
			}
			System.out.println("My smsFlag: " + smsFlag);
			if(smsFlag == null) {
				flag = false;
			}else if (!smsFlag.equalsIgnoreCase("N")) {
				flag = true;
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		System.out.println("Checked Mail" + flag);
		return flag;
	}
	
	public boolean isAccountExist(String tin) {
		boolean existAccount = false;
		try {
			connection = conn.getConnection();
			//String psql = "SELECT SMSFlag From People where LicenceNumber='" + tin + "' AND PEOPLECODE IN(1,3,4,5)";
			String psql = "SELECT SMSFlag From People where LicenceNumber='" + tin + "'";
			createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery(psql);
			while (executeQuery.next()) {
				existAccount = true;
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return existAccount;
	}

	public String getSmsFlag(int peopleRSN) {
		System.out.println("===>"+peopleRSN);
		String smsFlag = "";
		
		try {
			connection = conn.getConnection();
			String psql = "SELECT SMSFlag From People where peopleRSN=" + peopleRSN + "";
			createStatement = connection.createStatement();
			ResultSet executeQuery = createStatement.executeQuery(psql);
			while (executeQuery.next()) {
				smsFlag = executeQuery.getString("SMSFlag");
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		System.out.println("smsfla from DB: "+smsFlag);

		return smsFlag;

	}

	public void setSmsFlag(String answer, String peopleRSN) {
		try {
			connection = conn.getConnection();
			Statement createStatement = connection.createStatement();
			System.out.println("Updating Date of Incorporation".toUpperCase());
			String updateDateSql = "UPDATE PEOPLE SET SMSFlag='" + answer + "' where peopleRsn='" + peopleRSN + "'";
			int executePeopleUpdate = createStatement.executeUpdate(updateDateSql);
			System.out.println(executePeopleUpdate + " Account Activation Successfully Updated");

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static String getActivationURL(String peopleRSN, String key, String pageUri) {
		// String rootURL = "http://tcc.firs.gov.ng/UAT/" +
		// "registrationControllerService";
		String rootURL = pageUri + "/registrationControllerService";
		String URL = "";
		String encryptedKey;
		try {
			encryptedKey = getSHA2Hash(key);
			URL = rootURL + "?user=" + peopleRSN + "&key=" + encryptedKey + "&command=ActivateAccount";
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		System.out.println(URL);
		return URL;
	}

	public void sendSMS(String phone, String otp) {
		
		String msg = "Your Login PassCode Is " + otp;
		String phone1 = phone.replace("080", "2340");
		String url = "";
		//String url = "http://api.rmlconnect.net/OtpApi/generatesms?username=xe-firs&password=abuja&msisdn=2348038024755&msg=Your%20OTP%20is%20%25m&source=OTPFIRS&otplen=9&exptime=60";
        System.out.println(phone1);
		URL obj;
		try {
			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			// print result
			System.out.println(response.toString());

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String getSHA2Hash(String key) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(key.getBytes());
		byte byteData[] = md.digest();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}

	public boolean getPeopleByTin(String tin) throws ClassNotFoundException, SQLException {
		try {
			connection = conn.getConnection();
			//String sql = "SELECT * FROM people where LicenceNumber='" + tin + "' AND PEOPLECODE IN(1,3,4,5)";
			
			String sql = "SELECT * FROM people where LicenceNumber='" + tin + "'";
			
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(sql);
			if (rs.next() == false) {
				flag = false;
			} else {
				flag = true;
			}
		}

		catch (SQLException e) {
			throw e;
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				throw e;
			}

		}
		return flag;
	}

	
	public boolean getPeopleByPeopleRsn(int peopleRsn) throws ClassNotFoundException, SQLException {
		try {
			connection = conn.getConnection();
			String sql = "SELECT * FROM people where PEOPLERSN=" + peopleRsn + " AND PEOPLECODE IN(1,3,4,5)";
			createStatement = connection.createStatement();
			ResultSet rs = createStatement.executeQuery(sql);
			if (rs.next() == false) {
				flag = false;
			} else {
				flag = true;
			}
		}

		catch (SQLException e) {
			throw e;
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				throw e;
			}

		}
		return flag;
	}
	
	
	public static String getRandomString(int length) {
		String randomStr = UUID.randomUUID().toString();
		while (randomStr.length() < length) {
			randomStr += UUID.randomUUID().toString();
		}
		return randomStr.substring(0, length);
	}
	
	
	

	public int addPeople(People people, String randomString, int peopleCode) {
		System.out.println("Inside add people".toUpperCase());
		ResultSet prs = null;
		ResultSet frs = null;
		int peopleRSN = 0;
		try {
			connection = conn.getConnection();
			String psql = "SELECT NEXT VALUE FOR PEOPLESEQ";
			createStatement = connection.createStatement();
			prs = createStatement.executeQuery(psql);

			while (prs.next()) {
				peopleRSN = prs.getInt(1);
				// System.out.println("People RSN " + peopleRSN);
			}

			// String fsql = "SELECT NEXT VALUE FOR FOLDERSEQ";
			// createStatement = connection.createStatement();
			// frs = createStatement.executeQuery(fsql);

			// while(frs.next()) {
			// folderRsn = frs.getInt(1);
			// System.out.println("Folder RSN " + folderRsn);
			// }
			insertPeople(peopleRSN, randomString, people, peopleCode);
			String UpdateSql = "Update people set smsFlag='N' where peoplersn='"+peopleRSN+"'";
			System.out.println(createStatement.executeUpdate(UpdateSql) + " Updated");

		}

		catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return peopleRSN;
	}
	
	
	public boolean updatePeopleByTin(People people) {
		System.out.println("Update people....".toUpperCase());
		ResultSet prs = null;
		ResultSet frs = null;
		String peopleRSN = "";
		String internetAnswer = "", internetQuestion = "";
		People peopleQr = null;
		try {
			connection = conn.getConnection();
			String psql = "SELECT * from people where licenceNumber='"+ people.getTIN() +"'";
			createStatement = connection.createStatement();
			
				
			String tin = people.getTIN();
			String address = people.getAddress();
			String email = people.getEmail();
			String taxPayerName = people.getTaxPayerName();
			String taxOfficeID = people.getTaxOfficeID();
			String taxOfficeName = people.getTaxOfficeName();
			String RCNumber = people.getRCNumber();
			String phone = people.getPhone();
			
			if(address.equals("")) {
				address = "NOT AVAILABLE";
			}
			
			if(email.equals("")) {
				email = "NOT AVAILABLE";
			}
			if(taxPayerName.equals("")) {
				taxPayerName = "NOT AVAILABLE";
			}
			if(taxOfficeID.equals("")) {
				taxOfficeID = "NOT AVAILABLE";
			}
			
			if(taxOfficeName.equals("")) {
				taxOfficeName = "NOT AVAILABLE";
			}
			
			if(RCNumber.equals("")) {
				RCNumber = "NOT AVAILABLE";
			}
			
			if(phone.equals("")) {
				phone = "NOT AVAILABLE";
			}
			
			String UpdateSql = "Update people set community='"+taxOfficeID+"', Weight='20', phone1='"+phone+"', addressLine1='"+address+"', addressLine2='"+address+"', emailAddress='"+email+"', organizationName='"+taxPayerName+"', orgNameUpper='"+taxPayerName+"', referenceFile='"+RCNumber+"' where licenceNumber='"+tin+"'";
			System.out.println(createStatement.executeUpdate(UpdateSql) + " Updated");
			flag = true;
			
		}

		catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return flag;
	}
	
	
	
	
	public People updatePeople(People people, String randomString, int peopleCode) {
		System.out.println("Update people....".toUpperCase());
		ResultSet prs = null;
		ResultSet frs = null;
		String peopleRSN = "";
		String internetAnswer = "", internetQuestion = "";
		People peopleQr = null;
		try {
			connection = conn.getConnection();
			String psql = "SELECT * from people where licenceNumber='"+ people.getTIN() +"'";
			createStatement = connection.createStatement();
			
			prs = createStatement.executeQuery(psql);
			while(prs.next()) {
				peopleRSN = prs.getString("peopleRsn");
				internetAnswer = prs.getString("InternetAnswer");
				internetQuestion = prs.getString("internetQuestion");
			}
			
			peopleQr = new People(peopleRSN, internetAnswer,internetQuestion);
			
			String tin = people.getTIN();
			String address = people.getAddress();
			String email = people.getEmail();
			String taxPayerName = people.getTaxPayerName();
			String taxOfficeID = people.getTaxOfficeID();
			String taxOfficeName = people.getTaxOfficeName();
			String RCNumber = people.getRCNumber();
			String phone = people.getPhone();
			
			if(address.equals("")) {
				address = "NOT AVAILABLE";
			}
			
			if(email.equals("")) {
				email = "NOT AVAILABLE";
			}
			if(taxPayerName.equals("")) {
				taxPayerName = "NOT AVAILABLE";
			}
			if(taxOfficeID.equals("")) {
				taxOfficeID = "NOT AVAILABLE";
			}
			
			if(taxOfficeName.equals("")) {
				taxOfficeName = "NOT AVAILABLE";
			}
			
			if(RCNumber.equals("")) {
				RCNumber = "NOT AVAILABLE";
			}
			
			if(phone.equals("")) {
				phone = "NOT AVAILABLE";
			}
			
			String UpdateSql = "Update people set internetAccess=null, internetQuestion=null, internetAnswer='"+randomString+"', smsFlag='N', community='"+taxOfficeID+"', peopleCode='" + peopleCode + "', Weight='10', phone1='"+phone+"', addressLine1='"+address+"', addressLine2='"+address+"', emailAddress='"+email+"', organizationName='"+taxPayerName+"', orgNameUpper='"+taxPayerName+"', referenceFile='"+RCNumber+"' where licenceNumber='"+tin+"'";
			System.out.println(createStatement.executeUpdate(UpdateSql) + " Updated");
			
		}

		catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
		return peopleQr;
	}
	

	public void updateInternetQuestion(String tin, String password) throws UnsupportedEncodingException {
		String encryptedPassword = encrypt(password);
		try {
			connection = conn.getConnection();
			Statement createStatement = connection.createStatement();

			String updateDateSql = "UPDATE PEOPLE SET internetQuestion='" + encryptedPassword
					+ "', internetAccess='A', securityCode='1' where LicenceNumber='" + tin + "'";
			int internetQuestionUpdate = createStatement.executeUpdate(updateDateSql);
			System.out.println(internetQuestionUpdate + " InternetQuestion Successfully Updated");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public void updateDateOfIncorporation(String doi, int peopleRSN) {
		try {
			connection = conn.getConnection();
			Statement createStatement = connection.createStatement();
			System.out.println("Updating Date of Incorporation".toUpperCase());
			String updateDateSql = "UPDATE PEOPLE SET BirthDate='" + doi + "' where peopleRsn='" + peopleRSN + "'";
			int executePeopleUpdate = createStatement.executeUpdate(updateDateSql);
			System.out.println(executePeopleUpdate + " Date of Incorporation Successfully Updated");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				createStatement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	private void insertPeople(int peopleRsn, String randomString, People people, int peopleCode) throws SQLException {
		System.out.println("Inside insert People".toUpperCase());
		String insertPeople = "INSERT INTO PEOPLE(peopleRsn, organizationName, phone1, licenceNumber, peopleCode, BirthDate, stampUser, AddressLine1, AddressLine2, emailAddress, OrgNameUpper, community, securityCode, ReferenceFile, internetQuestion, internetAnswer, SMSFlag, EmailFlag, CreditCardProcessingFlag)  VALUES ('"
				+ peopleRsn + "', '" + people.getTaxPayerName() + "', '" + people.getPhone() + "', '" + people.getTIN()
				+ "', '" + peopleCode + "', '" + getCurrentDate() + "', 'FIRS', '" + people.getAddress() + "','" + people.getAddress() + "', '"
				+ people.getEmail() + "', '" + people.getTaxPayerName() + "', '" + people.getTaxOfficeID()
				+ "', '0', '" + people.getRCNumber() + "', 'NULL', '" + randomString + "', 'N', 'N', 'N')";
		int executePeopleUpdate = createStatement.executeUpdate(insertPeople);
		System.out.println(executePeopleUpdate + " People Created Successfully " + people.getTaxPayerName());
	}

	// private void insertPeople(int peopleRsn, People people) throws SQLException {
	// System.out.println("Inside Insert People".toUpperCase());
	// String insertPeople = "INSERT INTO PEOPLE(peopleRsn, organizationName,
	// phone1, licenceNumber, BirthDate, stampUser, AddressLine2, emailAddress,
	// OrgNameUpper, statusCode, securityCode, ReferenceFile, internetQuestion,
	// internetAnswer, smsFlag, EmailFlag, creditCardProcessingFlag) "
	// + "VALUES ('"+ peopleRsn + "', '" + people.getAddress() + "', '" +
	// people.getEmail() + "', '" + people.getTaxOfficeName().toUpperCase() + "',
	// '2015-07-29 06:00:00.000', 'FIRS', '" + people.getTaxOfficeName() + "', '1',
	// '" + people.getTIN()+ "', '1', '1','0', '', '', 'N','N','N')";
	// int executePeopleUpdate = createStatement.executeUpdate(insertPeople);
	// System.out.println(executePeopleUpdate + " People Created Successfully");
	// }

	// private void insertPeople(int peopleRsn, People people) throws SQLException {
	// System.out.println("Inside insert Insert People".toUpperCase());
	// String insertPeople = "INSERT INTO PEOPLE(peopleRsn, organizationName,
	// phone1, licenceNumber, BirthDate, AddressLine2, emailAddress, OrgNameUpper,
	// statusCode, ReferenceFile, internetQuestion, internetAnswer) VALUES ('" +
	// peopleRsn + "', '" + people.getTaxOfficeName() + "', '" + people.getPhone() +
	// "', '" + people.getTIN() + "', '2015-07-29 06:00:00.000', '" +
	// people.getAddress() + "', '"+ people.getEmail() +"', '"+
	// people.getTaxOfficeName() +"', '1', '1A', '', '')";
	// int executePeopleUpdate = createStatement.executeUpdate(insertPeople);
	// System.out.println(executePeopleUpdate + " People Created Successfully");
	// }

	private String getCurrentDate() {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00");
		System.out.println(myDateObj.format(myFormatObj));
		return myDateObj.format(myFormatObj);
	}

	private void insertFolderPeople(int folderRsn, int peopleRsn) throws SQLException {
		String insertFolderPeople = "INSERT INTO FOLDERPEOPLE(FolderRsn, PeopleCode, PeopleRsn, StampUser) "
				+ "VALUES ('" + folderRsn + "', '7', '" + peopleRsn + "', 'FIRS' )";
		int executeFolderPeopleUpdate = createStatement.executeUpdate(insertFolderPeople);
		System.out.println(executeFolderPeopleUpdate + "FolderPeople Created Successfully");
	}

	private void insertFolder(int folderRsn, int peopleRsn) throws SQLException {
		String insertFolder = "INSERT INTO FOLDER(FolderRsn, FolderYear, FolderSequence, FolderSection, FolderRevision, FolderType, propertyRsn) VALUES ('"
				+ folderRsn + "', '20', '12000', '000', '00', 'FQ','1')";
		int executeFolderUpdate = createStatement.executeUpdate(insertFolder);
		System.out.println(executeFolderUpdate + " Folder Created Successfully");
	}

	/*
	 * "INSERT INTO FOLDER(FolderRsn, FolderYear, FolderSequence, FolderSection," +
	 * "FolderRevision, FolderType, propertyRsn, inDate, IssueDate, ParentRsn, copyFlag,"
	 * +
	 * "stampUser, statusCode, Priority, FolderName, FolderCentury, ReferenceFile2)"
	 * + "VALUES ('" + folderRsn +
	 * "', '20', '120', '000', '00', 'FRtest','0', 'inDate', " +
	 * "'issueDate', 'ParentRsn', 'copyFlag', 'stampUser', 'statusCode', 'Priority', "
	 * + "'FolderName', '20', 'TIN')";
	 */

}
