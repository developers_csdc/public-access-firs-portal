package com.org.firs;

import java.sql.ResultSet;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class SearchProfileReceipt {
	static DBConnection dbc = new DBConnection();

	public SearchProfileReceipt() {

	}

	public static String getReceipts(String tin, String date, String type, String dailyMonthly) {
		String query = "", newType = "";
		JsonArray jar = null;
		if (type.equalsIgnoreCase("RCPT") || type.equalsIgnoreCase("FR")) {
			newType = "'RCPT','FR'";
		} else {
			newType = "'RCTI','FRCT'";
		}
		if (dailyMonthly.equalsIgnoreCase("MONTHY")) {
			query = "SELECT F.FOLDERTYPE RECEIPT_TYPE,AP.PAYMENTRESPONSE1 PAYMENT_REFERENCE,AP.PAYMENTAMOUNT SUM_OF_AMT,AP.PAYMENTDATE PAYMENT_DATE,AP.RECEIPTNUMBER,F.REFERENCEFILE2 TIN FROM FOLDER F JOIN ACCOUNTPAYMENT AP ON AP.FOLDERRSN=F.FOLDERRSN WHERE F.REFERENCEFILE2='"
					+ tin + "' AND CAST(AP.PAYMENTDATE AS DATE) BETWEEN '" + date + "' AND EOMONTH('" + date
					+ "') AND F.FOLDERTYPE IN(" + newType + ")";
		} else {
			query = "SELECT F.FOLDERTYPE RECEIPT_TYPE,AP.PAYMENTRESPONSE1 PAYMENT_REFERENCE,AP.PAYMENTAMOUNT SUM_OF_AMT,AP.PAYMENTDATE PAYMENT_DATE,AP.RECEIPTNUMBER,F.REFERENCEFILE2 TIN FROM FOLDER F JOIN ACCOUNTPAYMENT AP ON AP.FOLDERRSN=F.FOLDERRSN WHERE F.REFERENCEFILE2='"
					+ tin + "' AND CAST(AP.PAYMENTDATE AS DATE)='" + date + "' AND F.FOLDERTYPE IN(" + newType + ")";
		}
		try {
			jar = new JsonArray();
			ResultSet result = dbc.getConnection().createStatement().executeQuery(query);
			while (result.next()) {
				JsonObject job = new JsonObject();
				job.addProperty("RECEIPT_TYPE", result.getString("RECEIPT_TYPE"));
				job.addProperty("PAYMENT_REFERENCE", result.getString("PAYMENT_REFERENCE"));
				job.addProperty("SUM_OF_AMT", result.getString("SUM_OF_AMT"));
				job.addProperty("PAYMENT_DATE", result.getString("PAYMENT_DATE"));
				job.addProperty("RECEIPTNUMBER", result.getString("RECEIPTNUMBER"));
				job.addProperty("TIN", result.getString("TIN"));
				jar.add(job);
			}
		} catch (Exception e) {
			System.out.println("Error fetching receipt: " + e.getMessage());
		}
		System.out.println(jar.toString());
		return jar.toString();
	}
}
