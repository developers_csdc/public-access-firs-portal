package com.org.firs;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@WebServlet("/registrationControllerServiceBackup")
public class RegistrationControllerServiceBackup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String applicationType;
	PortalUtil portalUtil = new PortalUtil();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("Inside doget".toUpperCase());

		PrintWriter out = response.getWriter();
		String applicationType = request.getParameter("applicationType");
		String tin = request.getParameter("tin");
		String command = request.getParameter("command");

		String peopleRSN = request.getParameter("user");
		String key = request.getParameter("key");

		switch (command) {
		case "RetrieveUser":

			getTinValidationData(request, response, tin, applicationType);
			break;
		case "ActivateAccount":
			activateAccount(peopleRSN, key, tin, request, response);
			break;
		default:
			return;

		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		applicationType = request.getParameter("applicationType");
		String tin = request.getParameter("ftin");
		String email = (request.getParameter("email")).trim();

		String command = request.getParameter("command");
		switch (command) {
		case "AddUser":
			processTaxPayer(request, response, tin);
			break;

		case "SendPassword":
			sendPassCode(request, response, tin, email);
			break;

		default:
			return;

		}
	}

	public void sendPassCode(HttpServletRequest request, HttpServletResponse response, String tin, String email) {
		System.out.println("Recovering User Password...".toUpperCase());
		String xr = "No Password Found for this Tin";
		boolean tinExist = false;
		People people = portalUtil.getAllPeople(tin);
		String taxFinalOfficeName = people.getTaxPayerName();
		String internetAnswer = people.getInternetAnswer();
		String internetQuestion = people.getInternetQuestion();
		String internetAccess = people.getInternetAccess();
		String emailAddress = (people.getEmail()).trim();
		System.out.println("Your Email " + emailAddress);
		System.out.println("Email Address Supplied " + email);
		try {
			System.out.println("TIN Found " + tin);
			tinExist = portalUtil.getPeopleByTin(tin);
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		System.out.println("=====>" + tinExist);

		if (tinExist) { // TIN Exist

			/*
			 * if(internetAnswer==null) {
			 * 
			 * System.out.println("Redirecting to Non TCC Registered Account"); System.out.
			 * println("Redirecting to Non TCC Registered Account because of Wrong Email");
			 * request.setAttribute("errorMessage", "Register"); RequestDispatcher
			 * requestDispatcher = request.getRequestDispatcher("NonTCCErrorMessage.jsp");
			 * 
			 * try { requestDispatcher.forward(request, response); } catch (ServletException
			 * e) { // TODO Auto-generated catch block e.printStackTrace(); } catch
			 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
			 * 
			 * }
			 */ // else {

			// Get user email

			if (!email.equalsIgnoreCase(emailAddress)) {
				System.out.println("Email do not match. Please Try Again");
				System.out.println("Redirecting to Non TCC Registered Account because of Wrong Email");
				request.setAttribute("errorMessage",
						"The email address you supplied does not match the email associated with this <b>TIN</b>, Please Check your email and Try again");

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("NonTCCErrorMessage.jsp");
				try {
					requestDispatcher.forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			else { // TIN exist and Email is Match

				System.out.println("Internet Access is " + internetAccess);

				if (internetAccess != null) {
					try {
						xr = PortalUtil.decrypt(internetQuestion);
						// System.out.println(xr);
						System.out.println("User has is Internet Question As PAssword " + xr);
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					xr = internetAnswer;
					System.out.println("User has default Internet Answer As PAssword " + xr);
				}

				String randomString = xr;
				String toEmail = email;
				String subject = "Password Recovering";
				String mailMessage = "Dear " + taxFinalOfficeName
						+ ",<br/><br/>Your Password is :<br/><br/>Password : <b style='color:red;'>" + randomString
						+ "</b><br><br>"
						+ "IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."
						+ "<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."
						+ "The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."
						+ "The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."
						+ "Before opening and accessing the attachment, if any, please check and scan for virus.</div>";

				portalUtil.activateSendMail(mailMessage, toEmail, subject);
				RequestDispatcher requestDispatcher = request
						.getRequestDispatcher("PasswordRecoverySuccessMessage.jsp");
				request.setAttribute("tin", tin);
				request.setAttribute("email", email);
				try {
					requestDispatcher.forward(request, response);
				} catch (ServletException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			System.out.println("No Record Found For This Tin".toUpperCase());

			request.setAttribute("errorMessage", " No Record for this <b>TIN</b> on"
					+ "<b style='color:red'>TCC web portal</b>, "
					+ "Please kindly check you TIN and Try Again or register a new TCC Account. If problem persist please contact <b><i>eservicesupport@firs.gov.ng</i></b>");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("NonTCCErrorMessage.jsp");
			try {
				requestDispatcher.forward(request, response);
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

	private void activateAccount(String peopleRSN, String key, String tin, HttpServletRequest request,
			HttpServletResponse response) {
		System.out.println("Activation Account is running");
		int peopleRsn = Integer.parseInt(peopleRSN);
		String activationKey = "";
		String isAccountActive = "";
		boolean peopleExists = false;

		isAccountActive = portalUtil.getSmsFlag(peopleRsn);

		try {
			peopleExists = portalUtil.getPeopleByPeopleRsn(peopleRsn);
			if (!isAccountActive.equals("Y")) {
				activationKey = PortalUtil.getSHA2Hash(portalUtil.getInternetAnswer(peopleRsn));
				if (peopleExists && !isAccountActive.equals("Y")) {
					if (activationKey.equals(key)) {
						portalUtil.setSmsFlag("Y", peopleRSN);
					}

					RequestDispatcher requestDispatcher = request.getRequestDispatcher("AccountActivation.jsp");
					request.setAttribute("message", "Your Account has been activated Successfully !");
					requestDispatcher.forward(request, response);

				} else if (peopleExists && isAccountActive.equals("Y")) {
					System.out.println("ActivationKey: Activated");

					RequestDispatcher requestDispatcher = request.getRequestDispatcher("AccountActivation.jsp");
					request.setAttribute("message", "Your Account has been Activated! ");
					requestDispatcher.forward(request, response);
				}

			} else {

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("AccountActivation.jsp");
				request.setAttribute("message", "Your Account has been Activated! ");
				requestDispatcher.forward(request, response);
			}

		} catch (Exception e) {
			System.out.println("Error occoured while fetching people: " + e.getMessage());
			// e.printStackTrace();
		}

		// try {
		// String xp = PortalUtil.encrypt("1234");
		// System.out.println(xp);
		//
		// String xr = PortalUtil.decrypt("b2xhbm9zaG8xNzQ3");
		// System.out.println(xr);
		//
		//
		// } catch (UnsupportedEncodingException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		// try {
		//
		// } catch (ClassNotFoundException e) {
		// e.printStackTrace();
		// } catch (SQLException e) {
		// e.printStackTrace();
		// }

	}

	public void processTaxPayer(HttpServletRequest request, HttpServletResponse response, String tin) {

		int peopleCode = 0;
		String doi = "";
		if (applicationType.equals("Individual")) {
			peopleCode = 4;
		}

		if (applicationType.equals("Corporate")) {
			peopleCode = 3;
		}

		if (applicationType.equals("Forex")) {
			peopleCode = 5;
		}

		System.out.println("Inside dopost".toUpperCase());

		String email = (request.getParameter("email")).trim();
		if (!applicationType.equals("Individual")) {

			System.out.println(applicationType);
			doi = (request.getParameter("doi")).trim();
		}
		String tIN = (request.getParameter("ftin")).trim();
		String applicationType = (request.getParameter("applicationType")).trim();
		String taxFinalOfficeName = (request.getParameter("taxFinalOfficeName")).trim();
		String taxOfficeID = (request.getParameter("taxOfficeID")).trim();
		String taxPayerName = (request.getParameter("taxPayerName")).trim();
		String taxPayerType = (request.getParameter("taxPayerType")).trim();
		String RCNumber = (request.getParameter("RCNumber")).trim();
		String phone = (request.getParameter("phone")).trim();
		String address = (request.getParameter("address")).trim();
		String JTBTIN = "";

		System.out.println("This is TaxPayerName " + taxPayerName);

		People people = new People(tIN, JTBTIN, taxPayerName, address, taxOfficeID, taxFinalOfficeName, taxPayerType,
				RCNumber, email, phone);

		PortalUtil portalUtil = new PortalUtil();
		String randomString = portalUtil.getRandomString(8);

		StringBuffer requestURL = request.getRequestURL();
		String t_tempURLWithGetURL = requestURL.toString();
		String t_contextURL = t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));

		try {
			// portalUtil.sendSMS("2348038024755", "Hello PP");
			boolean peopleExist = portalUtil.getPeopleByTin(tIN);
			System.out.println(tIN + " This person has records " + peopleExist);
			// System.out.println("People Record " + peopleExist);
			if (!peopleExist) {
				int peopleRSN = portalUtil.addPeople(people, randomString, peopleCode);

				// Reset default password here....

				System.out.println("Application type " + applicationType);

				if (applicationType.equals("Corporate") || applicationType.equals("Forex")) {
					// Updating Date Incorporation
					portalUtil.updateDateOfIncorporation(doi, peopleRSN);
				}

				String numberAsString = new Integer(peopleRSN).toString();
				String toEmail = people.getEmail();
				String subject = "User Account Activation";
				String phone1 = "08038024755";
				// portalUtil.sendSMS(phone1, numberAsString);

				String mailMessage = "Dear " + taxPayerName
						+ ",<br/><br/>Your first time login-password is :<br/><br/>Password : <b style='color:red;'>"
						+ randomString + "</b><br><br>"
						+ "<br/><br/>Activate your account by Clicking This link**:<br/><br/><a href="
						+ PortalUtil.getActivationURL(numberAsString, randomString, t_contextURL) + ">"
						+ PortalUtil.getActivationURL(numberAsString, randomString, t_contextURL) + "</a><br><br>"
						+ "<br/><br/>**NOTE: If the above link is not clickable, please copy the link and open in a separate window for account activation.<br/><br/>"
						+ "<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."
						+ "<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."
						+ "The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."
						+ "The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."
						+ "Before opening and accessing the attachment, if any, please check and scan for virus.</div>";

				portalUtil.activateSendMail(mailMessage, toEmail, subject);

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("RegistrationSuccessMessage.jsp");
				request.setAttribute("tin", tin);
				request.setAttribute("peopleRSN", numberAsString);
				request.setAttribute("t_contextURL", t_contextURL);
				requestDispatcher.forward(request, response);

			} else {

				System.out.println("This user already registered redirect with error message");

				// Update the record reset internet question and internet answer and finally
				// send activation link

				People peopleQr = portalUtil.updatePeople(people, randomString, peopleCode);
				int peopleRSN = Integer.parseInt(peopleQr.getPeopleRSN());

				System.out.println("Application type " + applicationType);

				if (applicationType.equals("Corporate") || applicationType.equals("Forex")) {
					// Updating Date Incorporation
					portalUtil.updateDateOfIncorporation(doi, peopleRSN);
				}

				String numberAsString = new Integer(peopleRSN).toString();
				System.out.println("This is PeopleRSN " + numberAsString);

				String toEmail = people.getEmail();
				String pq = peopleQr.getInternetQuestion();
				String pa = peopleQr.getInternetAnswer();

				System.out.println("This is InternetQuestion " + pq);
				System.out.println("This is InternetAnswers " + pa);

				/*
				 * if(pq!=null) { randomString = PortalUtil.decrypt(pq); }else if(pa!=null) {
				 * randomString = pa; }
				 */

				String subject = "User Account Activation";
				String phone1 = "08038024755";
				// portalUtil.sendSMS(phone1, numberAsString);

				System.out.println("This is  numberAsString" + numberAsString);

				String mailMessage = "Dear " + taxPayerName
						+ ",<br/><br/>Your first time login-password is :<br/><br/>Password : <b style='color:red;'>"
						+ randomString + "</b><br><br>"
						+ "<br/><br/>Activate your account by Clicking This link**:<br/><br/><a href="
						+ PortalUtil.getActivationURL(numberAsString, randomString, t_contextURL) + ">"
						+ PortalUtil.getActivationURL(numberAsString, randomString, t_contextURL) + "</a><br><br>"
						+ "<br/><br/>**NOTE: If the above link is not clickable, please copy the link and open in a separate window for account activation.<br/><br/>"
						+ "<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."
						+ "<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."
						+ "The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."
						+ "The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."
						+ "Before opening and accessing the attachment, if any, please check and scan for virus.</div>";

				portalUtil.activateSendMail(mailMessage, toEmail, subject);

				RequestDispatcher requestDispatcher = request.getRequestDispatcher("RegistrationSuccessMessage.jsp");
				request.setAttribute("tin", tin);
				request.setAttribute("peopleRSN", numberAsString);
				request.setAttribute("t_contextURL", t_contextURL);
				requestDispatcher.forward(request, response);

			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void getTinValidationData(HttpServletRequest request, HttpServletResponse response, String tin,
			String applicationType) {
		boolean peopleExist = false;
		try {
			peopleExist = portalUtil.getPeopleByTin(tin);
		} catch (Exception e) {
			System.out.print("Error getting people by tin: " + e.getMessage());
		}

		/*if (peopleExist) {
			
			
			
			request.setAttribute("errorMessage",
					"An account already exist with this TIN on the portal. If you have forgotten your password, please use the forgot password button on the home page.");
			request.setAttribute("errorType", "accountExist");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("NonTCCErrorMessage.jsp");
			try {
				requestDispatcher.forward(request, response);
			}catch(Exception e) {
				System.out.println("Could not dispatch request: "+e.getMessage());
			}
			
			
			
			
			
			
			
			
			
		} */
		
		
		//else {
			
			ApiFactory apiFactory = new ApiFactory();

			JSONParser parser = new JSONParser();
			JSONObject json;
			String redirectUrl;
			String error = "Tin Not Found";
			try {
				String data = apiFactory.tinValidation(tin);
				json = (JSONObject) parser.parse(data);
				People tinObject = null;
				if (!(json.get("Message") == null)) {
					System.out.println(json.get("Message"));
					// Redirect with error message
					request.setAttribute("error", error);
					redirectUrl = "FirsRegistration.jsp";

				} else {

					tinObject = new People((String) json.get("TIN"), (String) json.get("JTBTIN"),
							(String) json.get("TaxPayerName"), (String) json.get("Address"),
							(String) json.get("TaxOfficeID"), (String) json.get("TaxOfficeName"),
							(String) json.get("TaxPayerType"), (String) json.get("RCNumber"),
							(String) json.get("Email"), (String) json.get("Phone"));
					request.setAttribute("data", tinObject);
					redirectUrl = "UserDetails.jsp";
				}

				RequestDispatcher requestDispatcher = request.getRequestDispatcher(redirectUrl);
				requestDispatcher.forward(request, response);

			}

			catch (Exception e) {
				String errorMessage = "Tin validation Api failed\nThis may be due to Network issue,\nPlease Check your Network and try Again!";
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("ErrorMessage.jsp");
				request.setAttribute("error", errorMessage);

				try {
					requestDispatcher.forward(request, response);
				} catch (ServletException | IOException e1) {
					System.out.println("Failed to forward tin validation error message to error page");

				}
			}
		//}

	}
	
	
	
	//End getPeopleBY Tin

}
