package com.org.firs;

import java.sql.*;

public class DBConnection {

	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection con = null;
		try {
			con = DriverManager.getConnection("jdbc:sqlserver://10.2.252.38;databaseName=FIRS", "API01",
					"APICSDC@123");
		} catch (Exception e) {
			System.out.println("Could not get database connection: " + e.getMessage());
		}
		return con;
	}

}
