package com.org.firs;

import java.io.IOException;

import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class ApiFactory {

	public String tinValidation(String tin){
	JSONParser parser = new JSONParser();
	CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	String emailAddress = "";
	HttpUriRequest getRequest = new HttpGet("http://10.2.0.251/apis/tinvalidation/" + tin.trim() + "");
	getRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
	String data = "";

		CloseableHttpResponse httpResponse;
		try {
			httpResponse = httpClient.execute(getRequest);
			data = EntityUtils.toString(httpResponse.getEntity());
			int statusCode = httpResponse.getStatusLine().getStatusCode();
		} catch (IOException e) {
			return "false";
		}
		    return data;
	}
	
	public static String getTaxOfficeID(String tin){
		JSONParser parser = new JSONParser();
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		String taxOfficeID = "";
		
		HttpUriRequest getRequest = new HttpGet("http://10.2.0.251/apis/tinvalidation/" + tin.trim() + "");
		getRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
		String data = "";

			CloseableHttpResponse httpResponse;
			try {
				httpResponse = httpClient.execute(getRequest);
				data = EntityUtils.toString(httpResponse.getEntity());
				int statusCode = httpResponse.getStatusLine().getStatusCode();
				JSONObject json = (JSONObject) parser.parse(data);
				
				taxOfficeID = (String) json.get("TaxOfficeID");
				
			} catch (IOException | ParseException e) {
				return "false";
			}
			    return taxOfficeID;
		
		}
	
}
