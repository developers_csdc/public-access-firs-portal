package com.org.firs;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AuthenticationController
 */
@WebServlet("/authenticationControllerBackup")
public class AuthenticationControllerBackup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PortalUtil portalUtil = new PortalUtil();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		String tin = request.getParameter("username").trim();
		String password = request.getParameter("password").trim();
		try {
			// System.out.println("internetAccess " + portalUtil.getInternetAccess(tin));

			if (portalUtil.isAccountExist(tin)) {
				// isAccountExist
				if (!portalUtil.getSmsFlagByTin(tin)) {
					// Account exist but not activated
					resendEmail(request, tin);
					RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
					request.setAttribute("message",
							"Your Account has not been activated. Kindly check your email again to activate your Account.");
					requestDispatcher.forward(request, response);
				} else {
					// Account exist and activated, check login credentials
					if (portalUtil.getInternetAccess(tin) == null) {

						if (portalUtil.activateUser(session, request, password, tin)) {
							System.out.println(
									"Redirecting to home for valid user account...That has not changed password!!");
							RequestDispatcher requestDispatcher = request.getRequestDispatcher("IndividualProfile.jsp");
							requestDispatcher.forward(request, response);

						} else {
							System.out.println(
									"Redirecting to Index for Invalid user account..That has not changed password");
							RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
							request.setAttribute("message", "Incorrect Password! Try Again");
							requestDispatcher.forward(request, response);
						}

					} else {
						boolean loginFlag = portalUtil.loginUser(session, request, tin, password);
						if (loginFlag) {
							// Redirect to homePage
							System.out.println(
									"Redirecting to home for valid user account...That has Chanded his password");
							RequestDispatcher requestDispatcher = request.getRequestDispatcher("IndividualProfile.jsp");
							session.setAttribute("tin", tin);
							requestDispatcher.forward(request, response);
						} else {
							// redirect to index
							System.out.println(
									"Redirecting to index for Invalid user account... That has Chanded his password");
							RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
							request.setAttribute("message", "Incorrect Password! Try Again");
							requestDispatcher.forward(request, response);
						}
					}
				}

			} 
			
			
			
			else {
				// Account does not exist
				System.out.println("Redirecting to index for account not exist");
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
				request.setAttribute("message",
						"You do not have an account on this portal. Please register an account.");
				requestDispatcher.forward(request, response);
			}
		} catch (Exception e) {
			System.out.println("Login User:===> " + e.getMessage());
		}

		/*
		 * else { System.out.println("Account does not exist"); if
		 * (portalUtil.getInternetAccess(tin) == null) {
		 * 
		 * if (portalUtil.activateUser(session, request, password, tin)) {
		 * System.out.println(
		 * "Redirecting to home for valid user account...That has not changed password!!"
		 * ); RequestDispatcher requestDispatcher =
		 * request.getRequestDispatcher("IndividualProfile.jsp");
		 * requestDispatcher.forward(request, response);
		 * 
		 * } else { System.out.println(
		 * "Redirecting to Index for Invalid user account..That has not changed password"
		 * ); RequestDispatcher requestDispatcher =
		 * request.getRequestDispatcher("index.jsp"); request.setAttribute("message",
		 * "Invalid Login! Try Again"); requestDispatcher.forward(request, response); }
		 * 
		 * }
		 * 
		 * else { boolean loginFlag = portalUtil.loginUser(session, request, tin,
		 * password); if (loginFlag) { // Redirect to homePage System.out
		 * .println("Redirecting to home for valid user account...That has Chanded his password"
		 * ); RequestDispatcher requestDispatcher =
		 * request.getRequestDispatcher("IndividualProfile.jsp");
		 * session.setAttribute("tin", tin); requestDispatcher.forward(request,
		 * response); } else { // redirect to index System.out.println(
		 * "Redirecting to index for Invalid user account... That has Chanded his password"
		 * ); RequestDispatcher requestDispatcher =
		 * request.getRequestDispatcher("index.jsp"); request.setAttribute("message",
		 * "Invalid Login! Try Again"); requestDispatcher.forward(request, response); }
		 * }
		 * 
		 * }
		 * 
		 * } catch (ClassNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (SQLException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 * 
		 * 
		 */
	}

	private void resendEmail(HttpServletRequest request, String tin) {
		StringBuffer requestURL = request.getRequestURL();
		String t_tempURLWithGetURL = requestURL.toString();
		String t_contextURL = t_tempURLWithGetURL.substring(0, t_tempURLWithGetURL.lastIndexOf("/"));
		People people = portalUtil.getAllPeople(tin);
		String subject = "User Account Activation";

		String mailMessage = "Dear " + people.getTaxPayerName()
				+ ",<br/><br/>Your first time login-password is :<br/><br/>Password : <b style='color:red;'>"
				+ people.getInternetAnswer() + "</b><br><br>"
				+ "<br/><br/>Activate your account by Clicking This link**:<br/><br/><a href="
				+ PortalUtil.getActivationURL(people.getPeopleRSN(), people.getInternetAnswer(), t_contextURL) + ">"
				+ PortalUtil.getActivationURL(people.getPeopleRSN(), people.getInternetAnswer(), t_contextURL)
				+ "</a><br><br>"
				+ "<br/><br/>**NOTE: If the above link is not clickable, please copy the link and open in a separate window for account activation.<br/><br/>"
				+ "<div style='color:grey;font-size:12px'>IMPORTANT: Please do not reply to this message or mail address. For any queries, please call our 24 Hrs Customer Contact Centre at our toll free number*- 000000000000."
				+ "<br>DISCLAIMER: This communication is confidential and privileged and is directed to and for the use of the addressee only."
				+ "The recipient if not the addressee should not use this message if erroneously received,and access and use of this e-mail in any manner by anyone other than the addressee is unauthorized."
				+ "The recipient acknowledges that FIRS may be unable to exercise control or ensure or guarantee the integrity of the text of the email message and the text is not warranted as to completeness and accuracy."
				+ "Before opening and accessing the attachment, if any, please check and scan for virus.</div>";

		portalUtil.activateSendMail(mailMessage, people.getEmail(), subject);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served With get request at: ").append(request.getContextPath());

	}

}
