package com.org.firs;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AuthenticationController
 */
@WebServlet("/changePasswordController")
public class ChangePasswordController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	PortalUtil portalUtil = new PortalUtil();
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		//HttpSession session = request.getSession();
		String tin = request.getParameter("tin").trim();
		String newPassword = request.getParameter("newPassword").trim();
		String cPassword = request.getParameter("cpassword").trim();
		
		if(validPasswordExtries(newPassword, cPassword)) {
			String error = "";
			boolean status ;
			System.out.println("password match");
			String internetAccess = portalUtil.getInternetAccess(tin);
			
			if(internetAccess==null) {
				System.out.println("This is 1" + internetAccess);
				status = updatePassword(tin, newPassword);
				System.out.println("Updating User Password When there is no internet answer " + status);
				//Update internetQuestion
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("PasswordChangedSuccessMessage.jsp");
				requestDispatcher.forward(request, response);
				
			}else {
				System.out.println("This is 2" + internetAccess);
				status = updatePassword(tin, newPassword);
				//There is internetAnwer update internet question
				System.out.println("Updating User Password When there is internet answer " + status);
				RequestDispatcher requestDispatcher = request.getRequestDispatcher("PasswordChangedSuccessMessage.jsp");
				request.setAttribute("tin",  tin);
				request.setAttribute("ps",  newPassword);
				requestDispatcher.forward(request, response);
			}
				
		}else {
			
			System.out.println("password not match");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("IndividualProfile.jsp");
			request.setAttribute("error", "New Password and Confirmed Password Do not Match Try Again!");
			requestDispatcher.forward(request, response);
			
			//Redirect with a message
		}
	}
	
	
   public boolean updatePassword(String tin, String password) {
	   try {
		portalUtil.updateInternetQuestion(tin, password);
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   return true;
   }
	
	public boolean validPasswordExtries(String newpassword, String cPassword) {
		if(newpassword.equals(cPassword)) {
			return true;
		}else {
			return false;
		}	
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
