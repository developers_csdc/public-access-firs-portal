package com.org.firs.tccValidation;

public class Assessment {
	String year;
	String turnOver;
	String assessableProfit;
	String totalProfit;
	String taxPaid;
	String remmitanceAmountApproved;
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getTurnOver() {
		return turnOver;
	}
	public void setTurnOver(String turnOver) {
		this.turnOver = turnOver;
	}
	public String getAssessableProfit() {
		return assessableProfit;
	}
	public void setAssessableProfit(String assessableProfit) {
		this.assessableProfit = assessableProfit;
	}
	public String getTotalProfit() {
		return totalProfit;
	}
	public void setTotalProfit(String totalProfit) {
		this.totalProfit = totalProfit;
	}
	public String getTaxPaid() {
		return taxPaid;
	}
	public void setTaxPaid(String taxPaid) {
		this.taxPaid = taxPaid;
	}
	public String getRemmitanceAmountApproved() {
		return remmitanceAmountApproved;
	}
	public void setRemmitanceAmountApproved(String remmitanceAmountApproved) {
		this.remmitanceAmountApproved = remmitanceAmountApproved;
	}
}
