package com.org.firs.tccValidation;

public class CorporateTIN {
	int statusCode;
	String description;
	String taxPayerType;
	String taxPayerName;
	String taxOffice;
	String tin;
	String tccNumber;
	String tccStatus;
	String tccIssueDate;
	String tccExpiryDate;
	String rcNumber;
	Assessment[] assessment;
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTaxPayerType() {
		return taxPayerType;
	}
	public void setTaxPayerType(String taxPayerType) {
		this.taxPayerType = taxPayerType;
	}
	public String getTaxPayerName() {
		return taxPayerName;
	}
	public void setTaxPayerName(String taxPayerName) {
		this.taxPayerName = taxPayerName;
	}
	public String getTaxOffice() {
		return taxOffice;
	}
	public void setTaxOffice(String taxOffice) {
		this.taxOffice = taxOffice;
	}
	public String getTin() {
		return tin;
	}
	public void setTin(String tin) {
		this.tin = tin;
	}
	public String getTccNumber() {
		return tccNumber;
	}
	public void setTccNumber(String tccNumber) {
		this.tccNumber = tccNumber;
	}
	public String getTccStatus() {
		return tccStatus;
	}
	public void setTccStatus(String tccStatus) {
		this.tccStatus = tccStatus;
	}
	public String getTccIssueDate() {
		return tccIssueDate;
	}
	public void setTccIssueDate(String tccIssueDate) {
		this.tccIssueDate = tccIssueDate;
	}
	public String getTccExpiryDate() {
		return tccExpiryDate;
	}
	public void setTccExpiryDate(String tccExpiryDate) {
		this.tccExpiryDate = tccExpiryDate;
	}
	public String getRcNumber() {
		return rcNumber;
	}
	public void setRcNumber(String rcNumber) {
		this.rcNumber = rcNumber;
	}
	public Assessment[] getAssessment() {
		return assessment;
	}
	public void setAssessment(Assessment[] assessment) {
		this.assessment = assessment;
	}
}
