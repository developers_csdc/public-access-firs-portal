package com.org.firs;

public class People {
	
	private String TIN;
	private String JTBTIN;
	private String TaxPayerName;
	private String Address ;
	private String TaxOfficeID ;
	private String TaxOfficeName;
	private String TaxPayerType ;
	private String RCNumber ;
	private String Email ;
	private String Phone ;
	private String InternetAnswer;
	private String InternetQuestion;
	private String InternetAccess;
	private String PeopleCode;
	private String PeopleRSN;
	
	
	
	public String getPeopleRSN() {
		return PeopleRSN;
	}
	public void setPeopleRSN(String peopleRSN) {
		PeopleRSN = peopleRSN;
	}
	
	public String getTIN() {
		return TIN;
	}
	public void setTIN(String tIN) {
		TIN = tIN;
	}
	
	public String getJTBTIN() {
		return JTBTIN;
	}
	public void setJTBTIN(String jTBTIN) {
		JTBTIN = jTBTIN;
	}
	public String getTaxPayerName() {
		return TaxPayerName;
	}
	public void setTaxPayerName(String taxPayerName) {
		TaxPayerName = taxPayerName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getTaxOfficeID() {
		return TaxOfficeID;
	}
	public void setTaxOfficeID(String taxOfficeID) {
		TaxOfficeID = taxOfficeID;
	}
	public String getTaxOfficeName() {
		return TaxOfficeName;
	}
	public void setTaxOfficeName(String taxOfficeName) {
		TaxOfficeName = taxOfficeName;
	}
	public String getTaxPayerType() {
		return TaxPayerType;
	}
	public void setTaxPayerType(String taxPayerType) {
		TaxPayerType = taxPayerType;
	}
	public String getRCNumber() {
		return RCNumber;
	}
	public void setRCNumber(String rCNumber) {
		RCNumber = rCNumber;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	
	
	public String getInternetAnswer() {
		return InternetAnswer;
	}
	public void setInternetAnswer(String internetAnswer) {
		InternetAnswer = internetAnswer;
	}
	
	public String getInternetQuestion() {
		return InternetQuestion;
	}
	public void setInternetQuestion(String internetQuestion) {
		InternetQuestion = internetQuestion;
	}
	
	
	public String getInternetAccess() {
		return InternetAccess;
	}
	public void setInternetAccess(String internetAccess) {
		InternetAccess = internetAccess;
	}
	
	
	
	
	public People(String peopleRSN, String internetAnswer, String internetQuestion) {
		InternetAnswer = internetAnswer;
		InternetQuestion = internetQuestion;
		PeopleRSN = peopleRSN;
	}
	
	
	public People(String tIN, String jTBTIN, String taxPayerName, String address, String taxOfficeID, String taxOfficeName,
			String rCNumber, String email, String phone) {
		
		TIN = tIN;
		JTBTIN = jTBTIN;
		TaxPayerName = taxPayerName;
		Address = address;
		TaxOfficeID = taxOfficeID;
		TaxOfficeName = taxOfficeName;
		RCNumber = rCNumber;
		Email = email;
		Phone = phone;
	}
	
	
	public People(String tIN, String jTBTIN, String taxPayerName, String address, String taxOfficeID, String taxOfficeName,
			String taxPayerType, String rCNumber, String email, String phone) {
		
		TIN = tIN;
		JTBTIN = jTBTIN;
		TaxPayerName = taxPayerName;
		Address = address;
		TaxOfficeID = taxOfficeID;
		TaxOfficeName = taxOfficeName;
		TaxPayerType = taxPayerType;
		RCNumber = rCNumber;
		Email = email;
		Phone = phone;
	}
	
	
	public People(String tIN, String jTBTIN, String taxPayerName, String address, String taxOfficeName,
			String taxPayerType, String email, String phone, String internetAnswer, String internetQuestion, String internetAccess, String peopleRSN) {
		
		TIN = tIN;
		JTBTIN = jTBTIN;
		TaxPayerName = taxPayerName;
		Address = address;
		TaxOfficeName = taxOfficeName;
		TaxPayerType = taxPayerType;
		Email = email;
		Phone = phone;
		InternetAnswer = internetAnswer;
		InternetQuestion = internetQuestion;
		InternetAccess = internetAccess;
		PeopleRSN = peopleRSN;
	}
	
	@Override
	public String toString() {
		return "tin [TIN=" + TIN + ", JTBTIN=" + JTBTIN + ", TaxPayerName=" + TaxPayerName + ", Address=" + Address
				+ ", TaxOfficeID=" + TaxOfficeID + ", TaxOfficeName=" + TaxOfficeName + ", TaxPayerType=" + TaxPayerType
				+ ", RCNumber=" + RCNumber + ", Email=" + Email + ", Phone=" + Phone + "]";
	}
	
}
